/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYSTEM__H__
#define __SYSTEM__H__
#include "../common/inttypes.h"
#include "../common/common.h"
#include "sysConfig.h"


typedef I16 NODE_ADDR;

typedef struct {
	NODE_ADDR	nodeAddr;
	uint8_t		IDD;
}ADDR;

//#define MAKE_ADDR(D_HI,D_LO,D_IDD) (ADDR) { .devAddr.hi = D_HI, .devAddr.lo = D_LO, .IDD = D_IDD }
#define MAKE_NODE_ADDR(D_HI,D_LO) (NODE_ADDR){((D_HI<<8)+D_LO)}
#define MAKE_ADDR(D_HI,D_LO,D_IDD) ((ADDR) {{((D_HI<<8)+D_LO)},D_IDD})



typedef enum { NORMAL_MODE = 0, ERROR_MODE = 1} SYSTEM_MODE;
#ifndef MAX_SYSTEM_NAME_LEN
	#define MAX_SYSTEM_NAME_LEN 20
#endif
extern uint8_t master;
extern ADDR sysAddr;
extern UID uid;
extern uint8_t systemName[MAX_SYSTEM_NAME_LEN];
extern uint8_t systemMode; 

#define SetMaster()	master=1
#define IsMaster()	master
#define GetSystemMode() systemMode
#define GetSystemUID() &uid
#define SetSystemNodeAddr(addr) sysAddr.nodeAddr = addr
uint8_t SetSystemName(uint8_t *name,uint8_t len); 
void GetSystemAddr(ADDR *addr);
ADDR* GetSystemAddrP();
#define SetSystemAddress(addr)  sysAddr = addr
INLINE void SetSystemAddr(ADDR *addr){
	sysAddr = *addr;
}

void InitSystem();

#define  IsMaster() master

#ifdef MASTER


#endif

#endif
