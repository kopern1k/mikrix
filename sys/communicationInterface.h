/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

/* Generic header for CommunicationInnterface 
 * do not include without platform */
#ifndef __COMMUNICATION_INTERFACE__
#define __COMMUNICATION_INTERFACE__
#include "../common/common.h"
#include "communicationManager.h"
#include "responseManager.h"
#include "maskAddrManager.h"
#include "moduleManager.h"
#include "../modules/common/addressManagerCommon.h"
#include "sysConfig.h"

/* specify your config in your platform specific CommunicationInnterface.h 

typedef enum { COM_PORT, SOCKET_PORT, DEBUG_PORT} PORT_ID;
typedef enum { COMM_INTERFACE1, COMM_INTERFACE2} COMM_INTERFACE;

#define INTERFACES_COUNT 2
*/
//descriptiom table

#define INTERFACE_NA MAX_INTERFACES

typedef enum  {PointToPoint,Broadcasting, Broadcast_or_PointToPoint} Inerface_type;

typedef struct {
	uint8_t name[30];
	Inerface_type type;
} interface_info_t;

typedef void (*InterfaceWrite_t)(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending);
typedef uint8_t (*InterfaceInit_t)(uint8_t *par, COM_MNG_DATA *cm);
typedef uint8_t (*InterfaceClose_t)(COM_MNG_DATA *cm);

typedef struct {
	InterfaceWrite_t Write;
	InterfaceInit_t Init;
	InterfaceClose_t Close;
	interface_info_t info;
} INTERFACE;

#define MAKE_INTERFACE_ENUM_ID(...)  DECLARE_TYPEDEF_ENUM(INTERFACE_TYPE_ID,,_ID,CONCAT_PREFIX_POSTFIX,__VA_ARGS__)
#define MAKE_TMP_ENUM_AVAILABLE_INTERFACES_FOR_INTERFACES_COUNTING(...) DECLARE_TYPEDEF_ENUM(TMP_INTERFACE_ID,__,__TAG__,CONCAT_PREFIX_POSTFIX,__VA_ARGS__,LAST_INTERFACE)

extern COM_MNG_DATA interfejs[MAX_INTERFACES];

#define GetInterfacesCount() MAX_INTERFACES

#define GetCommInterface(index)	interfejs[index]	

//uint8_t SetCommInterface(COMM_INTERFACE id, PORT_ID port, uint8_t *configPar );

INLINE uint8_t SendData(uint8_t *data, uint16_t len, uint8_t IDD){
	//chceck if data could be send inside of module
	ADDR addr;
	uint8_t ret;
	ret=0;
	GetSystemAddr(&addr);
	addr.IDD = IDD;

	CallCallbackFromData(addr,data,(uint8_t)len);
	if(GetModuleData(IDD)->transferMode == NORMAL_DATA_TRANSFER){
/*		if( SendDataToIf(&interfejs[COMM_INTERFACE2],data,len,IDD)){
			ret=2;
		}
		//filter to specific interface (depends on deployment and architecture)
		//FilterDataToSpecificInterface(uin8_t IDD);
*/
		uint8_t i;
		for(i=0;i<MAX_INTERFACES;i++){
			ret+= SendDataToIf(&interfejs[i],data,len,IDD);
		}
		return ret;
	}
	return 0;
}

//Send request
int8_t SendHandledRequest(uint8_t moduleIDD, ADDR *dest,uint8_t cmd,uint8_t *data, uint16_t len,ResponseCallbackFunc_t callback, uint16_t timeoutMs, uint8_t multiResponses);

void InitCommunicationIntefraces();
int16_t InitInterface(uint8_t sysInterfaceID,uint8_t selectedInterfaceID,uint8_t *portConfig);
int8_t GetFreeInterface();
uint8_t CloseInterface(uint8_t sysInterfaceID);
#endif
