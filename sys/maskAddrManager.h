/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */


#ifndef __MASKADDRMANAGER__H__
#define __MASKADDRMANAGER__H__
#include "../common/inttypes.h"
#include "moduleDef.h"
#include "system.h"



//#define INTERNAL_MASK_ADDR_TABLE_SIZE	5





extern MASK maskTable[MASK_ADDR_TABLE_SIZE];	//mask table
//extern INTERNAL_MASK internalMaskTable[INTERNAL_MASK_ADDR_TABLE_SIZE]; //internal mask table for internal data transfer
 
//return ID of 
uint8_t Connect(uint8_t IDD, ADDR srcAddr,ADDR maskAddr, DATA_OP operation,uint8_t offset, uint8_t size, uint8_t variableNum);
uint8_t AddMaskAddr(MASK *maskData);
uint8_t DeleteMaskAddr(ADDR *srcAddr, MODULE_DATA *data );
uint8_t DeleteMaskAddrFromID(uint8_t maskTableID);
//Object dont need information about mask objects,  object and assigned mask address will be listed by MODULE_DATA
//len as input -> length of outIndexArray, 
//len as output -> number of found elements (indexes) outIndexArray
uint8_t GetMaskAddrListFromObject(MODULE_DATA *data, uint8_t *outIndexArray, uint8_t *len);

uint8_t CallCallbackFromData(ADDR addr, uint8_t *dataBuf, uint8_t len);
uint8_t SetMaskAddrManagerConfig();
uint8_t GetMaskAddrManagerConfig();

#endif
