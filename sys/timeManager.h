/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TIMEMANAGER__H__
#define __TIMEMANAGER__H__
#include "../common/inttypes.h"
#include "../common/common.h"
#include "pt.h"

#define MAX_DELAY_VALUES 8

typedef uint64_t mTime_t;

extern volatile mTime_t sysActualTimerTime;
extern volatile uint8_t forceNotSleepMainLoop;
extern uint8_t totalTimerCnt;
/* return current time in us */
mTime_t GetSysTickCount();

typedef struct TIMER_t {
	mTime_t time;
	struct TIMER_t* next;
} TIMER_t;

void SetSysActualTimerTime(mTime_t value);

/* add new one-shot timer to ordered list */
uint8_t GeneralTimerAdd(TIMER_t **pCurrentTimer, TIMER_t *timer, uint64_t value);
/* check if timer expired */
TIMER_t* GeneralTimerExpired(TIMER_t *currentTimer);
/* set next timer in ordered list */
uint8_t GeneralTimerNext(TIMER_t **pCurrentTimer);
/* remove specific timer from odrered list */
uint8_t GeneralTimerRemove(TIMER_t **pCurrentTimer, TIMER_t *timer);
/* update timer value of one-shot timer */
uint8_t GeneralTimerUpdate(TIMER_t **currentTimer, TIMER_t *timer, uint64_t value);

//pole dat s casom a indexom na dalsi prvok (predchadzajuci prvok nie je potrebny, data su ulozene podla velkosti). 
//index  rovny MAX_DELAY_VALUES urcuje ze 
typedef struct  {
	uint16_t time;
	uint8_t nextInd;
}M_DELAY;


typedef struct  {
	M_DELAY delayArray[MAX_DELAY_VALUES]; 
	uint8_t startInd;
	uint8_t freeStart; //start of circle buffer
	uint8_t freeEnd;  //end of circle buffer
} M_DELAYLIST;

typedef union{
	struct {
		uint8_t hunderdths; //7b
		uint8_t seconds;	//6b
		uint8_t minutes;	//6b
		uint8_t hours;		//5b
	};
	uint32_t all;
}TIME;

extern M_DELAYLIST mDelayList;

volatile extern mTime_t sysUsClocks;

typedef uint8_t TimerHndl;
//functions
//HWND UsDelay(uint32_t usec);
void InitTimeManager();

//void HardWait(uint8_t index);

#define SimpleTimerExpired(val) ((val)<GetSysTickCount())

void SetTime(uint64_t time);
//return uint64_t in microseconds
// first param: thread
//second us (64bit pointer to data
#define WaitUs(thr,us){\
	*us= GetSysTickCount() + *us;\
	PT_WAIT_WHILE(thr,GetSysTickCount()<us);\
}
//#define InitWaitMs() static mTime_t timeToCatch
#define InitWaitMs(variable) static mTime_t variable
//ms - uint16_t

uint8_t TestWaitMsTime(mTime_t time);

#define WaitMs(thr, variable, ms){\
	variable = GetSysTickCount() + ms*1000;\
	totalTimerCnt++;\
	PT_WAIT_WHILE(thr,TestWaitMsTime(variable));\
}

#endif
