/*
 * Copyright (c) 2015, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYS_SIGNAL__H__
#define __SYS_SIGNAL__H__
#include "../common/inttypes.h"
#include "../common/common.h"
#include "sysConfig.h"


typedef uint16_t sysSignal_t;

extern sysSignal_t sysSignals;

typedef enum  {
	SIG_NO_SIGNAL= 0,
	SIG_DATA_BUF_WRITTING_DATA = BIT(0),
	SIG_CMD_BUF_WRITTING_DATA = BIT(1),
	SIG_TIMESYNC_WRITTING_DATA = BIT(2),
	SIG_SEND_ENABLED = BIT(3),
	SIG_INTERFACE_STATE_CHANGED = BIT(4),
	SIG_PRIORITY_WRITE_EXIT = BIT(5),
	SIG_USR_AREA = SIG_PRIORITY_WRITE_EXIT,
	SIG_USR10 = BIT(6),
	SIG_USR9 = BIT(7),
	SIG_USR8 = BIT(8),
	SIG_USR7 = BIT(9),
	SIG_USR6 = BIT(10),
	SIG_USR5 = BIT(11),
	SIG_USR4 = BIT(12),
	SIG_USR3 = BIT(13),
	SIG_USR2 = BIT(14),
	SIG_USR1 = BIT(15),
} sysSignals_t;

extern sysSignals_t lastUsrSignal;

void InitSysSignal();

uint8_t RegisterUsrSignal(sysSignals_t signal);

uint8_t UnregisterUsrSignal(sysSignals_t signal);

void SysSignal(sysSignals_t signal) ;

INLINE sysSignal_t GetSignals(){
  return sysSignals;
}

INLINE void ClearSysSignal(sysSignals_t signal){
	CLEAR(sysSignals,signal);
}

INLINE void ClearAllSignals() {
	sysSignals = SIG_NO_SIGNAL;
}

INLINE uint8_t IsSignalPending() {
	return (uint8_t)!!sysSignals;
}

void CleanInfoSysSignals();



#endif
