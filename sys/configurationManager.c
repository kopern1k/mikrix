/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "configurationManager.h"
#include "sysConfig.h"
#include "sysHW.h"

typedef struct {
	uint8_t moduleID;
	DATA data;
} CONFIG;



uint8_t configBuf[MAX_CONFIG_BUF];
uint8_t configLen;
DATA configData;
#define NO_CONFIG_DATA 0xFF


void InitConfigurationManager(){
	//load configuration into memory
	configData.data =configBuf;
	configData.len = MAX_CONFIG_BUF;
	if (LoadConfig(&configData) !=0){
		configBuf[0]=NO_CONFIG_DATA;
		configLen=1;
	}else{
		configLen=configData.len;
	}
}

//return pointer to data stored in config memory
DATA* GetModuleConfig( uint8_t moduleID){
	uint8_t i=0;
	//find conrete module
	configData.data=NULL;
	configData.len=0;
	while((configBuf[i]!= NO_CONFIG_DATA) && (i< MAX_CONFIG_BUF)){
		if(((CONFIG*)&configBuf[i])->moduleID == moduleID){
			configData.data = (uint8_t*)&((CONFIG*)&configBuf[i])->data.data;
			configData.len =((CONFIG*)&configBuf[i])->data.len;
			break;
		}else{
			i=i+((CONFIG*)&configBuf[i])->data.len + 2;
			//configData.len = i;
		}
	}
	return &configData;
}


uint8_t DeleteModuleConfig(uint8_t module){
	uint8_t i,j,k;
	uint8_t max; 
	GetModuleConfig(module);
	i=configData.len+2;
	k=i;
	j=0;
	configData.data-=2;
	max=MAX_CONFIG_BUF - (uint8_t)(configData.data - configBuf);
	if(configData.data == NULL) return 1;
	//find length of next module and copy data to position of deleted module
	while( configData.data[i]!=NO_CONFIG_DATA ){
		if(i>= max){
			return 2;
		}
		i=i+((CONFIG*)&configData.data[i])->data.len + 2;
		//copy data
		for(;k<i;k++){
			configData.data[j++]=configData.data[k];
		}
	}
	configData.data[j] = configData.data[i];
	return 0;
}


//set configuration to buffer, if no data is set data->data == NULL, reserve only memory space - return 2, 
uint8_t SetModuleConfig(uint8_t moduleID, DATA *data){
	
	uint8_t i=0;
	uint8_t j=0;
	uint8_t *pdata;
	int8_t dif,moveStart;
	//find specific module
	if(data==NULL ){
		return 2;
	}
	while((configBuf[i]!= NO_CONFIG_DATA) && (i< MAX_CONFIG_BUF)){
		if(((CONFIG*)&configBuf[i])->moduleID == moduleID){
			dif=data->len - ((CONFIG*)&configBuf[i])->data.len;
			if((configLen+dif) > MAX_CONFIG_BUF) { return 3; }
			moveStart=(i+((CONFIG*)&configBuf[i])->data.len + 2);
			if(dif > 0){	
				for(j=dif+configLen-1;j>=moveStart+dif;j--){
					configBuf[j]=configBuf[j-dif];
				}
			}else if(dif<0){
				for(j=moveStart+dif;j<configLen+dif;j++){
					configBuf[j]=configBuf[j-dif];
				}
			}
			configLen+=dif;
			((CONFIG*)&configBuf[i])->data.len = data->len;
			pdata=(uint8_t*)&(((CONFIG*)&configBuf[i])->data.data);
			if(data->data == NULL){
				return 1;
			}
			for( j=0;j<data->len;j++){
				pdata[j]=data->data[j];
			}
			return 0;
		}else{
			i=i+((CONFIG*)configBuf)->data.len + 2;
			//configData.len = i;
		}
	}
	//copy data at the end of configBuf
	configBuf[configLen -1]= moduleID;
	configBuf[configLen]=data->len;
	configLen++;

	pdata = & configBuf[configLen];
	configLen+=data->len;
	configBuf[configLen] =NO_CONFIG_DATA;
	configLen++;
	if(data->data == NULL){
		return 2;
	}
	for( j=0;j<data->len;j++){
		pdata[j]=data->data[j];
	}

	return 0;
}

DATA* GetModulesConfig(){
	configData.data=configBuf;
	configData.len=configLen;
	return &configData;
}

uint8_t SetModulesConfig(DATA* data){
	uint8_t i;
	if(data->len >configLen) return 1;
	for(i=0;i<data->len;i++){
		configBuf[i] = data->data[i];
	}
	
	return 0;
}

//save data do file or flash
uint8_t SaveModulesConfig(){
	//
	configData.data=configBuf;
	configData.len=configLen;
	return SaveConfig(&configData);
}

DATA* GetModuleMemorySpace(uint8_t moduleID){
	uint8_t i=0;
	while((configBuf[i]!= NO_CONFIG_DATA) && (i< MAX_CONFIG_BUF)){
		if(((CONFIG*)&configBuf[i])->moduleID == moduleID){
			configData.data = &configBuf[i+2];
			configData.len = configBuf[i+1];
			return &configData;
		}else{
			i=i+((CONFIG*)configBuf)->data.len + 2;
		}
	}
	configData.data= &configBuf[i];
	configData.len = MAX_CONFIG_BUF - i;
	return &configData;
}
//start memory address (dataSpace), with len

/*
uint8_t ReserveMemorySpace( DATA *dataSpace){
	uint8_t dif;
	if(*dataSpace->data !=NO_CONFIG_DATA){
		dif=  dataSpace->len - dataSpace->data[1];
	}else{
		
		if(dataSpace->len + 3
	}
	return 0;
}
*/
