#include "sysSignal.h"
#include "sysHW.h"


sysSignal_t sysSignals;
sysSignal_t usedSignals;

void InitSysSignal() {
	sysSignals = 0;
	usedSignals = 0;
}

void SysSignal(sysSignals_t signal) {
	SET(sysSignals,signal);
	//SysSleepWakeup();
}


uint8_t RegisterUsrSignal(sysSignals_t signal) {
	if(usedSignals & signal) {
		return 0;
	}
	SET(usedSignals, signal);
	return 1;
}

uint8_t UnregisterUsrSignal(sysSignals_t signal) {
	if(signal <=SIG_USR_AREA) {
		return 0;
	}
	CLEAR(usedSignals,signal);
	return 1;
}

void CleanInfoSysSignals() {
	if(sysSignals & (SIG_INTERFACE_STATE_CHANGED | SIG_PRIORITY_WRITE_EXIT | SIG_SEND_ENABLED)) {
		CLEAR(sysSignals,(SIG_INTERFACE_STATE_CHANGED | SIG_PRIORITY_WRITE_EXIT | SIG_SEND_ENABLED));
	}
}
