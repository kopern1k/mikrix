/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "communicationInterface.h"

#include <stdio.h>
#include <string.h>
#include "availableInterfaces.h"
#include "sysConfig.h"


COM_MNG_DATA interfejs[MAX_INTERFACES];

void InitCommunicationIntefraces(){
	uint8_t i;
	for(i=0;i<MAX_INTERFACES;i++){
		interfejs[i].state = CM_STATE_CLOSED;
	}
}


uint8_t SetCommInterface(uint8_t sysInterfaceId, INTERFACE_TYPE_ID selectedInterface, uint8_t *configPar ){
	interfejs[sysInterfaceId].InterfaceWrite = availableInterfaces[selectedInterface].Write;
	interfejs[sysInterfaceId].interfaceID = sysInterfaceId;
	interfejs[sysInterfaceId].InterfaceClose = availableInterfaces[selectedInterface].Close;
	interfejs[sysInterfaceId].selectedInterfaceTypeID = selectedInterface;
	return availableInterfaces[selectedInterface].Init(configPar,&interfejs[sysInterfaceId]);
}

int16_t InitInterface(uint8_t sysInterfaceID,uint8_t selectedInterfaceID,uint8_t *portConfig){
	uint8_t ret;
	if(sysInterfaceID >= MAX_INTERFACES){
		return -1;
	}
	if(selectedInterfaceID >= (sizeof(availableInterfaces)/sizeof(INTERFACE))){
		return -2;
	}
	//check if intefrace is in use
	if(interfejs[sysInterfaceID].state != CM_STATE_CLOSED) {
		return -3;
	}

	InitCommManager(&interfejs[sysInterfaceID]);

	if((ret = SetCommInterface(sysInterfaceID,(INTERFACE_TYPE_ID)selectedInterfaceID,portConfig))) {
		return ret;
	}

	return 0;
}

int8_t GetFreeInterface() {
	uint8_t i;
	for(i=0;i<MAX_INTERFACES;i++){
		if(interfejs[i].state == CM_STATE_CLOSED) {
			return i;
		}
	}
	return -1;
}


uint8_t CloseInterface(uint8_t sysInterfaceID) {
	if(sysInterfaceID >= MAX_INTERFACES){
		return 1;
	}
	if(interfejs[sysInterfaceID].InterfaceClose){
		return 2;
	}
	if(interfejs[sysInterfaceID].state != CM_STATE_CLOSED) {
		interfejs[sysInterfaceID].InterfaceClose(&interfejs[sysInterfaceID]);
	}
	return 0;
}



int8_t SendHandledRequest(uint8_t moduleIDD, ADDR *dest,uint8_t cmd,uint8_t *data, uint16_t len,ResponseCallbackFunc_t callback, uint16_t timeoutMs, uint8_t multiResponses){
//chceck addr in address manager to get interface
	int8_t i, ind;
	int8_t reqInd = 0;
	ind = -1;
/*
	ResponseManagerT rt;
	rt.addr = *dest;
	rt.cmd = cmd;
	rt.Data = data;
	rt.len = (uint8_t)len;
*/
	if(callback) {
		ind=AddResponseAddrCallback(dest->nodeAddr.data,dest->IDD,cmd,callback,timeoutMs, multiResponses);
		if(ind<0){
			return -3;
		}
	}
	if(dest->nodeAddr.data == sysAddr.nodeAddr.data){
		CallModuleCMD(NULL,dest->IDD,cmd,data,(uint8_t)len,*dest);
		return ind;
	}
	if(addresManagerInitialized) {
		for(i=0;i<NODE_TABLE_SIZE;i++){
			if(nodeTable[i].addr == dest->nodeAddr.data){
				reqInd=SendCmdReq(&interfejs[nodeTable[i].interfaceID], moduleIDD, dest, cmd, data, (uint8_t)len);
				if(reqInd){
					return -1;
				}
				return ind;
			}
		}
	}
	//resend commnand request to all interfaces - maybe noTable is not initialized yet
	for(i=0;i<GetInterfacesCount();i++){
		if(interfejs[i].state == CM_STATE_ENABLED) {
			reqInd += SendCmdReq(&interfejs[i], moduleIDD, dest, cmd, data, (uint8_t)len);
		}
	}
	if(reqInd){
		return -1;
	}
	return ind;
}


//Usage
//InitInterface(1,COM_PORT,(uint8_t*) &(MAKE_SERIAL_CONFIG("/dev/ttyUSB0",115200)));
//InitInterface(0,SOCKET_PORT,NULL);
