/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef FLATSTORAGE_H
#define FLATSTORAGE_H
#include "../common/inttypes.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct FSIO_t FSIO_t;

typedef struct FS_t FS_t;
typedef struct FSF_t FSF_t;

struct FSIO_t {
	uint8_t (*Init)(FS_t *fs, uint16_t size, void *config); // 0 - OK, !=0 Err
	uint8_t (*Open)(FS_t *fs, FSF_t *f, char *fileName);				// 0 - OK, !=0 Err
	uint8_t (*Read)(FSF_t *f, void *data, uint16_t size);	// 0 - Ok, !=0 err
	uint8_t (*Write)(FSF_t *f, void *data, uint16_t size);	// 0 - Ok, !=0 err
	uint8_t (*Close)(FSF_t *f);								// 0 - Ok, !=0 err
	uint8_t (*FileExist)(FS_t *f, char *fileName);			// 1 - Exist, 0- Not exist
};


struct FS_t {
	FSIO_t *io;
	void *storageData;
};

typedef enum  {FSF_WAIT_FOR_OPERATION, FSF_READING, FSF_WRITTING, FSF_SYNCING} FSF_OP_t;
struct FSF_t { //flat storage file
	FSIO_t *io;
	volatile FSF_OP_t op;
	void *userData;
};


/*
typedef struct {
	uint32_t fileNameHash;
	uint16_t nextPart;		//if file has increased, nextPart will point to next place. No next Part  == 0xFFFF
	uint16_t endInd;
	uint16_t nextFile;
} FSDataElement_t;

*/

uint8_t InitFS(FS_t *fs, FSIO_t *fsIo, uint16_t size, void* fsConfig);

void InitFSFile(FSF_t *f);

uint8_t OpenFS(FS_t *fs, FSF_t *f, char* fileName);
uint8_t ReadFSF(FSF_t *f, void* data, uint16_t len);
uint8_t WriteFSF(FSF_t *f, void* data, uint16_t len);
uint8_t CloseFSF(FSF_t *f);

uint8_t FileExistFS(FS_t *f, char *fileName);

uint8_t IsFSFileOpen(FSF_t *f);

#ifdef __cplusplus
}
#endif

#endif // FLATSTORAGE_H
