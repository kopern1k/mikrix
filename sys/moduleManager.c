/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */


#include "moduleManager.h"
#include "threadManager.h"
#include "responseManager.h"
#include "system.h"
#include "sysConfig.h"
#include "moduleReceiveQueue.h"
#include "stdlib.h"
#include "string.h"
#include "../include/sysAllocMem.h"
#include "../common/oneTypeAlloc.h"

#ifdef MODULE_MANAGER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"


MODULE_OBJ moduleObj[MAX_MODULE_OBJECTS];	//IDDs table

uint16_t sysTotalModuleMessagesCnt;


#ifndef MAX_CMD_DATA_OUT_ITEMS
#define MAX_CMD_DATA_OUT_ITEMS MAX_RESPONSES
#endif


MEM_ITEM(CMD_DATA_OUT_MEM_ITEM_t,
		 CMD_DATA_OUT data;
);


CMD_DATA_OUT_MEM_ITEM_t cmdDataOutBuf[MAX_CMD_DATA_OUT_ITEMS];

INLINE CMD_DATA_OUT *AllocCmdDataOut() {
	return (CMD_DATA_OUT*) GetOneTypeMemItem((memItem_t*)cmdDataOutBuf,sizeof(CMD_DATA_OUT_MEM_ITEM_t),MAX_CMD_DATA_OUT_ITEMS);
}

INLINE void PrintCmdDataOutBufInfo() {
	PrintOneTypeMemInfo((memItem_t*) cmdDataOutBuf,sizeof(CMD_DATA_OUT_MEM_ITEM_t),MAX_CMD_DATA_OUT_ITEMS);
}

int8_t FreeCmdDataOut(CMD_DATA_OUT *pData) {
	if(pData == NULL) {
		return 1;
	}
	if(pData->data && pData->FreeFunct) {
		pData->FreeFunct((void*)pData->data);
	}
	return FreeOneTypeMemItem((memItem_t*) cmdDataOutBuf, sizeof(CMD_DATA_OUT_MEM_ITEM_t),MAX_CMD_DATA_OUT_ITEMS,(void*)pData);
}

void InitModuleManager(){
	uint8_t i;
	for(i=0;i<MAX_MODULE_OBJECTS;i++){
		moduleObj[i].data=0;
		moduleObj[i].module=0;
		moduleObj[i].initialized = 0;
	}
	sysTotalModuleMessagesCnt = 0;
	InitOneTypeMemItems((memItem_t*)cmdDataOutBuf,sizeof(CMD_DATA_OUT_MEM_ITEM_t),MAX_CMD_DATA_OUT_ITEMS);
}

uint16_t GetDefaultModulesLen(){
	uint16_t i;
	for (i=0;i < MAX_PLATFORM_MODULES_ARRAY;i++){
		if(allModules[i] == NULL) return i;
	}
	return 0;
}

uint8_t GetDefaultModulesInfo(MODULE_INFO *info,  uint16_t *len){
	uint16_t size =GetDefaultModulesLen();
	uint16_t i;
	if(*len<size) {
		return 1;
	}
	*len = size;
	for (i=0; i<size;i++){
		info[i].moduleID = allModules[i]->info->moduleID;
		strcpy( (char*)info[i].name,(char*)allModules[i]->info->name);
	}
	
	return 0;
}
int16_t AddModuleAtPosition(uint8_t moduleIndex, uint8_t position, void* initParam){
	//if(moduleObj[position].module ==0) return 1;
	moduleObj[position].data= (MODULE_DATA*)malloc(sizeof(MODULE_DATA));
	if( moduleObj[position].data == 0) {
		return -2;
	}


	moduleObj[position].module=allModules[moduleIndex];

	//set init parameter
	moduleObj[position].data->initParam = initParam;
	//set IDD for module
	moduleObj[position].data->IDD=position;
	//initialize timer
	moduleObj[position].data->currentTimer = NULL;

	if(allModules[moduleIndex]->Init(moduleObj[position].data)){
		moduleObj[position].data->IDD=0;
		free(moduleObj[position].data);
		moduleObj[position].module=0;
		return -3; //Add
	}

	//set sheduling if needed (pFunc !=NULL)
	if(moduleObj[position].module->MainFunction !=NULL){
		if(AddThread((void*)moduleObj[position].data,(thrFunc_t)moduleObj[position].module->MainFunction) == 0){
			//could not set new thread 
			moduleObj[position].module->DeInit(moduleObj[position].data);
			free(moduleObj[position].data);
			moduleObj[position].data = 0;
			return -4;
		}
	}
	moduleObj[position].initialized = 1;
	return position;
}
int16_t AddModule(uint8_t moduleIndex, void* initParam){	//add new modulule to scheduling, create and initialize memory context of module
	//find first free place
	uint8_t i;
	for(i=0;i<MAX_MODULE_OBJECTS;i++){
		if(moduleObj[i].module==0) break;
	}
	if(i==MAX_MODULE_OBJECTS) return -10;
	
	return AddModuleAtPosition(moduleIndex, i, initParam);
}

uint8_t MoveModuleScheduling(uint8_t moduleIndex, uint8_t moveAfterModuleIndex){
	
	return MoveThreadFromData(moduleObj[moduleIndex].data,moduleObj[moveAfterModuleIndex].data);
}
uint8_t DeleteModule(uint8_t IDD){
	if(moduleObj[IDD].module==0) return 1; //module object is free
	//if(IDD ==0) return 2;		//system module
	moduleObj[IDD].module->DeInit(moduleObj[IDD].data);
	free(moduleObj[IDD].data);
	
	//if(moduleObj[IDD].module->MainFunction !=NULL){ //delete thread if needed ... control not needed DeleteThread have 
	DeleteThreadFromMemoryContext((void*)moduleObj[IDD].data);
	moduleObj[IDD].data = 0;
	moduleObj[IDD].module = 0;
	moduleObj[IDD].initialized = 0;
	//}
	//delete from Thread;
	return 0;
}

COM_MNG_DATA *lastCallModuleCM;




uint8_t CallModuleCMD(COM_MNG_DATA *cm, uint8_t moduleIDD,uint8_t cmd,uint8_t *data, uint8_t len,ADDR srcAddr){
	DEBUGPRINTF("[%llu] CallModuleCMD called. srcAddr=%hhu.%hhu.%hhu,moduleIDD=%hhu,cmd=%hhu,len=%hhu\n",(unsigned long long) GetSysTickCount(), srcAddr.nodeAddr.hi,srcAddr.nodeAddr.lo,srcAddr.IDD,moduleIDD, cmd, len);
	if(moduleIDD >= MAX_MODULE_OBJECTS) {
		return 2;
	}
	if(moduleObj[moduleIDD].module == 0){
		return 3;
	}
	if(moduleObj[moduleIDD].initialized == 0) {
		return 4;
	}
	lastCallModuleCM = cm;

	if(moduleObj[moduleIDD].module->cmdTbl->nCommands <= cmd){
		return 5;
	}
	if(moduleObj[moduleIDD].module->cmdTbl->commands[cmd].Command == NULL ){
		return 6;
	}
	CMD_DATA_IN dataIn;
	dataIn.cm = cm;
	dataIn.srcAddr = srcAddr;
	dataIn.data = data;
	dataIn.len = len;

	CMD_DATA_OUT *pDataOut = AllocCmdDataOut();
	if(pDataOut == NULL) {
	#ifdef TEST
		PrintCmdDataOutBufInfo();
		PrintThreadsInfo();
	#endif
		return 7;
	}
	pDataOut->FreeFunct = NULL;
	pDataOut->data = NULL;
	pDataOut->len = 0;

	DEBUGPRINTF("[%llu] moduleObj[moduleIDD].module->cmdTbl->commands[cmd].Command called\n", (unsigned long long) GetSysTickCount());
	uint8_t err = moduleObj[moduleIDD].module->cmdTbl->commands[cmd].Command(moduleObj[moduleIDD].data,&dataIn,pDataOut);
	if(err == NO_SEND_RESPONSE) {
		FreeCmdDataOut(pDataOut);
		return 0;
	}
	if(cm == NULL) {
		//send resp to specific module from response manager, Only request callbacks are supported
		if(CallResponseFromData(NULL,GetSystemAddrP()->nodeAddr.data,moduleIDD,cmd,err,pDataOut->data,pDataOut->len) <0){
			FreeCmdDataOut(pDataOut);
			return 8;
		}
		FreeCmdDataOut(pDataOut);
		return 0;
	}

	if(SendCmdResp(cm, cmd,moduleIDD,err,(struct CMD_DATA_OUT*)pDataOut ,srcAddr)) {
		FreeCmdDataOut(pDataOut);
		return 9;
	}
	// do freeing after sending data

	return 0;
}

uint8_t GetCreatedModuleList(MODULE_LIST* mo, uint8_t *moLen ){
	uint8_t i,ii;
	ii=0;
	if(moLen == NULL) {
		return 1;
	}
	if(*moLen == 0) {
		*moLen = 0;
		return 2;
	}
	if(mo == NULL) {
		return 3;
	}
	for(i = 0; i< MAX_MODULE_OBJECTS; i++){
		if(moduleObj[i].module){
			mo[ii].module = moduleObj[i].module; 
			mo[ii++].mId = i;
			if(ii>=*moLen) {
				*moLen = ii; 
				return 4;
			}
		}
	}
	*moLen = ii;
	return 0;
}

uint8_t GetCreatedModulesCnt() {
	uint8_t cnt = 0, i;
	for(i = 0; i< MAX_MODULE_OBJECTS; i++){
		if(moduleObj[i].module){
			cnt++;
		}
	}
	return cnt;
}

uint8_t	OnDataCommon(ADDR *srcAddr,uint8_t *data, uint8_t len, uint8_t dataOffset, uint8_t dataLen, MODULE_DATA *moduleData, uint8_t moduleInOffset, DATA_OP dataOperation, HAVE_DATA_MASK	varIdentifier){
	//set data
	uint8_t err;
	uint8_t *pData;
	/* dataLen == 0 specifies that all data should be stored */
	if(dataLen == ALL_RECEIVED_DATA) {
		dataLen = len;
	}
	if(dataOffset + dataLen >len){
		return 1;
	}
	if( moduleData->inData.maxDataItemSize < (dataOffset + dataLen) ){
		return 2;
	}
	//Get Free place in Queue
	pData = MRQGetNextFreeItemP(&moduleData->inData,&err);
	switch(err) {
	case QUEUE_NO_ERR:
		break;
	case QUEUE_ERR_BAD_POINTER:
		return 3;
	case QUEUE_ERR_IS_FULL:
		return 4;
	case QUEUE_ERR_OVERRIDING_LAST:
		//DEBUGPRINTF("Warning, OnData received data, but data will be overriden");
		break;
	}

	switch(dataOperation){
		case COPY:
			CopyMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case SET:
			//SetMem(moduleData->inData.data + moduleInOffset,data+dataOffset,dataLen, = );
			SetMem((pData + moduleInOffset),0xFF,dataLen);
			/*
			for(i=0;i<dataLen;i++){
				(moduleData->inData.data + moduleInOffset)[i] = data[i+dataOffset]; 
			}
			*/
			break;
		case RESET:
			SetMem((pData + moduleInOffset),0x00,dataLen);
			break;
		case XOR:
			XorMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case AND:
			AndMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case NEG:
			NegMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		default:
			break;
	}

	//Store new position in Queue
	MRQAllocNextFreeItem(&moduleData->inData);
	//moduleData->inData;
	//set havedata
	SET(moduleData->haveData,varIdentifier);
	IncTotalModulesMessages();
	return 0;
}

// 0xFF NO_MODULE_ID
#define NO_MODULE_ID 0xFF
uint8_t GetModuleIndexFromModuleID(uint32_t id){
	uint8_t i;
	for( i=0;i<MAX_PLATFORM_MODULES_ARRAY;i++){
		if(allModules[i]->info->moduleID == id) return i;
		if(allModules[i] == NULL) return NO_MODULE_ID;
	}
	return NO_MODULE_ID;
}
