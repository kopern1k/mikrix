/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "maskAddrManager.h"
#include "../common/common.h"
#include "configurationManager.h"
#include "moduleManager.h"
//#include "types.h"

MASK maskTable[MASK_ADDR_TABLE_SIZE];
//INTERNAL_MASK internalMaskTable[INTERNAL_MASK_ADDR_TABLE_SIZE];
uint8_t freeStart=0; //starts at the start of array //points to free place!
uint8_t internalFreeStart=MASK_ADDR_TABLE_SIZE-1; //ends points to free place!
uint8_t middle=0;
uint8_t internalMiddle =MASK_ADDR_TABLE_SIZE-1;

 void SwitchTableData(uint8_t ind1, uint8_t ind2){
	MASK tmp;
	tmp=maskTable[ind1];
	maskTable[ind1]=maskTable[ind2];
	maskTable[ind2]=tmp;
}


 uint8_t Connect(uint8_t IDD, ADDR srcAddr,ADDR maskAddr, DATA_OP operation,uint8_t offset, uint8_t size, uint8_t variableNum){
	 MASK mask;
	 mask.srcAddr = srcAddr;
	 mask.mask = maskAddr;
	 mask.dataOperation = operation;
	 mask.dataOffset = offset;
	 mask.dataSize = size;
	 mask.varIdentifier = variableNum;
	 mask.moduleDataInOffset = 0;
	 mask.OnData = GetModule(IDD)->OnData;
	 mask.moduleData = GetModuleData(IDD);
	 return AddMaskAddr(&mask);
 }


uint8_t AddMaskAddr(MASK *maskData){
	
	int8_t inc=1;//sizeof(MASK);
	uint8_t top=freeStart;
	uint8_t bottom=0;
	int8_t i;
	ADDR addr;
	if( freeStart == internalFreeStart ){
		return 1; //no free memory
	}
	
	GetSystemAddr( &addr );
	if(addr.nodeAddr.data == maskData->srcAddr.nodeAddr.data ) { // /*&& (addr.IDD == maskData->srcAddr.IDD)*/){	//chceck if it is local addr.
		NEG(inc);															//local addr, Add data from the end of table
		top = internalFreeStart;
		internalFreeStart--;
		bottom = MASK_ADDR_TABLE_SIZE-1;
		internalMiddle= (bottom + top)>>1; //(2b-b+t)/2 = (b+t)/2
	}else{
		middle = top>>1;
		freeStart++;
	}
	
	maskTable[top] = *maskData;	//Give data at the end of mTable
	for( i=top;i!=bottom;i -= inc){			// sort data 
		if( (maskTable[i].srcAddr.nodeAddr.data >= maskTable[i - inc].srcAddr.nodeAddr.data) &&  (maskTable[i].srcAddr.IDD >= maskTable[i - inc].srcAddr.IDD)){
			SwitchTableData(i,i - inc);
		};
	}
/*
	top+=inc;
	middle=top>>1;
	//find first occu
	i=top;
	do{
		i--;
	}while(maskData->srcAddr.devAddr.data == maskTable[i].srcAddr.devAddr.data  &&  maskData->srcAddr.IDD == maskTable[i].srcAddr.IDD && i>=0);
	top=i>=0?i:0;
*/
	return 0;
}

uint8_t DeleteMaskAddr(ADDR *srcAddr, MODULE_DATA *data ){
	int8_t inc=1;//sizeof(MASK);
	uint8_t* top=&freeStart;
	uint8_t bottom=0;
	uint8_t i;
	uint8_t j;
	ADDR addr;
	if(srcAddr==NULL){
		return 1;
	}

	GetSystemAddr( &addr );
	if(addr.nodeAddr.data == srcAddr->nodeAddr.data){	//chceck if it is local addr.
		NEG(inc);															//local addr, Add data from the end of table
		top = &internalFreeStart;
		bottom = MASK_ADDR_TABLE_SIZE-1;
	}
	
	//find index of deleted data...
	
	for( i=*top;i!=bottom;i -= inc){			// sort data 
		if( (maskTable[i].srcAddr.nodeAddr.data == srcAddr->nodeAddr.data) && ( maskTable[i].srcAddr.IDD == srcAddr->IDD) &&  maskTable[i].moduleData == data  ){
			//we have data index to delete
			//clear index
			maskTable[i].moduleData=0;
			maskTable[i].dataOffset=0;
			maskTable[i].dataOperation=COPY;
			maskTable[i].dataSize=0;
			maskTable[i].moduleDataInOffset=0;
			maskTable[i].OnData =NULL;
			maskTable[i].srcAddr.nodeAddr.data =0;
			maskTable[i].srcAddr.IDD =0;
			maskTable[i].mask.nodeAddr.data =-1;
			maskTable[i].mask.IDD =-1;
			//sort array get this point to the top
			for( j=i;j!=*top;j += inc){
					SwitchTableData(j,j+ inc);
			}
			*top-=inc;
			if(inc>0){
				middle=*top>>1;
			}else{
				internalMiddle=(*top+bottom + 1)>>1;
			}
			return 0;
		};
	}
	return 1;
}

uint8_t DeleteMaskAddrFromID(uint8_t maskTableID){

	int8_t inc=1;//sizeof(MASK);
	uint8_t* top=&freeStart;
	uint8_t bottom=0;
	uint8_t j;
	//ADDR addr;
	if(((maskTableID >= freeStart) && (maskTableID <= internalFreeStart)) ||( maskTableID >= MASK_ADDR_TABLE_SIZE) ){
		return 1;
	}

	if(maskTableID >internalFreeStart){	//chceck if it is local addr.
		NEG(inc);															//local addr, Add data from the end of table
		top = &internalFreeStart;
		bottom = MASK_ADDR_TABLE_SIZE-1;
	}
	maskTable[maskTableID].moduleData=0;
	maskTable[maskTableID].dataOffset=0;
	maskTable[maskTableID].dataOperation=COPY;
	maskTable[maskTableID].dataSize=0;
	maskTable[maskTableID].moduleDataInOffset=0;
	maskTable[maskTableID].OnData =NULL;
	maskTable[maskTableID].srcAddr.nodeAddr.data =0;
	maskTable[maskTableID].srcAddr.IDD =0;
	maskTable[maskTableID].mask.nodeAddr.data =0;
	maskTable[maskTableID].mask.IDD =0;
	//sort array get this point to the top
	for( j=maskTableID;j!=*top;j += inc){
			SwitchTableData(j,j+ inc);
	}
	*top-=inc;
	if(inc>0){
		middle=*top>>1;
	}else{
		internalMiddle=(*top+bottom + 1)>>1;
	}
	return 0;
}
// ret 0 -> Ok. data was set
// re. 1 -> No data was set
uint8_t CallCallbackFromData(ADDR addr, uint8_t *dataBuf, uint8_t len){
	int8_t step;
	uint8_t i=middle;
	uint8_t bottom=0;
	uint8_t top = freeStart;
	ADDR sAddr;
	PINTEST_INIT(BIT3);
//	uint8_t finded=0;
	if( (freeStart == 0 ) && (internalFreeStart == (MASK_ADDR_TABLE_SIZE-1))){
		return 1; //table is free
	}
	//test system addr
	
	GetSystemAddr( &sAddr );
	if( addr.nodeAddr.data == sAddr.nodeAddr.data ){	//chceck if it is local addr.
		i=internalMiddle;
		bottom = internalFreeStart;
		top = MASK_ADDR_TABLE_SIZE-1;
	}
	//compute delta
	if(maskTable[i].srcAddr.nodeAddr.data < addr.nodeAddr.data){
		step = 1;
	}else if(maskTable[i].srcAddr.nodeAddr.data > addr.nodeAddr.data){
		step = -1;
	} else {
		if(maskTable[i].srcAddr.IDD < addr.IDD) {
			step = 1;
		} else {
			step = -1;
		}
	}

	
	do{
		if((maskTable[i].srcAddr.nodeAddr.data  == (addr.nodeAddr.data & maskTable[i].mask.nodeAddr.data )) && (maskTable[i].srcAddr.IDD == (addr.IDD & maskTable[i].mask.IDD))){
			PINTEST_SET(BIT3);
			//return back to first possible place with same src address
			while((i-1>bottom) && (maskTable[i-1].srcAddr.IDD == addr.IDD)) {
				i--;
			}
			while(maskTable[i].srcAddr.IDD == addr.IDD) {
				//check if module is initialized.
				if(moduleObj[addr.IDD].initialized && maskTable[i].OnData) {
					// run OnData function
					maskTable[i].OnData(&addr, dataBuf, len, maskTable[i].dataOffset,maskTable[i].dataSize,maskTable[i].moduleData,maskTable[i].moduleDataInOffset,maskTable[i].dataOperation,maskTable[i].varIdentifier);
				}
				i++;
			}
			PINTEST_CLEAR(BIT3);
			return 0;
		}
		i += step;
	}while(i>bottom && i<=top);
	return 1;
}
//len input -> length of outIndexArray, output -> len of setted array
uint8_t GetMaskAddrListFromObject(MODULE_DATA *data, uint8_t *outIndexArray, uint8_t *len){
	uint8_t ind=0;
	uint8_t i;
	for(i=0; i<freeStart;i++){
		if(maskTable[i].moduleData == data){
			if(ind>=*len) return 1;
			outIndexArray[ind++]=i;
		}
	}
	for(i=internalFreeStart; i<(MASK_ADDR_TABLE_SIZE-1);i++){
		if(maskTable[i].moduleData == data){
			if(ind>=*len) return 2;
			outIndexArray[ind++]=i;
		}
	}
	*len=ind;
	return 0;
}

typedef struct{
	ADDR	srcAddr;			//3B	source address
	DATA_OP dataOperation;		//1B Set, And, Or ...
	uint8_t dataOffset;			//1B	offset inside inBuf
	uint8_t dataSize;			//1B	length of data to be set
	uint8_t moduleDataInOffset;	//1B	tells which data variable will be set inside inData module buffer  
	HAVE_DATA_MASK	varIdentifier;
	uint8_t	moduleID;
}MASK_STORE_DATA; //11B

#define MASK_ADDR_MANAGER_VIRT_ID			0xFE
#define MASK_ADDR_MANAGER_INTERNAL_VIRT_ID	0xFD
uint8_t SetMaskAddrManagerConfig(){
	uint8_t ret=0;
	DATA data;
	DATA *pData;
	//MASK_STORE_DATA store;
	uint8_t i,j;
	data.data=NULL;
	data.len = freeStart*sizeof(MASK_STORE_DATA); //compute stored data len
	if(data.len == 0) {
		ret =BIT0;
	}else{
		//reserve memory space
		if(SetModuleConfig(MASK_ADDR_MANAGER_VIRT_ID,&data)>1){
			return ret^BIT2;
		}
		//copy data
		pData=GetModuleMemorySpace(MASK_ADDR_MANAGER_VIRT_ID);
		for( i=0;i<freeStart;i++){
			/*
			store.dataOffset=maskTable[i].dataOffset;
			store.dataOperation=maskTable[i].dataOperation;
			store.dataSize = maskTable[i].dataSize;
			store.moduleDataInOffset = maskTable[i].moduleDataInOffset;
			store.srcAddr.devAddr=maskTable[i].srcAddr.devAddr;
			store.srcAddr.IDD=maskTable[i].srcAddr.IDD;
			store.moduleID = maskTable[i].moduleData->IDD;
			*/
			for( j = 0;j<OFFSETOF_8(MASK_STORE_DATA,moduleID);j++){
				(pData->data)[j] = ((uint8_t*)&maskTable[i])[j];
			}
			((MASK_STORE_DATA*)pData->data)->moduleID = maskTable[i].moduleData->IDD;
			pData->data += sizeof( MASK_STORE_DATA );	
		}
	}

	//internal mask table 
	data.data=NULL;
	data.len = freeStart*sizeof(MASK_STORE_DATA); //compute stored data len
	if(data.len == 0) {
		ret =ret^BIT1;
	}else{
		//reserve memory space
		if(SetModuleConfig(MASK_ADDR_MANAGER_INTERNAL_VIRT_ID,&data)>1){
			return ret^BIT3; 
		}
		//copy data
		pData=GetModuleMemorySpace(MASK_ADDR_MANAGER_INTERNAL_VIRT_ID);
		for( i=MASK_ADDR_TABLE_SIZE-1;internalFreeStart<i;i--){
			//copy stucture data
			for( j = 0;j<OFFSETOF_8(MASK_STORE_DATA,moduleID);j++){
				(pData->data)[j] = ((uint8_t*)&maskTable[i])[j];
			}
			((MASK_STORE_DATA*)pData->data)->moduleID = maskTable[i].moduleData->IDD;
			pData->data += sizeof( MASK_STORE_DATA );	//new offset
		}
	}
	return ret;
}

uint8_t GetMaskAddrManagerConfig(){
	DATA *data;
	uint8_t i,j,size;
	data=GetModuleConfig(MASK_ADDR_MANAGER_VIRT_ID);
	if(data->data == NULL){ 
		return 1;
	}
	size=data->len/sizeof(MASK_STORE_DATA);
	for(i=0;i<size;i++){
		for( j = 0;j<OFFSETOF_8(MASK_STORE_DATA,moduleID);j++){
			((uint8_t*)&maskTable[i])[j]=(data->data)[j];
		}
		maskTable[i].OnData=GetModule(((MASK_STORE_DATA*)data->data)->moduleID)->OnData;
		maskTable[i].moduleData=GetModuleData(((MASK_STORE_DATA*)data->data)->moduleID);
		data->data += sizeof( MASK_STORE_DATA );
	}
	//set the size of table + 1 -> free place 
	freeStart=i;
	middle=i>>1;
	//internal mask table
	data=GetModuleConfig(MASK_ADDR_MANAGER_INTERNAL_VIRT_ID);
	if(data->data == NULL){ 
		return 2;
	}
	size=MASK_ADDR_TABLE_SIZE - data->len/sizeof(MASK_STORE_DATA);
	for(i=MASK_ADDR_TABLE_SIZE-1;i>=size;i--){
		for( j = 0;j<OFFSETOF_8(MASK_STORE_DATA,moduleID);j++){
			((uint8_t*)&maskTable[i])[j]=(data->data)[j];
		}
		maskTable[i].OnData=GetModule(((MASK_STORE_DATA*)data->data)->moduleID)->OnData;
		maskTable[i].moduleData=GetModuleData(((MASK_STORE_DATA*)data->data)->moduleID);
		data->data += sizeof( MASK_STORE_DATA );
	}
	internalFreeStart=i;
	internalMiddle=i>>1;
	return 0;

}
