/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
//#define TEST
//#define TEST_VIEW_DATA
//TEST_VIEW_CALL_THRSEND
#include "sysConfig.h"
#include "communicationManager.h"
#include "timeManager.h"
#include "responseManager.h"
#include "maskAddrManager.h"
#include "moduleManager.h"
#include "threadManager.h"
#include "responseManager.h"
#include "sysSignal.h"
#include "timeCorr.h"
#include "moduleDef.h"
#include "crc16.h"
#include "pt.h"
#include "stdlib.h"
#include "stdio.h"
#include "atomic.h"

//#define COMMSPEED			38400					10 bits/byte
#ifdef COMMUNICATION_MANAGER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"

#define NO_TIMER_HANDLE 0xFF

//#define MAX_RANDOM_TIME WAIT_FOR_ONE_BYTE*10

void PrintParserFingerInfo(COM_MNG_DATA *cm) {
	int i;
	DEBUGPRINTF("top.start=%hu, top.end=%hu\n",cm->fingerTop.start, cm->fingerTop.end);
	for(i = 0; i<MAX_SEND_BUFFERS; i++) {
		DEBUGPRINTF("sendbuf[%d].start= %hhu, sendbuf[%d].end= %hhu\n", i, cm->sendBufFinger[i].start, i, cm->sendBufFinger[i].end);
	}
}

void PrintParserInfo(COM_MNG_DATA *cm) {
	DEBUGPRINTF("parser state: %hhu, crc: %hu, packet cnt: %hd, send en: %hu\n", cm->parser.parserState, cm->parser.crc, cm->sendBufPacketsCnt, cm->sendEnabled);
	PrintParserFingerInfo(cm);
}

void ResetParser(COM_MNG_DATA *cm) {
	cm->parser.parserState = WaitForPacket;
	cm->parser.crc = 0xFFFF;
}

void InitParser(COM_MNG_DATA *cm) {
	ResetParser(cm);
	cm->parser.dataLen = 0;
	cm->parser.packetLen = 0;
	cm->parser.actLen = 0;
	InitInData(cm);
}


void InitCommManager(COM_MNG_DATA *cm){
	//blocking - sync variables
	cm->sendingData =0;
	cm->sendEnabled=1;
	cm->writtingToBuffer =0;
	cm->priorityWrite =0;
	SysSignal(SIG_PRIORITY_WRITE_EXIT);
	cm->CommSendErrorReport = 0;
	cm->timerValue=0;
	//parse received data. 
	InitParser(cm);
	cm->idd=0xFF;
	cm->sendRequest =0;
	cm->l = 0;
	cm->bytesSend = 0;
	InitRandomTime(cm->communicationSpeed);


	IntitSendBuf(cm);
}
void DiOnLinkBusy(){}; //INT0 disable
void EnOnLinkBusy(){}; //INT0 enable

ADDR reqAddr={{0},0}; //address for requesting data
#define GetReqAddrP()	(&reqAddr)
#define SetReqAddr( address )	reqAddr = address 

ADDR reqTimeSyncAddr={{0},0}; //address for requesting data
#define GetTimeSyncReqAddrP()	(&reqTimeSyncAddr)
#define SetTimeSyncReqAddr( address )	reqTimeSyncAddr = address 

#define CM() ((COM_MNG_DATA *)data) 
#define CMD_RESP() ((CMD_RESP_DATA *)data)

uint8_t GetRandomTime(){
//max random time
	//get random number
	//srand(20);
	//return (uint8_t) (rand()>>8);
	return (uint8_t) rand();
}


void SendErrorReport(COM_MNG_DATA *cm){

	cm->CommSendErrorReport = 0;
}

//wait for timer elapsed ... this timer is shared, and changed after one byte o
uint8_t LinkFreeTimeoutThr(void *data, pt* Thr){
	PT_BEGIN(Thr);
	PT_WAIT_WHILE(Thr,(CM()->timerValue)>GetSysTickCount());
	EnOnLinkBusy();
	if((CM()->parser.parserState ==  LengthError) || (CM()->parser.parserState == HeaderError) || ( CM()->parser.parserState == FullBuffer) ){
		CM()->parser.parserState = WaitForPacket;
		if(IsMaster()){	
			SendErrorReport(CM());
		}else{
			CM()->sendEnabled =1;
			EnOnLinkBusy();
			CM()->timerValue = GetSysTickCount() + (((uint16_t)CM()->oneByteDelayUs) + ((uint16_t)CM()->oneByteDelayUs)/5 + GetRandomTime());
			PT_WAIT_WHILE(Thr,(CM()->timerValue) > GetSysTickCount());
			if( !CM()->CommSendErrorReport && CM()->sendEnabled == 1 ){
				SendErrorReport(CM());
			}else{
				return PT_ENDED;
			}
		}
	}
	CM()->parser.parserState = WaitForPacket;
	CM()->sendEnabled =1;
	EnOnLinkBusy();
	DeleteThread(Thr);
	PT_END(Thr);
}
 
//length - bytes waiting 
uint16_t tmpTime;
void SetLinkFreeTimeot(COM_MNG_DATA *cm, uint8_t length){ //
	if(cm->fullDuplex) {
		return;
	}

	cm->timerValue = GetSysTickCount();
	cm->timerValue +=  ((uint16_t)cm->oneByteDelayUs)*length + ((uint16_t)cm->oneByteDelayUs)/4 + ( IsMaster()?0:GetRandomTime() );
	AddThread((void*)cm,LinkFreeTimeoutThr); //wait for timer elapsed
}
void SetLinkFreeStrictTimeot(COM_MNG_DATA *cm, uint8_t length){ //
	
	cm->timerValue = GetSysTickCount() + (((uint16_t)cm->oneByteDelayUs)*length + ((uint16_t)cm->oneByteDelayUs)/5);
	AddThread((void*)cm,LinkFreeTimeoutThr); //wait for timer elapsed
	
}






#define SENDBUF_NO_DATA 0xFF

void IntitSendBuf(COM_MNG_DATA *cm){
//	uint8_t i;
	cm->fingerTop.start=SENDBUF_NO_DATA;	//start of data (index to sendBufFinger)
	cm->fingerTop.end=SENDBUF_NO_DATA;	//start of free data (index to sendBufFinger)
	cm->sendBufPacketsCnt = 0;
/*	for( i=0;i<MAX_SEND_BUFFERS;i++){
		cm->sendBufFinger[i].start=0;
		cm->sendBufFinger[i].len=0;
	}
*/
}

#define LinkFree() 1

#define SENDED 0xFF
//return start index of placed data,

//<0 numbers - error
int16_t CreateDataPacket(COM_MNG_DATA *cm,I16 *srcAddr, ADDR *destAddr, uint8_t *data, uint8_t len, uint8_t moduleIDD, uint8_t block,uint8_t cmd,uint8_t err, PacketType type){
	uint8_t overhead;
	uint8_t ind;
	uint16_t crc;
	uint8_t freeSpace;
	uint8_t top;
	uint8_t i;
	int16_t dif;
	//chceck free space in buffer
#ifdef TEST_VIEW_DATA
	DEBUGPRINTF("CreateDataPacket called.\n");
	DEBUG_DATA(data,len,"data for packet\n");
#endif
	//if(cm == NULL || srcAddr == NULL || destAddr == NULL || data == NULL) {
	//	return ERR_COMM_MNGR_NULL_DATA;
	//}
	switch(type){
		case DataPacket:
			overhead = 7;
			break;
		case BlockDataPacket:
		case RequestPacket:
			overhead = 11;
			break;
		case ResponsePacket:
			overhead = 12;
			break;
		case TimeSyncReqPacket:
		case TimeSyncFollowPacket:
		case TimeSyncCompReqPacket:
		case TimeSyncCompRespPacket:
		case TimeSyncCorrReqPacket:
		case TimeSyncCorrFollowPacket:
			overhead = 5;
			//break;
		default:
			return ERR_COMM_MNG_UNKNOWN_MESSAGE;
			break;
	}
	if(len>SEND_BUFF_SIZE - overhead ){ // sizeof data overhead + send indicator
		return ERR_COMM_MNGR_TOO_BIG_DATA;
	}
	if(atomic_inc(&cm->writtingToBuffer) != 1){
		return ERR_COMM_MNG_BUF_IN_USE;
	}

	if(cm->fingerTop.start == SENDBUF_NO_DATA){
		cm->fingerTop.start=0;
		cm->fingerTop.end=MAX_SEND_BUFFERS_IND;
		cm->sendBufFinger[MAX_SEND_BUFFERS_IND].start=0;
		cm->sendBufFinger[MAX_SEND_BUFFERS_IND].end=MAX_SEND_BUF_IND;
	}else{
		if(cm->sendBufFinger[cm->fingerTop.start].start >= cm->sendBufFinger[cm->fingerTop.end ].end){
			dif = cm->sendBufFinger[cm->fingerTop.start].start - cm->sendBufFinger[cm->fingerTop.end ].end;
		}else{
			dif = SEND_BUFF_SIZE - (cm->sendBufFinger[cm->fingerTop.end ].end - cm->sendBufFinger[cm->fingerTop.start].start);
		}

		if(dif==0){
			//full buffer
			cm->writtingToBuffer = 0;
			return ERR_COMM_MNG_BUF_FULL;
		}
		//chceck free space
		if((len + overhead > dif)){
			cm->writtingToBuffer =0;
			return ERR_COMM_MNGR_NO_FREE_SPACE;
		}
		//check free fingers
		if(((cm->fingerTop.start-1) & MAX_SEND_BUFFERS_IND )== cm->fingerTop.end){
			cm->writtingToBuffer =0;
			return ERR_COMM_MNGR_NO_FREE_FINGERS;
		}
	}

	
	//get first byte, where to put data
	ind = len;
	crc=0xFFFF;
	freeSpace =cm->sendBufFinger[cm->fingerTop.end].end;
	freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	//new finger space
	top  = (cm->fingerTop.end + 1 )& MAX_SEND_BUFFERS_IND;

	cm->fingerTop.end =  top;
	cm->sendBufFinger[top].start = freeSpace;
	DEBUGPRINTF("CreatePacket: new top.end %hhu, end.start [%hhu]. top.start %hhu, start.start [%hhu]  \n",cm->fingerTop.end,freeSpace, cm->fingerTop.start,cm->sendBufFinger[cm->fingerTop.start].start);
	//fill head
	cm->sendBuf[freeSpace] = type;
	crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
	freeSpace=(freeSpace+1) & MAX_SEND_BUF_IND;
	//check if type is one of the timSync packets
	if((type&TIME_SYNC_HEADER_MARK_MASK) != TIME_SYNC_HEADER_MARK){
		// type != TimeSyncReqPacket && type != TimeSyncRespPacket && type != TimeSyncCorrPacket
		//fill length
		cm->sendBuf[freeSpace] = len + overhead;
		crc = crc_ccitt_update(crc,len + overhead);
		freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	}
	//fill DEV_ADDR
	
	
	
	cm->sendBuf[freeSpace] = srcAddr->hi;//*(((uint8_t *)&addr.devAddr) + 1);
	crc = crc_ccitt_update(crc,srcAddr->hi);
	freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	
	cm->sendBuf[freeSpace] = (uint8_t)srcAddr->lo;//*(((uint8_t *)&addr.devAddr) + 1);
	crc = crc_ccitt_update(crc,(uint8_t)srcAddr->lo);
	freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	//fill IDD
	//if(type != TimeSyncReqPacket && type != TimeSyncRespPacket && type != TimeSyncCorrPacket){
	if((type&TIME_SYNC_HEADER_MARK_MASK) != TIME_SYNC_HEADER_MARK){
		cm->sendBuf[freeSpace] = moduleIDD;//*(((uint8_t *)&addr.devAddr) + 1);
		crc = crc_ccitt_update(crc,moduleIDD);
		freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	}

	switch(type){
		case BlockDataPacket:
			cm->sendBuf[freeSpace] = block;//
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			break;
		case RequestPacket:
			//destination address
			cm->sendBuf[freeSpace] = destAddr->nodeAddr.hi;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			cm->sendBuf[freeSpace] = destAddr->nodeAddr.lo;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			cm->sendBuf[freeSpace] = destAddr->IDD;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;

			cm->sendBuf[freeSpace] = cmd;//
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			break;
		case ResponsePacket:
			//destination address
			cm->sendBuf[freeSpace] = destAddr->nodeAddr.hi;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			cm->sendBuf[freeSpace] = destAddr->nodeAddr.lo;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			cm->sendBuf[freeSpace] = destAddr->IDD;
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;

			cm->sendBuf[freeSpace] = cmd;//
			crc = crc_ccitt_update(crc,cmd);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			cm->sendBuf[freeSpace] = err;//
			crc = crc_ccitt_update(crc,cm->sendBuf[freeSpace]);
			freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
			break;
		//case DataPacket:
		//case TimeSyncReqPacket:
		//case TimeSyncRespPacket:
		//case TimeSyncCorrPacket:
		default:
			break;
	}
	//fill data
	i=0;
	while(i!=ind){
		cm->sendBuf[freeSpace]=data[i];
		crc = crc_ccitt_update(crc,data[i++]);
		freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	}
	//Add CRC at the end
	cm->sendBuf[freeSpace] = ((I16*)&crc)->hi;
	freeSpace=(freeSpace+1)&MAX_SEND_BUF_IND;
	cm->sendBuf[freeSpace] = (uint8_t)crc;
	//set fingers to buffer
	cm->sendBufFinger[top].end = freeSpace;
	//cm->sendBufFinger[(top+1)&MAX_SEND_BUFFERS_MASK].start= freeSpace;
	//cm->sendBufFinger[(top+1)&MAX_SEND_BUFFERS_MASK].end = freeSpace;
	if(cm->sendBufPacketsCnt < 0) {
		DEBUGPRINTF("Wrong sendBufPacketsCnt %hu! Cleaning!\n",cm->sendBufPacketsCnt);
		cm->sendBufPacketsCnt = 0;
	}
	atomic_inc(&cm->sendBufPacketsCnt);
	cm->writtingToBuffer =0;
	return cm->sendBufFinger[top].start;
}

INLINE int16_t CreateRespPacket(CMD_RESP_DATA* data) {
	if(data == NULL) {
		DEBUGPRINTF("CreateRespPacket Null Data!!!\n");
		return ERR_COMM_MNGR_NULL_DATA;
	}
	data->ret = CreateDataPacket(data->cm,&GetSystemAddrP()->nodeAddr,&data->destAddr, ((CMD_DATA_OUT*)data->pDataOut)->data ,((CMD_DATA_OUT*)data->pDataOut)->len , data->IDD,0, data->cmd, data->err, ResponsePacket);
	DEBUGPRINTF("CreateDataPacket returned %hd.\n",data->ret);
	return data->ret;
}

uint8_t ThrDataWritterWr(void *data,pt* Thr){
	PT_BEGIN(Thr);

	PT_WAIT_UNTIL(Thr,((CM()->state == CM_STATE_ENABLED) && CM()->sendEnabled &&  (!(CM()->sendingData)) && !(CM()->priorityWrite)  ) ); //check if is possible to send data
	SysSignal(SIG_DATA_BUF_WRITTING_DATA);
	//CM()->DWact  =( CM()->fingerTop.start + 1)& MAX_SEND_BUFFERS_MASK;;
	CM()->DWact  =( CM()->fingerTop.start);
	CM()->sendingData =1;
	CM()->InterfaceWrite(CM(),CM()->sendBuf,(CM()->sendBufFinger[CM()->DWact]).start,(CM()->sendBufFinger[CM()->DWact]).end,MAX_SEND_BUF_IND,&(CM()->sendingData) );
	if(CM()->fingerTop.start == CM()->fingerTop.end){
		CM()->fingerTop.start = SENDBUF_NO_DATA;
	}else{
		CM()->fingerTop.start = (CM()->DWact + 1)& MAX_SEND_BUFFERS_IND;
	}
	atomic_dec(&CM()->sendBufPacketsCnt);
//	sendingData =0;
	DeleteThread(Thr);
	ClearSysSignal(SIG_DATA_BUF_WRITTING_DATA);
	PT_END(Thr);
}
#ifdef _MSC_VER
 INLINE uint8_t ThrDataWritter(MODULE_DATA *data,pt* Thr){
	return	ThrDataWritterWr((COM_MNG_DATA*)data,Thr);
}
#else 
#define ThrDataWritter ThrDataWritterWr
#endif


//static uint8_t tHandleThreeBytes;
#define MAX_REPEAT_COUNT 3
uint8_t ThrLastDataWritter(void *data,pt* Thr){
	DEBUGPRINTF("[%llu] ThrLastDataWritter. state %hhu, sendEnabled %hhu, sendingData %hhu\n",(unsigned long long) GetSysTickCount(), CM()->state,CM()->sendEnabled, CM()->sendingData);
	PT_BEGIN(Thr);
	DEBUGPRINTF("ThrLastDataWritter called\n");
	PT_WAIT_UNTIL(Thr,((CM()->state == CM_STATE_ENABLED) && CM()->sendEnabled &&  !(CM()->sendingData)) ); //check if is possible to send data
	CM()->priorityWrite = 1;
	CM()->LDWact  = CM()->fingerTop.end;
	CM()->repeatCount = MAX_REPEAT_COUNT;
	CM()->sendingData =1;
	SysSignal(SIG_CMD_BUF_WRITTING_DATA);
LDW_SendDataAgain:
	DEBUGPRINTF("[%llu] ThrLastDataWritter interface write\n", (unsigned long long) GetSysTickCount());
	CM()->InterfaceWrite(CM(), CM()->sendBuf, CM()->sendBufFinger[CM()->LDWact].start,CM()->sendBufFinger[CM()->LDWact].end, MAX_SEND_BUF_IND, &(CM()->sendingData));
	if(CM()->sendingData == PORT_WRITE_ERROR) {
		// detected lost of communication interface
		//TODO report error while sending data
		CM()->parser.parserState = InterfaceError;
		//close thread
		DEBUGPRINTF("ThrLastDataWritter interface error!\n");
		goto LDW_Quit;
	}
	PT_WAIT_WHILE(Thr,CM()->sendingData);	//wait for sending data
	CM()->sendingData = 1;

	//----- media access controll -----
	//returned data from network, resend data ?

	// Wait 1 Byte for another longer packet detection
	CM()->tTimerOneByte = GetSysTickCount() + (((uint16_t)CM()->oneByteDelayUs)*1 + CM()->oneByteDelayUs/10  );
	//PT_WAIT_WHILE(Thr,(CM()->tTimerOneByte)>GetSysTickCount());

	while((CM()->tTimerOneByte)>GetSysTickCount()){};

	//On data detect data ... error report ... -> then set new timer for waiting 3B -> if there is
	//chceck if error detected (sended by Master)
	DEBUGPRINTF("ThrLastDataWritter wait for CommSendErrorReport\n");
	PT_WAIT_WHILE(Thr,CM()->CommSendErrorReport);
	if( CM()->CommSendErrorReport && CM()->repeatCount ){
		//send data again if line is free, otherwise wait for sendEnabled ...
		CM()->repeatCount--;
		CM()->CommSendErrorReport = 0;
		PT_WAIT_UNTIL(Thr,((CM()->state == CM_STATE_ENABLED) && CM()->sendEnabled && CM()->priorityWrite) ); //check if is possible to send data
		goto LDW_SendDataAgain;
	}
LDW_Quit:
	PT_WAIT_WHILE(Thr,CM()->writtingToBuffer);
	DEBUGPRINTF("[%llu] Clearing packet buff top.Start %hhu, top.end= %hhu\n",(unsigned long long)GetSysTickCount(), CM()->fingerTop.start,CM()->fingerTop.end);
	if(CM()->fingerTop.start == CM()->fingerTop.end){
		CM()->fingerTop.start = SENDBUF_NO_DATA;
	}else{
		if(CM()->LDWact == 0){
			DEBUGPRINTF("ThrLastDataWritter LDWact == 0 \n");
		}
		CM()->fingerTop.end = (CM()->LDWact - 1 )& MAX_SEND_BUFFERS_IND;
	}
	atomic_dec(&CM()->sendBufPacketsCnt);
	CM()->sendingData =0;
	CM()->priorityWrite =0;
	SysSignal(SIG_PRIORITY_WRITE_EXIT);
	DEBUGPRINTF("top.Start %hhu, top.end= %hhu\n",CM()->fingerTop.start,CM()->fingerTop.end);
	DeleteThread(Thr);
	ClearSysSignal(SIG_CMD_BUF_WRITTING_DATA);
	PT_END(Thr);
}

uint8_t  SendFirstPacket(COM_MNG_DATA *cm){
	//chceck if data are in buffer
	DEBUGPRINTF("SendFirstPacket called \n");
	if(cm->sendBufPacketsCnt<=0) {
		return ERR_COMMNMGR_NO_DATA_IN_BUFF;
	}
	if(AddThread((MODULE_DATA*)cm, ThrDataWritter)){
		return 0;
	}
	return ERR_COMMNMGR_THR_NOT_CREATED;
}

uint8_t SendLastPacket(COM_MNG_DATA *cm){
	//chceck if data are in buffer
	if(cm->sendBufPacketsCnt <= 0) {
		return ERR_COMMNMGR_NO_DATA_IN_BUFF;
	}
	if(AddThread((void*)cm, ThrLastDataWritter) == 0) {
		return ERR_COMMNMGR_THR_NOT_CREATED;
	}
	return 0;
}
 

uint8_t ThrTimeSyncWritter(void *data, pt* Thr){
	//static void (*tmp)(uint8_t, uint8_t);
	PT_BEGIN(Thr);
	PT_WAIT_UNTIL(Thr,CM()->state && CM()->sendEnabled); //check if is possible to send data
	SysSignal(SIG_TIMESYNC_WRITTING_DATA);
	// Set time to protocol head
	CM()->sendingData = 1;
	if(CM()->timeSyncBuf[0] != TimeSyncCompRespPacket){
		*((uint64_t*)&CM()->timeSyncBuf[3]) =  (uint64_t)GetSysTickCount() + TIME_SYNC_SENDING_DELAY_CORRECTION;
	}
	//compute crc
	CM()->parser.crc = 0xffff;
	for(CM()->i=0; CM()->i<(7+4);){
		CM()->parser.crc = crc_ccitt_update(CM()->parser.crc,CM()->timeSyncBuf[CM()->i++]);
	}
	*((uint16_t*)& (CM()->timeSyncBuf[7+4])) = CM()->parser.crc;
	CM()->InterfaceWrite(CM(), CM()->timeSyncBuf,0,8+4, 0xF, &(CM()->sendingData));

	CM()->priorityWrite = 0;
	SysSignal(SIG_PRIORITY_WRITE_EXIT);
	DeleteThread(Thr);
	ClearSysSignal(SIG_TIMESYNC_WRITTING_DATA);
	PT_END(Thr);
}


//fill time when first byte is send
uint8_t SendTimeSyncPacket(COM_MNG_DATA *cm){
	DEBUGPRINTF("SendTimeSyncPacket\n");
	cm->priorityWrite =1;
	AddThread((void*)cm, ThrTimeSyncWritter);
	return 0;
}
uint8_t SendTimeSync(COM_MNG_DATA *cm,TimeSyncType type, uint64_t *timeCompensation){
	
	cm->timeSyncBuf[1] = (GetTimeSyncReqAddrP())->nodeAddr.hi;
	cm->timeSyncBuf[2] = (GetTimeSyncReqAddrP())->nodeAddr.lo;
	
	switch(type){
		case TimeRequest:
			cm->timeSyncBuf[0] = TimeSyncReqPacket;
			break;
		case TimeFollow:
			cm->timeSyncBuf[0] = TimeSyncFollowPacket;
			break;
		case TimeCompensationRequest:
			cm->timeSyncBuf[1] = (GetSystemAddrP())->nodeAddr.hi;
			cm->timeSyncBuf[2] = (GetSystemAddrP())->nodeAddr.lo;
			cm->timeSyncBuf[0] = TimeSyncCompReqPacket;
			break;
		case TimeCompensationResponse:
			cm->timeSyncBuf[0] = TimeSyncCompRespPacket;
			*((uint64_t*)&cm->timeSyncBuf[3]) = *timeCompensation;
			break;
		case TimeCorrectionRequest:
			cm->timeSyncBuf[0] = TimeSyncCorrReqPacket;
			break;
		case TimeCorrectionFollow:
			cm->timeSyncBuf[0] = TimeSyncCorrFollowPacket;
			break;
		default:
			DEBUGPRINTF("SendTimeSync - not supported packet type.\n");
			return 1;
		break;
	}

	SendTimeSyncPacket(cm);
	return 0;
}

#define MAX_DATA_IN_DATA_PACKET (SEND_BUFF_SIZE -7)
#define MAX_DATA_IN_BLOCK_DATA_PACKET (SEND_BUFF_SIZE -8)

uint8_t SendDataToIf(COM_MNG_DATA *cm, uint8_t *data, uint16_t len, uint8_t IDD){

	int16_t err;
	DEBUGPRINTF("SendDataToIf called, %hhu \n", cm->interfaceID);

	if(cm->state != CM_STATE_ENABLED) {
		return 4;
	}
	
	if(cm->sendRequest && (IDD != cm->idd) ){ // je ochrana na to, ze ked chce niekto iny (ine IDD)  zasielat data, tak ho nepusti, teda ked sa vysielaju data v blokoch (ked sa nezmestia data do ramca)
		return 2;
	}
	cm->sendRequest =1; //send concrete set of packets if  
	if(len>(MAX_DATA_IN_DATA_PACKET) ){
		if( cm->l == 0){
			cm->l=len/MAX_DATA_IN_BLOCK_DATA_PACKET;
			cm->l++;
		}
		cm->idd = IDD;
		do{
			err = CreateDataPacket(cm,&GetSystemAddrP()->nodeAddr,NULL,data + cm->bytesSend ,(cm->l-1)?MAX_DATA_IN_BLOCK_DATA_PACKET:(len-cm->bytesSend) , IDD,cm->l-1,0,0, BlockDataPacket);
			if(err<0){
				return 1;
			}
			cm->l--;
			cm->bytesSend+=MAX_DATA_IN_BLOCK_DATA_PACKET;
			SendFirstPacket(cm);
		}while(cm->l);

		cm->idd = 0xFF;
		cm->bytesSend=0;
		cm->l=0;
	}else{
		err =CreateDataPacket(cm,&GetSystemAddrP()->nodeAddr,NULL,data, (uint8_t)len, IDD,0,0,0, DataPacket);
			if(err<0){
				cm->sendRequest =0;
				return 1;
			}
			err = SendFirstPacket(cm);
			if(err){
				//PrintThreads();
				return 3;
			}
	}
	cm->sendRequest =0;
	return 0;
}

INLINE CMD_RESP_DATA* AllocCmdRespData() {
	return (CMD_RESP_DATA*)malloc(sizeof(CMD_RESP_DATA));
}

uint8_t FreeCmdRespData(CMD_RESP_DATA * cmdResp) {
	if(cmdResp == NULL){
		return 1;
	}
	free(cmdResp);
	return 0;
}

/* ThrSendCmd
 * Send command, priority sending
 */

#ifndef MAX_SEND_CMD_WAIT_TIME_US
#define CREATE_PACKET_EXPIRED(timeUs) 0
#else
#define CREATE_PACKET_EXPIRED(timeUs) SimpleTimerExpired(timeUs)
#endif

uint8_t ThrSendCmd(void *data, pt* Thr){
#ifdef TEST_VIEW_CALL_THRSEND
	DEBUGPRINTF("ThrSendCmd ret [%hhd] (%p)\n",CMD_RESP()->ret, data);
#endif
	PT_BEGIN(Thr);
	PT_WAIT_WHILE(Thr, (CreateRespPacket(CMD_RESP()) <0) && !CREATE_PACKET_EXPIRED((CMD_RESP())->maxWaitToSend));
	if(CMD_RESP()->ret >=0) {
		PT_WAIT_WHILE(Thr,SendLastPacket((CMD_RESP())->cm) == ERR_COMMNMGR_THR_NOT_CREATED);
	} else {
		DEBUGPRINTF("ThrSendCmd timer expired! \n");
	}
	FreeCmdDataOut((CMD_DATA_OUT*)(CMD_RESP())->pDataOut);
	FreeCmdRespData((CMD_RESP()));
	DeleteThread(Thr);
	PT_END(Thr);
}

uint8_t SendCmdResp(COM_MNG_DATA *cm,uint8_t cmd,uint8_t moduleIDD, uint8_t err, struct CMD_DATA_OUT *pDataOut, ADDR destAddr){
	DEBUGPRINTF("SendCmdResp called\n");
	CMD_RESP_DATA*	cmdData = AllocCmdRespData();
	if(cmdData == NULL) {
		return 3;
	}
	cmdData->cmd =cmd;
	cmdData->pDataOut = pDataOut;
	cmdData->err =err;
	cmdData->IDD = moduleIDD;
	cmdData->destAddr = destAddr;
	cmdData->cm = cm;
#ifdef MAX_SEND_CMD_WAIT_TIME_US
	cmdData->maxWaitToSend = GetSysTickCount() + MAX_SEND_CMD_WAIT_TIME_US;
#endif

	//cm->cmdData=cmdData;
	if(freeThreadsCnt <= 1) {
		FreeCmdRespData(cmdData);
		return 1;
	}
	if(AddThread((void*)cmdData,ThrSendCmd) == NULL) {
		FreeCmdRespData(cmdData);
		return 2;
	}
	return 0;
}

uint8_t SendCmdReq(COM_MNG_DATA *cm,uint8_t moduleIDD, ADDR *destAddr,uint8_t cmd,uint8_t *data,uint8_t len){
	int16_t err;
	if(cm->state != CM_STATE_ENABLED) {
		return ERR_CM_INT_NOT_READY;
	}

	err = CreateDataPacket(cm,&GetSystemAddrP()->nodeAddr,destAddr,data, (uint8_t)len, moduleIDD,0,cmd,0, RequestPacket);
	if(err<0){
		return 1;
	}
	SendFirstPacket(cm);
	return 0;
}

//---------------------------------------------------------------------------


//note crc is in data array
typedef struct{
	uint8_t type;
	uint8_t len;
	uint8_t addrHi;
	uint8_t addrLo;
	uint8_t IDD;
	uint8_t data0;
} PACKET_DATA;

typedef struct{
	uint8_t type;
	uint8_t len;
	uint8_t addrHi;
	uint8_t addrLo;
	uint8_t IDD;
	uint8_t count;
	uint8_t data0;
} PACKET_BLOCKDATA;

typedef struct{
	uint8_t type;
	uint8_t len;
	uint8_t srcAddrHi;
	uint8_t srcAddrLo;
	uint8_t srcIDD;
	uint8_t dstAddrHi;
	uint8_t dstAddrLo;
	uint8_t dstIDD;
	uint8_t cmd;
	uint8_t data0;
} PACKET_REQUEST;

typedef struct{
	uint8_t type;
	uint8_t len;
	uint8_t srcAddrHi;
	uint8_t srcAddrLo;
	uint8_t srcIDD;
	uint8_t dstAddrHi;
	uint8_t dstAddrLo;
	uint8_t dstIDD;
	uint8_t cmd;
	uint8_t err;
	uint8_t data0;
} PACKET_RESPONSE;

typedef struct{
	uint8_t type;
	uint8_t addrHi;
	uint8_t addrLo;
	uint64_t time;
} PACKET_TIMESYNC;





//#define NO_DATA 0xFFFF
void InitInData(COM_MNG_DATA *cm){
	cm->parser.InData.act =0;
	cm->parser.InData.actFrame =0;
	cm->parser.crc = 0xFFFF;
}

#define GetInDataFreePlace()	cm->parser.InData.actFrame= (cm->parser.InData.actFrame + 1)&MAX_INBUF_FRAME_IND
//#define SetInDataFreePlace(index)	
#define AddByteInData(data)		cm->parser.InData.data[cm->parser.InData.actFrame][cm->parser.InData.act++] = (data)
//#define IsPossibleAddData()		
//#define FullBuff()				
#define NoDataInBuf()			cm->parser.InData.act == 0
#define DataInBuf()				cm->parser.InData.act != 0
#define SetNoDataInBuf()		cm->parser.InData.act = 0
//#define SetStartInBuf(index)	
#define GetStartInBuf()			cm->parser.InData.start[cm->parser.InData.act]
#define SetNextInBufFrame()		cm->parser.InData.actFrame = (cm->parser.InData.actFrame + 1)& MAX_INBUF_FRAME_IND
#define GetActInBufFrame()		cm->parser.InData.actFrame
//#define SetNoDataInBufForFrame(frame) 
#define GetPacketType()			(PacketType)cm->parser.InData.data[cm->parser.InData.actFrame][0]
#define GetPacketStart()		(cm->parser.InData.data[cm->parser.InData.actFrame])
#define GetPacketVarVal(type,field) *(cm->parser.InData.data[cm->parser.InData.actFrame] + OFFSETOF_8(type,field) )

void OnDataWrapper(void *cm,uint8_t data){
	OnDataReceive((COM_MNG_DATA*) cm, data);
}

//, uint8_t *data, uint8_t len

void ResendPacket(COM_MNG_DATA* cm, uint8_t frame, uint8_t len){
	//get nextinterface for sending
	//free buffer
	SetNoDataInBuf();
	//SetNoDataInBufForFrame(frame);
}

static uint8_t tstCnt =0;

void OnDataReceive(COM_MNG_DATA *cm,uint8_t data){
	ADDR tmpAddr;
	ADDR tmpSrcAddr;
	uint64_t tmpTimeDiff;

	if(!cm->fullDuplex){
		cm->sendEnabled = 0;
	}
	//DEBUGPRINTF("?");
	//PRINTF("%.2x:(%d)",data,cm->state);

	switch(cm->parser.parserState){
		case WaitForPacket:
			switch(data){
				case DataPacket:
				case BlockDataPacket:
				case RequestPacket:
				case ResponsePacket:
					cm->parser.parserState = Length;
					//set 2B timeout
					DEBUGPRINTF("[%llu] Received packet: header %hhu\n",(unsigned long long)GetSysTickCount() ,data);
					break;
				case TimeSyncReqPacket:			//client
				case TimeSyncFollowPacket:		//client
				case TimeSyncCompReqPacket:		//time server (compute time correction)
				case TimeSyncCorrReqPacket:		//client
				case TimeSyncCorrFollowPacket:	//client
					cm->timeStampArrival = GetSysTickCount();
					//compute time correction
				case TimeSyncCompRespPacket:
					cm->parser.parserState = SrcAddrHi;
					//set packet length
					cm->parser.packetLen = 9+4;
					cm->parser.dataLen = 4+4;
					cm->parser.actLen = 1;
					//set packet timeout
					cm->parser.crc = crc_ccitt_update(cm->parser.crc,data);
					//if(DataInBuf()){
					//	SetNextInBufFrame();
					//}
					/*else{
						//get next frame
					}*/
					AddByteInData(data);
					return;
					break;
				case ErrorRespondPacket:
					cm->parser.parserState = ErrorRespond;
					cm->CommSendErrorReport = 1;
					SetLinkFreeTimeot(cm,2+3); //2bytes for send and 3 for receiving packet 
					break;
				default:	//header Error, wait 2B network silence
					cm->parser.parserState=HeaderError;
					//set timeout for 2Bytes
					SetLinkFreeTimeot(cm,1); 
					return;	//do not anythink .. no count CRC ..
			}

			cm->parser.packetLen=0;
			cm->parser.dataLen =0;
			cm->parser.actLen = 0;
			if(cm->sendingData && !SimpleTimerExpired(cm->tTimerOneByte) && cm->fullDuplex ==0){ //device sended data, but another device too...
				cm->CommSendErrorReport = 1;
				cm->parser.parserState = PacketOverrunError;
				SetLinkFreeTimeot(cm,1);
			}
			//if(DataInBuf()){
			//	SetNextInBufFrame();
				//checke if there is no aviable memory .. (not implemented, need one act byte inside data structure
				//cm->parser.parserState = FullBuffer;
			//}
			break;

		case Length: //standard communication 
			if(data > MAX_PACKET_LEN){
				cm->parser.parserState = LengthError;
				SetLinkFreeTimeot(cm,1);
				break;
			}
			cm->parser.packetLen=data;
			cm->parser.parserState = SrcAddrHi;
			switch ( GetPacketType()){
				case DataPacket:
					cm->parser.dataLen=data- 7;
					break;
				case BlockDataPacket:
				case RequestPacket:
					cm->parser.dataLen=data-11;
					break;
				case ResponsePacket:
					cm->parser.dataLen=data-12;
					break;
				default:
					cm->parser.parserState = LengthError; //should not happen
					break;
			}
			SetLinkFreeTimeot(cm,cm->parser.packetLen+1);
			break;
		case SrcAddrHi:
			cm->parser.parserState = SrcAddrLo;
			break;
		case SrcAddrLo:
			//TimeSyncReqPacket || GetPacketType() == TimeSyncRespPacket || GetPacketType() == TimeSyncCorrPacket)
			if( (GetPacketType() & TIME_SYNC_HEADER_MARK_MASK ) ==  TIME_SYNC_HEADER_MARK) {
				cm->parser.parserState = ReadData;
			}else{
				cm->parser.parserState = SrcIdd;
			}
			break;
		case SrcIdd:
			if( GetPacketType()  == RequestPacket || GetPacketType()  == ResponsePacket ){
				cm->parser.parserState = DstAddrHi;
			}else{
				if( GetPacketType() == BlockDataPacket){
					cm->parser.parserState = Block;
				}else{
					cm->parser.parserState = ReadData;
				}
			}
			break;
		case DstAddrHi:
			cm->parser.parserState = DstAddrLo;
			break;
		case DstAddrLo:
			cm->parser.parserState = DstIdd;
			break;
		case DstIdd:
			cm->parser.parserState = Command;
			break;
		case Command:
			if(GetPacketType() == ResponsePacket){
				cm->parser.parserState = ReadErr;
			}else{
				cm->parser.parserState = ReadData;
			}
			break;
		case Block:
		case ReadErr:
			cm->parser.parserState = ReadData;
			break;
		case ErrorRespond:
			cm->parser.parserState = CRCHi;
			break;
		case ReadData:
			if(cm->parser.packetLen-2-1 <= cm->parser.actLen){
				cm->parser.parserState = CRCHi;
			}
			if(cm->parser.packetLen-2-1 >= cm->parser.actLen){
				break;
			}
		case CRCHi:
			cm->parser.packetCrc.hi = data;
			cm->parser.parserState = CRCLo;
			break;
		case CRCLo:
			cm->parser.packetCrc.lo = data;
			//compare crc
			if(cm->parser.crc == cm->parser.packetCrc.data){
					if(++tstCnt==23){
						tstCnt=0;
					}

				switch (GetPacketType() ){
					case DataPacket://check if address is in mask addr. table
						tmpAddr.nodeAddr.hi =((PACKET_DATA*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_DATA*)GetPacketStart())->addrLo;
						tmpAddr.IDD = ((PACKET_DATA*)GetPacketStart())->IDD;
						CallCallbackFromData( tmpAddr,&((PACKET_DATA*)GetPacketStart())->data0,cm->parser.dataLen);
							//Release data
						break;
					case BlockDataPacket:
						break;
					case RequestPacket:
							//from IDD select module and then cmd from cmd table
						tmpAddr.nodeAddr.hi = ((PACKET_REQUEST*)GetPacketStart())->dstAddrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_REQUEST*)GetPacketStart())->dstAddrLo;
						tmpAddr.IDD = ((PACKET_REQUEST*)GetPacketStart())->dstIDD;
						DEBUGPRINTF("[%llu] RequestPacket for [%hhu.%hhu.%hhu] received (System[%hhu.%hhu])\n",(unsigned long long)GetSysTickCount(), tmpAddr.nodeAddr.hi,tmpAddr.nodeAddr.lo,tmpAddr.IDD,GetSystemAddrP()->nodeAddr.hi, GetSystemAddrP()->nodeAddr.lo);
						if(tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data || (IsMaster() && tmpAddr.nodeAddr.data  == 0)){
							tmpSrcAddr.nodeAddr.hi =  ((PACKET_REQUEST*)GetPacketStart())->srcAddrHi;
							tmpSrcAddr.nodeAddr.lo = ((PACKET_REQUEST*)GetPacketStart())->srcAddrLo;
							tmpSrcAddr.IDD = ((PACKET_REQUEST*)GetPacketStart())->srcIDD;
							uint8_t ret = CallModuleCMD( cm, ((PACKET_REQUEST*)GetPacketStart())->dstIDD,((PACKET_REQUEST*)GetPacketStart())->cmd,&((PACKET_REQUEST*)GetPacketStart())->data0,cm->parser.dataLen, tmpSrcAddr);
							DEBUGPRINTF("[%llu] CallModuleCMD returned = %hhu\n",(unsigned long long)GetSysTickCount(), ret);
							(void)ret;
							//release data
							break;
						}
						DEBUGPRINTF("RequestPacket is not dedicated for this device.\n");
						break;
					case ResponsePacket:
						//chceck if response address and command 
						tmpAddr.nodeAddr.hi = ((PACKET_RESPONSE*)GetPacketStart())->dstAddrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_RESPONSE*)GetPacketStart())->dstAddrLo;
						if(tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data) {
							tmpAddr.nodeAddr.hi = ((PACKET_RESPONSE*)GetPacketStart())->srcAddrHi;
							tmpAddr.nodeAddr.lo = ((PACKET_RESPONSE*)GetPacketStart())->srcAddrLo;
							tmpAddr.IDD = ((PACKET_RESPONSE*)GetPacketStart())->srcIDD;
							CallResponseFromData(cm,tmpAddr.nodeAddr.data,tmpAddr.IDD,((PACKET_RESPONSE*)GetPacketStart())->cmd,((PACKET_RESPONSE*)GetPacketStart())->err,&((PACKET_RESPONSE*)GetPacketStart())->data0,cm->parser.dataLen);
							break;
						}
						DEBUGPRINTF("ResponsePacket is not dedicated for this device.\n");
						break;
					case TimeSyncReqPacket:
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0; //not defined in packet
						if(tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data){
#ifdef TIMER_COMPENSATE
							StartSynchronization(((PACKET_TIMESYNC*)GetPacketStart())->time,cm->timeStampArrival);
#else
							//compute difference from arrivalTime
							tmpTimeDiff= (GetSysTickCount() - cm->timeStampArrival);
							SetTime( ((PACKET_TIMESYNC*)GetPacketStart())->time + tmpTimeDiff); //or use SetPreciousTime
							//send response
							SendTimeSync(cm, TimeCompensationRequest,(uint64_t *) NULL); //when put data on network, replace data, independent write
							//release data
							break;
#endif
						}
						DEBUGPRINTF("TimeSyncReqPacket is not dedicated for this device.\n");
						break;
					case TimeSyncFollowPacket:
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0; //not defined in packet
						if( tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data){
							FinishSynchronization(((PACKET_TIMESYNC*)GetPacketStart())->time,cm->timeStampArrival);
							SendTimeSync(cm, TimeCompensationRequest, (uint64_t*) NULL); //when put data on network, replace data, independent write
							break;
						}
						DEBUGPRINTF("TimeSyncFollowPacket is not dedicated for this device.\n");
						break;
					case TimeSyncCompReqPacket: //compute time correction, if this device was timesync iniciator 
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0;
//						if( tmpAddr.devAddr.data == GetSystemAddrP()->devAddr.data){

							tmpTimeDiff= (uint16_t) ( ((PACKET_TIMESYNC*)GetPacketStart())->time - cm->timeStampArrival);
							tmpTimeDiff=tmpTimeDiff>>1;
							SendTimeSync(cm,TimeCompensationResponse,(uint64_t*)&tmpTimeDiff); //when put data on network, replace data , independent wtire 
							//release data
//						}
						break;
					case TimeSyncCompRespPacket:
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0;
						if( tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data){
							SetTime( GetSysTickCount() + ((PACKET_TIMESYNC*)GetPacketStart())->time );
							//release data
						}
						break;
					case TimeSyncCorrReqPacket:
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0; //not defined in packet
						if( tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data){
							StartCorrection(((PACKET_TIMESYNC*)GetPacketStart())->time,cm->timeStampArrival);
						}
						break;
					case TimeSyncCorrFollowPacket:
						tmpAddr.nodeAddr.hi = ((PACKET_TIMESYNC*)GetPacketStart())->addrHi;
						tmpAddr.nodeAddr.lo = ((PACKET_TIMESYNC*)GetPacketStart())->addrLo;
						tmpAddr.IDD = 0; //not defined in packet
						if( tmpAddr.nodeAddr.data == GetSystemAddrP()->nodeAddr.data){
							FinishSynchronization(((PACKET_TIMESYNC*)GetPacketStart())->time,cm->timeStampArrival);
						}
						break;
					case ErrorRespondPacket:
						//if device is sending data and error occures, enable resending
						cm->CommSendErrorReport =0;
						SetLinkFreeTimeot(cm,3);
						break;
					default:
						DEBUGPRINTF("PacketType not known!\n");
					break;
				}
				//forward data (copy it to another buffer)
				//release data
				cm->parser.parserState = WaitForPacket;
				if(!cm->fullDuplex){
					cm->sendEnabled = 1;
					SysSignal(SIG_SEND_ENABLED);
				}
				//set 2Bytes waiting on network
			}else{
				//bad crc 
				//delete data, go to parserState
				SetNoDataInBuf();
				DEBUGPRINTF("Received data has bad CRC!\n");
				//wait for master got error respond, or send error respond
			}
			
			SetLinkFreeTimeot(cm,1);
			cm->parser.crc =0xFFFF;
			AddByteInData(data);
			//set next frame
	
			//call ResendPacket
			ResendPacket(cm,GetActInBufFrame(),cm->parser.actLen);
			SetNextInBufFrame();
			if(DataInBuf()){
				cm->parser.parserState = FullBuffer;
			}
			return;
			break;
		case PacketOverrunError:
		case FullBuffer:
		case HeaderError: //error, wait 2B network silence
		case LengthError:
		case InterfaceError:
			cm->parser.crc =0xFFFF;
			SetNoDataInBuf();
			SetLinkFreeTimeot(cm,1);
			return;
	}
	cm->parser.actLen ++;
	AddByteInData(data);
	//PRINTF("[%d],",cm->parser.parserState);
	if(cm->parser.parserState != CRCLo){
		cm->parser.crc = crc_ccitt_update(cm->parser.crc,data);
	}
}



void OnLinkBusy(COM_MNG_DATA* cm){
	if(!cm->fullDuplex){
		cm->sendEnabled =0;
	}
	DiOnLinkBusy();
}
