/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __THREADMANAGER__H__
#define __THREADMANAGER__H__

#include "pt.h"
#include "../common/inttypes.h"
#include "moduleDef.h"
#define NOTHREADS 0xFF

typedef uint8_t (*thrFunc_t)(void *data, pt*);

//#define MAX_THREADS 10
typedef struct {
	pt pthr;				//2B
	volatile thrFunc_t thrFunc;		//2B
	volatile void *memoryContext;	//2B
	volatile uint8_t next;			//1B
}THR;

extern THR threads[MAX_THREADS];
extern volatile uint8_t freeThreadsCnt;

MODULE_DATA* GetCurrentSheduledModuleData();

void InitThreadManager();

void MainLoop();
INLINE void MikrixRun() { MainLoop(); }
// Add thread to first free place ( pfunction == 0 ).  
pt* AddThread(void *data, thrFunc_t thrFunc);
// first thread must be implemented always (as a system thread added by AddThread())
pt* AddNextScheduledThread(void *data, thrFunc_t thrFunc);
uint8_t MoveThreadFromData(void *dataFrom, void *dataAfter );
uint8_t DeleteThreadFromCallback(thrFunc_t thrFunc);
uint8_t DeleteThreadFromMemoryContext(void* memCont);
uint8_t DeleteThread(pt* thr);

INLINE void PrintThreadsInfo() {
	PRINTF("Threads info: %hhu/%hhu\n",freeThreadsCnt,MAX_THREADS);
}

#endif
