/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "flatStorage.h"


uint8_t InitFS(FS_t *fs, FSIO_t *fsIo, uint16_t size, void* fsConfig) {
	fs->io = fsIo;
	return fs->io->Init(fs,size, fsConfig);

}

uint8_t OpenFS(FS_t *fs, FSF_t *f, char* fileName) {
	return fs->io->Open(fs, f, fileName);
}

uint8_t ReadFSF(FSF_t *f, void* data, uint16_t len) {
	return f->io->Read(f, data, len);
}

uint8_t WriteFSF(FSF_t *f, void* data, uint16_t len) {
	return f->io->Write(f, data, len);
}


uint8_t CloseFSF(FSF_t *f) {
	return f->io->Close(f);
}


uint8_t FileExistFS(FS_t *fs, char *fileName) {
	return fs->io->FileExist(fs, fileName);
}

void InitFSFile(FSF_t *f) {
    f->userData = NULL;
}

uint8_t IsFSFileOpen(FSF_t *f) {
	return !!f->userData;
}

