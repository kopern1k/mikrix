/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "timeManager.h"
#include "sysHW.h"
#include "../common/divByConst.h"
#include "sleep.h"

#include "sysConfig.h"
#ifdef TIME_MANAGER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"

M_DELAYLIST mDelayList;

volatile uint8_t forceNotSleepMainLoop = 0;
uint8_t totalTimerCnt = 0;

volatile uint8_t timerUsed = 0;

volatile mTime_t sysUsClocks = 0;  //hodinky tikajuce 0.5us :) s takym ofajcom, ze sa prepisuju kazdych 100Hz

volatile mTime_t sysActualTimerTime = 0;

uint8_t TestWaitMsTime(mTime_t time) {
    if(GetSysTickCount()<time) {
        SetSysActualTimerTime(time);
        return 1;
    }
    if(totalTimerCnt) {
        totalTimerCnt --;
    }
    return 0;
}

void SetSysActualTimerTime(mTime_t value) {
	if(sysActualTimerTime > GetSysTickCount()) {
		if(value < sysActualTimerTime) {
			sysActualTimerTime = value;
			SysSleepWakeup();
		}
	} else {
		sysActualTimerTime = value;
		SysSleepWakeup();
		if(value <=  GetSysTickCount()) {
			forceNotSleepMainLoop = 1;
		}
	}
	DEBUGPRINTF("sysActualTimerTime = %llu, value = (%llu), diff= %llu forceNotSleepMainLoop = %hhu.\n",(unsigned long long)sysActualTimerTime, (unsigned long long)value,(unsigned long long) sysActualTimerTime - GetSysTickCount(),forceNotSleepMainLoop);
}



void InitTimeManager(){
/*
	unsigned char i;
	for(i = 0;i<MAX_DELAY_VALUES;i++){
		mDelayList.delayArray[i].nextInd=i+1;
		mDelayList.delayArray[i].time=(uint16_t)0xFFFF;
	}
	mDelayList.delayArray[MAX_DELAY_VALUES-1].nextInd = (uint8_t)0xFF; //koniec dat
	mDelayList.startInd=(uint8_t)0xFF; //prazdne pole, nemame alokovane prvky, index hovoriaci, kde sa nachadza
	mDelayList.freeStart=0; //pole zacina indexom 0, je ukazovatel na miesto v spajanom zozname!
	mDelayList.freeEnd=(uint8_t)(MAX_DELAY_VALUES-1); //miesto kde sa moze pridat novy prvok
	*/
	InitSysTimer();
	SYS_INTERRUPTABLE_SLEEP_INIT();
}


TIME t;



/* add new one-shot timer to ordered list */
uint8_t GeneralTimerAdd(TIMER_t **pCurrentTimer, TIMER_t *timer, uint64_t value) {
	TIMER_t * tmpTimer;
	TIMER_t * prev;
	if(pCurrentTimer == NULL) {
		return 1;
	}
	value = GetSysTickCount() + value;
	if(*pCurrentTimer == NULL) {
		*pCurrentTimer = timer;
		(*pCurrentTimer)->next = NULL;
		(*pCurrentTimer)->time = value;
		SetSysActualTimerTime(value);
		totalTimerCnt++;
		return 0;
	}
	tmpTimer = *pCurrentTimer;
	prev = NULL;
	//check if timer is in list
	do {
		if(tmpTimer == timer) {
			return 2;
		}
		tmpTimer = tmpTimer ->next;
	} while (tmpTimer);
	tmpTimer = *pCurrentTimer;
	do {
		if(tmpTimer->time > value) {
			timer->next = tmpTimer;
			timer->time = value;
			if(prev) {
				prev->next = timer;
			} else {
				*pCurrentTimer = timer;
				SetSysActualTimerTime(value);
			}
			totalTimerCnt++;
			return 0;
		}
		prev = tmpTimer;
		tmpTimer = tmpTimer->next;
	}while(tmpTimer);
	prev->next = timer;
	timer->time = value;
	timer->next = 0;
	totalTimerCnt++;
	return 0;
}

/* check if timer expired */
TIMER_t* GeneralTimerExpired(TIMER_t *currentTimer) {
	if(currentTimer == NULL) {
		return 0;
	}
	if(currentTimer->time <= GetSysTickCount()) {
		//forceNotSleepMainLoop = 1; // will be updatet in GeneralTimerNext
		return currentTimer;
	}
	SetSysActualTimerTime(currentTimer->time);
	return 0;
}

/* set next timer in ordered list */
uint8_t GeneralTimerNext(TIMER_t **pCurrentTimer) {
	forceNotSleepMainLoop = 1;
	if(*pCurrentTimer == NULL) {
		return 1;
	}
	if((*pCurrentTimer)->next == NULL) {
		*pCurrentTimer = NULL;
		if(totalTimerCnt) {
			totalTimerCnt--;
		}
		return 2;
	}
	*pCurrentTimer = (*pCurrentTimer)->next;
	if(*pCurrentTimer) {
		SetSysActualTimerTime((*pCurrentTimer)->time);
		if(totalTimerCnt) {
			totalTimerCnt--;
		}
	}
	return 0;
}

uint8_t GeneralTimerRemove(TIMER_t **pCurrentTimer, TIMER_t *timer) {
	TIMER_t* prev;
	TIMER_t* tmp;
	if(pCurrentTimer == NULL) {
		return 1;
	}
	if(*pCurrentTimer == NULL) {
		return 2;
	}
	//check first element
	if((*pCurrentTimer) == timer) {
		*pCurrentTimer = (*pCurrentTimer)->next;
		if(*pCurrentTimer) {
			SetSysActualTimerTime((*pCurrentTimer)->time);
		}
		if(totalTimerCnt) {
			totalTimerCnt--;
		}
		return 0;
	}
	prev = *pCurrentTimer;
	tmp = (*pCurrentTimer)->next;
	while ((tmp != timer) && tmp) {
		prev = tmp;
		tmp = tmp->next;
	}
	if(tmp == NULL) {
		return 3;
	}
	prev->next = tmp->next;
	if(totalTimerCnt) {
		totalTimerCnt--;
	}
	return 0;
}

uint8_t GeneralTimerUpdate(TIMER_t **currentTimer, TIMER_t *timer, uint64_t value) {
	if(timer == NULL) {
		return 1;
	}
	if(currentTimer == NULL) {
		return 2;
	}
	if(*currentTimer == NULL) {
		return 3;
	}

	// if only one timer is in que update it immediately
	if(((*currentTimer)->next == NULL) && *currentTimer == timer) {
		timer->time = GetSysTickCount() + value;
		SetSysActualTimerTime(timer->time);
	} else {
		//remove timer and add new
		if(GeneralTimerRemove(currentTimer,timer)) {
			return 4;
		}
		GeneralTimerAdd(currentTimer,timer,value);
	}
	return 0;
}

void OnTimMax(){
	//odratanie COMPAREMAX :)

	//timerUsed=1;
	//uint8_t tmpInd = mDelayList.startInd;

	
	sysUsClocks+=(uint16_t)DEFAULT_SYS_TIM_COMPAREMAX;
/*	
	while(tmpInd !=(uint8_t) 0xFF){
		if( mDelayList.delayArray[tmpInd].time > (uint16_t)DEFAULT_SYS_TIM_COMPAREMAX ){
			mDelayList.delayArray[tmpInd].time -= (uint16_t)DEFAULT_SYS_TIM_COMPAREMAX;
			//if(mDelayList.delayArray[tmpInd].time < (uint16_t)COMPAREMAX){ }
		}
		tmpInd = mDelayList.delayArray[tmpInd].nextInd;
	}
*/
/*	
	//ratanie hodin, zaklad 100Hz
	t.all =0;
	if(++t.hunderdths>=100){
		t.hunderdths = 0;
		if(++t.seconds>=60){
			t.seconds = 0;
			if(++t.minutes>=60){
				t.minutes = 0;
				if(++t.hours>=24){
					t.hours = 0;
				}
			}

		}
	}
*/
	
	//timerUsed=0;

}

//#include <time.h>
//time_t t;
//return uint64_t in microseconds
//#define GetSysTickCount() (sysUsClocks + GET_SYS_TIMER_VAL())

mTime_t GetSysTickCount(){

	return sysUsClocks + GET_SYS_TIMER_VAL_SCALED();
}

void SetTime(uint64_t time){
	uint16_t rem;
	divu64_10000(time, &rem);
	sysUsClocks=time - rem;
	SET_SYS_TIMER_VAL_SCALED( rem);

}

//set time with computing correction 


