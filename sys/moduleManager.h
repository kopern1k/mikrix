/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __MODULEMANAGER__H__
#define __MODULEMANAGER__H__
#include "moduleDef.h"
#include "communicationManager.h"
#include "sleep.h"
#include "sysConfig.h"

typedef struct{
	MODULE *module;
	MODULE_DATA *data;
	uint8_t initialized;
}MODULE_OBJ;

extern MODULE_OBJ moduleObj[MAX_MODULE_OBJECTS];
extern COM_MNG_DATA *lastCallModuleCM;


#define GetLastCallModuleCM() lastCallModuleCM
typedef struct{
	uint8_t mId;
	MODULE *module;
}MODULE_LIST;


INLINE MODULE* GetModule( uint8_t idd){
	return moduleObj[idd].module;
}

#define GET_MODULE_DATA(idd) (moduleObj[idd].data)

INLINE MODULE_DATA* GetModuleData( uint8_t idd){
	return moduleObj[idd].data;
}
uint8_t GetModuleIndexFromModuleID(uint32_t id);


#define NO_SEND_RESPONSE		0xFF
#define UNDEFINED_MODULE_IDD 0xFF
#define NO_MODULE_IDD UNDEFINED_MODULE_IDD

void InitModuleManager();
uint16_t GetDefaultModulesLen();
uint8_t GetDefaultModulesInfo(MODULE_INFO *info,  uint16_t *len);
int16_t AddModule(uint8_t moduleIndex, void* initParam); //moduleID is id from modules included inside your platform - default modules
int16_t AddModuleAtPosition(uint8_t moduleIndex, uint8_t position, void* initParam); ////moduleID is id from modules included inside your platform - default modules, position is final IDD of module
uint8_t DeleteModule(uint8_t IDD);
uint8_t MoveModuleScheduling(uint8_t moduleIndex, uint8_t moveAfterModuleIndex); //move module with index moduleIndex to position after moveAfterModuleIndex
uint8_t GetCreatedModuleList(MODULE_LIST* mo, uint8_t *moLen );	//list of running modules (find it in thread manager??? or get them from moduleOBJ 
uint8_t GetCreatedModulesCnt();
uint8_t CallModuleCMD(COM_MNG_DATA* mmd, uint8_t moduleID,uint8_t cmd,uint8_t *data, uint8_t len, ADDR srcAddr);


int8_t FreeCmdDataOut(CMD_DATA_OUT *pData);

#endif
