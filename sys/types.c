/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "types.h"
//enum TYPE			{ NULL_T=0,	BOOL_T,	UINT8_T,INT8_T,	UINT16_T,	INT16_T,	UINT32_T,	INT32_T,	UINT64_T,	INT64_T,	FLOAT_T,	DOUBLE_T,	STRING_T,	TIME_US_T,	TIME_T,	ID_T,	UID_T,	ADDR_T,	PERCENT_T,	PROMILE_T,	VERSION_T,	STRUCT_T,	ARRAY_T,	TIMESTAMP_DATA_T};
uint8_t TYPE_LEN[]=	{0,			1,		1,		1,		2,			2,			4,			4,			8,			8,			4,			8,			0,			4,			4,		2,		6,		3,		1,			2,			4,			0,			0,			4};

//compute length of data stream from data type array
uint8_t ComputeLen(uint8_t *typeArray, uint8_t len){
	uint8_t outLen=0;
	uint8_t i;
	if(( len==0 )|| (typeArray== NULL) ){
		return 0;
	}
	
	for( i=0;i<len;i++){
		if((TYPE)typeArray[i]==STRUCT_T || (TYPE)typeArray[i]==ARRAY_T || ( TYPE)typeArray[i]==STRING_T ){
			i++;
			outLen+=ComputeLen(&typeArray[i+1],typeArray[i]);
			i+=typeArray[i];
			continue;
		}
		outLen+= TYPE_LEN[typeArray[i]];
	}

	return outLen;
}

