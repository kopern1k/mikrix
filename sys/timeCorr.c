/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "timeCorr.h"
#include "sysTimer.h"
#include "../common/divByConst.h"
//volatile uint32_t sysUsClocks=0; //system Useconds //defined in timeManager.h
//volatile uint32_t inputTime=0; //vstupny cas
volatile int16_t microCorrection=0;
// virtual
//DEFAULT_SYS_TIM_COMPAREMAX



//timer max, which is used to compute 100Hz
//#define DEFAULT_TIMER_MAX  20000ULL


#define SET_TIMER_MAX_OFFSET(val) SET_SYS_TIM_CMP_VAL(CRYSTAL_SCALE(STANDARD_TIMER_MAX + (val) ))

#define GetTime() GetSysTickCount() //((TCNT1>>1) + sysUsClocks)


volatile char hunderts = 0;
volatile char tmpCnt=0;

volatile int16_t corrInterval=1;
volatile int32_t tmpCorrInterval=0;
volatile int8_t corrSign=1;
volatile int32_t D=1;
volatile int16_t timeDiff=0;
volatile int16_t tmpTimeDiff=0;
volatile uint16_t roundCorrection=0;

volatile int8_t constMicroCorrection;
volatile int8_t tmpConstMicroCorrection;
volatile int16_t timeShift=0;
volatile uint8_t ii=0;
//volatile uint16_t periodicErrorPeriod=-1;
//volatile uint16_t periodicError=0;

#define TIMESYNC_NOT_SYNC		  	0
#define TIMESYNC_PROCESS_SYNC 		1
#define TIMESYNC_JUST_SYNCED		2
#define TIMESYNC_SYNCED				3

volatile uint8_t synchronization= TIMESYNC_NOT_SYNC;

void OnTimerCorr(){
	uint8_t cnt;
	if(synchronization==TIMESYNC_JUST_SYNCED){
		synchronization=TIMESYNC_SYNCED;
		hunderts =0;
	}
	hunderts++;

	if(hunderts>=100){

	//LEDOFF();
		
	//	mainState.getTemperature =1;
		hunderts=0;
	}
	/*
	if(hunderts>10){
		LEDOFF();
		//LEDON();
		
	}
	*/

	SET_TIMER_MAX_OFFSET(microCorrection + timeShift);
	sysUsClocks+=(uint16_t)DEFAULT_SYS_TIM_COMPAREMAX;
	if(timeShift){
		timeShift=0;
		synchronization=TIMESYNC_JUST_SYNCED;
	}	
	
	//compute next microcorrection
	cnt=0;
	D= D + (timeDiff-corrInterval*constMicroCorrection);
	do{
		//if(D>corrInterval ){
		if(D>(corrInterval + roundCorrection)){
			D=D-corrInterval;
			cnt++;
		}else{
			break;
		}
	}while(1);
	
	microCorrection=corrSign*(constMicroCorrection + cnt);

}




typedef struct {
	uint64_t capturedTime;
	uint64_t inputTime;

}CORRECTION_TIME;


CORRECTION_TIME dataCorr[2];
uint8_t sync=0;

int16_t tmpD;
int64_t tmpTimeShift;
//int16_t swPartTimeShift;
int64_t swPartTimeShift;


void StartSynchronization(uint64_t inputTime, uint64_t capturedTime){
			sync=1;
			D=1;	

			timeDiff=0;
			constMicroCorrection=0;	
			corrSign=1;
			microCorrection=0;
			corrInterval =1;
			synchronization=TIMESYNC_NOT_SYNC;
			tmpTimeDiff=0;

			dataCorr[0].capturedTime=capturedTime;
			dataCorr[0].inputTime=inputTime;
}


void StartCorrection(uint64_t inputTime, uint64_t capturedTime){
		

			tmpTimeDiff=0;

			dataCorr[0].capturedTime=capturedTime;
			dataCorr[0].inputTime=inputTime;
}



uint16_t rem;
int8_t tmpTimeShiftSign;
void FinishCorrection(uint64_t inputTime,  uint64_t capturedTime){
			
				dataCorr[1].capturedTime=capturedTime;
				dataCorr[1].inputTime = inputTime;
				tmpCorrInterval = (uint32_t)(dataCorr[1].inputTime - dataCorr[0].inputTime);
				tmpTimeDiff = (uint32_t)(dataCorr[1].capturedTime - dataCorr[0].capturedTime - tmpCorrInterval );
				//tmpCorrInterval = tmpCorrInterval/(uint16_t)STANDARD_TIMER_MAX;
				tmpCorrInterval = divu32_10000((uint32_t)tmpCorrInterval,&rem);
				if(synchronization){ //no run first time
					//recompute to first correction interval (it is used to compute line interpolation parameters
					tmpTimeDiff = ((int32_t)(corrInterval*(int32_t)tmpTimeDiff))/((int16_t)tmpCorrInterval); //recompute
					tmpCorrInterval = corrInterval; //first correction interval
				}


				tmpTimeDiff+=timeDiff*corrSign;


				//compute first micro-correction; 
				if(tmpTimeDiff<0){
					tmpTimeDiff=-1*tmpTimeDiff;
					corrSign=-1;
				}else{
					corrSign=1;
				}				
				tmpConstMicroCorrection=0;
				tmpD=tmpTimeDiff+1;
				do{
					tmpD=tmpD-tmpCorrInterval;
					tmpConstMicroCorrection++;					
				}while(tmpD>tmpCorrInterval);
				//}while(D>tmpCorrInterval + (timeDiff>>1));
				
				if(tmpTimeDiff<tmpCorrInterval){
					tmpConstMicroCorrection=0;
					roundCorrection=0;
				}else{
					//roundCorrection=(tmpTimeDiff)>>1;
					roundCorrection=0;
				}
				//D+=tmpDiff;
				timeDiff=tmpTimeDiff;
				corrInterval=tmpCorrInterval;
				constMicroCorrection=tmpConstMicroCorrection;				
				//D=tmpD;
				//constMicroCorrection=tmpMicroCorrection;
				//microCorrection=corrSign*constMicroCorrection;
				microCorrection =0;
				if(sync==0){
					tmpTimeShift =  dataCorr[1].inputTime - dataCorr[1].capturedTime;
					if(tmpTimeShift<0){
							tmpTimeShift*=-1;
							tmpTimeShiftSign=-1;
					}else{
						tmpTimeShiftSign=1;
					}
					//swPartTimeShift=tmpTimeShift / (uint16_t)STANDARD_TIMER_MAX; //Speed???
					swPartTimeShift=tmpTimeShiftSign*divu64_10000((uint32_t)tmpTimeShift,&rem); //Speed???
					tmpTimeShift =   -tmpTimeShiftSign*rem;//(swPartTimeShift*(uint16_t)STANDARD_TIMER_MAX) - tmpTimeShift;
					
					if(STANDARD_TIMER_MAX + tmpTimeShift <= 100){		//because some processsors doesn't have enought power, to run all operation if timerCompareCal is < 100 (koli nedostatku vykonu, ktory je potrebny na vykonanie prerusenia OnTIMCOMPA)
						timeShift=STANDARD_TIMER_MAX + (int16_t)tmpTimeShift;
						swPartTimeShift--;
					}else{
						timeShift=(int16_t)tmpTimeShift;
					}
					sysUsClocks+=(int32_t)swPartTimeShift*(uint16_t)STANDARD_TIMER_MAX;
				}
				
				//tmpD=D;


				synchronization =TIMESYNC_PROCESS_SYNC;									

}
