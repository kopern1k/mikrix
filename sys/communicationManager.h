/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __COMMUNICATION_MANAGER__H__
#define __COMMUNICATION_MANAGER__H__
#include "../common/inttypes.h"
#include <stdlib.h>
#include "system.h"
#include "../platform/include/UID.h"
#include "sysSignal.h"

#define ERR_COMMNMGR_NO_DATA_IN_BUFF 1
#define ERR_COMMNMGR_THR_NOT_CREATED 2
#define ERR_CM_INT_NOT_READY 3

#define MAX_SEND_BUF_IND		(SEND_BUFF_SIZE-1)
#define MAX_SEND_BUFFERS_IND	(MAX_SEND_BUFFERS -1) //NUMER OF PACKET INSIDE SEND_BUF MUST BE POWER oF TWO - 1
#define MAX_PACKET_LEN	SEND_BUFF_SIZE		

#define PORT_WRITE_ERROR 0xFF

#define MAX_INBUF_IND (INBUF_SIZE-1)
#define MAX_INBUF_FRAME_IND (INBUF_FRAME_COUNT-1)

#define WAIT_FOR_ONE_BYTE(commSpeed)	((1000000 * 10 )/(commSpeed))
#define InitRandomTime(speed) srand(GetUID().SN)

#define ERR_COMM_MNGR_NULL_DATA -1
#define ERR_COMM_MNGR_TOO_BIG_DATA -2
#define ERR_COMM_MNG_BUF_IN_USE -3
#define ERR_COMM_MNGR_NO_FREE_SPACE -4
#define ERR_COMM_MNG_BUF_FULL -5
#define ERR_COMM_MNGR_NO_FREE_FINGERS -6
#define ERR_COMM_MNG_UNKNOWN_MESSAGE -100

typedef struct {
	uint8_t start;
	uint8_t end;
}FINGER;

struct COM_MNG_DATA;
struct CMD_DATA_OUT;

#define TIME_SYNC_HEADER_MARK 0x10
#define TIME_SYNC_HEADER_MARK_MASK 0x30
typedef enum {DataPacket = 0x40, BlockDataPacket = 0x48, RequestPacket = 0x60, ResponsePacket = 0x70,ErrorRespondPacket =0x78, TimeSyncReqPacket =0x50, TimeSyncFollowPacket=0x51, TimeSyncCompReqPacket=0x52,TimeSyncCompRespPacket=0x53, TimeSyncCorrReqPacket=0x54,TimeSyncCorrFollowPacket=0x55} PacketType;
typedef enum {WaitForPacket=0,Length, SrcAddrHi,CRCHi,CRCLo, SrcAddrLo, SrcIdd,DstAddrHi,DstAddrLo,DstIdd,Block,Command,ReadErr, ReadData,ErrorRespond, HeaderError=100, LengthError, PacketOverrunError, FullBuffer, InterfaceError} commStates_t;
typedef uint8_t packetLen_t;
typedef packetLen_t dataLen_t;
typedef uint16_t crc_t;

typedef struct {
	uint8_t cmd;
	uint8_t IDD;
	uint8_t err;
	ADDR destAddr;
	struct CMD_DATA_OUT *pDataOut;
	int16_t ret;
	struct COM_MNG_DATA *cm;
} CMD_RESP_DATA;

typedef struct {
	uint8_t data[INBUF_FRAME_COUNT][INBUF_SIZE];
	uint8_t act;
	uint8_t actFrame;
} IN_DATA;

typedef enum {
	CM_STATE_CLOSED = 0,
	CM_STATE_ENABLED,
	CM_STATE_INITIALIZING,
	CM_STATE_REINITIALIZING
} CM_STATE_T;

typedef struct {
	commStates_t parserState;
	packetLen_t packetLen;
	dataLen_t dataLen;
	dataLen_t actLen;
	crc_t crc;
	IN_DATA InData;
	I16 packetCrc;
} MParser_t;

struct COM_MNG_DATA {
	//blocking variables
	//COM_MNG_DATA* pCM;
	volatile uint8_t sendingData;
	volatile uint8_t state; //interface enabled
	uint8_t sendEnabled;
	uint8_t fullDuplex;
	volatile uint8_t writtingToBuffer;
	uint8_t priorityWrite;
	uint8_t CommSendErrorReport;

	uint64_t timerValue;

	//parse received data. 
	MParser_t parser;
	uint16_t oneByteDelayUs;
	uint32_t communicationSpeed;
	uint8_t i;
	//----------------- buffer --------------------
	uint8_t timeSyncBuf[9+4];				//buffer for timesync packet
	uint8_t sendBuf[SEND_BUFF_SIZE];	//circle buf for all packets, packets are identified by FINGER struct
	FINGER sendBufFinger[MAX_SEND_BUFFERS]; //circle buf
	FINGER fingerTop; //index to sendBufFinger first and last index
	int16_t sendBufPacketsCnt;
	uint8_t repeatCount;
	uint64_t tTimerOneByte;
	uint8_t LDWact;
	uint8_t DWact;
	
	uint8_t idd;
	uint8_t sendRequest;
	uint8_t l;
	uint8_t bytesSend;
	int8_t c;
	uint64_t timeStampArrival;

	uint8_t interfaceID;
	uint8_t selectedInterfaceTypeID;
	void (*InterfaceWrite)(struct COM_MNG_DATA *cm,uint8_t * pData, uint8_t startInd, uint8_t endInd, uint8_t mask,volatile uint8_t *sending);
	uint8_t (*InterfaceClose)(struct COM_MNG_DATA *cm);
	void* interfaceData;
};
typedef struct COM_MNG_DATA COM_MNG_DATA;


INLINE void ChangeInterfaceState(COM_MNG_DATA *cm, CM_STATE_T state) {
	cm->state = (uint8_t) state;
	SysSignal(SIG_INTERFACE_STATE_CHANGED);
}




//extern uint8_t timeSyncBuf[9];
//extern uint8_t sendBuf[SEND_BUFF_SIZE]; //circle buf for sending data
//extern volatile uint8_t sendingData;
void InitCommManager(COM_MNG_DATA *cm);
void IntitSendBuf(COM_MNG_DATA *cm);
void InitInData(COM_MNG_DATA *cm);
void InitParser(COM_MNG_DATA *cm);
void ResetParser(COM_MNG_DATA *cm);
void OnDataWrapper(void *cm,uint8_t data);
void OnDataReceive(COM_MNG_DATA *cm,uint8_t data); //follow data stream, control error

void PrintParserFingerInfo(COM_MNG_DATA *cm);
void PrintParserInfo(COM_MNG_DATA *cm);


//int8_t CreateDataPacket(ADDR *addr, uint8_t *data, uint8_t len, uint8_t IDD,uint8_t block,uint8_t cmd,uint8_t err,PacketType type);	
//uint8_t  SendFirstPacket();
#include "communicationManagerCommon.h"

#endif
