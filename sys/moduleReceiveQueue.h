#ifndef __MODULE_RECEIVE_QUEUE_H__
#define __MODULE_RECEIVE_QUEUE_H__
#include "sysConfig.h"

#if defined(MODULE_RECEIVE_QUEUE_ITEM_6)
#include "../common/queue6.h"
#define MODULE_RECEIVE_SIZE_TAG 6
typedef QUEUE_6 MRQ_T;
#elif defined(MODULE_RECEIVE_QUEUE_ITEM_16)
#include "../common/queue16.h"
#define MODULE_RECEIVE_SIZE_TAG 16
typedef QUEUE_16 MRQ_T;
#else
#ifndef MODULE_RECEIVE_QUEUE_ITEM_8
//#pragma message("Setting default MODULE_RECEIVE_QUEUE_ITEM_8")
#endif
#define MODULE_RECEIVE_SIZE_TAG 8
#include "../common/queue8.h"
#define QUEUE_LENGTH 8

typedef QUEUE_8 MRQ_T;
#endif



INLINE uint8_t MRQInitEX(MRQ_T* pQueue,uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue, dataArray, maxDataItemtSize, N, overrideLast, staticInit);
}

// initialize queue to dynamically allocated memory
INLINE uint8_t MRQInit(MRQ_T* pQueue,uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue,(uint8_t*)malloc(sizeof(uint8_t)*N*maxDataItemtSize),maxDataItemtSize,N,overrideLast,0);
}

INLINE uint8_t MRQFree(MRQ_T* pQueue) {
	return QUEUE_FUNC_NAME(FreeQueue)(pQueue);
}

INLINE uint8_t* MRQGetItem(MRQ_T* pQueue) {
        return QUEUE_FUNC_NAME(GetFirstQueueData)(pQueue);
}

INLINE uint8_t MRQRemoveFirstItem(MRQ_T* pQueue) {
        return QUEUE_FUNC_NAME(RemoveFirstQueueData)(pQueue);
}

INLINE uint8_t MRQWiteItem(MRQ_T* pQueue,uint8_t *data, uint8_t dataLen) {
	return QUEUE_FUNC_NAME(WriteDataToQueue)(pQueue, data, dataLen);
}

INLINE uint8_t* MRQGetNextFreeItemP(MRQ_T* pQueue,uint8_t* err) {
	return QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(pQueue, err);
}

INLINE uint8_t MRQAllocNextFreeItem(MRQ_T* pQueue) {
	return QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(pQueue);
}

INLINE uint8_t MRQSerialize(MRQ_T* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes) { // 0 - OK
	return QUEUE_FUNC_NAME(SerializeQueue)(pQueue, data, maxDataSize, writtenBytes);
}

INLINE uint8_t MRQDeserialize(MRQ_T* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size) { // 0 - OK
	return QUEUE_FUNC_NAME(DeserializeQueue)(pQueue, data, size);
}

INLINE DOUBLE_QUEUE_LENGTH_TYPE MRQGetSerializedSize(MRQ_T* pQueue) {
	return QUEUE_FUNC_NAME(GetSerializedQueueSize)(pQueue);
}

INLINE QUEUE_LENGTH_TYPE MRQGetItemsCnt(MRQ_T* pQueue) {
	return QUEUE_FUNC_NAME(GetItemsCntInQueue)(pQueue);
}

#endif
