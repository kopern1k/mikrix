/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "responseManager.h"
#include "threadManager.h"

#include "sysConfig.h"
#ifdef RESPONSE_MANAGER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"

ResponseManagerT ResponseManager[MAX_RESPONSES];
#define RM_NO_DATA 0xFF
static uint8_t startInd=RM_NO_DATA; 
static uint8_t endInd=RM_NO_DATA;

void InitResponseManager(){
	uint8_t i;
	for(i=0;i<MAX_RESPONSES;i++){
		ResponseManager[i].callback = NULL; //identifies free place
		ResponseManager[i].next =RM_NO_DATA;
		ResponseManager[i].onResponseExpired = NULL;

	}
}

uint8_t ResponseWaitThr(void *data, pt* Thr) {
	PT_BEGIN(Thr);
	PT_WAIT_WHILE(Thr,TestWaitMsTime(((ResponseManagerT*)data)->expiredTime));
	if(((ResponseManagerT*)data)->onResponseExpired && ((ResponseManagerT*)data)->callback) {
		((ResponseManagerT*)data)->onResponseExpired((ResponseManagerT*)data);
	}
	DEBUGPRINTF("ResponseWaitThr removing!\n");
	DeleteResponseGen((ResponseManagerT*)data);
	DeleteThread(Thr);
	PT_END(Thr);
}

int8_t AddResponseAddrCallback(uint16_t addr, uint8_t idd, uint8_t cmd, ResponseCallbackFunc_t callback, uint16_t timeoutMs, uint8_t allowMultipleResponses){
	//find free place
	uint8_t i;
	for(i =0;i<MAX_RESPONSES; i++){
		if(ResponseManager[i].callback == 0){
			ResponseManager[i].addr.nodeAddr.data =addr;
			ResponseManager[i].addr.IDD =idd;
			if(callback == NULL) {
				callback = NO_CALL_CALLBACK;
			}
			ResponseManager[i].callback =callback;
			ResponseManager[i].type = allowMultipleResponses ? RMMultipleCallbacks:RMOneShotCallback;
			ResponseManager[i].cmd =cmd;
			ResponseManager[i].haveData = 0;
			ResponseManager[i].expiredTime =  timeoutMs ? timeoutMs*1000 + GetSysTickCount() : 0;
			ResponseManager[i].onResponseExpired = NULL;
			ResponseManager[i].waitThr = NULL;
			if(ResponseManager[i].expiredTime) {
				ResponseManager[i].waitThr = AddThread((void*) &ResponseManager[i],ResponseWaitThr);
			}

			if(endInd!=RM_NO_DATA){
				ResponseManager[endInd].next = i;
			}
			endInd=i;
			ResponseManager[endInd].next = RM_NO_DATA;
			if( startInd == RM_NO_DATA){
				startInd = i;
			}
			//Add general thread for watch all callbacks timeouts
			DEBUGPRINTF("response added %hhu\n",i);
			return i;
		}
	}
	return -1; // no free memory
}
// if OK return index of added response  
// else return error <0
int8_t AddResponseAddrWaitData( ResponseManagerT *reqManager){
	uint8_t i;
	for(i =0;i<MAX_RESPONSES; i++){
		if(ResponseManager[i].callback == 0){
			ResponseManager[i] = *reqManager;
			ResponseManager[i].next =RM_NO_DATA;
			ResponseManager[i].type = RMData;
			ResponseManager[i].haveData = 0;
			ResponseManager[i].callback=NO_CALL_CALLBACK;
			ResponseManager[i].onResponseExpired = NULL;
			ResponseManager[i].expiredTime = 0;
			ResponseManager[i].waitThr = 0;
			if(endInd!=RM_NO_DATA){
				ResponseManager[endInd].next = i;
			}
			endInd=i;
			ResponseManager[endInd].next = RM_NO_DATA;
			endInd=i;
			if( startInd == RM_NO_DATA){
				startInd = i;
			}
			DEBUGPRINTF("response added %hhu\n",i);
			return i;
		}
	}
	return -1;
}


void RemoveRespItem(uint8_t i, uint8_t prevInd) {
	if( i == endInd){
		endInd=prevInd;
	}
	if( i == startInd){
		startInd = ResponseManager[i].next;
	}
	if(prevInd!=RM_NO_DATA){
		ResponseManager[prevInd].next = ResponseManager[i].next;
	}
	ResponseManager[i].callback = NULL;
	DeleteThread(ResponseManager[i].waitThr);
	ResponseManager[i].waitThr = 0;
	DEBUGPRINTF("response removed %hhu\n",i);
}
/*
uint8_t DeleteResponse(uint8_t ind){
	uint8_t i;
	uint8_t prevInd=RM_NO_DATA;
	if(startInd == RM_NO_DATA){
		return 1;
	}
	i=startInd;
	do{
		if(i==ind ){
			RemoveRespItem(i,prevInd);
			return 0;
		}
		prevInd = i;
		i=ResponseManager[i].next;
	}while(i!= RM_NO_DATA);
	return 2;
}
*/

uint8_t DeleteResponseGen(ResponseManagerT* resp) {
	uint8_t i;
	uint8_t prevInd=RM_NO_DATA;
	if(startInd == RM_NO_DATA){
		return 1;
	}
	if(resp == NULL) {
		return 2;
	}
	i=startInd;
	do{
		if(&ResponseManager[i] == resp){
			RemoveRespItem(i,prevInd);
			return 0;
		}
		prevInd = i;
		i=ResponseManager[i].next;
	}while(i!= RM_NO_DATA);
	return 3;
}

uint8_t DeleteResponse(uint8_t ind) {
	return DeleteResponseGen(&ResponseManager[ind]);
}


int8_t CallResponseFromData(COM_MNG_DATA *cm, uint16_t addr, uint8_t idd, uint8_t cmd, uint8_t err, uint8_t *data, uint8_t len){
	uint8_t i;
	uint8_t prevInd=RM_NO_DATA;
	if(startInd == RM_NO_DATA){
		return -1;
	}
	i=startInd;
	DEBUGPRINTF("CallResponseFromData!\n");
	do{
		if(ResponseManager[i].addr.nodeAddr.data == addr && ResponseManager[i].addr.IDD == idd && ResponseManager[i].cmd == cmd ){
			/*
			if( i == endInd){
				endInd=prevInd;
			}
			if( i == startInd){
				startInd = ResponseManager[i].next;
			}
			*/
			if(prevInd!=RM_NO_DATA){
				ResponseManager[prevInd].next = ResponseManager[i].next;
			}
			if( ResponseManager[i].type == RMData){
				ResponseManager[i].Data = data;
				ResponseManager[i].len = len;
				ResponseManager[i].errCode =err;
				ResponseManager[i].haveData =1;
			}else{ //call response callback func
				//uint8_t (*ResponseCallbackFunc)(uint8_t* data, uint8_t len, uint8_t errCode)
				ResponseManager[i].called =1;
				ResponseCallbackFunc_t tmpCallback = ResponseManager[i].callback;
				if(tmpCallback != NO_CALL_CALLBACK &&tmpCallback != NULL) {
					ResponseManager[i].callback(cm,data,len, err);
				}
				if(ResponseManager[i].type  == RMOneShotCallback)
					RemoveRespItem(i,prevInd);
				else {
				//- Do not remove callback! There can be multiple responses from different devices, which need to be handled. Use timeout for removing respomse handle
				}
				
			}
			DEBUGPRINTF("response called %hhu\n",i);
			return 0;
		}
		prevInd = i;
		i=ResponseManager[i].next;
	}while(i!= RM_NO_DATA);
	return -2;
}
