/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "threadManager.h"
#include "sysHW.h"
#include "sysConfig.h"
#include "sysSignal.h"
#include "atomic.h"

volatile static int thrMngrLock = 0;

#ifdef THREAD_MANAGER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"

#ifdef SYS_MAINLOOP_DEBUG
  #define DEBUG_PRINTF(...) printf(__VA_ARGS__);fflush(stdout)
#else
  #define DEBUG_PRINTF(...)
#endif
#ifdef SYS_MAINLOOP_DEBUG_INTENSIVE
  #define DEBUG_PRINTF_INTENSIVE(...) printf(__VA_ARGS__);fflush(stdout)
#else
  #define DEBUG_PRINTF_INTENSIVE(...)
#endif

#define ThreadManagerCheckLock() \
	while (atomic_inc(&thrMngrLock) != 1) {\
		Sleep(1);\
	}

#define ThreadManagerClearLock() thrMngrLock = 0

#ifndef DEFAULT_SYS_WAKEUP_TIME_US
#define DEFAULT_SYS_WAKEUP_TIME_US 0
#endif

THR threads[MAX_THREADS];	//5xn = 50B
volatile uint8_t lastThrInd=NOTHREADS; // nothink add in threads stack
static uint8_t currThr =-1;
volatile uint8_t freeThreadsCnt = 0;

void InitThreadManager(){
	unsigned char i;
	for( i=0;i<MAX_THREADS;i++){
		threads[i].pthr.lc=0;
		threads[i].thrFunc= NULL;
		threads[i].next=0xFF;
		threads[i].memoryContext =0;
	}
	currThr = 0;
	freeThreadsCnt = MAX_THREADS;
	ThreadManagerClearLock();
}

// Add thread to first free place ( pfunction == 0 ).  
pt* AddThread(void *data, thrFunc_t thrFunc){
	unsigned char i;

	DEBUGPRINTF("[%llu] AddThread called\n",(unsigned long long) GetSysTickCount());
	if( thrFunc ==0) return 0;
	ThreadManagerCheckLock();
	for(i=0;i<MAX_THREADS;i++) {
		if(threads[i].thrFunc == 0) {
				threads[i].next=NOTHREADS;
				//update data to thread
				threads[i].thrFunc=thrFunc;
				threads[i].memoryContext=data;
				PT_INIT(&(threads[i].pthr));

				forceNotSleepMainLoop = 1;
				freeThreadsCnt--;

				//take action to schedule thread
				if(lastThrInd!=NOTHREADS) {
					threads[lastThrInd].next=i;
				}
				MemSync();
				lastThrInd=i;
				ThreadManagerClearLock();
				SysSleepWakeup();
				DEBUGPRINTF("[%llu] AddThread == %hhu\n", (unsigned long long) GetSysTickCount(), i);
				return &(threads[i].pthr);
		}
	}
	ThreadManagerClearLock();
	DEBUGPRINTF("AddThread returned 0\n");
	return 0;
};
// insert thread between actual and next thread, add position at the end of threads buffer
pt* AddNextScheduledThread(void *data, thrFunc_t thrFunc){
	unsigned char i;
	DEBUGPRINTF("AddNextScheduledThread called\n");
	if( thrFunc ==0) return 0;
	for(i=0;i<MAX_THREADS;i++){
		if(	threads[i].thrFunc==0) { 
			//add thread
			threads[i].next=threads[currThr].next;
			threads[i].thrFunc=thrFunc;
			threads[i].memoryContext=data;
			PT_INIT(&(threads[i].pthr));

			forceNotSleepMainLoop = 1;
			freeThreadsCnt--;

			MemSync();
			threads[currThr].next = i;
			if(currThr == lastThrInd){
				lastThrInd = i;
			}
			MemSync();
			ThreadManagerClearLock();
			SysSleepWakeup();
			return &(threads[i].pthr);
		}
	}
	ThreadManagerClearLock();
	return 0;
};

uint8_t MoveThreadFromData(void *dataFrom, void *dataAfter ){
	DEBUGPRINTF("MoveThreadFromData called\n");
	uint8_t tmpi;
	uint8_t i=0;
	uint8_t from=0xFF,from_b=0xFF,to=0xFF;
	if (dataFrom == NULL) {
		return 1;
	}
	if(lastThrInd == NOTHREADS){
		return 2;
	};
	ThreadManagerCheckLock();
	while(threads[i].next !=0xFF){ //zero thread is system thread and should not be moved
		tmpi=i;
		i=threads[i].next;
		if(threads[i].memoryContext == dataFrom){
			from =i;
			from_b =tmpi;
		}
		if(threads[i].memoryContext == dataAfter){
			to=i;
		}
		if(from !=0xFF && to !=0xFF){
			break;
		}
	}
	if((from ==0xFF && to ==0xFF) || from_b==0xFF){
		ThreadManagerClearLock();
		return 3;
	}

	//move indexes
	threads[from_b].next = threads[from].next;
	threads[from].next = threads[to].next;
	threads[to].next = from;
	if( threads[from_b].next == NOTHREADS){
		lastThrInd = from_b;
	}
	if( threads[from].next == NOTHREADS){
		lastThrInd = from;
	}
	ThreadManagerClearLock();
	return 0;
}

/*
// first thread must be implemented always
uint8_t DeleteThreadFromCallback(uint8_t (*thrFunc)(MODULE_DATA *data, pt*)){
	unsigned char i = 0;
	if(threads[0].next == 0xFF) return 1; //only one - First thread, which isn't deletable
	while(threads[i].next!=0xFF){
		if(threads[threads[i].next].thrFunc == thrFunc){
			threads[threads[i].next].thrFunc =0;
			threads[threads[i].next].pthr.lc=0;
			threads[threads[i].next].memoryContext =0; //was blocked??, why???
			if(threads[threads[i].next].next == 0xFF) {
				lastThrInd=i;
			}
			threads[i].next=threads[threads[i].next].next;
			
			return 0;
		}else{
			i=threads[i].next;
		}
	}
	return 2;
}
*/
uint8_t DeleteThreadFromMemoryContext(void* memCont){
	DEBUGPRINTF("DeleteThreadFromMemoryContext called\n");
	uint8_t tmpInd;
	unsigned char i = 0;
	ThreadManagerCheckLock();
	if(threads[0].next == 0xFF) {
		ThreadManagerClearLock();
		DEBUGPRINTF("DeleteThreadFromMemoryContext return 1\n");
		return 1; //only one - First thread, which isn't deletable
	}
	//find thread
	do {
		tmpInd = i;
		i = threads[i].next;
		if(i == NOTHREADS) {
			ThreadManagerClearLock();
			DEBUGPRINTF("DeleteThreadFromMemoryContext return 2 threads[%hhu]->next==NOTHREADS\n",i);
			return 2;
		}
	}while(threads[i].memoryContext != memCont);
	//delete thread i
	threads[tmpInd].next = threads[i].next;
	if(lastThrInd == i){
		lastThrInd = tmpInd;
	}
	MemSync();
	threads[i].thrFunc = NULL;
	threads[i].pthr.lc = 0;
	threads[i].memoryContext =0;
	freeThreadsCnt++;
	//MemSync();
	ThreadManagerClearLock();
	DEBUGPRINTF("DeleteThreadFromMemoryContext: removed thread %hhu. Next index %hhu.\n",i,threads[tmpInd].next);
	return 0;
}


// first thread must be implemented always
uint8_t DeleteThread( pt* thr){
	DEBUGPRINTF("DeleteThread called\n");
	uint8_t tmpInd;
	unsigned char i = 0;
	ThreadManagerCheckLock();
	if(threads[0].next == 0xFF) {
		ThreadManagerClearLock();
		DEBUGPRINTF("DeleteThread return 1\n");
		return 1; //only one - First thread, which isn't deletable
	}
	//find thread
	do {
		tmpInd = i;
		i = threads[i].next;
		if(i == NOTHREADS) {
			ThreadManagerClearLock();
			DEBUGPRINTF("DeleteThread return 2 threads[%hhu]->next==NOTHREADS\n",i);
			return 2;
		}
	}while(&threads[i].pthr != thr);
	//delete thread i

	threads[tmpInd].next = threads[i].next;
	if(lastThrInd == i){
		lastThrInd = tmpInd;
	}
	MemSync();
	threads[i].thrFunc = NULL;
	threads[i].pthr.lc = 0;
	threads[i].memoryContext =0;
	freeThreadsCnt++;
	//MemSync();
	ThreadManagerClearLock();
	DEBUGPRINTF("DeleteThread: removed thread %hhu. Next index %hhu.\n",i,threads[tmpInd].next);
	return 0;
}

MODULE_DATA* GetCurrentSheduledModuleData() {
	if(currThr == NOTHREADS) {
		return NULL;
	}
	return (MODULE_DATA*)threads[currThr].memoryContext;
}


//uint8_t thrRet;
void MainLoop(){
	//check not initialized thread manager
	uint8_t thrEnded = FALSE;
	int32_t timeDiff;
	forceNotSleepMainLoop = 0;
	if(currThr == (uint8_t)-1) {
		return;
	}
	//check not initialized system -  no threads to shedule
	if(threads[0].thrFunc == NULL) {
		return;
	}
	
	while(1) {
		//thrRet=
		if(threads[currThr].thrFunc((void*)threads[currThr].memoryContext,&(threads[currThr].pthr)) == PT_ENDED) {
			thrEnded = TRUE;
		}
		if(threads[currThr].next!=0xFF){
			currThr=threads[currThr].next;
		}else{
			currThr=0;
			//check when to sleep
			//chceck I am a module
			//SwitchToThread();
#ifdef MIKRIX_OPEN_LOOP
			return;
#endif
            //sleep defined time
            //max time
			//SLEEP(1);
			//continue;

			// check system signals
			if(IsSignalPending()) {
				forceNotSleepMainLoop = 1;
				CleanInfoSysSignals();
			}

			DEBUG_PRINTF_INTENSIVE("Current main loop params: thrEnded %hhu,sysTotalModuleMessagesCnt %hu, forceNotSleepMainLoop %hhu\n",thrEnded,sysTotalModuleMessagesCnt,forceNotSleepMainLoop);
			if(thrEnded == FALSE && sysTotalModuleMessagesCnt == 0 && forceNotSleepMainLoop == 0) {
				timeDiff = sysActualTimerTime - GetSysTickCount();
				DEBUG_PRINTF("going to sleep: %d (%llus)\n",timeDiff,(unsigned long long) GetSysTickCount());
				if(timeDiff > 0) {
					SYS_INTERRUPTABLE_SLEEP(timeDiff);
				} else if(totalTimerCnt == 0) {
					SYS_INTERRUPTABLE_SLEEP(DEFAULT_SYS_WAKEUP_TIME_US);
				}
			}
			thrEnded = FALSE;
			forceNotSleepMainLoop = 0;
		}
	
	}
}
