/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __REQUEST_MANAGER__H
#define __REQUEST_MANAGER__H


#include "system.h"
#include "communicationManager.h"
#include "timeManager.h"
#include "pt.h"


typedef enum {RMOneShotCallback=0, RMMultipleCallbacks, RMData} RManagerType;
typedef  uint8_t (*ResponseCallbackFunc_t)(COM_MNG_DATA *cm,uint8_t* data, uint8_t len, uint8_t errCode);

#define NO_CALL_CALLBACK (ResponseCallbackFunc_t)0xFFFFFFFF

typedef struct ResponseManagerT ResponseManagerT;

typedef  uint8_t (*OnResponseExpired_t)(ResponseManagerT* responseManagerData);

struct ResponseManagerT {
	ADDR addr;
	uint8_t cmd;
	RManagerType type;
	uint8_t errCode;
	union{
		uint8_t* Data;
		volatile ResponseCallbackFunc_t callback;
	};
	uint8_t len;
	union{
		uint8_t haveData;
		uint8_t called;
	};
	uint8_t next;
	mTime_t expiredTime;
	pt* waitThr;
	OnResponseExpired_t onResponseExpired;
};

extern ResponseManagerT ResponseManager[MAX_RESPONSES];
#define IsCommandReplied( RespManArrayInd) ResponseManager[RespManArrayInd].haveData != 0
#define FreeRequest( RespManArrayInd) ResponseManager[RespManArrayInd].Data = 0

#define IsResponseTimeoutElapsed(ind) (ResponseManager[ind].expiredTime<GetSysTickCount()?1:0)
// 0- not called, 1 called
#define WasResponseCalled(ind)  ResponseManager[(ind)].called
#define WaitForResponse(Thr,ind) PT_WAIT_WHILE(Thr, (ResponseManager[(ind)].called == 0) && (ResponseManager[(ind)].expiredTime>GetSysTickCount() ))
void InitResponseManager();
//will store UID with assigned ADDRESS. Default address is 0.0
int8_t AddResponseAddrCallback(uint16_t addr, uint8_t idd, uint8_t cmd, ResponseCallbackFunc_t callback, uint16_t timeoutMs, uint8_t allowMultipleResponses);
int8_t AddResponseAddrWaitData( ResponseManagerT *respManager);
uint8_t DeleteResponse(uint8_t ind);
uint8_t DeleteResponseGen(ResponseManagerT* resp);
//RequestManagerT* CheckRequest(ADDR *addr);
int8_t CallResponseFromData(COM_MNG_DATA *cm, uint16_t addr, uint8_t idd, uint8_t cmd, uint8_t err, uint8_t *data, uint8_t len);

void OnReceivedUnassignedUID( UID uid);



#endif
