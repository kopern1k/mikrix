/*
 * Copyright (c) 2015, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

extern uint8_t QUEUE_FUNC_NAME(InitQueueEX)(QUEUE_NAME()* pQueue, uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit);
extern uint8_t QUEUE_FUNC_NAME(FreeQueue)(QUEUE_NAME()* pQueue);
extern DOUBLE_QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetSerializedQueueSize)(QUEUE_NAME()* pQueue);
extern QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetItemsCntInQueue)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetFirstQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetLastQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetPrenultimateQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemoveFirstQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemoveLastQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemovePrenultimateQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(WriteDataToQueue)(QUEUE_NAME()* pQueue,uint8_t *data, uint8_t dataLen);
extern uint8_t* QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(QUEUE_NAME()* pQueue,uint8_t* err);
extern uint8_t QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(SerializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes); // 0 - OK
extern uint8_t QUEUE_FUNC_NAME(DeserializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size);

