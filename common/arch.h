#ifndef __ARCH__H__
#define __ARCH__H__


#if defined __arm__
#define ARCHITECTURE "ARM"
#elif  __aarch64__
#define ARCHITECTURE "ARM64"
#elif defined __i386__
#define ARCHITECTURE "x86"
#elif defined __amd64__
#define ARCHITECTURE "AMD64"
#elif defined __avr__
#define ARCHITECTURE "AVR"
#else
#define ARCHITECTURE "unknown arch"
#endif

#endif