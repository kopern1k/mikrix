/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "oneTypeAlloc.h"
#include "stdio.h"
#include "atomic.h"

//#define USE_ATOMIC_ALLOC

//#define DEBUGPRINTF(...) printf(__VA_ARGS__)

void InitOneTypeMemItems(memItem_t* memItemsBuf, int memItemSize, int16_t memBuffItemsCnt) {
	uint8_t* pData = (uint8_t*)memItemsBuf;
	for(; memBuffItemsCnt; memBuffItemsCnt--) {
		((memItem_t*)(pData))->isAllocated = 0;
		pData += memItemSize;
	}
}


void* GetOneTypeMemItem(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt) {

	for(; memBuffItemsCnt; memBuffItemsCnt--) {
		if(memItemsBuff->isAllocated == 0) {
#ifndef USE_ATOMIC_ALLOC
			memItemsBuff->isAllocated = 1;
#else
			if(atomic_inc(&memItemsBuff->isAllocated) != 1) continue;
#endif
			return &memItemsBuff->data;
		}
		memItemsBuff += memItemSize;
	}
	return NULL;
}

uint8_t FreeOneTypeMemItem(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt, void* pDataToFree) {
	for(; memBuffItemsCnt; memBuffItemsCnt--) {
		if(memItemsBuff->isAllocated && (void*)&memItemsBuff->data == pDataToFree) {
			memItemsBuff->isAllocated = 0;
			return 0;
		}
		memItemsBuff += memItemSize;
	}
	return 1;
}

#ifdef TEST
void PrintOneTypeMemInfo(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt) {
	int i = 0;
	for(; memBuffItemsCnt; memBuffItemsCnt--) {
		printf("%.2hhu: %hhu, %p\n",++i, memItemsBuff->isAllocated, &memItemsBuff->data);
		memItemsBuff += memItemSize;
	}

}
#endif



/* ------ EXAMPLE -----

#include <stdio.h>
#define TEST
#include "oneTypeAlloc.h"

typedef struct {
	uint8_t data;
} MY_ITEM;

MEM_ITEM(DATA_MEM_ITEM_t,
		 MY_ITEM data;
);

#define MAX_DATA_ITEMS 2

DATA_MEM_ITEM_t cmdDataOutBuf[MAX_DATA_ITEMS];

void MemDataInit() {
	InitOneTypeMemItems((memItem_t*) cmdDataOutBuf,sizeof(DATA_MEM_ITEM_t),MAX_DATA_ITEMS);
}

MY_ITEM *AllocData() {
	return (MY_ITEM*) GetOneTypeMemItem((memItem_t*)cmdDataOutBuf,sizeof(DATA_MEM_ITEM_t),MAX_DATA_ITEMS);
}

int8_t FreeData(MY_ITEM *pData) {
	if(pData == NULL) {
		return 1;
	}
	if(pData->data) {
		printf("data are set!\n");
	}
	return FreeOneTypeMemItem((memItem_t*) cmdDataOutBuf,sizeof(DATA_MEM_ITEM_t),MAX_DATA_ITEMS,(void*)pData);
}

void PrintCmdDataOut() {
	PrintMemInfo((memItem_t*) cmdDataOutBuf,sizeof(DATA_MEM_ITEM_t),MAX_DATA_ITEMS);
}

int main(int argc, char *argv[])
{

	MY_ITEM* pData0,*pData1,*pData2 ;

	MemDataInit();
	PrintCmdDataOut();
	printf("--- alloc pdata0 ---\n");
	if((pData0 = AllocData()) == NULL) printf("Not Allocated!\n");
	pData0->data = 11;
	PrintMemInfo();

	printf("--- alloc pdata1 ---\n");
	if((pData1 = AllocData()) == NULL) printf("Not Allocated!\n");
	PrintMemInfo();

	printf("--- alloc pdata2 ---\n");
	if((pData2 = AllocData()) == NULL) printf("Not Allocated!\n");
	PrintMemInfo();

	printf("--- free pdata0---\n");
	FreeData(pData0);
	PrintMemInfo();

	printf("--- free pdata2---\n");
	FreeData(pData2);
	PrintMemInfo();

	printf("--- alloc pdata0 ---\n");
	pData0 = AllocData();
	pData0->data = 11;
	PrintMemInfo();


	printf("Hello World!\n");
	return 0;
}

*/
