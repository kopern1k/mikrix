#ifndef __QUEUE_16_H__
#define __QUEUE_16_H__
#ifdef __cplusplus
extern "C" {
#endif

#ifdef USE_QUEUE_SIZE_6
#undef USE_QUEUE_SIZE_6
#endif
#ifdef USE_QUEUE_SIZE_8
#undef USE_QUEUE_SIZE_8
#endif

#ifdef QUEUE_LENGTH
#undef QUEUE_LENGTH
#undef QUEUE_LENGTH_TYPE
#undef DOUBLE_QUEUE_LENGTH_TYPE
#undef MAX_QUEUE_DEPTH
#endif

#define QUEUE_LENGTH 16
#define QUEUE_LENGTH_TYPE uint16_t
#define DOUBLE_QUEUE_LENGTH_TYPE uint32_t
#define MAX_QUEUE_DEPTH 65534
#define USE_QUEUE_SIZE_16

#include "queueTemplateConst.h"

// for better code completition in developer apps, instead of includeinf "queueTemplate.h", expansion of macros is used

typedef struct {
	uint16_t firstDataIndex;
	uint16_t writeIndex;
	uint16_t N;
	uint8_t overrideLast:1;
	uint8_t staticInit:1;
	uint8_t maxDataItemSize;
	volatile uint8_t op;
	uint8_t *data;
} QUEUE_16;

typedef struct {
	uint32_t dataLen;
	uint8_t data;
} DATA_QUEUE_Serialized_16_t;

#include "queueFunctionPrototypes.h"

INLINE uint8_t InitQueueEX16(QUEUE_NAME()* pQueue,uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue, dataArray, maxDataItemtSize, N, overrideLast, staticInit);
}

// initialize queue to dynamically allocated memory
INLINE uint8_t InitQueue16(QUEUE_NAME()* pQueue,uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue,(uint8_t*)malloc(sizeof(uint8_t)*N*maxDataItemtSize),maxDataItemtSize,N,overrideLast,0);
}

INLINE uint8_t FreeQueue16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(FreeQueue)(pQueue);
}

INLINE uint8_t* GetFirstQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetFirstQueueData)(pQueue);
}

INLINE uint8_t* GetLastQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetLastQueueData)(pQueue);
}

INLINE uint8_t* GetPrenultimateQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetPrenultimateQueueData)(pQueue);
}

INLINE uint8_t RemoveFirstQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemoveFirstQueueData)(pQueue);
}

INLINE uint8_t RemoveLastQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemoveLastQueueData)(pQueue);
}

INLINE uint8_t RemovePrenultimateQueueData16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemovePrenultimateQueueData)(pQueue);
}

INLINE uint8_t WriteDataToQueue16(QUEUE_NAME()* pQueue,uint8_t *data, uint8_t dataLen) {
	return QUEUE_FUNC_NAME(WriteDataToQueue)(pQueue, data, dataLen);
}

INLINE uint8_t* GetNextFreeDataPlaceInQueue16(QUEUE_NAME()* pQueue,uint8_t* err) {
	return QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(pQueue, err);
}

INLINE uint8_t TakeNextFreePlaceInQueue16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(pQueue);
}

INLINE uint8_t SerializeQueue16(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes) { // 0 - OK
	return QUEUE_FUNC_NAME(SerializeQueue)(pQueue, data, maxDataSize, writtenBytes);
}

INLINE uint8_t DeserializeQueue16(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size) { // 0 - OK
	return QUEUE_FUNC_NAME(DeserializeQueue)(pQueue, data, size);
}

INLINE DOUBLE_QUEUE_LENGTH_TYPE GetSerializedQueueSize16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetSerializedQueueSize)(pQueue);
}

INLINE QUEUE_LENGTH_TYPE GetItemsCntInQueue16(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetItemsCntInQueue)(pQueue);
}

#ifdef __cplusplus
}
#endif
#endif
