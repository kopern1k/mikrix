/*
 * Copyright (c) 2005, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __COMMON_H__
#define __COMMON_H__
#include "inttypes.h"
#include "stddef.h"

#define BIT0 1
#define BIT1 2
#define BIT2 4
#define BIT3 8
#define BIT4 16
#define BIT5 32
#define BIT6 64
#define BIT7 128
#define BIT(bit) (1<<bit)

#define SET(port,bit) port|=(bit)		//nastavi bit na porte na log. 1
#define CLEAR(port,bit) port&=~(bit) 	//nastavi bit na porte na log. 0
#define TRIGGER(port,bit) port^=(bit) //preklapa vystup na bite XOR
#define XOR(port,bit) port^=(bit)		//XOR
#define GET(port,bits) ((port)&(bits))
#define GET_BIT(port,bit) ((port)&(bit))
#define SET_DATA(port,data,mask) (port = (port & (~mask)) | (data & mask)) //set more data to port
#define SET_MASKED_DATA(port,data,mask) (port=(port & (~mask)) | ( data )) //data must be masked before
#define SET_PORT(port,data)		(port)=(data))
#define NEG(data) data=~(data)+1		//vynasobi cislo -1
#define nop __asm__ __volatile__ ("nop\n")

//#define OFFSETOF_8(type, field)    ((uint8_t) &(((type *) 0)->field))
#define OFFSETOF_8(type, field) offsetof(type,field)

#ifndef TRUE
	#define TRUE		1
#endif
#ifndef FALSE
	#define FALSE		0
#endif

#if defined(LINUX)
    #define ATTRIB_ALIGN_8() __attribute__((packed, aligned(1)))
#else
    #define ATTRIB_ALIGN_8()
#endif

// PRINTF
#if defined(WIN)
  #include "stdio.h"
  #define PRINTF(...) printf(__VA_ARGS__)
#elif defined(LINUX)
  #include <stdio.h>
  #define PRINTF(...) printf(__VA_ARGS__); fflush(stdout)
#else
  #define PRINTF(...)
#endif

#ifdef ATM8SL
#include <avr/io.h>
#define PINTEST_SET( pin )	 set(PORTB,pin)
#define PINTEST_CLEAR( pin ) clear(PORTB,pin)
#define PINTEST_INIT( pin)	 set(DDRB,pin)
#else
#define PINTEST_SET( pin )
#define PINTEST_CLEAR( pin )
#define PINTEST_INIT( pin)
#endif

// Usage printf("Binary data:"BYTE_TO_BINARY_PATTERN"\n",BYTE_TO_BINARY(data));
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

#if defined(x86) || defined(ARM_LITTLE_ENDIAN)
#define toLE(val,size) (val)
//convert BE data to LE
#define toLE32(pData) ((((uint8_t*)pData)[0]<<24) + (((uint8_t*)pData)[1]<<16) + (((uint8_t*)pData)[2]<<8) + ((uint8_t*)pData)[3])
#define toLE24(pData) ((((uint8_t*)pData)[0]<<16) + (((uint8_t*)pData)[1]<<8) + ((uint8_t*)pData)[2])
#define toLE16(pData) ((((uint8_t*)pData)[0]<<8) + ((uint8_t*)pData)[1])
//convert LE data to BE
#define toBE16(pData)	toLE16(pData)
#define toBE24(pData)	toLE24(pData)
#define toBE32(pData)	toLE32(pData)
#else
INLINE void toLE(uint8_t *data, uint8_t size) {
	uint8_t tmp,i=0;
	do {
		tmp = data[i];
		data[i] = data[--size];
		data[size] = tmp;
	 } while (size >>1);
}

INLINE void toLE32(uint32_t *data) {
	toLE((uint8_t*)&data, 4);
}

INLINE void toLE16(uint16_t *data) {
	toLE((uint8_t*)&data, 2);
}
#endif


typedef 	union{
		uint32_t data;
		struct {
                        uint8_t hi16hi;
                        uint8_t hi16lo;
                        uint8_t lo16hi;
                        uint8_t lo16lo;
		};
		struct {
			uint16_t hi16;
			uint16_t lo16;
		};
}I32BE;

typedef 	union{
		uint16_t data;

		struct{
			uint8_t hi;
			uint8_t lo;
		};
}I16BE;

typedef 	union{
                uint64_t data;
                struct {
                    I32BE hi32;
                    I32BE lo32;
                };
}I64BE;


typedef 	union{
		uint32_t data;
		struct {
                        uint8_t lo16lo;
                        uint8_t lo16hi;
                        uint8_t hi16lo;
                        uint8_t hi16hi;
		};
		struct {
			uint16_t lo16;
			uint16_t hi16;
		};
}I32;

typedef 	union{
		uint16_t data;
		struct{
			uint8_t lo;
			uint8_t hi;
		};
}I16;

#define HI16(var) (((I16*)&(var))->hi)
#define LO16(var) (((I16*)&(var))->lo)
#define HIHI32(var) (((I32*)&(var))->hi16hi)
#define HILO32(var) (((I32*)&(var))->hi16lo)


#define BE16_HI(var) (((I16BE*)&(var))->hi)
#define BE16_LO(var) (((I16BE*)&(var))->lo)

#define BE32_HI16(var) (((I32BE*)&(var))->hi16)
#define BE32_LO16(var) (((I32BE*)&(var))->lo16)

#define BE32_HI16_HI(var) (((I32BE*)&(var))->hi16hi)
#define BE32_HI16_LO(var) (((I32BE*)&(var))->hi16lo)

#define BE32_LO16_HI(var) (((I32BE*)&(var))->lo16hi)
#define BE32_LO16_LO(var) (((I32BE*)&(var))->lo16lo)
#define BE64_HI32(var) (((I64BE*)&(var))->hi32.data)
#define BE64_LO32(var) (((I64BE*)&(var))->lo32.data)

#define BCD8TOINT8(bcd) (((((uint8_t)(bcd))>>4)*10) + (((uint8_t)(bcd))&0x0F))
#define BCD16TOINT16(bcd) (((BE16_HI(bcd)>>4)*1000) + ((BE16_HI(bcd)&0x0F)*100) + ((BE16_LO(bcd)>>4)*10) + (BE16_LO(bcd)&0x0F))
#define BCD24TOINT32(bcd) (((BE32_HI16_LO(bcd)>>4)*100000) + ((BE32_HI16_LO(bcd)&0x0F)*10000) + BCD16TOINT16(BE32_LO16(bcd)))
#define BCD32TOINT32(bcd) (((BE32_HI16_HI(bcd)>>4)*10000000) + ((BE32_HI16_HI(bcd)&0x0F)*1000000) + BCD24TOINT32(bcd))
#define BCD40TOINT64(bcd) (((BE32_HI16_HI(BE64_HI32(bcd))>>4)*1000000000) + ((BE32_HI16_HI(BE64_HI32(bcd))&0x0F)*100000000) + BCD32TOINT32(BE64_LO32(bcd)))

INLINE int8_t BCDToInt64(uint8_t* pData, uint8_t len, uint64_t* outNum){
    uint64_t out = 0;
    uint64_t multiply = 1;
    if(pData == NULL) {
        return -1;
    }
    while(len--){
        out += (pData[len] & 0x0F) * multiply;
        out += ((pData[len] & 0xF0) >> 4) * 10 * multiply;
        multiply *= 100;
    }
    *outNum = out;
    return 0;
}

#define toBCD8(data)	(((((data)/10) % 10)<<4) + (data)%10)
#define toBCD16(data)	(((((data)/1000) % 10)<<12) + ((((data)/100) % 10)<<8) + toBCD8(data))
#define toBCD24(data)	(((((data)/100000) % 10)<<20) + ((((data)/10000) % 10)<<16) + toBCD16(data))
#define toBCD32(data)	(((((data)/10000000) % 10)<<28) + ((((data)/100000) % 10)<<24) + toBCD24(data))

//convert number to BCD number into buffer. rest bytes are filled by zerro. return: bytes with significant data
INLINE int8_t toBCD(uint32_t data, uint8_t* buf, uint8_t len) {
	uint32_t rem;
	uint8_t i,n,j;
	if(buf == NULL) {
		return -1;
	}
	if(len == 0) {
		return -2;
	}
	n = len - 1;
	i = 0;
	len = len<<1;
	do {
		if(len <= i) {
			return -3;
		}
		rem = data % 10;
		data = data / 10;
		if((i&0x01) == 0) {
			buf[n - (i>>1)] = rem;
		} else {
			buf[n - (i>>1)] = buf[n - (i>>1)] + (rem<<4);
		}
		i++;
	} while(data != 0);
	j = (i+1) >> 1;
	j--;
	//fill rest bytes with zerro
	while(n > j) {
		buf[n - (++j)] = 0;
	}
	return (i+1) >> 1;
}

INLINE int8_t ConvertToBCD(uint64_t value, uint8_t* pBuf, uint8_t maxBytes, uint8_t startSize){
        int8_t ret;
        // if maxBytes are not specified, startSize is taken as defaut
        if(maxBytes == 0) {
                maxBytes = startSize;
        }
        //try to convert with starting size startSize
        do{
                ret = toBCD(value, pBuf, startSize);
                if(ret <0) { //converted numver doesn't fit into buffer, try to increase it, if possible
                        startSize++;
                        if(startSize >= maxBytes) {
                                return -2; // number could not fit into selected byte buffer!
                        }
                        continue;
                }
                break;
        } while(1);
        return startSize;
}

typedef struct{
	uint16_t len;
	uint8_t *data;
}DATA;

typedef struct{
        uint16_t producer;
	uint8_t platformID;
        uint8_t optData;
	uint32_t SN;

	//uint32_t userDef:8;
}UID;

typedef unsigned char MACAddress_t[6];

#define CMPSTRUCT(pStructA, pStructB,STRUCTSIZE) CMPSTRUCT ## STRUCTSIZE(pStructA,pStructB)

#define CMPSTRUCT1(pStructA, pStructB) (*(uint8_t*)(pStructA) == *(uint8_t*)(pStructB))
#define CMPSTRUCT2(pStructA, pStructB) (*(uint16_t*)(pStructA) == *(uint16_t*)(pStructB))
#define CMPSTRUCT3(pStructA, pStructB) (CMPSTRUCT2(pStructA,pStructB) && CMPSTRUCT1(pStructA + sizeof(uint16_t),pStructB + sizeof(uint16_t)))
#define CMPSTRUCT4(pStructA, pStructB) (*(uint32_t*)(pStructA) == *(uint32_t*)(pStructB))
#define CMPSTRUCT5(pStructA, pStructB) (CMPSTRUCT4(pStructA,pStructB) && CMPSTRUCT1(pStructA + sizeof(uint32_t),pStructB + sizeof(uint32_t)))
#define CMPSTRUCT6(pStructA, pStructB) (CMPSTRUCT4(pStructA,pStructB) && CMPSTRUCT2(pStructA + sizeof(uint32_t),pStructB + sizeof(uint32_t)))
#define CMPSTRUCT7(pStructA, pStructB) (CMPSTRUCT4(pStructA,pStructB) && CMPSTRUCT3(pStructA + sizeof(uint32_t),pStructB + sizeof(uint32_t)))

#ifdef x86
INLINE uint8_t CmpStruct(void *pStructA, void *pStructB, const uint16_t structSize) {
	uint16_t i = 0;
	uint16_t cnt = structSize / 4;
	const uint16_t mod = structSize % 4;

	while(cnt--) {
		if(CMPSTRUCT((uint8_t*)pStructA + i,(uint8_t*)pStructB + i,4)) {
			return 1;
		}
		i+=4;
	}

	switch(mod) {
		case 0:
			break;
		case 1:
			return !CMPSTRUCT((uint8_t*)pStructA + i,(uint8_t*)pStructB + i,1);
		case 2:
			return !CMPSTRUCT((uint8_t*)pStructA + i,(uint8_t*)pStructB + i,2);
		case 3:
			return !CMPSTRUCT((uint8_t*)pStructA + i,(uint8_t*)pStructB + i,3);
	}
	return 0;
}

#else
INLINE uint8_t CmpStruct(void *pStructA, void *pStructB, const uint16_t structSize) {
	uint16_t i;
	for(i = 0;i<structSize;i++){
		if(((uint8_t*)pStructA)[i] != ((uint8_t*)pStructB)[i]) return 1;
	}
	return 0;
}

#endif

INLINE void CopyMem(void *var1,void* var2,uint8_t copyLen){
	uint8_t i;
	for(i=0;i<copyLen;i++){
                ((uint8_t*)var1)[i]=((uint8_t*)var2)[i];
	}
}

INLINE void CopyMemRot( uint8_t *var1,uint8_t* var2,uint8_t copyLen, const uint8_t* memTop, const uint8_t* memStart){
	uint8_t i;
	uint8_t *pData;
	if((var1+copyLen)>memTop){
		i=0;
		for(pData=var1;pData<memTop;){
			*pData++=var2[i++];
		}
		for(pData=(uint8_t*)memStart;i<copyLen;){
			*pData++=var2[i++];
		}
	}else{
		for(i=0;i<copyLen;i++){
			var1[i]=var2[i];
		}
	}
}


INLINE void NegMem(uint8_t *var1,uint8_t* var2,uint8_t copyLen){
	uint8_t i;
	for(i=0;i<copyLen;i++){
		var1[i]=~var2[i];
	}
}
INLINE void SetMem(uint8_t *var1,uint8_t val,uint8_t copyLen){
	uint8_t i;
	for(i=0;i<copyLen;i++){
		var1[i]=val;
	}
}

INLINE void XorMem(uint8_t *var1,uint8_t* var2,uint8_t copyLen){
	uint8_t i;
	for(i=0;i<copyLen;i++){
		var1[i]^=var2[i];
	}
}
INLINE void AndMem(uint8_t *var1,uint8_t* var2,uint8_t copyLen){
	uint8_t i;
	for(i=0;i<copyLen;i++){
		var1[i]&=var2[i];
	}
}

#define CREATE_TAG(tag) #tag
#define TAG_TO_STRING(tag) CREATE_TAG(tag)

#define ARG_LEN(...) \
		 ARG_LEN_(__VA_ARGS__,PP_RSEQ_N())
#define ARG_LEN_(...) \
		 ARG_LEN_N(__VA_ARGS__)
#define ARG_LEN_N( \
		  _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
		 _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
		 _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
		 _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
		 _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
		 _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
                 _61,_62,_63,N,...) N
#define PP_RSEQ_N() \
                 63,62,61,60,                   \
		 59,58,57,56,55,54,53,52,51,50, \
		 49,48,47,46,45,44,43,42,41,40, \
		 39,38,37,36,35,34,33,32,31,30, \
		 29,28,27,26,25,24,23,22,21,20, \
		 19,18,17,16,15,14,13,12,11,10, \
		 9,8,7,6,5,4,3,2,1,0

#endif

#define DECLARE_TYPEDEF_ENUM(typeName,prefix,postfix,operation,...) typedef enum { EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),operation,prefix,postfix,__VA_ARGS__) } typeName
#define DECLARE_ENUM(typeName,prefix,postfix,operation,...) enum typeName { EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),operation,prefix,postfix,__VA_ARGS__) }
#define DECLARE_ID_ENUM(...) DECLARE_ENUM(,,_ID,CONCAT_PREFIX_POSTFIX,__VA_ARGS__)

// note use DECLARE_ENUM_WITH_STRINGS macro with care. It will create multiple copies of string table in each included file.
// Use DECLARE_TAG_ENUM_STRINGS instead.
#define DECLARE_ENUM_WITH_STRINGS(typeName,maxStringLength,...)  DECLARE_ENUM(typeName,,,CONCAT_PREFIX_POSTFIX,__VA_ARGS__);\
	static char typeName ## _str [][maxStringLength] = {EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),TAG_TO_STRING,,,__VA_ARGS__) }

/*  DECLARE_TAG_ENUM_STRINGS Example:
	testEnum.h
	#define TEST_ENUM_TAGS \
	ENUM_PAR1,\
	ENUM_PAR2,\
	ENUM_PAR3
	enum testEnum {TEST_ENUM_TAGS};

	testEnum.c
	DECLARE_TAG_ENUM_STRINGS(testEnum,10,TEST_ENUM_TAGS)
	...
	printf("%s",testEnum_str[ENUM_PAR1]);
*/

#define EXPAND_LIST_OF_TOKENS_TO_STR_LIST(...) EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),TAG_TO_STRING,,,__VA_ARGS__)

#define DECLARE_TAG_ENUM_STRINGS(typeName,maxStringLength,...) \
        char typeName ## _str [][maxStringLength] = {EXPAND_LIST_OF_TOKENS_TO_STR_LIST(__VA_ARGS__)}


#define INIT_ARRAY_OF_STRUCTURES(...) { EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),GET_ADDRESS_OF,,,__VA_ARGS__),0 }

#define PARAM_OPERATION_CONCAT_PREFIX_POSTFIX(prefix,postfix,first) prefix ## first ## postfix
#define PARAM_OPERATION_TAG_TO_STRING(prefix,postfix,first) TAG_TO_STRING(prefix ## first ## postfix)
#define PARAM_OPERATION_GET_ADDRESS_OF(prefix,postfix,first) &first

#define CONCAT_NAME(prefix, name, postfix) CONCAT_NAME1(prefix, name, postfix)
#define CONCAT_NAME1(prefix, name, postfix) prefix ## name ## postfix

#define CREATE_MSG_PREFIX(msg) MSG_ ## msg
#define CREATE_MSG_GROUP(msgGroup) ((msgGroup)<<6)
#define GET_MSG_GROUP(msgGroup) ((msgGroup)>>6)
#define GRP_MESSAGES(msgGroupBegin,first,...) enum { first = msgGroupBegin, EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),CONCAT_PREFIX_POSTFIX,,,__VA_ARGS__) }
#define GRP_MESSAGES_WITH_STRINGS(msgGroupBegin,maxStringLength,first,...) enum { first = msgGroupBegin, EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),CONCAT_PREFIX_POSTFIX,,,__VA_ARGS__) };\
        static char msgGroupBegin ## _str [][maxStringLength] = {TAG_TO_STRING(first), EXPAND_LIST_OF_TOKENS(ARG_LEN(__VA_ARGS__),TAG_TO_STRING,,,__VA_ARGS__) }
#define GET_GRP_MESSAGE_STRING(msgGroupBegin, message) msgGroupBegin ## _str[message - msgGroupBegin]

#define EXPAND_LIST_OF_TOKENS(N,operation,prefix,postfix,...) EXPAND_LIST_OF_TOKENS1(N,operation,prefix,postfix,__VA_ARGS__)
#define EXPAND_LIST_OF_TOKENS1(N,operation,prefix,postfix,...) MACRO_PARAMS ## N(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS0(operation,prefix,postfix,first,...)
#define MACRO_PARAMS1(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first)
#define MACRO_PARAMS2(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS1(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS3(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS2(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS4(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS3(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS5(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS4(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS6(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS5(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS7(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS6(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS8(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS7(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS9(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS8(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS10(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS9(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS11(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS10(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS12(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS11(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS13(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS12(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS14(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS13(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS15(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS14(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS16(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS15(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS17(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS16(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS18(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS17(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS19(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS18(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS20(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS19(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS21(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS20(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS22(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS21(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS23(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS22(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS24(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS23(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS25(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS24(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS26(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS25(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS27(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS26(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS28(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS27(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS29(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS28(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS30(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS29(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS31(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS30(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS32(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS31(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS33(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS32(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS34(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS33(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS35(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS34(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS36(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS35(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS37(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS36(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS38(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS37(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS39(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS38(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS40(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS39(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS41(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS40(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS42(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS41(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS43(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS42(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS44(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS43(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS45(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS44(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS46(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS45(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS47(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS46(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS48(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS47(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS49(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS48(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS50(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS49(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS51(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS50(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS52(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS51(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS53(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS52(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS54(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS53(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS55(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS54(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS56(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS55(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS57(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS56(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS58(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS57(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS59(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS58(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS60(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS59(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS61(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS60(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS62(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS61(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS63(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS62(operation,prefix,postfix,__VA_ARGS__)
#define MACRO_PARAMS64(operation,prefix,postfix,first,...) PARAM_OPERATION_ ## operation(prefix,postfix,first), MACRO_PARAMS63(operation,prefix,postfix,__VA_ARGS__)
