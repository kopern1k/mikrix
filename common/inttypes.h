/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __INTTYPES_H___
#define __INTTYPES_H___

#ifndef NULL
#define NULL			 0
#endif
#define true	1
#define false	0

#ifdef WIN32
typedef unsigned char	uint8_t;
typedef signed char		int8_t;
typedef unsigned short	uint16_t;
typedef short			int16_t;
typedef unsigned int	uint32_t;
typedef int				int32_t;
typedef long long		int64_t;
typedef unsigned long long uint64_t;
typedef uint32_t		time_us_t;
//typedef 
//typedef unsigned char	BOOL;


#else
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#endif 

#if defined (_MSC_VER ) 
  #define INLINE __forceinline /* use __forceinline (VC++ specific) */
#elif defined (LINUX)
  //#define INLINE __always_inline
  #define INLINE static inline
  typedef uint32_t DWORD;
  typedef int32_t LONG;

  typedef union _LARGE_INTEGER {
	struct {
	  DWORD LowPart;
	  LONG  HighPart;
	};
	struct {
	  DWORD LowPart;
	  LONG  HighPart;
	} u;
	int64_t QuadPart;
  } LARGE_INTEGER;

//  typedef long long int LARGE_INTEGER;
#ifndef __cplusplus
  typedef uint8_t bool;
#endif
#else
  #define INLINE inline        /* use standard inline */
#endif



#endif /* __INTTYPES_H__ */
