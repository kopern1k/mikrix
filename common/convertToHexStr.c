/*
 * Copyright (c) 2015, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "convertToHexStr.h"

char const hexChars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

uint16_t ConvertToHexStr(uint8_t *pStrBuf, uint16_t strBufSize, uint8_t* pData, uint8_t len) {
	int i;
	uint8_t *tmpStrBuf = pStrBuf;
	uint8_t tmpLen = len;

	if(strBufSize == 0) {
		return 0;
	}
	if(len == 0) {
		*pStrBuf = 0;
		return 0;
	}
	if((3*len) > strBufSize - 1) {
		len = ((strBufSize -3) / 3);
		pStrBuf[3*len - 1] = '~';
		pStrBuf[3*len + 0] = '~';
		pStrBuf[3*len + 1] = 0;
	}
	for( i = 0; i < len; i++) {
		*pStrBuf++ = hexChars[(pData[i] & 0xF0) >> 4];
		*pStrBuf++ = hexChars[pData[i] & 0x0F];
		*pStrBuf++ = ' ';
	}
	if(len == tmpLen) {
		*(--pStrBuf) = 0;
	} else {
		pStrBuf += 2;
	}
	return pStrBuf - tmpStrBuf;
}

#define DEBUG_DATA(data,len,...) PRINT_HEX_DATA(data,len);printf(__VA_ARGS__)

int TestConvertToHexStr(void) {
	uint8_t data[5] = {0xFF,0xF2,0xF3,4,5};
	uint8_t tmpBuf[3*sizeof(data) + 1];
	//tmpBuf[3*sizeof(data)] = 'a';
	//tmpBuf[3*sizeof(data) +1] = 'b';
	ConvertToHexStr(tmpBuf,sizeof(tmpBuf),data, sizeof(data));
	printf("Data: %s!\n",tmpBuf);
	DEBUG_DATA(data,sizeof(data),"\n");
	return 0;
}

