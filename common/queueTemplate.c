/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

//#include "queueTemplate.h"
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "sysConfig.h"

#if defined(USE_QUEUE_SIZE_16) || defined(USE_QUEUE_SIZE_8) || defined(USE_QUEUE_SIZE_6)
//typedef enum {NO_OVERRIDE =0, OVERRIDE_LAST_DATA_IN_QUEUE = 1} OverrideLastDataInQueue;

uint8_t QUEUE_FUNC_NAME(InitQueueEX)(QUEUE_NAME()* pQueue,uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit) {
	if(dataArray == NULL) {
		return 1;
	}
	if(N == 0 || N > MAX_QUEUE_DEPTH) {
		return 2;
	}
	pQueue->data = dataArray;
	pQueue->staticInit = staticInit;
	pQueue->firstDataIndex = (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE;
	pQueue->writeIndex = 0;
	pQueue->maxDataItemSize = maxDataItemtSize;
	pQueue->N = N;
	pQueue->overrideLast = overrideLast;
	pQueue->op = QUEUE_NO_OPERATION;
	return 0;
}


QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetItemsCntInQueue)(QUEUE_NAME()* pQueue) {
	if(pQueue->firstDataIndex ==  (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		return 0;
	}
	if(pQueue->writeIndex > pQueue->firstDataIndex) {
		return pQueue->writeIndex - pQueue->firstDataIndex;
	}
	return  pQueue->N - (pQueue->firstDataIndex - pQueue->writeIndex) ;
}


DOUBLE_QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetSerializedQueueSize)(QUEUE_NAME()* pQueue) {
	if(pQueue == NULL) {
		return -1;
	}
	return pQueue->N * pQueue->maxDataItemSize + OFFSETOF_8(QUEUE_SERIALIZED_NAME(),data) + OFFSETOF_8(QUEUE_NAME(),data);
}


 // <0 err, >0 serialize size
uint8_t QUEUE_FUNC_NAME(SerializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes) {
	DOUBLE_QUEUE_LENGTH_TYPE i;
	QUEUE_LENGTH_TYPE j = 0;

	if(pQueue == NULL) {
		return QUEUE_ERR_BAD_POINTER;
	}
	if(maxDataSize < QUEUE_FUNC_NAME(GetSerializedQueueSize)(pQueue)) {
		return QUEUE_ERR_SIZE;
	}


#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op != QUEUE_NO_OPERATION) {
		return QUEUE_ERR_BUSY;
	}
#endif

	pQueue->op = QUEUE_IS_SERIALIZING;

	// save queue indexes
	DOUBLE_QUEUE_LENGTH_TYPE dataSize = OFFSETOF_8(QUEUE_NAME(),data);

	for(i = 0; i<dataSize; i++) {
		((uint8_t*)&(((QUEUE_SERIALIZED_NAME()*)data)->data))[i] = ((uint8_t*)pQueue)[i];
	}

	dataSize += pQueue->N * pQueue->maxDataItemSize;
	for(; i< dataSize; i++) {
		((uint8_t*)&(((QUEUE_SERIALIZED_NAME()*)data)->data))[i] = pQueue->data[j++];
	}

	((QUEUE_SERIALIZED_NAME()*)data)->dataLen = dataSize;

	pQueue->op = QUEUE_NO_OPERATION;

	*writtenBytes = dataSize + OFFSETOF_8(QUEUE_SERIALIZED_NAME(),data);

	return 0;
}

uint8_t QUEUE_FUNC_NAME(DeserializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size) {
	DOUBLE_QUEUE_LENGTH_TYPE i;
	QUEUE_LENGTH_TYPE j = 0;
	QUEUE_SERIALIZED_NAME()* pSerializedData = (QUEUE_SERIALIZED_NAME()*) data;

	if(pQueue == NULL) {
		return QUEUE_ERR_BAD_POINTER;
	}
	if(size != QUEUE_FUNC_NAME(GetSerializedQueueSize)(pQueue)) {
		return QUEUE_ERR_SIZE;
	}

#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op != QUEUE_NO_OPERATION) {
		return QUEUE_ERR_BUSY;
	}
#endif

	pQueue->op = QUEUE_IS_SERIALIZING;

	//get current queue indexes
	DOUBLE_QUEUE_LENGTH_TYPE dataSize = OFFSETOF_8(QUEUE_NAME(),data);
	for(i = 0;i<dataSize; i++){
		((uint8_t*)pQueue)[i] = (&pSerializedData->data)[i];
	}
	//get data
	for(;i<pSerializedData->dataLen;i++) {
		pQueue->data[j++] = (&pSerializedData->data)[i];
	}

	pQueue->op = QUEUE_NO_OPERATION;

	return 0;
}

uint8_t QUEUE_FUNC_NAME(FreeQueue)(QUEUE_NAME()* pQueue) {
	if(pQueue == NULL) {
		return QUEUE_ERR_BAD_POINTER;
	}
	if(pQueue->data == NULL) {
		return QUEUE_ERR_BAD_DATA_POINTER;
	}
	if(pQueue->staticInit == 0){
		free(pQueue->data);
	}
	return 0;
}

uint8_t* QUEUE_FUNC_NAME(GetFirstQueueData)(QUEUE_NAME()* pQueue) {
	if(pQueue == NULL){
		return 0;
	}
	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE){
		return 0;
	}
	return ((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[pQueue->firstDataIndex];
}


uint8_t* QUEUE_FUNC_NAME(GetLastQueueData)(QUEUE_NAME()* pQueue) {
	QUEUE_LENGTH_TYPE lastDataIndex;
	if(pQueue == NULL){
		return 0;
	}
	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE){
		return 0;
	}
	// find last data index
	if(pQueue->writeIndex) {
		lastDataIndex = pQueue->writeIndex - 1;
	} else {
		lastDataIndex = pQueue->N - 1;
	}
	return ((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[lastDataIndex];
}


uint8_t* QUEUE_FUNC_NAME(GetPrenultimateQueueData)(QUEUE_NAME()* pQueue) {
	QUEUE_LENGTH_TYPE prenultimateDataIndex;
	if(pQueue == NULL){
		return 0;
	}
	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE){
		return 0;
	}
	if(QUEUE_FUNC_NAME(GetItemsCntInQueue)(pQueue) == 1) {
		return 0;
	}
	// find prenultimate data index
        if(pQueue->writeIndex > 1) {
		prenultimateDataIndex = pQueue->writeIndex - 2;
	} else {
                prenultimateDataIndex = pQueue->N - (2 - pQueue->writeIndex);
	}
	return ((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[prenultimateDataIndex];
}

uint8_t QUEUE_FUNC_NAME(RemoveFirstQueueData)(QUEUE_NAME()* pQueue) {
	if(pQueue == NULL){
		return QUEUE_ERR_BAD_POINTER;
	}

	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		return QUEUE_ERR_EMPTY_QUEUE;
	}

#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op == QUEUE_IS_SERIALIZING) {
		return QUEUE_ERR_BUSY;
	}
#endif


	pQueue->op = QUEUE_ITEM_IS_REMOVING;

	if(++(pQueue->firstDataIndex) >= pQueue->N){
		pQueue->firstDataIndex = 0;
	}
	if(pQueue->firstDataIndex == pQueue->writeIndex) {
		pQueue->firstDataIndex = (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE;
	}
	pQueue->op = QUEUE_NO_OPERATION;
	return 0;
}

uint8_t QUEUE_FUNC_NAME(RemoveLastQueueData)(QUEUE_NAME()* pQueue) {
	if(pQueue == NULL){
		return QUEUE_ERR_BAD_POINTER;
	}

	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		return QUEUE_ERR_EMPTY_QUEUE;
	}

#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op == QUEUE_IS_SERIALIZING) {
		return QUEUE_ERR_BUSY;
	}
#endif


	pQueue->op = QUEUE_ITEM_IS_REMOVING;


	if(pQueue->writeIndex) {
		pQueue->writeIndex--;
	} else {
		pQueue->writeIndex = pQueue->N - 1;
	}

	if(pQueue->firstDataIndex == pQueue->writeIndex) {
		pQueue->firstDataIndex = (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE;
	}

	pQueue->op = QUEUE_NO_OPERATION;
	return 0;
}

uint8_t QUEUE_FUNC_NAME(RemovePrenultimateQueueData)(QUEUE_NAME()* pQueue) {
        uint8_t i,destInd;
	if(pQueue == NULL){
		return QUEUE_ERR_BAD_POINTER;
	}

	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		return QUEUE_ERR_EMPTY_QUEUE;
	}

	if(QUEUE_FUNC_NAME(GetItemsCntInQueue)(pQueue) == 1) {
		return QUEUE_ERR_ONLY_ONE_ITEM;
	}

#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op == QUEUE_IS_SERIALIZING) {
		return QUEUE_ERR_BUSY;
	}
#endif


	pQueue->op = QUEUE_ITEM_IS_REMOVING;


	if(pQueue->writeIndex) {
		pQueue->writeIndex--;
	} else {
		pQueue->writeIndex = pQueue->N - 1;
	}

        if(pQueue->writeIndex) {
                destInd = pQueue->writeIndex - 1;
        } else {
                destInd = pQueue->N - 1;
        }
	// copy data
	for(i = 0;i<pQueue->maxDataItemSize;i++){
		((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[destInd][i] = ((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[pQueue->writeIndex][i];
	}

	if(pQueue->firstDataIndex == pQueue->writeIndex) {
		pQueue->firstDataIndex = (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE;
	}

	pQueue->op = QUEUE_NO_OPERATION;
	return 0;
}

uint8_t* QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(QUEUE_NAME()* pQueue,uint8_t* err) {
	*err = QUEUE_NO_ERR;
	if(pQueue == NULL){
		*err = QUEUE_ERR_BAD_POINTER;
		return NULL;
	}

	if(pQueue->firstDataIndex ==  pQueue->writeIndex) {
		if(pQueue->overrideLast == 0){
			*err = QUEUE_ERR_IS_FULL;
			return NULL;
		}
		*err = QUEUE_ERR_OVERRIDING_LAST; // identifies overridding last data
		// find last data wrire index
		if(pQueue->writeIndex) {
			pQueue->writeIndex--;
		} else {
			pQueue->writeIndex = pQueue->N - 1;
		}
	}

	return ((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[pQueue->writeIndex];

}

uint8_t QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(QUEUE_NAME()* pQueue) {
#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op == QUEUE_IS_SERIALIZING) {
		return QUEUE_ERR_BUSY;
	}
#endif
	pQueue->op = QUEUE_ITEM_IS_TAKING_PLACE;
	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		pQueue->firstDataIndex = pQueue->writeIndex;
	}

	pQueue->writeIndex++;

	if(pQueue->writeIndex >= pQueue->N) {
		pQueue->writeIndex = 0;
	}
	pQueue->op = QUEUE_NO_OPERATION;
	return 0;
}

uint8_t QUEUE_FUNC_NAME(WriteDataToQueue)(QUEUE_NAME()* pQueue,uint8_t *data, uint8_t dataLen) {
	uint8_t i;
	uint8_t ret = 0;

	if(pQueue == NULL){
		return QUEUE_ERR_BAD_POINTER;
	}
#ifdef QUEUE_USE_BUSY_CHECK
	if(pQueue->op == QUEUE_IS_SERIALIZING) {
		return QUEUE_ERR_BUSY;
	}
#endif
	pQueue->op = QUEUE_ITEM_IS_WRITTING;

	if(pQueue->firstDataIndex ==  pQueue->writeIndex) {
		if(pQueue->overrideLast == 0){
			pQueue->op = QUEUE_NO_OPERATION;
			return QUEUE_ERR_IS_FULL;
		}
		// find last data wrire index
		if(pQueue->writeIndex) {
			pQueue->writeIndex--;
		} else {
			pQueue->writeIndex = pQueue->N - 1;
		}
		ret = QUEUE_ERR_OVERRIDING_LAST; // identifies overridding last data
	}
	for(i = 0;i<dataLen;i++){
		((uint8_t(*)[pQueue->maxDataItemSize])(pQueue->data))[pQueue->writeIndex][i] = data[i];
	}
	if(pQueue->firstDataIndex == (QUEUE_LENGTH_TYPE)NO_DATA_IN_QUEUE) {
		pQueue->firstDataIndex = pQueue->writeIndex;
	}

	pQueue->writeIndex++;

	if(pQueue->writeIndex >= pQueue->N) {
		pQueue->writeIndex = 0;
	}
	pQueue->op = QUEUE_NO_OPERATION;
	return ret;
}

#endif

