/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "list.h"
#include <stdio.h>
#ifdef TEST_LIST
#include <stdlib.h>
#endif
void* ListTail(list_t* listHead) {
	listItem_t *l;

	if(*listHead == NULL) {
		return NULL;
	}
	for(l = (listItem_t*)*listHead; l->next != NULL; l = (listItem_t*)l->next);

	return l;
}

void ListRemove(list_t* listHead, listItem_t *item) {
	listItem_t *l, *r;

	if(*listHead == NULL) {
		return;
	}

	r = NULL;
	for(l = *listHead; l != NULL; l =(listItem_t*)l->next) {
		if(l == item) {
			if(r == NULL) {
				/* First on list */
				*listHead = (listItem_t*) l->next;
			} else {
				/* Not first on list */
				r->next = l->next;
			}
			l->next = NULL;
			return;
		}
		r = l;
	}
}


void ListAdd(list_t* listHead, void *item) {
	listItem_t *l;

	/* Make sure not to add the same element twice */
	ListRemove(listHead, item);

	((listItem_t *)item)->next = NULL;

	if(*listHead == NULL) {
		*listHead = item;
	} else {
		for(l = *listHead; l->next != NULL; l = (listItem_t*)l->next);
		l->next = (struct listItem_t*)item;
	}
}



#ifdef TEST_LIST
LIST_ITEM(myItem,
		  uint8_t varA;
);

void TestList(){
	list_t myList;
	InitList(myList);
	LIST_TYPE(myItem)* pItem = malloc(sizeof(LIST_TYPE(myItem)));

	pItem->varA = 1;
	ListAdd(&myList,(void*)pItem);
	pItem = malloc(sizeof(LIST_TYPE(myItem)));
	pItem->varA = 2;
	ListAdd(&myList,(void*)pItem);
	LIST_TYPE(myItem)* pItem0 = pItem;
	ListRemove(&myList,(listItem_t*)pItem0);
	//for(LIST_TYPE(myItem)* l = (LIST_TYPE(myItem)*) myList;l !=NULL; l = (LIST_TYPE(myItem)*)l->next) {
	ITERATE_LIST(myList,LIST_TYPE(myItem),l) {
		printf("my List variable %hhu\n",l->varA);
	}
}
#endif
