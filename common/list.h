/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __LIST__H__
#define __LIST__H__
#include "sysConfig.h"
#include "inttypes.h"
struct listItem_t{
	struct listItem_t *next;
};

typedef struct listItem_t listItem_t;


typedef listItem_t* list_t;


//#define LIST_TYPE(name) name ## List_t
//declare your list item type
#define LIST_ITEM(typeName,...) \
	typedef struct { \
		void *next;\
		__VA_ARGS__\
	} typeName


#define InitList(listName) listName = NULL

#define ITERATE_LIST(list, itemType, tmpItemVar) for(itemType* tmpItemVar = (itemType*) list;tmpItemVar !=NULL; tmpItemVar = (itemType*)tmpItemVar->next)
#define ITERATE_LIST_C(list, itemType, tmpItemVar) for(tmpItemVar = (itemType*) list;tmpItemVar !=NULL; tmpItemVar = (itemType*)tmpItemVar->next)
#define LIST_TYPE(name) name
#ifdef LIST_USE_MALLOC
#define NEW_LIST_ITEM(type,varName)  varName = malloc(sizeof(type))
#define FREE_LIST_ITEM(varName)  free(varName)
#endif

void* ListTail(list_t* listHead);
void ListAdd(list_t* listHead, void *item);
void ListRemove(list_t* listHead, listItem_t *item);
INLINE uint8_t IsListEmpty(list_t listHead) {
	return !!(listHead);
}

#endif
