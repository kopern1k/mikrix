/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __ONE_TYPE_ALLOC__H__
#define __ONE_TYPE_ALLOC__H__
#include <inttypes.h>
#include "sysConfig.h"

//#define TEST

#define MEM_ITEM(typeName,...) \
	typedef struct { \
		uint8_t isAllocated;\
		__VA_ARGS__\
	} typeName

typedef struct {
	volatile uint8_t isAllocated;
	uint8_t data;
} memItem_t;

void InitOneTypeMemItems(memItem_t* memItemsBuf, int memItemSize, int16_t memBuffItemsCnt);
void* GetOneTypeMemItem(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt);
uint8_t FreeOneTypeMemItem(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt, void* pDataToFree);

#ifdef TEST
void PrintOneTypeMemInfo(memItem_t* memItemsBuff, uint16_t memItemSize, uint8_t memBuffItemsCnt);
#else
#define PrintOneTypeMemInfo
#endif

#endif
