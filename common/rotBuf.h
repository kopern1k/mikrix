/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __ROTBUF__H__
#define  __ROTBUF__H__

#include "inttypes.h"

/* !! DEFINE ROTBUFTYPE before you include your this code !!!
 * !! set also your buffer size !!
 * example:
 * #define ROTBUFTYPE int
 * #define ROTBUFSIZE	4
 * #include "rotbuf.h"
 */

//FIFOSIZE max 256, modulo 2
//#define ROTBUFSIZE	4

#define ROTBUF_MASK (ROTBUFSIZE - 1)

#define ROTBUF_DATA(type) \
typedef struct {\
	unsigned char start;\
	unsigned char end;\
	type data[ROTBUFSIZE];\
} ROTBUF_t; \

//define specific data type for rotary buffer
ROTBUF_DATA(ROTBUFTYPE)



/*
INLINE void RotBufClear(void){
	char i;
	for(i=0;i<ROTBUFSIZE;i++){
		ROTBUF.data[i]=(ROTBUFTYPE*)0;
	}
}
*/


INLINE void RotBufInit(ROTBUF_t *ROTBUF){
	ROTBUF->start=0;
	ROTBUF->end=0;
	//can be initialized as follows (all buffer data are used), but there is problem with fetching non entered data
	//ROTBUF.start=1;
	//ROTBUF.end=0;
	//RotBufClear();
};


INLINE uint8_t RotBufAdd(ROTBUF_t *ROTBUF, ROTBUFTYPE data){
	if(ROTBUF->start != ((ROTBUF->end + 1) & ROTBUF_MASK)) {
		ROTBUF->data[ROTBUF->end]=data;
		ROTBUF->end= (ROTBUF->end + 1) & ROTBUF_MASK;
		return 0;
	}
	return 1;
};

INLINE uint8_t RotBufFetch(ROTBUF_t *ROTBUF, ROTBUFTYPE *data){
	if(ROTBUF->start != ROTBUF->end) {
		*data = ROTBUF->data[ROTBUF->start];
		ROTBUF->start = (ROTBUF->start + 1) & ROTBUF_MASK;
		return 0;
	}
	return 1;
}

INLINE uint8_t RotBufEmpty(ROTBUF_t *ROTBUF){
	return ROTBUF->start == ROTBUF->end;
}

//void RotBufInit(void);
//uint8_t RotBufAdd(tROTBUF data);
//void RotBufClear(void);
//uint8_t RotBufFetch(type *data)

#endif
