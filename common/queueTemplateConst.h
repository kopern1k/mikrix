/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
#ifndef __QUEUE_TEMPLATE_COMMON__H__
#define __QUEUE_TEMPLATE_COMMON__H__

#include <stdlib.h>
#include "../common/common.h"

#define NO_DATA_IN_QUEUE 0xFFFFFFFF

#define QUEUE_NO_ERR 0
#define QUEUE_ERR_BAD_POINTER 1
#define QUEUE_ERR_BAD_DATA_POINTER 2
#define QUEUE_ERR_EMPTY_QUEUE 3
#define QUEUE_ERR_SIZE 4
#define QUEUE_ERR_BUSY 5
#define QUEUE_ERR_OVERRIDING_LAST 6
#define QUEUE_ERR_IS_FULL 7
#define QUEUE_ERR_ONLY_ONE_ITEM 8

enum {NO_OVERRIDE =0 ,OVERRIDE_LAST_DATA_IN_QUEUE = 1};

enum {QUEUE_NO_OPERATION = 0, QUEUE_ITEM_IS_WRITTING, QUEUE_ITEM_IS_REMOVING, QUEUE_ITEM_IS_TAKING_PLACE, QUEUE_IS_SERIALIZING };
#define QUEUE_FUNC_NAME(name) CONCAT_NAME(name,_,QUEUE_LENGTH)
#define QUEUE_INLINE_NAME(name) CONCAT_NAME(name,,QUEUE_LENGTH)

#define QUEUE_NAME() CONCAT_NAME(QUEUE,_,QUEUE_LENGTH)
#define QUEUE_SERIALIZED_NAME() CONCAT_NAME(DATA_QUEUE_Serialized_,QUEUE_LENGTH,_t)

#endif
