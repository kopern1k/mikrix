/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __QUEUE_TEMPLATE_H__
#define __QUEUE_TEMPLATE_H__
#include "inttypes.h"
#include "common.h"
#include <stdlib.h>
#include "queueTemplateConst.h"


// !!!! Do not include this template in your files. Use specific target queuexx.h in your project.
// There is no default size. There is need to select specific option of this template.

#ifdef USE_QUEUE_SIZE_16
#define QUEUE_LENGTH 16
#define QUEUE_LENGTH_TYPE uint16_t
#define DOUBLE_QUEUE_LENGTH_TYPE uint32_t
#define MAX_QUEUE_DEPTH 65534
#elif defined(USE_QUEUE_SIZE_8)
#define QUEUE_LENGTH 8
#define QUEUE_LENGTH_TYPE uint8_t
#define DOUBLE_QUEUE_LENGTH_TYPE uint16_t
#define MAX_QUEUE_DEPTH 254
#elif USE_QUEUE_SIZE_6
#define QUEUE_LENGTH 6
#define QUEUE_LENGTH_TYPE uint8_t
#define DOUBLE_QUEUE_LENGTH_TYPE uint16_t
#define MAX_QUEUE_DEPTH 62
#endif

#if defined(USE_QUEUE_SIZE_16) || defined (USE_QUEUE_SIZE_8) || defined(USE_QUEUE_SIZE_6)

typedef struct {
	QUEUE_LENGTH_TYPE firstDataIndex;
	QUEUE_LENGTH_TYPE writeIndex;
#if defined(USE_DATA_QUEUE_SIZE_16) || defined(USE_DATA_QUEUE_SIZE_8)
	QUEUE_LENGTH_TYPE N;
#else
	QUEUE_LENGTH_TYPE N:6;
#endif
	uint8_t overrideLast:1;
	uint8_t staticInit:1;
	uint8_t maxDataItemSize;
	volatile uint8_t op;
	uint8_t *data;
} QUEUE_NAME();

typedef struct {
	DOUBLE_QUEUE_LENGTH_TYPE dataLen;
	uint8_t data;
} QUEUE_SERIALIZED_NAME();

/*
// declare real functions which are called from defined type
extern uint8_t QUEUE_FUNC_NAME(InitQueueEX)(QUEUE* pQueue, uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit);
extern uint8_t QUEUE_FUNC_NAME(FreeQueue)(QUEUE* pQueue);
//extern uint8_t QUEUE_FUNC_NAME(SerializeQueue)(DATA_QUEUE* pQueue, uint8_t *data, uint16_t maxDataSize, uint16_t* writtenBytes);
extern DOUBLE_QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetSerializedQueueSize)(QUEUE* pQueue);
extern QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetItemsCntInQueue)(QUEUE* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetCurrentQueueData)(QUEUE* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemoveCurrentQueueData)(QUEUE* pQueue);
extern uint8_t QUEUE_FUNC_NAME(WriteDataToQueue)(QUEUE* pQueue,uint8_t *data, uint8_t dataLen);
extern uint8_t* QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(QUEUE* pQueue,uint8_t* err);
extern uint8_t QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(QUEUE* pQueue);
extern uint8_t QUEUE_FUNC_NAME(SerializeQueue)(QUEUE* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes); // 0 - OK
extern uint8_t QUEUE_FUNC_NAME(DeserializeQueue)(QUEUE* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size);
*/


// declare real functions which are called from defined type
extern uint8_t QUEUE_FUNC_NAME(InitQueueEX)(QUEUE_NAME()* pQueue, uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit);
extern uint8_t QUEUE_FUNC_NAME(FreeQueue)(QUEUE_NAME()* pQueue);
extern DOUBLE_QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetSerializedQueueSize)(QUEUE_NAME()* pQueue);
extern QUEUE_LENGTH_TYPE QUEUE_FUNC_NAME(GetItemsCntInQueue)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetCurrentQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetLastQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t* QUEUE_FUNC_NAME(GetPrenultimateQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemoveCurrentQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemoveLastQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(RemovePrenultimateQueueData)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(WriteDataToQueue)(QUEUE_NAME()* pQueue,uint8_t *data, uint8_t dataLen);
extern uint8_t* QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(QUEUE_NAME()* pQueue,uint8_t* err);
extern uint8_t QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(QUEUE_NAME()* pQueue);
extern uint8_t QUEUE_FUNC_NAME(SerializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes); // 0 - OK
extern uint8_t QUEUE_FUNC_NAME(DeserializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size);


INLINE uint8_t QUEUE_INLINE_NAME(InitQueueEX)(QUEUE_NAME()* pQueue,uint8_t* dataArray, uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast, uint8_t staticInit) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue, dataArray, maxDataItemtSize, N, overrideLast, staticInit);
}

// initialize queue to dynamically allocated memory
INLINE uint8_t QUEUE_INLINE_NAME(InitQueue)(QUEUE_NAME()* pQueue,uint8_t maxDataItemtSize, QUEUE_LENGTH_TYPE N, uint8_t overrideLast) {
	return QUEUE_FUNC_NAME(InitQueueEX)(pQueue,(uint8_t*)malloc(sizeof(uint8_t)*N*maxDataItemtSize),maxDataItemtSize,N,overrideLast,0);
}

INLINE uint8_t QUEUE_INLINE_NAME(FreeQueue)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(FreeQueue)(pQueue);
}

INLINE uint8_t* QUEUE_INLINE_NAME(GetCurrentQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetCurrentQueueData)(pQueue);
}

INLINE uint8_t* QUEUE_INLINE_NAME(GetLastQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetLastQueuedData)(pQueue);
}

INLINE uint8_t* QUEUE_INLINE_NAME(GetPrenultimateQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetPrenultimateQueueData)(pQueue);
}

INLINE uint8_t QUEUE_INLINE_NAME(RemoveCurrentQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemoveCurrentQueueData)(pQueue);
}

INLINE uint8_t QUEUE_INLINE_NAME(RemoveLastQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemoveLastQueueData)(pQueue);
}

INLINE uint8_t QUEUE_INLINE_NAME(RemovePrenultimateQueueData)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(RemovePrenultimateQueueData)(pQueue);
}

INLINE uint8_t QUEUE_INLINE_NAME(WriteDataToQueue)(QUEUE_NAME()* pQueue,uint8_t *data, uint8_t dataLen) {
	return QUEUE_FUNC_NAME(WriteDataToQueue)(pQueue, data, dataLen);
}

INLINE uint8_t* QUEUE_INLINE_NAME(GetNextFreeDataPlaceInQueue)(QUEUE_NAME()* pQueue,uint8_t* err) {
	return QUEUE_FUNC_NAME(GetNextFreeDataPlaceInQueue)(pQueue, err);
}

INLINE uint8_t QUEUE_INLINE_NAME(TakeNextFreePlaceInQueue)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(TakeNextFreePlaceInQueue)(pQueue);
}

INLINE uint8_t QUEUE_INLINE_NAME(SerializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE maxDataSize, DOUBLE_QUEUE_LENGTH_TYPE* writtenBytes) { // 0 - OK
	return QUEUE_FUNC_NAME(SerializeQueue)(pQueue, data, maxDataSize, writtenBytes);
}

INLINE uint8_t QUEUE_INLINE_NAME(DeserializeQueue)(QUEUE_NAME()* pQueue, uint8_t *data, DOUBLE_QUEUE_LENGTH_TYPE size) { // 0 - OK
	return QUEUE_FUNC_NAME(DeserializeQueue)(pQueue, data, size);
}

INLINE DOUBLE_QUEUE_LENGTH_TYPE QUEUE_INLINE_NAME(GetSerializedQueueSize)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetSerializedQueueSize)(pQueue);
}

INLINE QUEUE_LENGTH_TYPE QUEUE_INLINE_NAME(GetItemsCntInQueue)(QUEUE_NAME()* pQueue) {
	return QUEUE_FUNC_NAME(GetItemsCntInQueue)(pQueue);
}

*/
#endif
#endif // __QUEUE_H__
