
#ifndef __DEBUG_PRINTF_H__
#define __DEBUG_PRINTF_H__

// DEBUGPRINTF
// depends on configuration. Use GLOBAL_DEBUGPRINTF_EN for global configuration. Use DEBUGPRINTF_EN locally.

#if defined(WIN) && (defined DEBUGPRINTF_EN || defined GLOBAL_DEBUGPRINTF_EN)
    #include "windows.h"
    #define DEBUGPRINTF(...) {char str[400]; sprintf(str,__VA_ARGS__); OutputDebugStringA(str);}
    #include "convertToHexStr.h"
    #define DEBUG_DATA(data,len,...) PRINT_HEX_DATA(data,len);DEBUGPRINTF(__VA_ARGS__)
#elif defined(LINUX) && (defined DEBUGPRINTF_EN || defined GLOBAL_DEBUGPRINTF_EN)
    #define DEBUGPRINTF(...) printf(__VA_ARGS__);fflush(stdout)
    #include "convertToHexStr.h"
    #define DEBUG_DATA(data,len,...) PRINT_HEX_DATA(data,len);DEBUGPRINTF(__VA_ARGS__)
#endif

#ifndef DEBUGPRINTF
    #define DEBUGPRINTF(...)
#endif
#ifndef DEBUG_DATA
    #define DEBUG_DATA(data,len,...)
#endif

#endif
