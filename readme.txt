The Mikrix system/middleware

Mikrix is distributed modular system based on protothreads. The final design 
works on wide range of processors - from 8-bit microcontrollers without 
operating system to 32 or 64-bit processors with operating system. It is 
suitable for building small or complex multi-pupose devices, which can be joined 
to collective of devices and can be distributed in wide space area. This system 
provides way for easily sharing data between devices. For shared communication 
mediums (RS485, radio) provides specialized medium access method, which enables 
precise time synchronization. Mikrix provides precise time synchronization 
method based on PTP, which enables synchronous data acquisition from different 
devices distributed in wide working area.

For more information, please visit www.mikrix.com
