#ifndef __SWITCH_MESSAGES_H__
#define __SWITCH_MESSAGES_H__
#include "msgGroups.h"


GRP_MESSAGES(MSG_SWITCH_GRP,
		MSG_SWITCH_CHANGED
);



DEFINE_MSG(MSG_SWITCH_CHANGED_T,
	uint8_t switchIndex;
	uint8_t value;
);



#endif
