#ifndef __SET_STATE_MESSAGES_H__
#define __SET_STATE_MESSAGES_H__
#include "msgGroups.h"

enum SET_STATE_T {SET_STATE_OFF = 0, SET_STATE_ON, SET_STATE_BLINK};

GRP_MESSAGES(MSG_SET_STATE_GRP,
			MSG_SET_STATE,
			MSG_SET_ON,
			MSG_SET_OFF,
			MSG_SET_BLINK
);

DEFINE_MSG(MSG_SET_STATE_T,
	uint8_t devIndex;
	uint8_t state;
);

DEFINE_MSG(MSG_SET_ON_T,
	uint8_t devIndex;
);

DEFINE_MSG(MSG_SET_OFF_T,
	uint8_t devIndex;
);

DEFINE_MSG(MSG_SET_BLINK_T,
	uint8_t devIndex;
);


/* helper macros */

#define SendMsgSetOn(ret, moduleID, deviceIndex)\
{MSG_SET_ON_T _myMsg;\
_myMsg.mesage = MSG_SET_ON;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgSetOff(ret, moduleID, deviceIndex)\
{MSG_SET_OFF_T _myMsg;\
_myMsg.mesage = MSG_SET_OFF;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgSetBlink(ret, moduleID, deviceIndex)\
{MSG_SET_BLINK_T _myMsg;\
_myMsg.mesage = MSG_SET_BLINK;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgSetState(ret, moduleID, deviceIndex,stateToSet)\
{MSG_SET_STATE_T _myMsg;\
_myMsg.mesage = MSG_SET_STATE;\
_myMsg.devIndex = deviceIndex;\
_myMsg.state = stateToSet;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#endif
