#ifndef __STATUS_MESSAGES_H__
#define __STATUS_MESSAGES_H__
#include "msgGroups.h"

GRP_MESSAGES(MSG_STATUS_GRP,
			MSG_ON,
			MSG_OFF,
			MSG_BLINK,
			MSG_CHANGE,
);

DEFINE_MSG(MSG_GPIO_STATE_T,
        uint8_t devIndex;
);

DEFINE_MSG(MSG_ON_T,
	uint8_t devIndex;
);

DEFINE_MSG(MSG_OFF_T,
	uint8_t devIndex;
);

DEFINE_MSG(MSG_BLINK_T,
	uint8_t devIndex;
);

DEFINE_MSG(MSG_CHANGE_T,
	uint8_t devIndex;
	uint8_t newValue;
);

/* helper macros */

#define SendMsgGpioState(ret, moduleID, deviceIndex, state)\
{DEBUGPRINTF("calling SendMsgGpioState\n");\
MSG_GPIO_STATE_T _myMsg;\
_myMsg.mesage = state;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgOn(ret, moduleID, deviceIndex)\
{DEBUGPRINTF("calling SendMsgOn\n");\
MSG_ON_T _myMsg;\
_myMsg.mesage = MSG_ON;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgOff(ret, moduleID, deviceIndex)\
{DEBUGPRINTF("calling SendMsgOff\n");\
MSG_OFF_T _myMsg;\
_myMsg.mesage = MSG_OFF;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#define SendMsgBlink(ret, moduleID, deviceIndex)\
{MSG_BLINK_T _myMsg;\
_myMsg.mesage = MSG_BLINK;\
_myMsg.devIndex = deviceIndex;\
ret = SendData((uint8_t*)&_myMsg,sizeof(_myMsg),moduleID);\
}

#endif
