#ifndef __LED_MESSAGES_H__
#define __LED_MESSAGES_H__
#include "msgGroups.h"

enum LED_STATE_T {LED_STATE_OFF = 0,LED_STATE_ON,LED_STATE_BLINK};

GRP_MESSAGES(MSG_LED_GRP,
			MSG_SET_LED_STATE,
			MSG_SET_LED_ON,
			MSG_SET_LED_OFF,
			MSG_SET_LED_BLINK,
			MSG_LED_ON,
			MSG_LED_OFF
);

DEFINE_MSG(MSG_SET_LED_STATE_T,
	uint8_t ledIndex;
	uint8_t value;
);
DEFINE_MSG(MSG_LED_ON_T,
	uint8_t ledIndex;
);

#endif
