/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __MESSAGEGROUPS__H__
#define __MESSAGEGROUPS__H__
#include "inttypes.h"
#include "../../common/common.h"

// define new type of message group
typedef enum {
	MSG_SYSTEM_GRP =1, //not used yet, specifi border for SYSTEM messages
	MSG_BUTTON_GRP,
	MSG_SWITCH_GRP,
	MSG_LED_GRP,
	MSG_SET_STATE_GRP,
	MSG_STATUS_GRP,
	MSG_USER_DEF_GRP = 1000 //messages aviable for specific user messages, which should not be globally visible
} MIKRIX_MSG_GROUP;

#define USERDEF_MSG(n) (MSG_USER_DEF_GRP + n)

typedef uint16_t MSG_Header_t;

#define DEFINE_MSG(msgGroupType,params) typedef struct ATTRIB_ALIGN_8() {\
	MSG_Header_t mesage;\
	params\
	} msgGroupType


// ---------- MESSAGES ----------------
GRP_MESSAGES(MSG_BUTTON_GRP,
		MSG_BUTTON_PRESSED
);



// ----------- MESSAGE TYPE ------------
/* Define your message type
typedef struct {
	uint16_t message;
	... your message params
} MSG_YOUR_MESSAGE_GROUP_T;
*/


DEFINE_MSG(MSG_BUTTON_PRESSED_T,
	uint8_t buttonIndex;
	uint8_t value;
);

#endif
