Unique identifier - UID
UID is number which exactly recognize each board in the world. 

UID fields:

|    1    |    2    |    3    |    4    |    5    |    6    |    7    |    8    |
 xxxx xxxx xxxx xxxx xxxx xxxx yyyy yyyy zzzz zzzz uuuu uuuu uuuu uuuu uuuu uuuu
|           Producer          |Board_ID | USR_DEF |        		SN 		        |

+ Producer: manufacturer number, you get this number after registering
+ Board_ID: Unique ID for board. This number is defined in makefile
	example: 1	ATMEGA_8
+ USR_DEF: not specified, may be used for serial number expansion.
+ SN: Serial Number for boards 

