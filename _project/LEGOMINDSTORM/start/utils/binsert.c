#include <stdio.h>
#include <stdlib.h>

// Utility for inserting a binary file into another (larger) binary file at
// a certain offset
// For Lego NXT it is used to insert the files, filetable, and fileversion into the
// gcc compiled firmware image. These binaries are taken from the actual LEGO firmware (.rfw)
// author: rup
// License: GPL

#define FILL 		0xFF		//valued used to fill unused space in the firmware
#define MAXFLASHADR	0x3FFFF		//max flash adr in NXT

typedef unsigned char byte;		//to read/write from the file

int main (int argc, char *argv[]) {

  FILE *ifp;
  FILE *ofp;

  char *ifpfilename;
  char *ofpfilename;

  unsigned long inOff; 	// offset in input file
  unsigned long outOff;	// offset in output file
  unsigned long len;	// length to copy
  long inSize; 			// size of input file
  long outSize;			// size of output file

  byte * inbuffer;
  byte * outbuffer;

  int i;

  // program name is first argument
  if(argc != 6)
  {
    printf("utility to insert the file table parts from a standard firmware to a custom firmware\n");
    printf("binsert usage: binary_input_file, binary_output_file, offset length\n");
    printf("Example: ./binsert LEGO_MINDSTORMS_NXT_Firmware_v1.03.bin 0x3FFFC 10 m_sched.bin 0x2FFFC\n");
    return 1;
  }

  printf("\nStarting binsert...\n");

  ifpfilename = argv[1];
  printf("input_file: %s\n", ifpfilename);

  inOff = strtoul(argv[2], NULL, 16);
  printf("inputfile offset: %d (0x%X)\n", inOff, inOff);

  len = strtoul(argv[3], NULL, 10);
  printf("length: %d bytes\n", len);

  ofpfilename = argv[4];
  printf("output_file: %s\n", ofpfilename);

  outOff = strtoul(argv[5], NULL, 16);
  printf("outputfile offset: %d (0x%X)\n",outOff, outOff);

  //check inOff and len
  if(inOff + len - 1 > MAXFLASHADR){
    printf("Error: inputfile offset (0x%X) + length (%d) -1 (=0x%X)is too big for flash 256K (max. addr 0x%X)\n",inOff, len, inOff + len - 1, MAXFLASHADR);
    return 1;
  }

  //check outOff and len
  if(outOff + len - 1 > MAXFLASHADR){
    printf("Error: outputfile offset (0x%X) + length (%d) -1 (=0x%X)is too big for flash 256K (max. addr 0x%X)\n",outOff, len, outOff + len - 1, MAXFLASHADR);
    return 1;
  }

  if((ifp = fopen(ifpfilename, "rb")) == NULL)
  {
    printf("Error: Could not find inputfile: %s\n", ifpfilename);
    return 1;
  }

  if((ofp=fopen(ofpfilename, "rb")) == NULL)
  {
    printf("Error: Could not find outputfile: %s\n", ofpfilename);
    return 1;
  }

  //check intputfile
  fseek(ifp, 0, SEEK_END);
  inSize = ftell(ifp);
  printf("inputfile size: %d bytes\n", inSize);
  if(inSize < inOff + len){
    printf("Error: input file too small for input offset + length.\n");
    return 1;
  }
  rewind(ifp);

  //check outputfile
  fseek(ofp, 0, SEEK_END);
  outSize = ftell(ofp);
  printf("outputfile size: %d bytes\n", outSize);
  if(outSize < outOff + len){
    printf("Error: Outputfile size too small for output offset + length.\n");
    return 1;
  }
  rewind(ofp);

  // read len bytes from input file
  inbuffer = (char*) malloc(len);
  fseek(ifp, inOff, SEEK_SET);
  fread (inbuffer, sizeof(char), len, ifp);
  fclose(ifp);

  outbuffer = (char*) malloc(outSize);
  fseek(ofp, 0, SEEK_SET);
  fread (outbuffer, sizeof(char), outSize, ofp);
  fclose(ofp);

  // sanity check that output is 0xFF which should be the fill value. (I do this FILL in the make script)
  // only show one warning
  int show = 1;
  for(i=0; i < len; i++){
    if(outbuffer[outOff + i] != FILL && show == 1){
      printf("First (and only) warning: outbuffer[0x%X] = 0x%X != 0xFF. Firmware image code is (perhaps) about to be overwritten! Expect human behaviour:-)\n", \
      outOff +i, outbuffer[outOff + i]);
      show = 0;
    }
  }

  // Copy len bytes from input image into output image
  memcpy(&outbuffer[outOff], &inbuffer[0], len);

  // write to output file
  if((ofp = fopen(ofpfilename, "wb")) == NULL)
  {
    printf("Error: Could not reopen %s for writing\n", ofpfilename);
    return 1;
  }

  for(i = 0; i < outSize; i++)
  {
    putc(outbuffer[i], ofp);
  }
  fclose(ofp);

  printf("\nbinsert is done...\n");

  free(inbuffer);
  free(outbuffer);

  return 0;
}
