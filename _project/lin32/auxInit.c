/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
#include "auxInit.h"
#include "../../sys/system.h"
#include "../../sys/moduleManager.h"
#include "../../sys/communicationManager.h"
#include "../../sys/configurationManager.h"
#include "../../sys/maskAddrManager.h"
#include "time.h"
#include "sysHW.h"
#include "../../sys/threadManager.h"
#include "crossPlatformTimer.h"
/*
#include "DefaultModules.h"
#include "ThreadManager.h"
#include "ModuleManager.h"
#include "CommunicationManager.h"
#include "System.h"
#include "TimeManager.h"
#include "sysHW.h"
#include "MaskAddrManager.h"
*/

uint8_t tst1(MODULE_DATA *data,pt* thr){
	PT_BEGIN(thr);
	//printf("System Starting %d,%d",initFuncTbl[0], sizeof(*initFuncTbl));	
	PRINTF("Deleting Module 1");
	DeleteModule(0);
	//allModules[1]->Init(moduleObj[0].data);
	//initFuncTbl[1].Function();
	//PushButtonInit();
	PT_END(thr);

}



uint8_t SendThr1(MODULE_DATA *data,  pt* thr){
	static 	uint32_t data1=0xCCFFAABB;
	static uint16_t d=0xAABB;
	static uint8_t c=0x66;
	static uint8_t buf[20];
	uint8_t i;
	ADDR a={ {0x1110},1};
	PT_BEGIN(thr);
	for(i=0; i<20;i++){
		buf[i]=i;
	}
	PT_WAIT_WHILE(thr, SendData(buf,20,2));

	PT_WAIT_WHILE(thr, SendData((uint8_t*)&data1,4,15));

	SendTimeSync(&interfejs[COMM_INTERFACE_SOCKET_MULTICAST],TimeRequest,(uint64_t*)NULL);
	PT_YIELD(thr);
	SendTimeSync(&interfejs[COMM_INTERFACE_SOCKET_MULTICAST],TimeFollow,(uint64_t*)NULL);
	PT_YIELD(thr);
	SendTimeSync(&interfejs[COMM_INTERFACE_SOCKET_MULTICAST],TimeCorrectionRequest, (uint64_t*)NULL);

	PT_WAIT_WHILE(thr, SendData((uint8_t*)&d,sizeof(d),15));

	PT_WAIT_WHILE(thr, SendCmdReq(&interfejs[COMM_INTERFACE_SOCKET_MULTICAST],&a,2,(uint8_t*)&d,sizeof(d)));

	SendCmdResp(&interfejs[COMM_INTERFACE_SOCKET_MULTICAST],3,10,55,&c,1);

	DeleteThread(thr);
	PT_END(thr);

}

uint8_t SimulModule1(MODULE_DATA *data,  pt* thr){
	uint32_t data1= 0;
	PT_BEGIN(thr);
	PT_WAIT_WHILE(thr, SendData((uint8_t*)&data1,4,IsMaster()?3:1)); //simulacia zaslania dat z modulu 1
	DeleteThread(thr);
	PT_END(thr);
}

uint8_t SimulModuleDelete(MODULE_DATA *data,  pt* thr){
	static uint8_t count=0;
	PT_BEGIN(thr);
	PT_WAIT_WHILE(thr,count++<3);
	DeleteModule(2);

	DeleteThread(thr);
	PT_END(thr);

}


void TestConfigurationManager(){
	//test configuration manager
	InitConfigurationManager();
	uint16_t x=10;
	DATA d;
	d.data = (uint8_t*)&x;
	d.len = sizeof(x);
	SetModuleConfig(0,&d);
	SetModuleConfig(1,&d);
	SetModuleConfig(2,&d);
	x=0x55;
	SetModuleConfig(1,&d);
	uint32_t y=0x66;
	d.data = (uint8_t*)&y;
	d.len = sizeof(y);
	SetModuleConfig(1,&d);
	uint8_t z=0x77;
	d.data = (uint8_t*)&z;
	d.len = sizeof(z);
	SetModuleConfig(0,&d);
	d.data = (uint8_t*)&y;
	d.len = sizeof(y);
	SetModuleConfig(2,&d);
	d.data = (uint8_t*)&z;
	d.len = sizeof(z);
	SetModuleConfig(2,&d);
	SetModuleConfig(3,&d);
	DATA *pd;
	pd=GetModuleConfig(2);
	SaveModulesConfig();
	DeleteModuleConfig(3);
}


#define MAX_DEF_MOD_ARRAY_LEN 10



void PrintModules(MODULE_INFO* info, uint16_t len){
	int i;
	PRINTF("STATIC MODULES\n");
	PRINTF("ID\t UID\t\t NAME\t\t\n");
	for (i =0;i<len;i++){
		PRINTF("%d\t 0x%.8x\t%s\n",i, info[i].moduleID,info[i].name);
	};
	PRINTF("\n");
};


INLINE void PrintMaskTable(){
	uint8_t i;
	PRINTF("MASK TABLE:\n");
	for( i = 0; i< MASK_ADDR_TABLE_SIZE;i++){
		
	}
}
/*
void PrintThreads(){
	uint8_t i=0;
	
	PRINTF("Thread\tThrFunc \tMemory\n");
	do{
		PRINTF("%d\t0x%x\t0x%x\n",i,threads[i].thrFunc,threads[i].memoryContext);
		i=threads[i].next;
	}while(i!=NOTHREADS);
}
*/

static void PrintTimerTable(){
	uint8_t i;
	PRINTF("Timer Table\n");
	for(i=0;i<MAX_DELAY_VALUES;i++){
		PRINTF("%.4x ",mDelayList.delayArray[i].time);
	}
	PRINTF("\n");
	for(i=0;i<MAX_DELAY_VALUES;i++){
		PRINTF("%.3d  ",mDelayList.delayArray[i].nextInd);
	}
	PRINTF("\nfreeEnd: %d",mDelayList.freeEnd);
	PRINTF("\nfreeStart: %d",mDelayList.freeStart);
	PRINTF("\nstartInd: %d\n",mDelayList.startInd);
}

void PrintThreads(){
	uint8_t i=0;
	
	PRINTF("\nThread\tThrFunc \tMemory\n");
	do{
		PRINTF("%d\t0x%x\t0x%x\n",i,threads[i].thrFunc,threads[i].memoryContext);
		i=threads[i].next;
	}while(i!=NOTHREADS);
}

void TestNumbers(){
int aa=sizeof(time_t);
	
	uint16_t a = 60000;
	uint16_t b = 500;
	uint16_t c = b-a;
	uint16_t data=0xAABB;
	uint32_t data1=101010;
	uint8_t lenA =10;
	//PRINTF("data:0x%x, hi:0x%x lo0x%x",data,((I16*)&data)->hi,((I16*)&data)->lo);
	//static char i=0,x=0;
	//MODULE_DATA m;
	c=~c+1;
}
//#include "../../sys/CRC16.h"

MODULE_INFO info[MAX_DEF_MOD_ARRAY_LEN];

void AuxiliarySystemInit(){
	uint8_t i,lenA;
	uint16_t len;
	ADDR address;
	address.devAddr.hi = 0;
	address.devAddr.lo = 0;
	address.IDD = 0;
	//len=crc_ccitt_update (10,20);
	//IntitSendBuf();
	//Test COM Port
	//com.sendChar('A');
	PRINTF("UID:");
	for(i=sizeof(UID); i;i--) {
		PRINTF("%.2x ",((uint8_t*)&uid)[i-1]);
	}
	PRINTF("\n");
	
	StartTimer();
	Sleep(1);
	PRINTF("Timer precision:%f\n",StopTimer(0));
	
	//AddThread(&m,System);
	
	//View list of modules
	len= sizeof(MODULE*);
	len=GetDefaultModulesLen();
	len=MAX_DEF_MOD_ARRAY_LEN;
	GetDefaultModulesInfo((MODULE_INFO *)&info,&len);
	
	PrintModules(info,len);

	AddModule(0);//System
	if(IsMaster()){
		AddModule(1);//AddressManager
		AddModule(2); //command
	}
	AddModule(4); //Increment 
	AddModule(3); //PushButton
	
	//PrintThreads();
	//MoveModuleScheduling(3,4);
	//AddModule(2);//Increment
	// for testing 
	//AddModule(1);
	//AddModule(1);
	MODULE_LIST ml[10];
	lenA = 10;
	GetCreatedModuleList(ml,&lenA);
	//print added modules
	
	PRINTF("Running modules\n");
	PRINTF("IDD\tName\t\t\tmoduleID\n");
	for( i=0; i<lenA;i++){
		PRINTF("%d\t%s\t\t0x%.8x\n",ml[i].id,ml[i].module->info->name, ml[i].module->info->moduleID );
	}
	PrintThreads();
	PrintTimerTable();
	
		
	MASK mask;
	
	address.devAddr.hi=10;
	address.devAddr.lo=0;
	address.IDD = 3;
	mask.mask.devAddr.data = -1;
	mask.mask.IDD = -1;
	mask.dataOffset = 0;
	mask.dataOperation = COPY;
	mask.dataSize = 4;
	mask.moduleDataInOffset = 0;
	mask.varIdentifier = 1;
	if(IsMaster()){
		mask.OnData = GetModule(3)->OnData;
		mask.moduleData = GetModuleData(3);
	}else{
		mask.OnData = GetModule(1)->OnData;
		mask.moduleData = GetModuleData(1);
	}
	mask.srcAddr = address; 
	AddMaskAddr(&mask);
	//AddThread(&m,tst1);
	//AddThread(NULL,SendThr1);
	
	//DeleteModule(2);
	//AddModule(1);
	AddThread(NULL,SimulModule1);
	//AddThread(NULL,SimulModuleDelete);
	//PrintThreads();

	//TODO:: mask addr 0.0.0 pre systémové volania, pokiaž nie je definovaná adresa

/*	
	CreateDataPacket(GetSystemAddrP(),(uint8_t *)&data,2,3,0,0,0,DataPacket);
	SendFirstPacket();
	SendFirstPacket();
	CreateDataPacket(GetSystemAddrP(),(uint8_t *)&data1,4,6,0,0,0,DataPacket);
	CreateDataPacket(GetSystemAddrP(),(uint8_t *)&data,2,3,0,0,0,DataPacket);
	SendFirstPacket();
	CreateDataPacket(GetSystemAddrP(),(uint8_t *)&data1,4,6,0,0,0,DataPacket);
	SendFirstPacket();
	SendFirstPacket();
	SendFirstPacket();
	CreateDataPacket(GetSystemAddrP(),(uint8_t *)&data,2,3,0,0,0,DataPacket);
	SendFirstPacket();
*/
}