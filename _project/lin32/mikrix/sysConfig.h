/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYSCONFIG__H__
#define __SYSCONFIG__H__



//#configure system parameters
//sys/system.h
#define MAX_SYSTEM_NAME_LEN 20
//sys/communicationInterface.h
#define MAX_INTERFACES 2				//max. number of communication interfaces
//platform/include/serialInterface.h
#define MAX_SERIAL_INTERFACE_NAME_SIZE 20 //max. serial port string length
//moduleDef.h
#define MAX_PLATFORM_MODULES_ARRAY 10			//maximalnumber of modules
//ThreadManager.h
#define MAX_THREADS 20					//maximal count of threads (do not forget, each module takes minimum one thread)
//request manager
//#define MAX_REQESTS 10
//ConfigurationManager.h
#define MAX_CONFIG_BUF 128				//maximal size of configuration buffer
//responseManager.h
#define MAX_RESPONSES 4					//max number of response requsts in system
//ModuleManager.h
#define MAX_MODULE_OBJECTS	15			//max. number of created modules in memory
//MaskAddrManager.h
#define MASK_ADDR_TABLE_SIZE	20		//max. number of mask adress in system
//CommunicationManager.h
#define SEND_BUFF_SIZE		256			//LENGTH OF SEND BUFFER MUST BE POWER oF TWO - 1 , max. size of sending buffer
#define MAX_SEND_BUFFERS	4			//max. count of sending buffers
#define INBUF_SIZE			256			//input buffer size
#define INBUF_FRAME_COUNT	2			//input buffer (data frames) count
#define TIME_SYNC_SENDING_DELAY_CORRECTION  5	//[sec] time sync repating interval, 5 is good value
//HW dependant - CommunicationInterface.h
#define COMMSPEED			10000000	// speed of Ethernet communication


//#define TIMER_COMPENSATE				//enable timer variations compensation
#define SIMPLE_TIMER					// enable simple timer without emulation basic HW timer in continous mode with seuped top.


#endif

//Address manager common 
#define NODE_TABLE_SIZE 255 //default 10