/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYSCONFIG__H__
#define __SYSCONFIG__H__



//#configure system parameters
//sys/system.h
#define MAX_SYSTEM_NAME_LEN 20
//sys/communicationInterface.h
#define MAX_INTERFACES 2				//max. number of communication interfaces
//platform/include/serialInterface.h
#define MAX_SERIAL_INTERFACE_NAME_SIZE 20 		//max. serial port string length
//moduleDef.h
#define MAX_PLATFORM_MODULES_ARRAY 10			//maximalnumber of modules
//#define MAX_MODULE_NAME_LEN 25			//set maximal module name string length. If not set, default value is 25
//ThreadManager.h
#define MAX_THREADS 20					//maximal count of threads (do not forget, each module takes minimum one thread)
#define DEFAULT_SYS_WAKEUP_TIME_US 0	//wakeup thread polling time, when no needed to wakeup - set 0. Default 0
//define if open-loop application is needed
//#define MIKRIX_OPEN_LOOP
//#define SYS_MAINLOOP_DEBUG

//common/oneTypeAlloc.h
#define USE_ATOMIC_ALLOC                                //support multithread allocation calls

//request manager
//#define MAX_REQESTS 10
//ConfigurationManager.h
#define MAX_CONFIG_BUF 128				//maximal size of configuration buffer
//responseManager.h
#define MAX_RESPONSES 4					//max number of response requsts in system
//ModuleManager.h
#define MAX_MODULE_OBJECTS	15			//max. number of created modules in memory
//MaskAddrManager.h
#define MASK_ADDR_TABLE_SIZE	20		//max. number of mask adress in system
//CommunicationManager.h
#define SEND_BUFF_SIZE		256			//LENGTH OF SEND BUFFER MUST BE POWER oF TWO - 1 , max. size of sending buffer
#define MAX_SEND_BUFFERS	4			//max. count of sending buffers
#define INBUF_SIZE			256			//input buffer size
#define INBUF_FRAME_COUNT	2			//input buffer (data frames) count
//#define MAX_SEND_CMD_WAIT_TIME_US 	10000000ULL	//set maximum wait time for sending command (ThrSendCmd). IF not set, no timeout is used.Default not used
//moduleManager.h
#define MAX_CALLBACK_CMD_RESP_BUF	100
#define TIME_SYNC_SENDING_DELAY_CORRECTION  5	//[sec] time sync repating interval, 5 is good value
//HW dependant - CommunicationInterface.h
#define COMMSPEED			10000000	// speed of Ethernet communication


//#define TIMER_COMPENSATE				//enable timer variations compensation
#define SIMPLE_TIMER					// enable simple timer without emulation basic HW timer in continous mode with seuped top.

//setup SSL client and server parameters
#if defined USE_SSL_CLIENT || defined USE_SSL_SERVER

// SSL CLIENT specific configuration
//#define SSLCLIENT_SOCKET_CONNECT_TIMEOUT 10000 // default is in SSLClient.h
#define SSL_DEFAULT_READ_BUF_SIZE 4096
#define SSL_MAX_FILE_NAME 80
#define SSL_MAX_KEY_PASSPHRASE 20
#define SSL_ERROR_MSG_MAX_LEN 240
//#define SSL_CLIENT_TCP_SOCK_PRIO 7 //set tcp sock priority 0-7 - max. Default: system normal priority

#define SSL_SERVER_MAX_CONNECTIONS 50

//#define SSLCLIENT_PROTOCOL_METHOD SSLv3_method //specify your protocol method. Default TLS
//#define SSLSERVER_PROTOCOL_METHOD SSLv3_method //specify your protocol method. Default TLS

#endif

#define LIST_USE_MALLOC

//platfrom/serial.c
//#define SERIAL_DEBUG_MSGS_EN	//enable debug messages


//util/bacupData.c

#define MAX_BACKUP_DATA_COPIES 1 //specify number of copies for backup data storage
//#define BACUP_DATA_DEBUG_EN 	//enable debug messaged for backupData

//platform/lin-win/common/flatFileStorage.c
#define FFS_ASYNC_WRITE			//enable asynchronnous write in writting thread. Do not wait for storing data
#define FFS_ASYNC_PIPE_SIZE 524288 //default 65536. Use only multiply by page size. Default 16 pages (4096). Max pipe size /proc/sys/fs/pipe-max-size
//#define FFS_SHOW_DEBUG_INFO	//enable debug messages
#endif

//Address manager common 
#define NODE_TABLE_SIZE 255 //default 10

//Global enable debugging messages
//#define GLOBAL_DEBUGPRINTF_EN
// EN/DI debugging messages
//systemModule.c
//#define SYSTEM_MODULE_DBG_MSG_EN
// communicationManager.c
//#define COMMUNICATION_MANAGER_DBG_MSG_EN
//UDPConnection.c
//#define UDP_CONNECTION_DBG_MSG_EN
//TCPClient.c
//#define TCP_CLIENT_DBG_MSG_EN
//socketCommon.c
//#define SOCKET_COMMON_DBG_MSG_EN
//interruptableSleep.c
//#define INTERRUPTABLE_SLEEP_DBG_MSG_EN
//responseManager.c
//#define RESPONSE_MANAGER_DBG_MSG_EN
//threadManager.c
//#define THREAD_MANAGER_DBG_MSG_EN
//timeManager.c
//#define TIME_MANAGER_DBG_MSG_EN
//moduleManager.c
//#define MODULE_MANAGER_DBG_MSG_EN
//TCPClientInterface.c
//#define TCP_CLIENT_INTERFACE_DBG_MSG_EN
//UDPCommInterface.c
//#define UDP_COMM_INTERFACE_DBG_MSG_EN
//generalInterface.c
//#define GENERAL_INTERFACE_DBG_MSG_EN
//SSLClient.c
//#define SSL_CLIENT_DBG_MSG_EN
//SSLServer.c
//#define SSL_SERVER_DBG_MSG_EN
