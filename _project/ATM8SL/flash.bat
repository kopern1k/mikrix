@echo off
setlocal
set path=D:\Prog_Jaz\procesory\WinAVR\utils\bin;D:\Prog_Jaz\procesory\WinAVR\bin;%path%
rem  Mega16     programator    erase   
avrdude -pm32 -cavrisp2 -Pusb      -e   -u^
		-U flash:w:bin\system.hex  ^
		-U hfuse:w:0x89:m	^
		-U lfuse:w:0xef:m		rem programuj dolne fuse bity		
rem		-U eeprom:w:eeprom.hex ^
rem		-U efuse:w:0xff:m	^    rem programuj extended fuse bity

rem povodne nastavenie: -U hfuse:w:0x99:m -U lfuse:w:0xe4:m pre 8Mhz internych
rem  nastavenie: -U hfuse:w:0x89:m -U lfuse:w:0xef:m pre 8Mhz-16 externych
rem 				hfuse:w:0xc9:m -> JTAG vypnuty
endlocal
pause
