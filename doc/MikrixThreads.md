# Mikrix threads
Mikrix threads are based on protothreads*, which  are lightweight threads without stack. Therefore if you are using protohtreads, variables which are used in second round of executing thread variables must not be temporary. It meams, you should use static or global or module variables to store your data. When you creating new thread, you can put optional pointer to data as a parameter. This parameter will be stored by mikrix thread for you.
Mikrix threads are different from standard system threads, because they are executed every time until `DeleteThread(Thr)` is called.

Look to the code which implement Mikrix thread callback function
```
uint8_t MyThread(void *data, pt* Thr) {
    PT_BEGIN(Thr)
    ...
    your code
    PT_WAIT_WHILE(Thr, your condition); // wait for condition occur
    if(...) {
    	DeleteThread(Thr);		//Delete thread from scheduling in main loop
    }
    ...
    PT_END(Thr)
}
```
`Thr` variable identifies where your application will jump when Thread is executed. This variable and data variable are stored by system for each tread.

*You can read more about protothreads [here](http://dunkels.com/adam/pt/).

## Using Mikrix Threads
Mikrix thread can be easiliy add to sheduling by calling `AddThread((void*)data, MyThread);` function. For Removing thread from scheduling call `DeleteThread(Thr);` function.


