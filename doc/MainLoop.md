# Main loop

Threads in Mikrix main lopp are executed by round robin algorithm. At the end of the loop, when all threads are executed, algorithm decides to execute loop again. Main loop will sleep execution only for necessary time.
Main loop will not sleep if:

* any of executed treads return PT_ENDED
* any of timers is pending
* any of modules has pending data in queues
* anyone set forceNotSleepMainLoop variable

System can be interrupted from sleep by storing data to module queue.

Note:
Main loop can be executed only once by enabling MIKRIX_OPEN_LOOP definition. This configuration is usefull when Mikrix is used in frameworks like Arduino.