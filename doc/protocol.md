#Mikrix protocol


- ####Standard communication

|    1    |    2    |    3    |    4    |    5    |    6    |    7    |    8    |    9    |
|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|0100 0000|xxxx xxxx|yyyy yyyy|yyyy yyyy|zzzz zzzz|uuuu uuuu|uuuu uuuu|vvvv vvvv|vvvv vvvv|
|HEAD-0000| LENGTH  |     DEV. ADDR    ||   IDD   |     N x DATA     ||      CRC16      |.|


- ####Block communication

|    1    |    2    |    3    |    4    |    5    |    6    |    7    |    8    |    9    |    10   |
|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|0100 1000|xxxx xxxx|yyyy yyyy|yyyy yyyy|zzzz zzzz|nnnn nnnn|uuuu uuuu|uuuu uuuu|vvvv vvvv|vvvv vvvv|
|HEAD-1000| LENGTH  |     DEV. ADDR    ||   IDD   |  COUNT  |      N x DATA    ||      CRC16      |.|

- #### Command Request

|    1    |    2    |    3    |    4    |    5    |    6    |    7    |    8    |    9    |
|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|0110 xxxx|xxxx xxxx|yyyy yyyy|yyyy yyyy|zzzz zzzz|cccc cccc|uuuu uuuu|vvvv vvvv|vvvv vvvv|
|HEAD-0000| LENGTH  |     DEST. ADDR   ||   IDD   | COMMAND |N x DATA |      CRC16      |.|

- #### Command response

|    1    |    2    |    3    |    4    |    5    |    6    |    7    |    8    |    9    |   10    |
|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|0111 xxxx|xxxx xxxx|yyyy yyyy|yyyy yyyy|zzzz zzzz|cccc cccc|eeee eeee|uuuu uuuu|vvvv vvvv|vvvv vvvv|
|HEAD-0000| LENGTH  |     DEV. ADDR     |   IDD   | OrigCMD |   ERR   |N x DATA |      CRC16      |.|


- #### Time sync
 1. sync req.
|    1    |    2    |    3    |    4    |    5    |~     ~|   10    |    11   |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0000|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz| zzzz  |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0000|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

 2. sync. follow
|    1    |    2    |    3    |    4    |    5    |~     ~|    10   |    11   |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0001|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz|  zzzz |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0001|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

 3. compensation req.
|    1    |    2    |    3    |    4    |    5    |~     ~|    10   |     11  |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0010|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz|  zzzz |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0010|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

 4. compenastion resp.
|    1    |    2    |    3    |    4    |    5    |~     ~|   10    |    11   |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0011|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz|  zzzz |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0011|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

 5. correction req.
|    1    |    2    |    3    |    4    |    5    |~     ~|    10   |    11   |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0100|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz|  zzzz |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0100|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

 6. correction follow
|    1    |    2    |    3    |    4    |    5    |~     ~|    10   |    11   |    12   |   13    |
|---------|---------|---------|---------|---------|-------|---------|---------|---------|---------|
|0101 0101|yyyy yyyy|yyyy yyyy|zzzz zzzz|zzzz zzzz|  zzzz |zzzz zzzz|zzzz zzzz|vvvv vvvv|vvvv vvvv|
|HEAD-0101|     DEV. ADDR    ||  TIME ~ |   ~~~   | ~(8B)~|   ~~~   | ~STAMP  |      CRC16      |.|

CRC16: Polynome x16 + x12 + x5 + 1, initialized with 0xFFFF
