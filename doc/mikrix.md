# Mikrix
Mikrix is middleware for small or huge distributed systems, which can run on embedded devices without operating system or run on without operating system. It is also suitable for building small or complex multi-pupose devices, which can be joined to collective of devices and can be distributed in wide space area.

Main features are:
* Modular design
* Actor model development
* Dynamic modules
* Time synchronization support
* Lightweight threads - protothreads
* Small footprint
* Portable (run from 8-bit microcontrollers to 32/64bit systems with - Linux, Windows, MAC OS)
* Run on Arduino, Raspberry, STM discovery kits, ...
* Support power managemet
* Open source

