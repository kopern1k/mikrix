/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "stdio.h"
//#include "stdlib.h"
#include "increment.h"
#include "..\\mikrix.h"



//#include "TimerManager.h"

/*
Desc: Increment receaved data, no need for storing output variables
*/

#define DATATRANSFER	INTERNAL_DATA_TRANSFER

//commands .. not needed in this module
static uint8_t GetData(MODULE_DATA* pModule,uint8_t *inData, uint8_t inLen,CMD_DATA_OUT *outData){
	PRINTF("GetData invoked");
	
	return 0;
};


static uint8_t	OnData(ADDR *srcAddr, uint8_t *data, uint8_t len, uint8_t dataOffset, uint8_t dataLen, MODULE_DATA *moduleData, uint8_t moduleInOffset, DATA_OP dataOperation, HAVE_DATA_MASK	varIdentifier){
	if(dataOffset + dataLen >len){
		return 1;
	}
	if( moduleData->inData.len < (dataOffset + dataLen) ){
		return 2;
	}
	switch(dataOperation){
		case COPY:
			CopyMem((moduleData->inData.data + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case SET:
			SetMem((moduleData->inData.data + moduleInOffset),0xFF,dataLen);
			break;
		case RESET:
			SetMem((moduleData->inData.data + moduleInOffset),0x00,dataLen);
			break;
		case XOR:
			XorMem((moduleData->inData.data + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case AND:
			AndMem((moduleData->inData.data + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case NEG:
			NegMem((moduleData->inData.data + moduleInOffset),(data+dataOffset),dataLen);
			break;
		default:
			break;
	}
	//set havedata
	set(moduleData->haveData,varIdentifier); 
	return 0;
}

static uint8_t GetConfig(MODULE_DATA* pModuleData){
	DATA *pData;
	pData=GetModuleConfig(pModuleData->IDD);
	if(pData->data == NULL) return 1;
	
	//...
	

	return 0;
}

//#define cmd1 {GetData,GetInType,GetOutType,CheckInParams,"GetData"}
//#define cmd2 {GetData,GetInType,GetOutType,CheckInParams,"GetData"}
static MODULE_INFO	module_info={MODULEID,MODULENAME};
//static MODULE_CMD cmds[]={cmd1};
//static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};
static uint8_t ModuleMain(MODULE_DATA* pModule,pt *Thr);
//uint8_t INIT(MODULENAME_TAG)();
MODULE	MODULENAME_TAG={ &module_info,(MODULE_CMD_TABLE*)NULL,ModuleMain,OnData,INIT(MODULENAME_TAG),DEINIT(MODULENAME_TAG),NULL,GetConfig  }; //can be in flash memory
//set module parameters, create instance of module, return address of module



uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data){
	
	PRINTF("Increment initializing...\n");
	if(data==NULL) return 1;
	data->transferMode = DATATRANSFER;
	data->outData.data = (uint8_t*)malloc(TYPE_LEN[INT64_T]);
	data->outData.len = (uint8_t)TYPE_LEN[INT64_T];
	data->inData.data =	(uint8_t*)malloc(TYPE_LEN[INT32_T]);
	data->inData.len =  (uint8_t)TYPE_LEN[INT32_T];
	*data->outData.data = 0;
	*data->inData.data=2;
	data->haveDataMask = 1;
	data->haveData = 0;
	set(DDRB,BIT0|BIT1); //output PB0
	return 0;
}


uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF("Increment finishing...\n");
	free(data->outData.data);
	free(data->inData.data);
	data->outData.data=NULL;
	data->inData.data=0;
	data->outData.len =0;
	data->inData.len =0;
	data->transferMode = 0;
	return 0;
}

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	PT_BEGIN(Thr);
	while(1){
		PT_WAIT_WHILE(Thr,pModule->haveData == 0);
		pModule->haveData = 0;	
		set(PORTB,BIT0);
		xor(PORTB,BIT1);
		*pModule->inData.data +=1; 
		clear(PORTB,BIT0);		
		SendData(pModule->inData.data,pModule->inData.len,pModule->IDD);
	}
	PT_END(Thr);
}
