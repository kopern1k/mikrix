/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

//#include <stdio.h>
//#include <stdlib.h>
#include "pushButton.h"
//#include "timerManager.h"

#define DATATRANSFER	NORMAL_DATA_TRANSFER
static uint8_t GetData(MODULE_DATA* pModule,uint8_t *inData, uint8_t inLen,CMD_DATA_OUT *outData){
	
	return 0;
};
static uint8_t GetInType(uint8_t *type, uint8_t *len){
	*type=(TYPE)NULL_T;
	*len=TYPE_LEN[NULL_T];
	return 0;
}
static uint8_t GetOutType(uint8_t *type, uint8_t *len){
	*type=(TYPE)BOOL_T;
	*len=1;
	return 0;
}

static uint8_t CheckInParams(uint8_t *type, uint8_t len){
	if(*type==(TYPE)BOOL_T) return 0;
	return 1;
}

#define cmd1 {GetData,GetInType,GetOutType,CheckInParams,"GetData"}
#define cmd2 {GetData,GetInType,GetOutType,CheckInParams,"GetData"}
static MODULE_INFO	module_info={MODULEID,MODULENAME};
static MODULE_CMD cmds[]={cmd1,cmd2};
static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};
static uint8_t ModuleMain(MODULE_DATA* pModule,pt *Thr);
//uint8_t INIT(MODULENAME_TAG)();
MODULE	MODULENAME_TAG={ &module_info,(MODULE_CMD_TABLE*)&cmdTbl,ModuleMain,NULL,INIT(MODULENAME_TAG),DEINIT(MODULENAME_TAG) }; //can be in flash memory
//set module parameters, create instance of module, return address of module
uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data){
	
//	printf("InitTest");
	if(data==NULL) return 1;
	data->transferMode= DATATRANSFER;
	data->outData.data = (uint8_t*)malloc(TYPE_LEN[BOOL_T]);
	data->outData.len = (uint8_t)TYPE_LEN[BOOL_T];
	data->inData.data=	(uint8_t*)malloc(TYPE_LEN[INT8_T]);
	data->inData.len = (uint8_t)TYPE_LEN[INT8_T];
	*data->outData.data =0;
	*data->inData.data=2;
	return 0;
}

uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data){
	if(data == NULL) return 1;
	//printf("InitTest");
	free(data->outData.data);
	free(data->inData.data);
	data->outData.data=0;
	data->inData.data=0;
	data->outData.len =0;
	data->inData.len =0;
	data->transferMode = 0;

	return 0;
}

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	static uint8_t cnt=0;
	PT_BEGIN(Thr);
//	while(1){
	//printf(MODULENAME " data:%d",*pModule->outData.data);	
		cnt++;
		if(cnt&0x01){
			*pModule->outData.data=1;
		}else{
			*pModule->outData.data=0;
		}
//	}
	PT_END(Thr);
}
