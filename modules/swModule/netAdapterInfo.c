/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "netAdapterInfo.h"
#include "../mikrix.h"

INLINE uint8_t* NetAdapterInfoAllocMem(uint8_t size) {
	return SysAllocMem(size);
}

INLINE uint8_t NetAdapterInfoFreeMem(uint8_t *pData) {
	return SysFreeMem(pData);
}


#define MAX_NET_ADAPTERS 3

static uint8_t COMMAND(GetNetAdaptersInfo)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData) {
	//get adapter count

	NetAdaptersInfo_t* pNetAdapters = (NetAdaptersInfo_t*) NetAdapterInfoAllocMem(sizeof(MSG_GetNetAdaptersInfo_Out_t) + sizeof(NetAdapterInfo_t) * MAX_NET_ADAPTERS);
	if(pNetAdapters == NULL) {
		return 100;
	}


	int8_t err = GetNetAdaptersInfo(pNetAdapters, MAX_NET_ADAPTERS);
	if(err){
		NetAdapterInfoFreeMem((uint8_t*)pNetAdapters);
		return 2;
	}

	outData->len  = sizeof(MSG_GetNetAdaptersInfo_Out_t) + sizeof(NetAdapterInfo_t) * pNetAdapters ->adapterCnt;
	outData->data = (uint8_t*) pNetAdapters;
	outData->FreeFunct = NetAdapterInfoFreeMem;
	return 0;
}


CREATE_COMMAND_MODULE(MODULENAME,ALL_COMMANDS);

#define IN_DATA_ELEMENT_SIZE 0
#define IN_DATA_DEPTH 0
uint8_t INIT(MODULENAME)(MODULE_DATA* data){
	if(data==NULL) return 1;
	PRINTF(TAG_TO_STRING(MODULENAME) " initializing ...[%d]\n",data->IDD);
	data->transferMode = NORMAL_DATA_TRANSFER;
	data->outData.data = NULL;
	MRQInit(&(data->inData),IN_DATA_ELEMENT_SIZE,IN_DATA_DEPTH,OVERRIDE_LAST_DATA_IN_QUEUE);
	//*data->outData.data =0;
	return 0;
}

uint8_t DEINIT(MODULENAME)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF(TAG_TO_STRING(MODULENAME) " finishing ...");
	data->outData.data=0;
	data->outData.len =0;
	MRQFree(&data->inData);
	data->transferMode = 0;

	return 0;
}
