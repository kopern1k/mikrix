/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "stdio.h"
#include "stdlib.h"
#include "increment.h"
#include "../mikrix.h"



//#include "TimerManager.h"
#ifdef WIN32
#include <windows.h>
#include <psapi.h>
#elif defined (LINUX)
#include "crossPlatformTimer.h"
#endif
/*
Desc: Increment receaved data, no need for storing output variables
*/

//#define DATATRANSFER	INTERNAL_DATA_TRANSFER
#define DATATRANSFER	NORMAL_DATA_TRANSFER
//commands .. not needed in this module
static uint8_t GetData(MODULE_DATA* pModuleData,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("GetData invoked");
	outData->data = MRQGetItem(&pModuleData->inData);

	if(outData->data) {
		outData->len = pModuleData->inData.maxDataItemSize;
	}else {
		outData->len = 0;
		return 1;
	}
	return 0;
};

#define MAX__ 1000
static double buffer[MAX__];
static int intBuf[MAX__];


static uint8_t	OnData(ADDR *srcAddr, uint8_t *data, uint8_t len, uint8_t dataOffset, uint8_t dataLen, MODULE_DATA *moduleData, uint8_t moduleInOffset, DATA_OP dataOperation, HAVE_DATA_MASK	varIdentifier){
	//set data
	uint8_t err;
	uint8_t *pData;
	/* dataLen == 0 specifies that all data should be stored */
	if(dataLen == ALL_RECEIVED_DATA) {
		dataLen = len;
	}
	if(dataOffset + dataLen >len){
		return 1;
	}
	if( moduleData->inData.maxDataItemSize < (dataOffset + dataLen) ){
		return 2;
	}
	//Get Free place in Queue
	pData = MRQGetNextFreeItemP(&moduleData->inData,&err);
	switch(err) {
	case QUEUE_NO_ERR:
		break;
	case QUEUE_ERR_BAD_POINTER:
		return 3;
	case QUEUE_ERR_IS_FULL:
		return 4;
	case QUEUE_ERR_OVERRIDING_LAST:
		//DEBUGPRINTF("Warning, OnData received data, but data will be overriden");
		break;
	}

	switch(dataOperation){
		case COPY:
			CopyMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case SET:
			//SetMem(moduleData->inData.data + moduleInOffset,data+dataOffset,dataLen, = );
			SetMem((pData + moduleInOffset),0xFF,dataLen);
			/*
			for(i=0;i<dataLen;i++){
				(moduleData->inData.data + moduleInOffset)[i] = data[i+dataOffset];
			}
			*/
			break;
		case RESET:
			SetMem((pData + moduleInOffset),0x00,dataLen);
			break;
		case XOR:
			XorMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case AND:
			AndMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		case NEG:
			NegMem((pData + moduleInOffset),(data+dataOffset),dataLen);
			break;
		default:
			break;
	}

	//Store new position in Queue
	MRQAllocNextFreeItem(&moduleData->inData);
	//moduleData->inData;
	//set havedata
	SET(moduleData->haveData,varIdentifier);
	return 0;
}

static uint8_t GetConfig(MODULE_DATA* pModuleData){
	DATA *pData;
	pData=GetModuleConfig(pModuleData->IDD);
	if(pData->data == NULL) return 1;
	
	//...
	

	return 0;
}
#define cmd1 {GetData,0,0,0,"GetData"}
//#define cmd2 {GetData,GetInType,GetOutType,CheckInParams,"GetData"}

//static MODULE_CMD cmds[]={cmd1};
//static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};
CREATE_MODULE_COMMANDS_TABLE(cmd1);
static MODULE_INFO	module_info={MODULEID,MODULENAME};

static uint8_t ModuleMain(MODULE_DATA* pModule,pt *Thr);
MODULE	MODULENAME_TAG={ &module_info,(MODULE_CMD_TABLE*)&cmdTbl,ModuleMain,OnData,INIT(MODULENAME_TAG),DEINIT(MODULENAME_TAG),NULL,GetConfig }; //can be in flash memory
//set module parameters, create instance of module, return address of module


	static FILE *f;
	static uint16_t count =0;

#define InDataVar(module)	*(uint32_t*)MRQGetItem(&module->inData)

uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data){
	
	PRINTF("Increment initializing...\n");
	if(data==NULL) return 1;
	data->transferMode = DATATRANSFER;
	data->outData.data = (uint8_t*)malloc(TYPE_LEN[INT64_T]);
	data->outData.len = (uint8_t)TYPE_LEN[INT64_T];
	MRQInit(&data->inData,TYPE_LEN[INT32_T],1,NO_OVERRIDE);
	QueryPerformanceCounter((LARGE_INTEGER*)data->outData.data);
	data->haveDataMask = 1;
	data->haveData = 0;

	f=fopen("data.txt","w");
	if(f==NULL) {
		PRINTF("could not opne file!");
	}

	return 0;
}


uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF("Increment finishing...\n");
	free(data->outData.data);
	data->outData.data=NULL;
	data->outData.len =0;
	MRQFree(&data->inData);
	data->transferMode = 0;
	return 0;
}

#ifdef WIN32
static PROCESS_MEMORY_COUNTERS pmc;
static HANDLE hProcess;
#endif

uint8_t ret;

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
static LARGE_INTEGER tick2;
	PT_BEGIN(Thr);

	while(1){
		//PRINTF("Increment waiting.");
		PT_WAIT_WHILE(Thr,pModule->haveData == 0);
		//PRINTF("Increment started.");
		QueryPerformanceCounter(&tick2);
		//PRINTF("%lld,",tick2);
		pModule->haveData = 0;
		//PRINTF(" Incrementig:%d ",*(int32_t*)pModule->inData.data);	
		InDataVar(pModule) +=1; 
		//PRINTF("%f  \n",ComputeTimeDiff(*((LARGE_INTEGER*)(pModule->outData.data)),tick2));
		/*
		if(count%10 == 0){
			SLEEP(100);
		}
		*/
		//PRINTF("%d,",count); 
		if(count<MAX__){
			intBuf[count] = InDataVar(pModule);
			buffer[count++]=ComputeTimeDiff(*((LARGE_INTEGER*)(pModule->outData.data)),tick2)*1000000;
			*((LARGE_INTEGER*)(pModule->outData.data)) = tick2;
			ret=SendData(MRQGetItem(&pModule->inData),pModule->inData.maxDataItemSize,pModule->IDD);
			MRQRemoveFirstItem(&pModule->inData);
//			DEBUGPRINTF("i=%d,r=%d",InDataVar(pModule),ret);
		}else{
			if(f!=NULL){
				for(uint16_t i=0; i<count;i++){
					fprintf(f,"%f\t%d\n",buffer[i],intBuf[i]);
				}
#ifdef WIN32
				hProcess = GetCurrentProcess();
				if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) ){
					memoryUsage = pmc.WorkingSetSize / 1024; 
				}
				fprintf(f,"%d",memoryUsage);
#endif
				fclose(f);
				f=NULL;
				PRINTF("Finished.");
			}
		}
		//DEBUGPRINTF("time:%f  \n",ComputeTimeDiff(*((LARGE_INTEGER*)(pModule->outData.data)),tick2));
		
	}
	PT_END(Thr);
}
