/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "stdio.h"
#include "stdlib.h"
#include "pushButton.h"
#include "../mikrix.h"

#ifdef WIN32
#include <windows.h>
#endif

#define DATATRANSFER	NORMAL_DATA_TRANSFER
static uint8_t GetData(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	printf("GetData invoked");
	
return 0;
}

//#define SetMem(var1, var2, len, op) for( i=0,i<len,i++){ (var1)[i]##op##(var2)[i];}



#define cmd1 {GetData,NULL,NULL,NULL,"GetData"}
#define cmd2 {GetData,NULL,NULL,NULL,"GetData"}
static MODULE_INFO	module_info={MODULEID,TAG_TO_STRING(MODULENAME)};
static MODULE_CMD cmds[]={cmd1,cmd2};
static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};
static uint8_t ModuleMain(MODULE_DATA* pModule,pt *Thr);
MODULE	MODULENAME={ &module_info,(MODULE_CMD_TABLE*)&cmdTbl,ModuleMain,OnDataCommon,INIT(MODULENAME),DEINIT(MODULENAME) }; //can be in flash memory
//set module parameters, create instance of module, return address of module
uint8_t INIT(MODULENAME)(MODULE_DATA* data){
	
	PRINTF("PushButton initializing...\n");
	if(data==NULL) return 1;
	data->transferMode = DATATRANSFER;
	data->outData.data = (uint8_t*)malloc(TYPE_LEN[BOOL_T]);
	data->outData.len = (uint8_t)TYPE_LEN[BOOL_T];
	MRQInit(&data->inData, TYPE_LEN[UINT8_T], 1, NO_OVERRIDE);
	*data->outData.data =0;
	*data->inData.data=2;
	data->haveDataMask = 1;
	data->haveData = 0;
	return 0;
}

uint8_t DEINIT(MODULENAME)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF("PushButton finishing...\n");
	free(data->outData.data);
	free(data->inData.data);
	data->outData.data=0;
	data->inData.data=0;
	data->outData.len =0;
	MRQFree(&data->inData);
	data->transferMode = 0;

	return 0;
}
static uint8_t cnt1=0;

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	InitWaitMs(timer1);
	PT_BEGIN(Thr);
//	while(1){
	PRINTF(TAG_TO_STRING(MODULENAME) "[%d] (%d) " ,pModule->IDD,*pModule->outData.data);
	WaitMs(Thr,timer1,1000);
	//PT_WAIT_WHILE(Thr,WaitMs(1000) == 0);
	//SLEEP(500);
	if(cnt1++&0x01){
		*pModule->outData.data=1;
	}else{
		*pModule->outData.data=0;
	}
//	}
	PT_END(Thr);
}
