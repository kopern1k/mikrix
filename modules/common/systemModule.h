/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYSTEMMODULE__H__
#define __SYSTEMMODULE__H__

#include "../../common/inttypes.h"
#include "../../sys/moduleDef.h"
#include "sysConfig.h"

#undef ALL_COMMANDS
#undef MODULENAME
#undef MODULEID

#define MODULEID	CREATE_MODULE_ID(MIKRIX,1)
#define MODULENAME	SystemModule

DECLARE_MODULE(MODULENAME);

#define ALL_COMMANDS	Identify,Alive,GetUIDCmd,SetAddrForUID,SetAddr,GetAddr,AddModuleCmd,DelModuleCmd,GetModulesUID,GetGlobalConfig,\
						SetGlobalConfig,SaveCurrentGlobalConfig,GetSystemModeCmd,SetSystemNameCmd,GetSystemNameCmd,ListModuleCommandsCmd,\
						GetProtoThreadStatCmd,GetInterfacesInfoCmd,GetPlatformInfoCmd


typedef struct{
	UID		uid;
	uint16_t addr;
} MSG_SetAddrForUID_In_t;

typedef struct {
	uint8_t mId;
	char name[MAX_MODULE_NAME_LEN];
} ListModuleCommands_t;

typedef struct {
	uint8_t count;
	uint8_t nameSize;
	ListModuleCommands_t moduleList[];
} MSG_ListModuleCommands_Out_t;

typedef struct {
	uint8_t mId;
	ModuleUID_t uid;
} GetModulesUID_Out_t;

typedef struct {
	uint8_t freeThreadsCnt;
	uint8_t maxThreadsCnt;
} MSG_GetThreadStat_Out_t;

typedef struct {
	uint8_t id;
	uint8_t status;
}InterfaceInfo_t;

typedef struct {
	uint8_t maxInterfacesCnt;
	InterfaceInfo_t interfaceInfo[];
} MSG_GetInterfacesInfo_Out_t;


typedef struct {
	char archName[7];
	char platformName[4];
} MSG_GetPlatformInfo_Out_t;

CREATE_COMMANDS_ID_ENUM(ALL_COMMANDS);

#endif
