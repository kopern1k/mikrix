/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
#include "addressManager.h"
#include "systemModule.h"
#include "../mikrix.h"
#include "../../sys/moduleManager.h"
#include "../../sys/communicationInterface.h"
#include "../../sys/threadManager.h"
#include "../common/platformName.h"
#include "../common/arch.h"
#include <stdio.h>
#include <string.h>
#include "sysConfig.h"
#ifdef SYSTEM_MODULE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"

/*
Desc: Set addr, 
If device addr. is 0.0.0 then send response with UID every second.
Load modules configuration, save configuration
Load new module (save config after loading module ? - no - explicit - user must do it, or default saving, and explicit )

Add own address to mask addr manager?? 
Config Interface, (add interface? -  not supported)
*/
#define DATATRANSFER	INTERNAL_DATA_TRANSFER

INLINE uint8_t* SystemModuleAllocMem(uint8_t size) {
	return SysAllocMem(size);
}

INLINE uint8_t SystemModuleFreeMem(uint8_t *pData) {
	return SysFreeMem(pData);
}


static uint8_t COMMAND(Identify)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("IDENTIFY\n");
	static uint8_t buf[sizeof(UID) + 1];
	CopyMem(buf,(uint8_t*)&uid,sizeof(UID));
	buf[sizeof(UID)]=(uint8_t)IsMaster();
	outData->data = buf;
	outData->len = sizeof(buf);
	return 0;
};

static uint8_t COMMAND(Alive)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("ALIVE\n");
	outData->data = (uint8_t*)&uid;
	outData->len = sizeof(uid);
	return 0;
};

static uint8_t COMMAND(GetUIDCmd)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("GetUID\n");
	outData->data = (uint8_t*)&uid;
	outData->len = sizeof(uid);
	return 0;
};


static uint8_t COMMAND(SetAddrForUID)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("Set ADDR for UID\n");
	if( (((MSG_SetAddrForUID_In_t*)inData->data)->uid.platformID == uid.platformID )&& ( ((MSG_SetAddrForUID_In_t*)inData->data)->uid.producer == uid.producer ) && (((MSG_SetAddrForUID_In_t*)inData->data)->uid.SN == uid.SN )){
		SetSystemNodeAddr(*(I16*)&((MSG_SetAddrForUID_In_t*)inData->data)->addr);
		return 0;
	}
	return NO_SEND_RESPONSE;
};

static uint8_t COMMAND(SetAddr)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("Set Addr\n");
	SetSystemNodeAddr(*(I16*)inData->data);
	return 0;
};

static uint8_t COMMAND(GetAddr)(MODULE_DATA* pModule,CMD_DATA_IN *inData,CMD_DATA_OUT *outData){
	PRINTF("Set Addr\n");
	outData->data = (uint8_t*)GetSystemAddrP();
	outData->len = sizeof(ADDR);
	return 0;
};

//todo add possibility to set module parameter
static uint8_t COMMAND(AddModuleCmd)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	uint8_t *pData = SystemModuleAllocMem(sizeof(uint8_t));
	*pData =(uint8_t) AddModule(*(uint8_t*)inData->data,NULL);
	outData->data = pData;
	outData->len = 1;
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}


static uint8_t COMMAND(GetModulesUID)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData) {

	uint8_t i;
	uint8_t len = GetCreatedModulesCnt();
	MODULE_LIST ml[len];
	GetCreatedModuleList(ml,&len);
	GetModulesUID_Out_t* tmpData = (GetModulesUID_Out_t*) SystemModuleAllocMem(len*sizeof(GetModulesUID_Out_t));
	for(i=0; i<len;i++){
		tmpData[i].mId=ml->mId;
		tmpData[i].uid=ml->module->info->moduleID;
	}
	outData->len = len*sizeof(GetModulesUID_Out_t);
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}

static uint8_t COMMAND(DelModuleCmd)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	uint8_t *pData = SystemModuleAllocMem(sizeof(uint8_t));
	*pData = DeleteModule(*(uint8_t*)inData->data);
	outData->data = pData;
	outData->len = 1;
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}


static uint8_t COMMAND(GetGlobalConfig)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	DATA*d;
	d = GetModulesConfig();
	outData->data = d->data;
	outData->len = d->len;
	return 0;
}

static uint8_t COMMAND(SetGlobalConfig)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	DATA d;
	uint8_t ret;
	d.data=inData->data;
	d.len = inData->len;
	ret =SetModulesConfig(&d);
	if(ret == 0) SaveModulesConfig();
	return ret;
}

static uint8_t COMMAND(SaveCurrentGlobalConfig)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	return SaveModulesConfig();
}

static uint8_t COMMAND(GetSystemModeCmd)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	outData->data = &GetSystemMode();
	outData->len = 1;
	return 0;
}

static uint8_t COMMAND(SetSystemNameCmd)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	return SetSystemName(inData->data,inData->len);
}

static uint8_t COMMAND(GetSystemNameCmd)(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	outData->data = systemName;
	outData->len=strlen((const char*)outData);
	return 0;
}

/*
static uint8_t AddAddrMaskCmd(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	strncpy((char*)outData,(char*)systemName,MAX_SYSTEM_NAME_LEN);
	*outLen=strlen((const char*)outData);
	return 0;
}

static uint8_t DelAddrMaskCmd(MODULE_DATA* pModule, CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	strncpy((char*)outData,(char*)systemName,MAX_SYSTEM_NAME_LEN);
	*outLen=strlen((const char*)outData);
	return 0;
}
*/


/*
 * inData: 	module_ID to list commnads, deault self
 * outData: [module ID,commandName]
 */
static uint8_t COMMAND(ListModuleCommandsCmd)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	uint8_t i;
	MSG_ListModuleCommands_Out_t *pData = (MSG_ListModuleCommands_Out_t*)SystemModuleAllocMem(GetModule(pModule->IDD)->cmdTbl->nCommands * sizeof(ListModuleCommands_t) + sizeof(MSG_ListModuleCommands_Out_t));
	pData->count = GetModule(pModule->IDD)->cmdTbl->nCommands;
	pData->nameSize = MAX_MODULE_NAME_LEN;
	for(i=0; i< GetModule(pModule->IDD)->cmdTbl->nCommands;i++) {
		pData->moduleList[i].mId = i;
		strncpy(pData->moduleList[i].name,(char*)GetModule(pModule->IDD)->cmdTbl->commands[i].name,MAX_MODULE_NAME_LEN);
	}
	outData->data = (uint8_t*) pData;
	outData->len = GetModule(pModule->IDD)->cmdTbl->nCommands * sizeof(ListModuleCommands_t) + sizeof(MSG_ListModuleCommands_Out_t);
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}



static uint8_t COMMAND(GetProtoThreadStatCmd)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData) {
	MSG_GetThreadStat_Out_t *pData = (MSG_GetThreadStat_Out_t*)SystemModuleAllocMem(sizeof(MSG_GetThreadStat_Out_t));
	if(pData == NULL) {
		return 1;
	}
	pData->freeThreadsCnt = freeThreadsCnt;
	pData->maxThreadsCnt = MAX_THREADS;
	outData->data = (uint8_t*) pData;
	outData->len = sizeof(MSG_GetThreadStat_Out_t);
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}


static uint8_t COMMAND(GetInterfacesInfoCmd)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData) {
	MSG_GetInterfacesInfo_Out_t *pData = (MSG_GetInterfacesInfo_Out_t*)SystemModuleAllocMem(sizeof(MSG_GetInterfacesInfo_Out_t) + (sizeof(InterfaceInfo_t) * MAX_INTERFACES));
	if(pData == NULL) {
		return 1;
	}
	pData->maxInterfacesCnt = MAX_INTERFACES;
	for(int i = 0 ; i< MAX_INTERFACES; i++) {
		pData->interfaceInfo[i].id = interfejs[i].interfaceID;
		pData->interfaceInfo[i].status = interfejs[i].state;
	}
	outData->data = (uint8_t*) pData;
	outData->len = sizeof(MSG_GetInterfacesInfo_Out_t) + (sizeof(InterfaceInfo_t) * MAX_INTERFACES);
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}


static uint8_t COMMAND(GetPlatformInfoCmd)(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData) {
	//get adapter count
	//uint8_t adapterCnt = 1;
	outData->len  = sizeof(MSG_GetPlatformInfo_Out_t) ;//+ sizeof(NetAdapterInfo_t) * adapterCnt;
	MSG_GetPlatformInfo_Out_t *pData = (MSG_GetPlatformInfo_Out_t*)SystemModuleAllocMem(outData->len);
	strncpy(pData->platformName,PLATFORM_NAME,sizeof(pData->platformName));
	strncpy(pData->archName,ARCHITECTURE,sizeof(pData->archName));
	outData->data = (uint8_t*) pData;
	outData->FreeFunct = SystemModuleFreeMem;
	return 0;
}


 typedef struct {
	 uint8_t moduleIndex; //module index from defaulModule Array
	 uint8_t moduleIDD;
 } RUNNING_MODULES;

 typedef struct{
	UID  uid;
	ADDR sysAddr;
	uint8_t sysName[MAX_SYSTEM_NAME_LEN];
	DATA runningModules;
 } SYSTEM_CONFIG;

#define MAX_MODULE_LIST 10

static uint8_t SetConfig(MODULE_DATA* pModuleData){
 	 
	//compute length
	MODULE_LIST mol[MAX_MODULE_LIST];
	uint8_t len=MAX_MODULE_LIST;
	DATA data;
	DATA* pData;
	uint8_t i;
	if( GetCreatedModuleList(mol,&len) != 0){
		return 1; // not all modules could be stored
	}
	data.data=NULL;
	data.len = len*sizeof(RUNNING_MODULES) + 1 + sizeof(SYSTEM_CONFIG) - sizeof(DATA);
	if(SetModuleConfig(0,&data)>1){
		return 2;
	}
	
	pData=GetModuleMemorySpace(0);
	//store data
	if(pData->data != NULL){
		return 3;
	}

	((SYSTEM_CONFIG*)pData->data)->uid = uid;
	((SYSTEM_CONFIG*)pData->data)->sysAddr =sysAddr;
	for( i=0;i<MAX_SYSTEM_NAME_LEN; i++){
		((SYSTEM_CONFIG*)pData->data)->sysName[i]=systemName[i];
		if(systemName[i]==0) break;
	}
	((SYSTEM_CONFIG*)pData->data)->runningModules.len = len;
	if(len ==0){
		SaveModulesConfig();	
		return 0;
	}
	
	for(i=0;i<len;i++){
		if(mol[i].mId !=0){
			((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIndex = GetModuleIndexFromModuleID(mol[i].module->info->moduleID);
			((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIDD = mol[i].mId;
		}
	}
	SaveModulesConfig();
	return 0;
 }

//load modules with config, but only with null initial parameter set: module init must consider that
static uint8_t GetConfig(MODULE_DATA* pModuleData){
	DATA *pData;
	uint8_t i;
	pData=GetModuleConfig(0);
	if(pData->data == NULL) return 1;
	sysAddr = ((SYSTEM_CONFIG*)pData->data)->sysAddr;
	uid = ((SYSTEM_CONFIG*)pData->data)->uid;
	for( i=0;i<MAX_SYSTEM_NAME_LEN; i++){
		systemName[i] = ((SYSTEM_CONFIG*)pData->data)->sysName[i];
		if(((SYSTEM_CONFIG*)pData->data)->sysName[i]==0) break;
	}
	if(((SYSTEM_CONFIG*)pData->data)->runningModules.len == 0){
		return 1;
	}
	for(i=0;i< ((SYSTEM_CONFIG*)pData->data)->runningModules.len; i++){
		AddModuleAtPosition(((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIndex,((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIDD,NULL);
		//load configuration ... now .. loaded from init function
		if( moduleObj[((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIDD].module->GetConfig!= NULL )
			moduleObj[((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIDD].module->GetConfig(moduleObj[((RUNNING_MODULES*)((SYSTEM_CONFIG*)pData->data)->runningModules.data)[i].moduleIDD].data);
	}
	
	//load mask addr manager tables
	//load response manager tables
	//load interface configuration
	return 0;
}

//create module
CREATE_MODULE(MODULENAME,ALL_COMMANDS, SetConfig, GetConfig);

uint8_t INIT(MODULENAME)(MODULE_DATA* data){
	DATA *d;
	if(data==NULL) return 1;
	DEBUGPRINTF(TAG_TO_STRING(MODULENAME) " initializing ...[%d]\n",data->IDD);
	data->transferMode = NORMAL_DATA_TRANSFER;
	// Get data for modules
	d=GetModuleConfig(0);
	if(d->data == NULL){//create default configuration
		
	}

	data->outData.data = (uint8_t*)malloc(TYPE_LEN[BOOL_T]);
	data->outData.len = (uint8_t)TYPE_LEN[BOOL_T];
	MRQInit(&(data->inData),TYPE_LEN[INT8_T],1,OVERRIDE_LAST_DATA_IN_QUEUE);
	//*data->outData.data =0;
	return 0;
}

uint8_t DEINIT(MODULENAME)(MODULE_DATA* data){
	if(data == NULL) return 1;
	DEBUGPRINTF(TAG_TO_STRING(MODULENAME) " finishing ...");
	free(data->outData.data);
	data->outData.data=0;
	data->outData.len =0;
	MRQFree(&data->inData);
	data->transferMode = 0;

	return 0;
}

#define BAD_CMD_DATA 1
#define ANOTHER_NODE_CMD 255


uint8_t OnAddressReceived(COM_MNG_DATA *cm,uint8_t* data, uint8_t len, uint8_t errCode) {
	if(len != sizeof(RequestAddressCmdOut_t)) {
		return BAD_CMD_DATA;
	}
	if(CmpStruct((void*)&(((RequestAddressCmdOut_t*)data)->uid),(void*)GetSystemUID(),sizeof(RequestAddressCmdOut_t))) {
		return ANOTHER_NODE_CMD;
	}
	SetSystemNodeAddr(((RequestAddressCmdOut_t*)data)->addr);
	return 0;
}

//static uint8_t cnt=0;

InitWaitMs(systemTimer1);
RequestAddressCmdIn_t tmpData;
int8_t ret;
static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	PT_BEGIN(Thr);
	WaitMs(Thr,systemTimer1,1000);
	if(sysAddr.nodeAddr.data == 0){
		//Send Addr. resposne (Alive)
		tmpData.uid = *GetSystemUID();
		tmpData.isMaster=IsMaster();
		ret = SendHandledRequest(pModule->IDD, &MAKE_ADDR(0,0,1),1,(uint8_t*)&tmpData,sizeof(tmpData),OnAddressReceived,1000,0);
		if(ret<0){
			PRINTF("Error (%d) While sending hadled request\n",ret);
			//try again
		}

		WaitForResponse(Thr,ret);
		if(IsResponseTimeoutElapsed(ret) && WasResponseCalled(ret)==0 ){
			//error
			//delete Responce
			DeleteResponse(ret);
			PRINTF("Module not responding!\n");
		}

	}

	PT_END(Thr);
}
