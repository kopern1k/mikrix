/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __ADDR_MANAGER__H__
#define __ADDR_MANAGER__H__

#include "../../common/inttypes.h"
#include "../../sys/moduleDef.h"
#include "addressManagerCommon.h"


#undef MODULENAME
#undef MODULENAME_TAG
#undef MODULEID

#define MODULEID	0x00010005 
#define MODULENAME	"AddressManager"
#define MODULENAME_TAG	AddressManager

//uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data);
//uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data);
//extern MODULE	MODULENAME_TAG;

//init paremeters
typedef struct {
	I16 startDevAddr;
	I16 endtDevAddr;
} AddressManagerInitParams_t;

uint8_t INIT(AddressManager)(MODULE_DATA* data);
uint8_t DEINIT(AddressManager)(MODULE_DATA* data);
extern MODULE	AddressManager;

typedef struct ATTRIB_ALIGN_8() {
	UID uid;
	uint8_t isMaster;
} RequestAddressCmdIn_t;


typedef struct ATTRIB_ALIGN_8() {
	UID uid;
	NODE_ADDR addr;
} RequestAddressCmdOut_t;

#endif
