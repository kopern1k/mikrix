/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "addressManager.h"

#include "stdio.h"
#include "stdlib.h"
#if defined WIN32
#include <conio.h>
#include <windows.h>
#endif
#include "../mikrix.h"
#include "../../sys/responseManager.h"
#include "../../sys/moduleManager.h"
#include "../../sys/communicationInterface.h"


NODE_TABLE nodeTable[NODE_TABLE_SIZE];
uint8_t addresManagerInitialized = 0;


static uint8_t GetNodeTableCmd(MODULE_DATA* pModule,CMD_DATA_IN *inData,CMD_DATA_OUT *outData){
	//PRINTF("ALIVE\n");
	outData->data = (uint8_t*)nodeTable;
	outData->len = (uint8_t)sizeof(nodeTable);
	/*
	for(i=0; i<NODE_TABLE_SIZE;i++){
		if(nodeTable[i].uid.producer == 0 ){
			break;
		}
		((NODE_TABLE*)outData)[i]=nodeTable[i];
	}
	*outLen=i*sizeof(NODE_TABLE);
	*/
	return 0;
};


//input uint8_t table ID
// out
//1- no input error
//2- data table range error
//return address for given UID

#define DEFAULT_ADDRESS MAKE_NODE_ADDR(10,0).data

#define RequestAddressCmdOK 0
#define RequestAddressCmdNeedAuthentification 1
#define RequestAddressCmdNoEnoughtSpace 2
#define RequestAddressCmdBad 3

static uint8_t RequestAddressCmd(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	uint8_t ret = RequestAddressCmdOK;
	uint16_t i;
	if(inData->len != sizeof(RequestAddressCmdIn_t)) {
		return RequestAddressCmdBad;
	}
	for(i=0;i<NODE_TABLE_SIZE;i++){
		if(nodeTable[i].uid.producer == 0  || ( nodeTable[i].uid.SN == ((UID*)inData->data)->SN && nodeTable[i].uid.producer == ((UID*)inData->data)->producer && nodeTable[i].uid.platformID == ((UID*)inData->data)->platformID ) ){
			break;
		}
	}
	if(i == NODE_TABLE_SIZE) {
		return RequestAddressCmdNoEnoughtSpace;
	}
	// store data
	nodeTable[i].uid=*((UID*)inData->data);
	nodeTable[i].nodeType = inData->data[sizeof(UID)]; //master, slave
	nodeTable[i].interfaceID = inData->cm->interfaceID;
	if(pModule->initParam) {
		nodeTable[i].addr = ((AddressManagerInitParams_t*)pModule->initParam)->startDevAddr.data + i;
	} else {
		nodeTable[i].addr = DEFAULT_ADDRESS + i;
	}

	
	return ret;
};

#define cmd0 {GetNodeTableCmd,NULL,NULL,NULL,"GetNodeList"}
#define cmd1 {RequestAddressCmd,NULL,NULL,NULL,"RequestAddressCmd"}

static MODULE_CMD cmds[]={cmd0,cmd1};
static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};

static uint8_t SetConfig(MODULE_DATA* pModuleData){
	static DATA data;
	static DATA* pData;

	//compute length
	for(data.len=0;data.len<NODE_TABLE_SIZE;data.len++){
		if(nodeTable[data.len].uid.producer == 0 ){
			break;
		}
	}
	data.data =0;
	if(data.len == 0) {
		return 1;
	}
	data.len = data.len*sizeof(NODE_TABLE);
	//reserve memory space
	if(SetModuleConfig(1,&data)>1){
		return 2;
	}
	//get pointer to data space
	pData=GetModuleMemorySpace(1);
	//store data
	if(pData->data != NULL){
		return 3;
	}
	for(data.len=0;data.len<NODE_TABLE_SIZE;data.len++){
		if(nodeTable[data.len].uid.producer == 0 ){
			break;
		}
		((NODE_TABLE*)pData->data)[data.len] = nodeTable[data.len];
	}
	

	return 0;
 }

static uint8_t GetConfig(MODULE_DATA* pModuleData){
	DATA *pData;
	uint8_t i;
	pData=GetModuleConfig(0);
	if(pData->data == 0 || pData->len ==0 ) {
		return 1;
	}
	for(i=0;i< (const uint8_t) pData->len/sizeof(NODE_TABLE);i++){
		nodeTable[i] = ((NODE_TABLE*)pData->data)[i];
	}
	return 0;
}


static MODULE_INFO	module_info={MODULEID,MODULENAME};

MODULE	MODULENAME_TAG={ &module_info,(MODULE_CMD_TABLE*)&cmdTbl,/*ModuleMain*/NULL,NULL,INIT(MODULENAME_TAG),DEINIT(MODULENAME_TAG), SetConfig, GetConfig }; //can be in flash memory
//set module parameters, create instance of module, return address of module



void InitNodeTable(){
	uint8_t i;
	for(i=0;i<NODE_TABLE_SIZE;i++){
		nodeTable[i].uid.platformID =0;
		nodeTable[i].uid.producer = 0;
		nodeTable[i].uid.SN = 0;
		nodeTable[i].addr =0;
	}
}

uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data){
	DATA *d;
	if(data==NULL) return 1;
	PRINTF(MODULENAME " initializing ...[%d]\n",data->IDD);
	InitNodeTable();
	data->transferMode = INTERNAL_DATA_TRANSFER;
	// Get data for modules
	d=GetModuleConfig(1);
	if(d->data != NULL){//create configuration
		
	}

	
	//AddUIDResponseCallback();
	addresManagerInitialized = 1;

	return 0;
}

uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF(MODULENAME " finishing ...");


	return 0;
}



/*
static uint8_t i;
InitWaitMs();

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	PT_BEGIN(Thr);
	//print >>>

	//get line into buffer
	//PT_WAIT_WHILE(Thr,ReadLine() !=0); 
	//parse line

	PT_END(Thr);
}
*/
