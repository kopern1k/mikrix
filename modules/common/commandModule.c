/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "commandModule.h"

#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#ifdef WIN32
#include <conio.h>
#include <windows.h>
#endif
#include "../mikrix.h"
#include "../../sys/threadManager.h"
#include "../../sys/moduleManager.h"
#include "../../sys/responseManager.h"
#include "../../sys/communicationInterface.h"
#include "addressManagerCommon.h"

#ifdef LINUX
#include <unistd.h>
#include "kbhit.h"
#endif

#define DATATRANSFER	INTERNAL_DATA_TRANSFER

uint8_t parent=0;
//ADDR selAddr=sysAddr;
ADDR selAddr;
uint8_t selNode;

static uint8_t Alive(MODULE_DATA* pModule,CMD_DATA_IN *inData, CMD_DATA_OUT *outData){
	PRINTF("ALIVE\n");
	outData->data = (uint8_t*)&uid;
	outData->len = sizeof(uid);
	return 0;
};

#define cmd1 {Alive,NULL,NULL,NULL,"Alive"}

static MODULE_CMD cmds[]={cmd1};
static MODULE_CMD_TABLE cmdTbl = {cmds,sizeof(cmds)/sizeof(MODULE_CMD)};

static MODULE_INFO	module_info={MODULEID,MODULENAME};
static uint8_t ModuleMain(MODULE_DATA* pModule,pt *Thr);
MODULE	MODULENAME_TAG={ &module_info,(MODULE_CMD_TABLE*)&cmdTbl,ModuleMain,NULL,INIT(MODULENAME_TAG),DEINIT(MODULENAME_TAG), NULL, NULL }; //can be in flash memory
//set module parameters, create instance of module, return address of module


#define MAX_MODULE_LIST 10
/*
static uint8_t SetConfig(MODULE_DATA* pModuleData){
 	 
	//compute length
	return 0;
 }

static uint8_t GetConfig(MODULE_DATA* pModuleData){
	return 0;
}
*/

//static variables


uint8_t INIT(MODULENAME_TAG)(MODULE_DATA* data){
	//DATA *d;
	PRINTF(MODULENAME " initializing ...\n");
	selAddr=sysAddr;
	if(data==NULL) return 1;
	data->transferMode = NORMAL_DATA_TRANSFER;
	// Get data for modules
	//d=GetModuleConfig(2);
	//if(d->data == NULL){//create default configuration		
	//}
	return 0;
}

uint8_t DEINIT(MODULENAME_TAG)(MODULE_DATA* data){
	if(data == NULL) return 1;
	PRINTF(MODULENAME " finishing ...");
	return 0;
}



static void PrintCMDLineHead(){
	PRINTF("\n[%.2d.%.2d.%.2d]>>>",selAddr.nodeAddr.hi,selAddr.nodeAddr.lo,selAddr.IDD);
}
// 0 Have line
#define MAX_LINE	80
static uint8_t line[MAX_LINE];
static uint8_t lineCnt=0;

#ifdef LINUX
#include <termios.h>
#define ECHO_ENABLED  0

#ifdef ECHO_ENABLED
  #define ECHO_DATA ECHO
#else
  #define ECHO_DATA 0
#endif
char __getch(void)
{
	struct termios oldt,
	newt;
	char ch;
	tcgetattr( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO_DATA );
	tcsetattr( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
	return ch;
}
#endif

static uint8_t _ReadLine(){
	static uint8_t c =0;
	do {
		if(_kbhit()){
			c=__getch();
			putchar(c);
			line[lineCnt]=c;
			lineCnt++;
		}else{
			break;
		}
	} while (!(c==13 || c==10));

	if(c!=13 && c!=10){
		return 1;
	}
	line[lineCnt-1]=0;
	lineCnt=0;
	c=0;
	return 0;
}

/*#else
static uint8_t ReadLine(){
  read(STDIN_FILENO, line, MAX_LINE);
  return 0;
}
#endif
*/
uint8_t CM_SelMaster(uint8_t *param);
uint8_t CM_Help(uint8_t *param);
uint8_t CM_ReturnBack(uint8_t *param);
uint8_t CM_ThreadList(uint8_t *param);
uint8_t CM_RunningModules(uint8_t *param);
uint8_t CM_TimerList(uint8_t *param);
uint8_t CM_NodeList(uint8_t *param);
uint8_t CM_SetNode(uint8_t *param);
uint8_t CM_SetAddr(uint8_t *param);
uint8_t CM_ModuleList(uint8_t *param);
uint8_t CM_AddModule(uint8_t *param);
uint8_t CM_DeleteModule(uint8_t *param);
uint8_t CM_SelectModule(uint8_t *param);
uint8_t CM_SaveConfig(uint8_t *param); //save config for all modules
uint8_t CM_SetMask(uint8_t *param);
uint8_t CM_NodeName(uint8_t *param);
uint8_t CM_ModuleCmd(uint8_t *param);

typedef struct{
	uint8_t parent;
	uint8_t name[20];
	uint8_t parsing[20];
	uint8_t desc[120];
	uint8_t (*CommandFunc)(uint8_t *param);
} COMMAND;

#define NODELEVEL_IND 7
#define MODULELEVEL_IND 13


COMMAND cmd[]={	{0xFF,"?","%19s","\t\tshow this help",CM_Help},
				{0xFF,"..","%19s","\t\treturn back",CM_ReturnBack},
				{0,"master","%s","\tlist of master devices",CM_SelMaster},
				{0,"threads","%s","\trunning threads",CM_ThreadList},
				{0xFF,"runningmodules","%s","\trunning modules",CM_RunningModules},
				{0,"timer","%s","\t\tPrint timer Table",CM_TimerList},
				{0,"nodelist","%s","\tshow devices connected to master node",CM_NodeList},  //7
				{0,"node","%s %s","\tset node to configure",CM_SetNode},	 //8
				{NODELEVEL_IND,"addr","%s %s","\t\tset address to selected node [addr] ex. addr 10.1",CM_SetAddr},
				{NODELEVEL_IND,"modulelist","%s","\tshow aviable modules on current node",CM_ModuleList},
				{NODELEVEL_IND,"addmodule","%s %s","\tadd specific module from modulelist [ID]",CM_AddModule},
				{NODELEVEL_IND,"deletemodule","%s %s","\tdelete specific module from modulelist [ID]",CM_DeleteModule},
				{NODELEVEL_IND,"name","%s %s","\t\tset, or get (without param.) node name",CM_NodeName},
				{NODELEVEL_IND,"module","%s %s","\tselect module from modulelist [ID]",CM_SelectModule}, //12
				{NODELEVEL_IND,"saveconfig","%s","\tSave modules configuration",CM_SaveConfig},
				{MODULELEVEL_IND,"mask","%s %[^\n]","\tset mask [addr,dataOffset,dataSize,moduleOffset,Operation {copy,set,reset,xor,and,neg),sync. input (1,2,4..)",CM_SetMask},
				{MODULELEVEL_IND,"cmd","%s %[^\n]","\t",CM_ModuleCmd},};


uint8_t CM_SelMaster(uint8_t *param){
	//
	PRINTF("\nmaster listing...");
	return 0;
}

uint8_t CM_Help(uint8_t *param){
	uint8_t i;
	for(i=0;i<sizeof(cmd)/sizeof(COMMAND);i++){
		if(cmd[i].parent == parent || cmd[i].parent ==(uint8_t)0xFF ){
			PRINTF("\n %s %s",cmd[i].name, cmd[i].desc);
		}
	}
	PRINTF("\n");
	return 0;
}



static void PrintThreads(){
	uint8_t i=0;
	
	PRINTF("Thread\tThrFunc \tMemory\n");
	do{
		PRINTF("%d\t0x%p\t0x%p\n",i,threads[i].thrFunc,threads[i].memoryContext);
		i=threads[i].next;
	}while(i!=NOTHREADS);
}

#define MAX_MODULES 10
static void PrintRunningModules(){
	MODULE_LIST ml[MAX_MODULES];
	uint8_t lenA = MAX_MODULES;
	uint8_t i,ret;
	ret = GetCreatedModuleList(ml,&lenA);
	//print added modules
	if(ret) {
		PRINTF("Got error [%d]!\n",ret);
	}
	PRINTF("Running modules:[%d] \n",lenA);
	PRINTF("IDD\tName\t\t\tmoduleID\n");
	for( i=0; i<lenA;i++){
		PRINTF("%d\t%s\t\t0x%.8x\n",ml[i].mId,ml[i].module->info->name, ml[i].module->info->moduleID );
	}
};

static void PrintTimerTable(){
	uint8_t i;
	PRINTF("Timer Table\n");
	for(i=0;i<MAX_DELAY_VALUES;i++){
		PRINTF("%.2d   ",i);
	}
	PRINTF("\n");
	for(i=0;i<MAX_DELAY_VALUES;i++){
		PRINTF("%.4x ",mDelayList.delayArray[i].time);
	}
	PRINTF("\n");
	for(i=0;i<MAX_DELAY_VALUES;i++){
		PRINTF("%.3d  ",mDelayList.delayArray[i].nextInd);
	}
	PRINTF("\nfreeEnd: %d",mDelayList.freeEnd);
	PRINTF("\nfreeStart: %d",mDelayList.freeStart);
	PRINTF("\nstartInd: %d\n",mDelayList.startInd);
	PRINTF("cmpVal: %d\n",GET_SYS_TIM_CMP_VAL());
}

//send request to master for nodes
uint8_t CheckNodeListTimeout(void *data, pt* Thr){
	PT_BEGIN(Thr);
	WaitForResponse(Thr,*(uint8_t*)data);
	if(IsResponseTimeoutElapsed(*(uint8_t*)data) && WasResponseCalled(*(uint8_t*)data)==0 ){
		//error 
		//delete Responce

		PRINTF("Module not responding!\n");
	}
	DeleteResponse(*(uint8_t*)data);
	DeleteThread(Thr);
	PT_END(Thr);
	return 0;
}

uint8_t CM_RunningModules(uint8_t *param){
	
	PrintRunningModules();	
	return 0;
}
uint8_t CM_ThreadList(uint8_t *param){
	
	PrintThreads();	
	return 0;
}

uint8_t CM_TimerList(uint8_t *param){
	
	PrintTimerTable();	
	return 0;
}

uint8_t NodeListCallBack(COM_MNG_DATA *cm,uint8_t* data, uint8_t len, uint8_t errCode){
	uint8_t i,j;
	if(len==0){
		PRINTF("No external nodes\n");
		return 0;
	}
	PRINTF("ID\tADDR\tINTERFACE\tMASTER\tUID\n");
	for(i=0;i<len/sizeof(NODE_TABLE);i++){
		PRINTF("%d\t[%.2d.%.2d]\t%d\t\t%d\t",i,((I16*)&((NODE_TABLE*)data)[i].addr)->hi,((I16*)&((NODE_TABLE*)data)[i].addr)->lo,((NODE_TABLE*)data)[i].interfaceID, ((NODE_TABLE*)data)[i].nodeType);
		for(j=sizeof(UID); j;j--) {
			PRINTF("%.2x ",((uint8_t*)&((NODE_TABLE*)data)[i].uid)[j-1]);
		}
		PRINTF("\n");
	}
	return 0;
}

uint8_t CM_NodeList(uint8_t *param){
	PRINTF("node listing ...\n");
	ADDR addr;
	addr.nodeAddr =selAddr.nodeAddr;
	addr.IDD = 1;
	static int8_t sendHandleReq;
	sendHandleReq=SendHandledRequest(GetCurrentSheduledModuleData()->IDD,&addr,0,NULL,0,NodeListCallBack,1000,0);
	//Add waiting thread
	if(sendHandleReq<0){
		PRINTF("Error (%d) While sending hadled request\n",sendHandleReq); 
		return 1;
	}
	AddThread((void*)&sendHandleReq,CheckNodeListTimeout);
	
	return 0;

}

uint8_t CM_SaveConfig(uint8_t *param){
	SaveModulesConfig();
	return 0;
}

uint8_t CM_ReturnBack(uint8_t *param){
	if(parent!=0){
		parent=cmd[parent].parent;
	}
	return 0;
}

uint8_t CM_SetNode(uint8_t *param){
	
	parent=NODELEVEL_IND;
	return 0;
}
//set address to specific node
uint8_t CM_SetAddr(uint8_t *param){
	return 0;
}
uint8_t CM_ModuleList(uint8_t *param){
	return 0;
}
uint8_t CM_AddModule(uint8_t *param){
	return 0;
}
uint8_t CM_DeleteModule(uint8_t *param){
	return 0;
}
uint8_t CM_SelectModule(uint8_t *param){
	parent =  MODULELEVEL_IND;
	return 0;
}
uint8_t CM_SetMask(uint8_t *param){
	return 0;
}
uint8_t CM_NodeName(uint8_t *param){
	//show node name
	if(*param == 0){
		//get system name
		
		PRINTF("NAME: %s",systemName);
	}
	return 0;
}
uint8_t CM_ModuleCmd(uint8_t *param){
	return 0;
}

uint8_t ParseCmdLine(){
	uint8_t tmpBuf1[20]="";
	uint8_t tmpBuf2[30]="";
	uint8_t i;
	int ret;
 	for(i=0;i<sizeof(cmd)/sizeof(COMMAND);i++){
		if( (cmd[i].parent == parent ) || ( cmd[i].parent == (uint8_t)0xFF ) ){
			ret=sscanf((const char*)line,(const char*)cmd[i].parsing,tmpBuf1,tmpBuf2);
			if(ret==EOF || ret == 0){
				continue;
			}
			//check name
			if(strcmp((const char*)cmd[i].name,(const char*)tmpBuf1) !=0){
				continue;
			}
			//call callback
			return cmd[i].CommandFunc(tmpBuf2);

		}
	}
	return 1;
}

//InitWaitMs();

static uint8_t ModuleMain(MODULE_DATA *pModule, pt *Thr){
	PT_BEGIN(Thr);
	//print >>>
	PrintCMDLineHead();
	//get line into buffer
	PT_WAIT_WHILE(Thr,_ReadLine() !=0);
	//parse line
	ParseCmdLine();
	PT_END(Thr);
}
