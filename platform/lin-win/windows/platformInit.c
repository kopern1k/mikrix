/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
//
#include "../../include/platformInit.h"
//#include "stdafx.h"
//#include <windows.h>
//#include <tchar.h>
#include "stdio.h"
#undef BOOL
#include <string.h>
#include <stdlib.h>
#include "../../../common/common.h"
#include "../../../sys/system.h"
#include "../../../sys/configurationManager.h"
#include "../../../sys/threadManager.h"
#include "../common/processInstanceHelper.h"
#include "auxInit.h"


void PlatformInit(int argc, char* argv[])
{
	ADDR address;
	uint8_t i;
	address.devAddr.hi = 0;
	address.devAddr.lo = 0;
	address.IDD = 0;

	//specific for windows apps
	
	//chceck if app is configuratior or specific node
	if( argc <= 1) {
		PRINTF("Application is starting with configurator UI.");
		PRINTF("Other nodes (paralell connected) need to define node name in commandline parameter. ");
		PRINTF("\nExample: mikrix.exe <node_name>\n");
		strncpy((char*)systemName,"system",sizeof(systemName));
		address.devAddr.hi = 10;
		SetMaster();
		//only one instance could be run.
		for(i=strlen(argv[0]);(argv[0][i]!='\\' && i);i--){};
		if(isAnotherInstanceRunning(&argv[0][i+1])==1){
			PRINTF("Another instance of application with configuration UI is running.\n");
			PRINTF("Please run  mikrix.exe <node_name> for running antoher node with name node_name. \n");
			PRINTF("Finishing.\n");
			exit(1);
			//return 1;
		}
	}else{
		strncpy((char*)systemName,argv[1],sizeof(systemName));
	}
	SetSystemAddr(&address);
	//return 0;
}

