/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../../include/serialInterface.h"
#include "sysSerial.h"

Serial com;

uint8_t SerialInitWr(uint8_t *config, COM_MNG_DATA *cm){
	return SerialInit((SERIAL_CONFIG *)config, cm);
}
uint8_t SerialInit(SERIAL_CONFIG *config, COM_MNG_DATA *cm){
	if(com.connect((char*)config->port,config->speed,spNONE) != 0){
		cm->enabled = 0;
		return 1;
	}
	cm->enabled = 1;
	com.initSerialReceive(OnDataWrapper, (void*) cm);
	cm->fullDuplex = 0;
	cm->communicationSpeed=config->speed;
	cm->oneByteDelayUs = WAIT_FOR_ONE_BYTE(cm->communicationSpeed);
	InitRandomTime(cm->communicationSpeed);
	return 0;
}



void SerialWrite(uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending){
	char buf[SEND_BUFF_SIZE];
	uint16_t len=0;
	endInd=(endInd+1)& mask;
	do{
		buf[len++]=pData[startInd];
		startInd=( startInd+1 )& mask;
	}while(startInd!=endInd);
	
	com.sendArray(buf,len);

	*Sending =0;
}


