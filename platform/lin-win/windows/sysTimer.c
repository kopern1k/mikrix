/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "sysTimer.h"
#include "../../common/common.h"
#include "../../sys/timeManagerDep.h"
#include "../../sys/timeCorr.h"
#include "crossPlatformTimer.h"
#include "sysConfig.h"

uint16_t sysTimerMaxVal=0xFFFF;
//uint16_t timer1;
uint16_t sysTimerCompareMax = DEFAULT_SYS_TIM_COMPAREMAX;
uint16_t timOffset=0;
uint8_t timerRunning = 1;

#define GET_SYS_TIME()	(uint16_t)(((uint64_t)GetTimer(1) + timOffset) )

uint16_t GET_SYS_TIMER_VAL_SCALED(){
	return GET_SYS_TIME() % sysTimerCompareMax;
}

void SET_SYS_TIMER_VAL_SCALED(uint16_t value){
	timOffset = value;
	StartTimer();
}


DWORD WINAPI TimerThr(){
	StartTimer();
	uint16_t tim,lastTim=0;
	while(timerRunning){
		tim=GET_SYS_TIME() ;

		while((tim>=sysTimerMaxVal )&& (lastTim<sysTimerMaxVal)){
			//OnCompareVal();
			lastTim =tim;
			tim=GET_SYS_TIME();	
		}
		lastTim=tim;
		if( tim>=sysTimerCompareMax){
			timOffset =0;//GET_TIME() % compareMax;
#ifdef TIMER_COMPENSATE
			OnTimerCorr();
#else
			OnTimMax();
#endif
			StartTimer();
			lastTim=0;
		}

		Sleep(1);
	} 
	return 0;
}

uint8_t InitSysTimer(){
	HANDLE t;
	DWORD tid;

	InitCrossPlatformTimer();
	//create working thread
	if (0 == (t = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)TimerThr, NULL, 0, &tid))){
		PRINTF("Error creating new thread, error=%d\n", GetLastError());
		return 0;
	}
  //and detach the thread so that it is released when it terminates
  CloseHandle(t);
  return 0;

}


double ComputeTimeDiff( LARGE_INTEGER tick1, LARGE_INTEGER tick2){
	LARGE_INTEGER ticksPerSecond;
	QueryPerformanceFrequency(&ticksPerSecond);
	return (double)(tick2.QuadPart-tick1.QuadPart)/(double)(ticksPerSecond.QuadPart);
}
