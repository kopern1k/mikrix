/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SYSSERIAL__H__
#define __SYSSERIAL__H__
#include "../../common/inttypes.h"
#include <stdio.h>
#if defined WIN32 || defined WIN64
#include <windows.h>
#endif

enum serial_parity  { spNONE,    spODD, spEVEN };


/* -------------------------------------------------------------------- */
/* -----------------------------  Serial  ---------------------------- */
/* -------------------------------------------------------------------- */
class Serial
{
    // -------------------------------------------------------- //
protected:
	char              port[15];                      // port name "com1",...
	int               rate;                          // baudrate
	serial_parity     parityMode;
	HANDLE            serial_handle;                 // ...
	HANDLE			  Thr;
	

    // ++++++++++++++++++++++++++++++++++++++++++++++
    // .................. EXTERNAL VIEW .............
    // ++++++++++++++++++++++++++++++++++++++++++++++
public:
	volatile unsigned char	  finishing;
		      Serial();
		      ~Serial();
	int           connect          (char *port_arg, int rate_arg,
                                    serial_parity parity_arg); //8n1 default
	void	      (*pSerOnData)( void *ParserObject,uint8_t data);
	void	      *ParserObject;
	void          sendChar         (char c);
	void          sendArray        (char *buffer, int len);
	char          getChar          (void);
	int           getArray         (char *buffer, int len);
	int           getNbrOfBytes    (void);
	void          disconnect       (void);
	void	      initSerialReceive(void (*OnData)(void *parser,uint8_t data), void * Parser);
//	static void CALLBACK OnTimer          (  HWND hwnd,   UINT uMsg, UINT_PTR idEvent,   DWORD dwTime  );
};
#endif