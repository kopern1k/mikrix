/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../../include/multicastInterface.h"

void OnMulticastSockData( uint8_t  *data, uint16_t len, void *parser){
	uint16_t i;
	//DEBUGPRINTF("OnSockData:%d",len);
	for(i=0;i<len;i++){
		OnDataReceive((COM_MNG_DATA*)parser,data[i]);
	}
}
//IP[16]
uint8_t MulticastSocketInterfaceInit( uint8_t *ip, COM_MNG_DATA *cm){
	int ret = InitSockSender();
	if(ret) {
	  cm->enabled = 0;
	  return ret;
	}
	cm->enabled = 1;
	cm->fullDuplex = 1;
	cm->communicationSpeed = 10000000;
	cm->oneByteDelayUs = WAIT_FOR_ONE_BYTE(cm->communicationSpeed);
	InitRandomTime(cm->communicationSpeed);
	return InitSockReceive( ip,OnMulticastSockData, (void*) cm);
}

void MulticastSocketInterfaceWrite(uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending)
{
	uint8_t buf[SEND_BUFF_SIZE];
	uint16_t len=0;
	int ret;
	if(startInd == endInd){
		*Sending = PORT_WRITE_ERROR;
		return;
	}
	endInd=(endInd + 1)& mask;
	do{
		buf[len++]=pData[startInd];
		startInd=( startInd+1 )& mask;
	}while(startInd!=endInd);
	//buf[len++]=pData[startInd];
	ret = SockSendData(buf, len);
	if(ret < 0) {
		  *Sending = PORT_WRITE_ERROR;
		  return;
	}
	*Sending = 0;
}