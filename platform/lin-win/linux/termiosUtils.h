#ifndef __TERMIOS_UTILS_H__
#define __TERMIOS_UTILS_H__
#include <inttypes.h>
#ifdef __cplusplus
extern "C" {
#endif
char _getch(void);

typedef enum  {
	READLINE_NO_ACTION = 0,
	READLINE_ARROW_UP,
	READLINE_ARROW_DOWN,
	READLINE_ARROW_LEFT,
	READLINE_ARROW_RIGHT,
	READLINE_DELETE_KEY,
	READLINE_BACK_KEY,
	READLINE_CARRIAGE_RETURN,
	READLINE_NEW_LINE,
	READLINE_ERR_DATA_LEN = -1
} ReadlineActions_t;

ReadlineActions_t ReadLine(char* data, uint16_t offset, uint16_t maxDataSize, uint16_t *readChars);

void DeleteStdoutChar();
uint8_t SetStdinEcho(uint8_t enable);
void ClearLine();

void InitReadline();
void ResumeStdinReadline();

#ifdef __cplusplus
}
#endif
#endif
