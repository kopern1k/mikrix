/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "../common/sleep.h"
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include <openssl/crypto.h>
//#include <openssl/ssl.h>
#include <openssl/err.h>
#include "socketCommon.h"
#include "threads.h"
#include "../../../common/common.h"

#include "SSLClient.h"

//#include <sys/ioctl.h>
#include <time.h>

#include <signal.h>

#include "sysConfig.h"
#ifdef SSL_CLIENT_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

uint8_t* SSLClientGetError(SSLClientSocket_t* sslSock) {
	return (uint8_t*)sslSock->errorMsg;
}

int8_t SSLClientInitDataEx(SSLClientSocket_t *sslSock, uint8_t ignoreSigPipe) {

	struct sigaction newAction;

	if(sslSock == NULL) {
		return 1;
	}
	pthread_mutex_init(&sslSock->initializing, NULL);
	pthread_mutex_init(&sslSock->cleaning, NULL);
	*sslSock->errorMsg = 0;
	sslSock->finish = 0;
	sslSock->sock = -1;
	sslSock->ssl = NULL;
	sslSock->ctx = NULL;
	sslSock->running = 0;
	sslSock->finish = 0;
	sslSock->maxReadTimeoutMs = 0;

	//if use ignore SIGPIPE
	if(ignoreSigPipe) {
		sigemptyset(&newAction.sa_mask);
		newAction.sa_handler = SIG_IGN;
		newAction.sa_flags = 0;
		if(sigaction(SIGPIPE, &newAction, NULL)) {
			return errno;
		}
	}
	return 0;
}
uint8_t SSLISRunning(SSLClientSocket_t *sslSock) {
	return sslSock->running;
}


uint8_t SSLClientIsInitializing(SSLClientSocket_t *sslSock) {
	if(pthread_mutex_trylock(&sslSock->initializing)) {
		return 1;
	}
	pthread_mutex_unlock(&sslSock->initializing);
	return 0;
}


uint8_t SSLClientIsClosing(SSLClientSocket_t *sslSock) {
	if(pthread_mutex_trylock(&sslSock->cleaning)) {
		return 1;
	}
	pthread_mutex_unlock(&sslSock->cleaning);
	return 0;
}


void SSLClientCleanup(SSLClientSocket_t* sslSock) {
	/* Free the SSL structure */

	pthread_mutex_lock(&sslSock->cleaning);

	if(sslSock->ssl) {
		SSL_shutdown(sslSock->ssl);
		SSL_free(sslSock->ssl);
	}

	sslSock->ssl = NULL;

	/* Free the SSL_CTX structure */
	if(sslSock->ctx){
		SSL_CTX_free(sslSock->ctx);
	}

	sslSock->ctx = NULL;


//#pragma message("OPENSSL_API_COMPAT: " TAG_TO_STRING(OPENSSL_API_COMPAT))
	/* cleanup error state */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	ERR_remove_thread_state(NULL);
#pragma GCC diagnostic pop

	/* Free error strings */
	ERR_free_strings();

	// close SSL Sock
	if(sslSock->sock !=-1) {
		close(sslSock->sock);
		sslSock->sock = -1;
	}

	pthread_mutex_unlock(&sslSock->cleaning);
}


THREAD_RETURN_TYPE ReadThread(void *arg)
{
	SSLClientSocket_t *sslSock;
	sslSock = ((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;

	int receivedLen;
	int ret,sslErrCode;
	fd_set set;
	struct timeval timeout;
	//listening socket
	int sock = sslSock->sock;

	sslSock->pRecBufLen = SSL_DEFAULT_READ_BUF_SIZE + 1;
	sslSock->pRecBuf = malloc(sslSock->pRecBufLen);
	sslSock->running = 1;
	pthread_mutex_unlock(&sslSock->initializing);
	pthread_mutex_unlock(&sslSock->cleaning);

	do {
		receivedLen = 0;
		FD_ZERO(&set); /* clear the set */
		FD_SET(sock, &set); /* add our file descriptor to the set */
		timeout.tv_sec = sslSock->maxReadTimeoutMs / 1000;
		timeout.tv_usec = (sslSock->maxReadTimeoutMs % 1000) * 1000;
		ret = select(sock + 1, &set, NULL, NULL, &timeout);
		if(ret == -1) {
			sprintf(sslSock->errorMsg,"SSL read Error: select error [-1]:\n");
			perror(sslSock->errorMsg); /* an error accured */
			ret = SSL_CLIENT_READ_ERR;
			goto ReadWriteThreadEnd;
		} else if(ret == 0) {
			//printf("timeout\n"); /* a timeout occured */
			strcpy(sslSock->errorMsg,"SSL select timeouted!");
			fprintf(stderr, "%s\n", (char*)sslSock->errorMsg);
			ret = SSL_CLIENT_TIMEOUTED;
			goto ReadWriteThreadEnd;
		} else {
			if(sslSock->ssl && sslSock->sock > 0) {
				ret = SSL_read(sslSock->ssl, sslSock->pRecBuf + receivedLen, SSL_DEFAULT_READ_BUF_SIZE);
			} else {
				// it seems connection was closed by SSLClientSockClose
				ret = SSL_CLIENT_MEM_CLEARED;
				goto ReadWriteThreadEnd;
			}
			sslErrCode = ERR_get_error();
			switch(SSL_get_error(sslSock->ssl,ret)){
			case SSL_ERROR_NONE:
				if(ret) {
					sslSock->pRecBuf[ret + receivedLen] = 0;
					DEBUGPRINTF("Received %d chars:'%s'\n", ret, sslSock->pRecBuf + receivedLen);
					receivedLen += ret;
				} else if(ret == 0) {
					fprintf(stderr, "SSL returned 0. Closing!\n");
					strcpy(sslSock->errorMsg,"SSL read: returned 0. Closing!");
					ret = SSL_CLIENT_READ_ZERRO_DATA_LEN;
					goto ReadWriteThreadEnd;
				}
				break;
			case SSL_ERROR_WANT_READ:
				strcpy(sslSock->errorMsg,"SSL read: want read");
				fprintf(stderr, "SSL want read\n");
				DEBUGPRINTF("want read\n");
				break;
			case SSL_ERROR_WANT_WRITE:
				strcpy(sslSock->errorMsg,"SSL read: want write");
				DEBUGPRINTF("want write\n");
				break;
			case SSL_ERROR_ZERO_RETURN:
				sprintf(sslSock->errorMsg,"SSL read: Zero return, TLS Connection was closed. %s, errno = %d", ERR_error_string(sslErrCode,NULL), errno);
				DEBUGPRINTF(sslSock->errorMsg);
				ret = SSL_CLIENT_TLS_CONNECTION_CLOSED;
				goto ReadWriteThreadEnd;
			case SSL_ERROR_SYSCALL:
				ret = SSL_CLIENT_SYSTEM_CONNECTION_ERR;
				sprintf(sslSock->errorMsg,"SSL read Error: SSL_ERROR_SYSCALL: %s. errno=%d.",ERR_error_string(sslErrCode,NULL),errno);
				DEBUGPRINTF("%s\n", sslSock->errorMsg);
				fprintf(stderr, "%s\n", sslSock->errorMsg);
				goto ReadWriteThreadEnd;
			default:
				sprintf(sslSock->errorMsg,"SSL read problem [%d]",SSL_get_error(sslSock->ssl,ret));
				sslSock->errorMsg[SSL_ERROR_MSG_MAX_LEN-1] = 0;
				fprintf(stderr, "%s\n", (char*)sslSock->errorMsg);
				ret = SSL_CLIENT_UNSPECIFIED_ERR;
				goto ReadWriteThreadEnd;
			}

			if(receivedLen) {
				if(sslSock->onDataCallback) {
					sslSock->onDataCallback(sslSock->pRecBuf,receivedLen, sslSock->userObject);
				}
			}
		}
	} while(sslSock->finish == 0);
ReadWriteThreadEnd:
	FD_CLR(sock, &set);
	if(sslSock->pRecBuf)
		free(sslSock->pRecBuf);
	sslSock->pRecBuf = NULL;
	SSLOnReceiveErrorHandler onReceiveErrorHandler = sslSock->onReceiveErrorHandler;
	void* userObject = sslSock->userObject;
	sslSock->running = 0;
	SSLClientCleanup(sslSock);
	// report error
	if(onReceiveErrorHandler){
		onReceiveErrorHandler(userObject, ret);
	}
  return 0;
}


int8_t SSLClientSendData(SSLClientSocket_t *sslSock, uint8_t* data, uint16_t len) {
	int ret;
	int8_t err = 0;
	uint16_t transferredData = 0;
	if(sslSock == NULL) {
		return SSLCLIENT_ERR_NULL_DATA;
	}
	if(pthread_mutex_trylock(&sslSock->cleaning)) {
		return SSLCLIENT_ERR_ALREADY_CLEANING;
	}
	if(len == 0) {
		ret = SSLCLIENT_ERR_DATA_LEN;
		goto SSLClientOnSentUnlockCleaning;
	}
	if(pthread_mutex_trylock(&sslSock->initializing)) {
		ret = SSLCLIENT_ERR_ALREADY_INITIALIZING;
		goto SSLClientOnSentUnlockCleaning;
	}


	if(sslSock->running == 0) {
		ret = SSLCLIENT_ERR_RECEIVE_THR_NOT_RUNNING;
		goto SSLClientOnSentUnlockInitializing;
	}
	if(SockIsclosed(sslSock->sock)) {
		DEBUGPRINTF("Socket closed!\n");
		ret = SSLCLIENT_ERR_SOCKET_IS_CLOSED;
		goto SSLClientOnSentUnlockInitializing;
	}

	sslSock->pSendBuf = data;
	sslSock->pSendBufLen =  len;
	ret = SSL_write(sslSock->ssl, sslSock->pSendBuf,sslSock->pSendBufLen);
	if(ret > 0) {
		transferredData = ret;
		if(sslSock->pSendBufLen != ret) {
			err = 1;
			ret = SSLCLIENT_ERR_DATA_PARTLY_TRANSFERRED;
		} else {
			ret = SSLCLIENT_OPERATION_OK;
		}
		//ret = 0;
	} else if(ret == 0) {
		transferredData = 0;
		err = 2; //probably connection was closed!
		ret = SSLCLIENT_ERR_PROBABLY_CONNECTION_WAS_CLOSED;
	} else {
		transferredData = 0;
		err = 3;
		ret = SSLCLIENT_ERR_DATA_NOT_TRANSFERRED;
		switch(SSL_get_error(sslSock->ssl,ret)){
		case SSL_ERROR_ZERO_RETURN:
			err = 4;
			break;
		case SSL_ERROR_WANT_READ:
			err = 5;
			break;
		case SSL_ERROR_WANT_WRITE:
			err = 6;
			break;
		case SSL_ERROR_WANT_CONNECT:
			err = 7;
			break;
		case SSL_ERROR_WANT_ACCEPT:
			err = 8;
			break;
		case SSL_ERROR_WANT_X509_LOOKUP:
			err = 9;
			break;
		case SSL_ERROR_SYSCALL:
			err = 10;
			break;
		case SSL_ERROR_SSL:
			err = 11;
			break;
		default:
			err = 12;
			break;
		}
	}
	sslSock->onSentCallback(sslSock->userObject,data,transferredData, err);

SSLClientOnSentUnlockInitializing:
	pthread_mutex_unlock(&sslSock->initializing);
SSLClientOnSentUnlockCleaning:
	pthread_mutex_unlock(&sslSock->cleaning);
	return ret;
}


int PemPasswdCallback(char *buf, int size, int rwflag, void *userdata) {
	SSLClientSocket_t *sslSock = (SSLClientSocket_t *)userdata;
	if(rwflag == 0) { //decryption
		DEBUGPRINTF("decryption password\n");
	}
	int maxlen = SSL_MAX_KEY_PASSPHRASE < size ? SSL_MAX_KEY_PASSPHRASE : size;
	if(*sslSock->clientKeyPassPhrase != 0) {
		if(strlen(sslSock->clientKeyPassPhrase) >= size) {
			return 0;
		}
		strncpy(buf,sslSock->clientKeyPassPhrase, maxlen);
		return strlen(sslSock->clientKeyPassPhrase);
	}
	return 0;
}


uint8_t SSLClientSockOpen(SSLClientSocket_t *sslSock, uint8_t ip[16], uint16_t port, uint8_t verifyClient, char* clientCertFile, char* clientKeyFile, char* clientKeyPassPhrase, char* CAServerCertFile, SSLClientOnDataCallback_t onDataCallback, SSLOnSentCallback_t onSentCallback, void *userObject, SSLOnReceiveErrorHandler onReceiveErrorHandler, uint32_t maxReadTimeoutMs) {
	int ret;
	char *str;
	pthread_mutex_lock(&sslSock->cleaning);
	pthread_mutex_lock(&sslSock->initializing);

	if(sslSock == NULL) {
		ret = 1;
		goto SSLInitializingExit;
	}

	if(sslSock->running) {
		ret = 100;
		goto SSLInitializingExit;
	}

	*sslSock->errorMsg = 0;
	sslSock->finish = 0;
	if(sslSock->sock != -1) {
		close(sslSock->sock);
	}
	sslSock->sock = -1;
	sslSock->ssl = NULL;
	sslSock->ctx = NULL;
	//sslSock->server = 0;
	sslSock->verifyClient = FALSE;

	if(ip == NULL) {
		ret = 2;
		goto SSLInitializingExit;
	}

	if(verifyClient) {
		if(clientCertFile == NULL) {
			ret = 3;
			goto SSLInitializingExit;
		}
		if(clientKeyFile == NULL) {
			ret = 4;
			goto SSLInitializingExit;
		}
		if(CAServerCertFile == NULL) {
			ret = 5;
			goto SSLInitializingExit;
		}
		if(clientKeyPassPhrase != NULL) {
			if(strlen(clientKeyPassPhrase) > SSL_MAX_FILE_NAME) {
				ret = 5;
				goto SSLInitializingExit;
			}
			strncpy(sslSock->clientKeyPassPhrase, clientKeyPassPhrase, SSL_MAX_KEY_PASSPHRASE);
		} else {
			*sslSock->clientKeyPassPhrase = 0;
		}

		sslSock->verifyClient = TRUE;

		strncpy(sslSock->clientCertFile,clientCertFile,SSL_MAX_FILE_NAME);
		strncpy(sslSock->clientKeyFile,clientKeyFile,SSL_MAX_FILE_NAME);

	}
	strncpy(sslSock->CACert,CAServerCertFile,SSL_MAX_FILE_NAME);

	//setup internal parameters
	strncpy(sslSock->sIPAddr,(char*)ip,16);
	sslSock->sPort = port;
	sslSock->onDataCallback = onDataCallback;
	sslSock->onSentCallback = onSentCallback;
	sslSock->userObject = userObject;
	sslSock->onReceiveErrorHandler = onReceiveErrorHandler;
	sslSock->maxReadTimeoutMs = maxReadTimeoutMs;


	/* Load encryption & hashing algorithms for the SSL program */
	SSL_library_init();

	/* Load the error strings for SSL & CRYPTO APIs */
	SSL_load_error_strings();
	//ERR_load_crypto_strings();

	/* Create an SSL_METHOD structure (choose an SSL/TLS protocol version) */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	sslSock->meth = (SSL_METHOD*)SSLCLIENT_PROTOCOL_METHOD();
#pragma GCC diagnostic pop
	/* Create an SSL_CTX structure */
	sslSock->ctx = SSL_CTX_new(sslSock->meth);

	if(sslSock->ctx == NULL) {
		strcpy(sslSock->errorMsg, "Error ctx not created");
		ret = 6;
		goto SSLInitializingExit;
	}

	/* set communication timeout */
	SSL_CTX_set_timeout(sslSock->ctx,10); //10 second timeout

	/*----------------------------------------------------------*/
	if(sslSock->verifyClient == TRUE) {

		/* Load the client certificate into the SSL_CTX structure */
		if ((ret = SSL_CTX_use_certificate_chain_file(sslSock->ctx, sslSock->clientCertFile)) <= 0) {
			ERR_error_string(ERR_get_error(),sslSock->errorMsg);
			ret = 7;
			goto SSLCleanupInitCTX;
		}

		/* setup password for key */
		SSL_CTX_set_default_passwd_cb(sslSock->ctx, PemPasswdCallback);
		SSL_CTX_set_default_passwd_cb_userdata(sslSock->ctx,(void*) sslSock);

		/* Load the private-key corresponding to the client certificate */
		if ((ret =SSL_CTX_use_PrivateKey_file(sslSock->ctx,  sslSock->clientKeyFile, SSL_FILETYPE_PEM)) <= 0) {
			ERR_error_string(ERR_get_error(),sslSock->errorMsg);
			ret = 8;
			goto SSLCleanupInitCTX;
		}

		/* Check if the client certificate and private-key matches */
		if (!SSL_CTX_check_private_key(sslSock->ctx)) {
			strcpy(sslSock->errorMsg, "Private key does not match the certificate public key");
			ret = 9;
			goto SSLCleanupInitCTX;
		}
	}

	/* Load the RSA CA certificate into the SSL_CTX structure */
	/* This will allow this client to verify the server's     */
	/* certificate.                                           */

	if (!(ret = SSL_CTX_load_verify_locations(sslSock->ctx, sslSock->CACert, NULL))) {
		ERR_error_string(ERR_get_error(),sslSock->errorMsg);
		ret = 10;
		goto SSLCleanupInitCTX;
	}

	/* Set flag in context to require peer (server) certificate */
	/* verification */
	if(sslSock->verifyClient == TRUE) {
		SSL_CTX_set_verify(sslSock->ctx, SSL_VERIFY_PEER,NULL);
	} else {
		SSL_CTX_set_verify(sslSock->ctx, SSL_VERIFY_NONE,NULL);
	}


	SSL_CTX_set_verify_depth(sslSock->ctx,2);
	/* ------------------------------------------------------------- */
	/* Set up a TCP socket */

	sslSock->sock = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(sslSock->sock == 0) {
		strcpy(sslSock->errorMsg, "SSL Socket could not be opened!");
		ret = 11;
		goto SSLCleanupInitCTX;
	}
#ifdef SSL_CLIENT_TCP_SOCK_PRIO
	if(SetSockPriority(sslSock->sock, 7)) {
		DEBUGPRINTF("SSL SetSockPriority was not sucessfull\n");
	}
#endif
	SetSockKeepAlive(sslSock->sock);
	memset (&(sslSock->serverAddr), '\0', sizeof(sslSock->serverAddr));
	sslSock->serverAddr.sin_family = AF_INET;

	sslSock->serverAddr.sin_port = htons(sslSock->sPort);       /* Server Port number */

	sslSock->serverAddr.sin_addr.s_addr = inet_addr(sslSock->sIPAddr); /* Server IP */

	 /* Establish a TCP/IP connection to the SSL client */

	if((ret = SockConnect(sslSock->sock, (struct sockaddr_in*) &sslSock->serverAddr, sizeof(sslSock->serverAddr), SSLCLIENT_SOCKET_CONNECT_TIMEOUT))) {
		strcpy(sslSock->errorMsg, "SSL Connect() problem!");
		ret = 12;
		goto SSLCleanupInitSock;
	}


	/* ----------------------------------------------- */
	/* An SSL structure is created */

	if((sslSock->ssl = SSL_new (sslSock->ctx)) == NULL) {
		strcpy(sslSock->errorMsg, "SSL SSL_new() problem!");
		ret = 13;
		goto SSLCleanupInitSock;
	}

	/* Assign the socket into the SSL structure (SSL and socket without BIO) */
	SSL_set_fd(sslSock->ssl, sslSock->sock);

	/* Perform SSL Handshake on the SSL client */
	if((ret = SSL_connect(sslSock->ssl)) == -1) {
		ERR_error_string_n(ERR_get_error(),sslSock->errorMsg,sizeof(sslSock->errorMsg));
		strncat(sslSock->errorMsg,"; ",sizeof(sslSock->errorMsg));
		ERR_error_string_n(SSL_get_error(sslSock->ssl,ret), &sslSock->errorMsg[strlen(sslSock->errorMsg)], sizeof(sslSock->errorMsg) - strlen(sslSock->errorMsg) );
		snprintf(&sslSock->errorMsg[strlen(sslSock->errorMsg)],sizeof(sslSock->errorMsg) - strlen(sslSock->errorMsg), "; verify result: %d - %s.",(uint32_t)SSL_get_verify_result(sslSock->ssl), X509_verify_cert_error_string (SSL_get_verify_result(sslSock->ssl)));
		//X509 *err_cert = X509_STORE_CTX_get_current_cert(&sslSock->ctx->cert_store->get_issuer());
		//strncat(sslSock->errorMsg,"; Issuer:",sizeof(sslSock->errorMsg));
		//X509_NAME_oneline(X509_get_issuer_name(err_cert), &sslSock->errorMsg[strlen(sslSock->errorMsg)], sizeof(sslSock->errorMsg) - strlen(sslSock->errorMsg));
		ret = 14;
		goto SSLCleanupInitSSL;
	}

	/* Informational output (optional) */
	DEBUGPRINTF("SSL connection using %s\n", SSL_get_cipher (sslSock->ssl));

	/* Get the server's certificate (optional) */
	sslSock->serverCert = SSL_get_peer_certificate (sslSock->ssl);

	if (sslSock->serverCert != NULL) {
		DEBUGPRINTF("Server certificate:\n");
		if((str = X509_NAME_oneline(X509_get_subject_name(sslSock->serverCert),0,0)) != NULL) {
			DEBUGPRINTF("\t subject: %s\n", str);
			free (str);
		}
		if((str = X509_NAME_oneline(X509_get_issuer_name(sslSock->serverCert),0,0)) != NULL) {
			DEBUGPRINTF ("\t issuer: %s\n", str);
			free(str);
		}
		if (SSL_get_verify_result(sslSock->ssl) == X509_V_OK) {
			DEBUGPRINTF("Certificate verified!\n");
		}

		X509_free (sslSock->serverCert);

	} else {
		DEBUGPRINTF("The SSL server does not have certificate.\n");
	}

	if(createThread(ReadThread, sslSock) == 0){
		ret = 15;
		sslSock->finish = 1;
		goto SSLCleanupInit;
	}
	//open read - write thread
	//pthread_mutex_unlock(&sslSock->initializing);
	return 0;

SSLCleanupInit:
	if(sslSock->ssl != NULL) {
		if(!SockIsclosed(sslSock->sock)) {
			if(SSL_get_shutdown(sslSock->ssl) == 0) { //close session if not yet close request sent
				SSL_shutdown(sslSock->ssl);
			}
		}
	}
SSLCleanupInitSSL:
	/* Free the SSL structure */
	if(sslSock->ssl) {
		SSL_free(sslSock->ssl);
	}

	sslSock->ssl = NULL;

SSLCleanupInitSock:
	if(sslSock->sock > 0) {
		close(sslSock->sock);
	}

	sslSock->sock = -1;
SSLCleanupInitCTX:
	/* Free the SSL_CTX structure */
	if(sslSock->ctx){
		SSL_CTX_free(sslSock->ctx);
	}

	sslSock->ctx = NULL;

	/* cleanup error state */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	ERR_remove_thread_state(NULL);
#pragma GCC diagnostic pop
	/* Free error strings */
	ERR_free_strings();
SSLInitializingExit:
	pthread_mutex_unlock(&sslSock->cleaning);
	pthread_mutex_unlock(&sslSock->initializing);
	return ret;
}


uint8_t SSLClientSockForceClose(SSLClientSocket_t* sslSock) {
	if(sslSock->running == 0) {
		return 1; // no need for close
	}
	//sslSock->running = 0;
	int err = 0;

	*sslSock->errorMsg = 0;
	sslSock->finish = 1;
	SSL_set_shutdown(sslSock->ssl,SSL_SENT_SHUTDOWN);
	/* Terminate communication on a socket */
	if(sslSock->sock > 0) {
		err = close(sslSock->sock);
		if(err < 0) {
			strcat(sslSock->errorMsg,"Error 2. Cannot close socket");
			//return 3;
		} else {
			sslSock->sock = -1;
		}
	}

	SSL_do_handshake(sslSock->ssl);
	// wait for thread to be closed. On error callback should be called

	return 0;
}


uint8_t SSLClientSockClose(SSLClientSocket_t* sslSock) {
	// do not close while initializing is in progress
	if(SSLClientIsInitializing(sslSock)) {
		return 1;
	}
	pthread_mutex_lock(&sslSock->cleaning);
	if(sslSock->running == 0) {
		pthread_mutex_unlock(&sslSock->cleaning);
		return 2; // no need for close
	}

	*sslSock->errorMsg = 0;
	sslSock->finish = 1;
	/*--------------- SSL closure ---------------*/
	/* Shutdown the client side of the SSL connection */
	int err = 0;
	int cnt = 0;
	do {
		if(sslSock->ssl != NULL) {
			if(!SockIsclosed(sslSock->sock)) {
				if(SSL_get_shutdown(sslSock->ssl) != SSL_SENT_SHUTDOWN) { // only one way shutdown is processed
					err = SSL_shutdown(sslSock->ssl);
				}
			}
		}
	} while (cnt++ < 3 && !(err == 0 || err == 1));


	if(err < 0) {
		strcpy(sslSock->errorMsg,"Error 1. SSL_shutdown. ");
		//return 2;
	}


	/* Terminate communication on a socket */
	if(sslSock->sock > 0) {
		err = close(sslSock->sock);
		if(err < 0) {
			strcat(sslSock->errorMsg,"Error 2. Cannot close socket");
			//return 3;
		}
	}

	sslSock->sock = -1;

	pthread_mutex_unlock(&sslSock->cleaning);
	return 0;
}


#define TEST_RSA_CLIENT_CERT 	"cert/client.pem"
#define TEST_RSA_CLIENT_KEY  	"cert/client.key"

#define TEST_RSA_CLIENT_CA_CERT	"cert/ca.pem"
SSLClientSocket_t sslSocket;

void TestOnSentCallback(void *parserObject, uint8_t *data, uint16_t transferredData, int8_t err) {
	printf("Sent data %p with len %hu, for object %p, with error %d\n", data, transferredData, parserObject, err);
	if(err) {
		printf("Got SSL error: %s", SSLClientGetError(&sslSocket));
	}
}

void TestReceiveCallback(uint8_t *data, uint32_t len, void *parserObject ) {
	printf("Have data %p with len %u and object %p\n", data, len, parserObject);
	/* store your received data elsewhere it will be deleted */

}

void TestSSLClient() {
	int ret;
	uint8_t data[] = "GET /\r\n\r\n";
	//ret = SSLSockInit(&sslSocket, "192.30.252.130", 443, FALSE, RSA_CLIENT_CERT, RSA_CLIENT_KEY,NULL, RSA_CLIENT_CA_CERT, TestReceiveCallback, TestOnSentCallback, NULL);
	ret = SSLClientSockOpen(&sslSocket, (uint8_t*)"127.0.0.1", 4444, TRUE, TEST_RSA_CLIENT_CERT, TEST_RSA_CLIENT_KEY,"testclient", TEST_RSA_CLIENT_CA_CERT, TestReceiveCallback, TestOnSentCallback, NULL,NULL,0);
	if(ret) {
		printf("Got SSL error: %s\n", SSLClientGetError(&sslSocket));
		SSLClientSockClose(&sslSocket);
		ret = SockIsclosed(sslSocket.sock);
		return;
	}
	uint8_t len = (uint8_t)strlen((char*)data);
	SSLClientSendData(&sslSocket,data,len);
	//defaultThreadArgs_t a;
	//a.started = 0;
	//a.args = &sslSocket;
//	ReadWriteThread(&a);

	while(true){
		SLEEP(1);
		fflush(stdout);
		fflush(stderr);
		//ret = SSL_write(sslSocket.ssl, sslSocket.pSendBuf,sslSocket.pSendBufLen);
		SSLClientSendData(&sslSocket,data,len);
	}

	SLEEP(1200);
	SSLClientSockClose(&sslSocket);
	ret = SockIsclosed(sslSocket.sock);
}


