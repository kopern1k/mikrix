#include <string.h>
#ifdef USE_OLD_MEMCPY_225

/* some systems do not have newest memcpy@@GLIBC_2.14 - stay with old good one */
__asm__ (".symver memcpy, memcpy@GLIBC_2.2.5");
void *__wrap_memcpy(void *dest, const void *src, size_t n)
{
    return memcpy(dest, src, n);
}
#endif
