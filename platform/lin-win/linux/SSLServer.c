/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
//#include <unistd.h> //included in sleep.h
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include "sleep.h"
#include <fcntl.h>
#include "socketCommon.h"
#include "threads.h"
#include "../../../common/common.h"

#include "SSLServer.h"

//#include <sys/ioctl.h>
//#define __need_timeval
//#define timeval
#include <time.h>

#include "sysConfig.h"
#ifdef SSL_SERVER_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

int CertVerifyCallback(int ok, X509_STORE_CTX* store) {
	char data[255];

	if (!ok) {
		X509* cert = X509_STORE_CTX_get_current_cert(store);
		int depth = X509_STORE_CTX_get_error_depth(store);
		int err = X509_STORE_CTX_get_error(store);

		printf("Error with certificate at depth: %d!\n", depth);
		X509_NAME_oneline(X509_get_issuer_name(cert), data, 255);
		printf("\tIssuer: %s\n", data);
		X509_NAME_oneline(X509_get_subject_name(cert), data, 255);
		printf("\tSubject: %s\n", data);
		printf("\tError %d: %s\n", err, X509_verify_cert_error_string(err));
	}

	return ok;
}


THREAD_RETURN_TYPE SSLServerReadThread(void *arg) {
	SSLServerClient_t *pSSLServerClient;
	SSLServerSocket_t *sslSock;
	pSSLServerClient = ((defaultThreadArgs_t *)arg)->args;
	if(pSSLServerClient == NULL) {
		return (THREAD_RETURN_TYPE)1;
	}
	sslSock = pSSLServerClient->pSSLSock;
	//((defaultThreadArgs_t *)arg)->started = 1;
	int ret;
	char *str;
	uint8_t receivedLen;


	/* ----------------------------------------------- */
	/* TCP connection is ready. */
	/* A SSL structure is created */
	pSSLServerClient->ssl = SSL_new(sslSock->ctx);
	if(pSSLServerClient->ssl == NULL) {
		return (THREAD_RETURN_TYPE)2;
	}

	/* Assign the socket into the SSL structure (SSL and socket without BIO) */
	SSL_set_fd(pSSLServerClient->ssl, pSSLServerClient->sock);

	/* Perform SSL Handshake on the SSL server */
	ret = SSL_accept(pSSLServerClient->ssl);
	if(ret< 0) {
		ERR_print_errors_fp(stderr);
		return (THREAD_RETURN_TYPE)3;
	}
	((defaultThreadArgs_t *)arg)->started = 1;

	/* Informational output (optional) */
	printf("SSL connection using %s\n", SSL_get_cipher (pSSLServerClient->ssl));

	if (sslSock->verifyClient == TRUE) {
		/* Get the client's certificate (optional) */
		pSSLServerClient->clientCert = SSL_get_peer_certificate(pSSLServerClient->ssl);
		if (pSSLServerClient->clientCert != NULL) {
			printf ("Client certificate:\n");
			str = X509_NAME_oneline(X509_get_subject_name(pSSLServerClient->clientCert), 0, 0);
			if(str == NULL) {
				return (THREAD_RETURN_TYPE)4;
			}
			printf ("\t subject: %s\n", str);
			free (str);
			str = X509_NAME_oneline(X509_get_issuer_name(pSSLServerClient->clientCert), 0, 0);
			if(str == NULL) {
				return (THREAD_RETURN_TYPE)44;
			}
			printf ("\t issuer: %s\n", str);

			if (SSL_get_verify_result(pSSLServerClient->ssl) == X509_V_OK) {
				/* The client sent a certificate which verified OK */
				DEBUGPRINTF("Client certificate verified\n");
			}
			free (str);
			X509_free(pSSLServerClient->clientCert);
		} else {
			printf("The SSL client does not have certificate.\n");
		}
	}
	if(sslSock->onClientConnectedCallback) {
		sslSock->onClientConnectedCallback(pSSLServerClient);
	}

	/*------- DATA EXCHANGE - Receive message and send reply. -------*/
	/* Receive data from the SSL client */
	//create buffer
	pSSLServerClient->pRecBufLen = SSL_DEFAULT_READ_BUF_SIZE + 1;
	pSSLServerClient->pRecBuf = malloc(pSSLServerClient->pRecBufLen);
	if(pSSLServerClient->pRecBuf == NULL) {
		return (THREAD_RETURN_TYPE)5;
	}

	do {
		receivedLen = 0;
		ret = SSL_read(pSSLServerClient->ssl, pSSLServerClient->pRecBuf, pSSLServerClient->pRecBufLen);

		switch(SSL_get_error(pSSLServerClient->ssl,ret)){
		case SSL_ERROR_NONE:
			if(ret) {
				pSSLServerClient->pRecBuf[ret] = 0; //for string len issues problem
				DEBUGPRINTF("Received %d chars:'%s'\n", ret, pSSLServerClient->pRecBuf);
				receivedLen = ret;
			}
			break;
		case SSL_ERROR_WANT_READ:
			strcpy(pSSLServerClient->errorMsg,"SSL read: want read");
			DEBUGPRINTF("want read\n");
			break;
		case SSL_ERROR_WANT_WRITE:
			strcpy(pSSLServerClient->errorMsg,"SSL read: want write");
			DEBUGPRINTF("want write\n");
			break;
		case SSL_ERROR_ZERO_RETURN:
			strcpy(pSSLServerClient->errorMsg,"SSL read: ZERO RETURN");
			DEBUGPRINTF("ZERO_RETURN\n");
			goto SSLServerReadThreadEnd;
		case SSL_ERROR_SYSCALL:
			strcpy(pSSLServerClient->errorMsg,"SSL Error: Premature close");
			DEBUGPRINTF("ERROR_SYSCALL\n");
			fprintf(stderr, "SSL Error: Premature close\n");
			goto SSLServerReadThreadEnd;
		default:
			strcpy(pSSLServerClient->errorMsg,"SSL read problem");
			fprintf(stderr, "SSL read problem\n");
			goto SSLServerReadThreadEnd;
		}
		if(receivedLen) {
			if(sslSock->onServerDataCallback)
				sslSock->onServerDataCallback((uint8_t*)pSSLServerClient->pRecBuf,receivedLen, pSSLServerClient, sslSock->userObject);
		}
	} while(sslSock->finish == 0);

SSLServerReadThreadEnd:
	if(pSSLServerClient->pRecBuf) {
		free(pSSLServerClient->pRecBuf);
	}
	/* Terminate communication on a socket */
	ret = close(pSSLServerClient->sock);
	if(ret < 0 ) {
		strcpy(pSSLServerClient->errorMsg,"SSL close error");
		//return (THREAD_RETURN_TYPE)6;
	}

	/*--------------- SSL closure ---------------*/
	/* Shutdown this side (server) of the connection. */

	ret = SSL_shutdown(pSSLServerClient->ssl);

	if(ret < 0) {
		strcpy(pSSLServerClient->errorMsg,"SSL_shutdown error");
		//return (THREAD_RETURN_TYPE) 5;
	}

	/* Free the SSL structure */
	SSL_free(pSSLServerClient->ssl);
	/* free pSSLServerClient structure */
	free(pSSLServerClient);
	return 0;
}



THREAD_RETURN_TYPE ServerThread(void *arg) {
	SSLServerSocket_t *sslSock;
	sslSock = ((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;
	//wait for client is connected
	int ret;

	memset (&sslSock->serverAddr, '\0', sizeof(sslSock->serverAddr));
	sslSock->serverAddr.sin_family      = AF_INET;
	sslSock->serverAddr.sin_addr.s_addr = INADDR_ANY;
	sslSock->serverAddr.sin_port        = htons (sslSock->sPort);          /* Server Port number */
	ret = bind(sslSock->listenSock, (struct sockaddr*)&sslSock->serverAddr,sizeof(sslSock->serverAddr));

	if(ret < 0) {
		strcpy(sslSock->errorMsg, "bind problem");
		return (THREAD_RETURN_TYPE)1;
	}

	/* Wait for an incoming TCP connection. */
	ret = listen(sslSock->listenSock, SSL_SERVER_MAX_CONNECTIONS);

	if(ret < 0) {
		strcpy(sslSock->errorMsg, "listen problem");
		return (THREAD_RETURN_TYPE)2;
	}

	do {

		//for n server sessions


		SSLServerClient_t *pSSLServerClient = malloc(sizeof(SSLServerClient_t));
		pSSLServerClient->pSSLSock = sslSock;

		int clientLen = sizeof(pSSLServerClient->clientSockAddr);

		/* Socket for a TCP/IP connection is created */
		pSSLServerClient->sock = accept(sslSock->listenSock, (struct sockaddr*)&pSSLServerClient->clientSockAddr,(socklen_t*) &clientLen);
		if(pSSLServerClient->sock < 0) {
			return (THREAD_RETURN_TYPE)3;
		}
		//close (sslSock->listenSock);

		printf ("Connection from %x, port %d\n", sslSock->serverAddr.sin_addr.s_addr, sslSock->serverAddr.sin_port);

		//defaultThreadArgs_t t;
		//t.args = pSSLServerClient;
		//SSLServerReadThread(&t);

		if(createThread(SSLServerReadThread, pSSLServerClient) == 0){
			return (THREAD_RETURN_TYPE)4;
		}
	}while(sslSock->finish == 0);

	//TODO: wait for clients

	close (sslSock->listenSock);
	/* Free the SSL_CTX structure */
	SSL_CTX_free(sslSock->ctx);

	return 0;
}


int8_t SSLServerSendData(SSLServerClient_t *pSSLServerClient, uint8_t* data, uint16_t len) {
	int ret;
	if(pSSLServerClient == NULL) {
		return -2;
	}
	if(len == 0) {
		return -3;
	}
	if(SockIsclosed((SOCKET)pSSLServerClient->sock)) {
		DEBUGPRINTF("Socket closed!\n");
		return -4;
	}

	ret = SSL_write(pSSLServerClient->ssl, data, len);
	if(ret > 0) {
		ret = 0;
	}
	pSSLServerClient->pSSLSock->onSentCallback(pSSLServerClient->pSSLSock->userObject,data,ret);
	return ret;
}


int PemServerPasswdCallback(char *buf, int size, int rwflag, void *userdata) {
	SSLServerSocket_t *sslSock = (SSLServerSocket_t *)userdata;
	if(rwflag == 0) { //decryption
		DEBUGPRINTF("decryption password\n");
	}
	int maxlen = SSL_MAX_KEY_PASSPHRASE < size ? SSL_MAX_KEY_PASSPHRASE : size;
	if(*sslSock->serverKeyPassPhrase != 0) {
		if(strlen(sslSock->serverKeyPassPhrase) >= size) {
			return 0;
		}
		strncpy(buf,sslSock->serverKeyPassPhrase, maxlen);
		return strlen(sslSock->serverKeyPassPhrase);
	}
	return 0;
}


uint8_t SSLSServerInit(SSLServerSocket_t *sslSock, uint8_t ip[16], uint16_t port, uint8_t verifyClient, char* serverCertFile, char* serverKeyFile, char* serverKeyPassPhrase, char* CAClientCertFile, SSLServerOnDataCallback_t onDataCallback, SSLServerOnSentCallback_t onSentCallback, SSLOnClientConnected_t onClientConected, SSLOnClientDisconected_t onClientDisconected, void *userObject ) {

	if(sslSock == NULL) {
		return 1;
	}
	*sslSock->errorMsg = 0;
	sslSock->finish = 0;
	//sslSock->server = 1;

	if(ip == NULL) {
		return 2;
	}

	if(verifyClient) {
		if(CAClientCertFile == NULL) {
			return 5;
		}
		strncpy(sslSock->CACert,CAClientCertFile,SSL_MAX_FILE_NAME);
		sslSock->verifyClient = TRUE;
	}

	if(serverCertFile == NULL) {
		return 3;
	}

	if(serverKeyFile == NULL) {
			return 4;
	}

	if(serverKeyPassPhrase != NULL) {
		if(strlen(serverKeyPassPhrase) > SSL_MAX_FILE_NAME) {
			return 5;
		}
		strncpy(sslSock->serverKeyPassPhrase, serverKeyPassPhrase, SSL_MAX_KEY_PASSPHRASE);
	} else {
		*sslSock->serverKeyPassPhrase = 0;
	}

	strncpy(sslSock->serverCertFile, serverCertFile, SSL_MAX_FILE_NAME);
	strncpy(sslSock->serverKeyFile, serverKeyFile, SSL_MAX_FILE_NAME);



	//setup internal parameters
	strncpy(sslSock->sIPAddr,(const char*)ip,16);
	sslSock->sPort = port;
	sslSock->onServerDataCallback = onDataCallback;
	sslSock->onSentCallback = onSentCallback;
	sslSock->userObject = userObject;
	sslSock->onClientConnectedCallback = onClientConected;
	sslSock->onClientDisconectedCallback = onClientDisconected;

	/*----------------------------------------------------------------*/
	/* Load encryption & hashing algorithms for the SSL program */
	SSL_library_init();

	/* Load the error strings for SSL & CRYPTO APIs */
	SSL_load_error_strings();

	/* Create a SSL_METHOD structure (choose a SSL/TLS protocol version) */
	sslSock->meth = (SSL_METHOD*) SSLSERVER_PROTOCOL_METHOD();

	/* Create a SSL_CTX structure */
	sslSock->ctx = SSL_CTX_new(sslSock->meth);

	if (!sslSock->ctx) {
		strcpy(sslSock->errorMsg, "Error ctx not created");
		return 6;
	}
	/* Load the server certificate into the SSL_CTX structure */
	if (SSL_CTX_use_certificate_chain_file(sslSock->ctx, sslSock->serverCertFile) <= 0) {
		strcpy(sslSock->errorMsg, "Error loading the server certificate into the SSL_CTX structure");
		//ERR_print_errors_fp(stderr);
		return 7;
	}

	/* setup password for key */
	SSL_CTX_set_default_passwd_cb(sslSock->ctx, PemServerPasswdCallback);
	SSL_CTX_set_default_passwd_cb_userdata(sslSock->ctx,(void*) sslSock);

	/* Load the private-key corresponding to the server certificate */
	if (SSL_CTX_use_PrivateKey_file(sslSock->ctx, sslSock->serverKeyFile, SSL_FILETYPE_PEM) <= 0) {
		strcpy(sslSock->errorMsg, "Error loading the server key File into the SSL_CTX structure");
		return 8;
	}

	/* Check if the server certificate and private-key matches */
	if (!SSL_CTX_check_private_key(sslSock->ctx)) {
		strcpy(sslSock->errorMsg, "Private key does not match the certificate public key\n");
		return 9;
	}

	if(sslSock->verifyClient == TRUE) {
	/* Load the RSA CA certificate into the SSL_CTX structure */
		if (!SSL_CTX_load_verify_locations(sslSock->ctx, sslSock->CACert, NULL)) {
			strcpy(sslSock->errorMsg, "Error loading CA certificate into the SSL_CTX structure");
			return 10;
		}
		/* Set to require peer (client) certificate verification */
		SSL_CTX_set_verify(sslSock->ctx, SSL_VERIFY_PEER, NULL);
		//SSL_CTX_set_verify(sslSock->ctx, SSL_VERIFY_PEER, CertVerifyCallback);
		/* Set the verification depth to 1 */
		SSL_CTX_set_verify_depth(sslSock->ctx,1);
	}
	/* ----------------------------------------------- */
	/* Set up a TCP socket */
	if(	(sslSock->listenSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		strcpy(sslSock->errorMsg, "Could not create socket");
		return 11;
	}
	//open reading thread
	if(createThread(ServerThread, sslSock) == 0){
		return 13;
	}

	return 0;
}

uint8_t* SSLServerGetError(SSLServerSocket_t* sslSock) {
	return (uint8_t*)sslSock->errorMsg;
}

uint8_t SSLServerClose(SSLServerSocket_t* sslSock) {
	if(sslSock == NULL) {
		return 1;
	}
	sslSock->finish = 1;
	return 0;
}


#define RSA_SERVER_CERT     "../cert/server.pem"
#define RSA_SERVER_KEY          "../cert/server.key"

#define RSA_SERVER_CA_CERT "../cert/ca.pem"
//#define RSA_SERVER_CA_PATH   "sys$common:[syshlp.examples.ssl]"

void SSLServerOnDataCallback(uint8_t *data, uint32_t len, SSLServerClient_t* pSSLServerClient, void *parser) {

	SSLServerSendData(pSSLServerClient,(uint8_t*)"pSSt",4);

}

void SSLServerOnSentCallback (void * object, uint8_t* data, uint8_t err) {
	DEBUGPRINTF("SSLServerOnSentCallback: err %d\n",err);
}

void SSLOnClientConnected(struct SSLServerClient_t *pSSLServerClient) {
	DEBUGPRINTF("SSLOnClientConnected\n");
}

void SSLOnClientDisconected(struct SSLServerClient_t *pSSLServerClient) {
	DEBUGPRINTF("SSLOnClientDisconected");
}

void TestSSLServer() {
	int cnt;
	int ret;
	cnt= 0;
	printf("SLL SERVER STARTED!\n");
	SSLServerSocket_t serverSocket;
	ret = SSLSServerInit(&serverSocket,(uint8_t*)"127.0.0.1",4444,TRUE,RSA_SERVER_CERT,RSA_SERVER_KEY,NULL,RSA_SERVER_CA_CERT,SSLServerOnDataCallback,SSLServerOnSentCallback,SSLOnClientConnected,SSLOnClientDisconected,NULL);
	if(ret) {
		DEBUGPRINTF("%s",SSLServerGetError(&serverSocket));
	}
	do{
//		usleep(10000);
		SLEEP(100);
	//}while(TRUE);
	}while(cnt++<1000);
	printf("SLL SERVER EXITED!\n");
}

