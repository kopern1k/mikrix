/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "sysConfig.h"
#include "../../include/multicastInterface.h"

#ifdef MULTISTREAM_INTERFACE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

void OnMulticastSockData(void *object, uint8_t *data, uint32_t len) {
	GeneralInterafceOnData(data,len,(COM_MNG_DATA*) object);
}


uint8_t MulticastWantReinit(uint8_t error) {
	return error > 0;
}

uint8_t MulticastIsReopenedCorrectly(uint8_t reopenRetVal) {
	return reopenRetVal == 0;
}

void MulticastOnReceiveError(void* object, uint16_t error) {
	GeneralInterfaceOnReceiveError(object,error);

}

void MulticastReopenFailed(uint8_t error, MulticastInterfaceData_t* data) {
	DEBUGPRINTF("%s, error= %hhu\n",data->sockMulticast.errorMsg, error);
}

void MulticastSocketInterfaceWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending) {
	GeneralInterfaceWrite((void*)&(((MulticastInterfaceData_t*)cm->interfaceData)->sockMulticast),
						  pData, startInd, endInd, mask,(GeneralInterfaceSendData_t)SockMulticastSendData,Sending);
}

uint8_t MulticastSocketInterfaceClose(COM_MNG_DATA *cm){

	return GeneralInterfaceClose( &(((MulticastInterfaceData_t*)cm->interfaceData)->GIData), 1);
}

uint8_t SockMulticastInterfaceOpen(MulticastInterfaceData_t *pInterfaceData, MulticastConfig_t* config) {
	return SockMulticastOpen(&pInterfaceData->sockMulticast, (char*)config->hostName, config->port, OnMulticastSockData, NULL, pInterfaceData->GIData.thrReinitData.cm, MulticastOnReceiveError);
}

uint8_t MulticastSocketInterfaceInit( uint8_t*data, COM_MNG_DATA *cm) {
	MulticastConfig_t* pData = (MulticastConfig_t*)data;

	if(pData == NULL) {
		return GI_ERROR_CONFIG_DATA;
	}

	if((cm->interfaceData = malloc(sizeof(MulticastInterfaceData_t))) == NULL) {
		ChangeInterfaceState(cm,CM_STATE_CLOSED);
		return GI_ERROR_MEMORY_ALLOC;
	}
	GeneralInterfacePreinit(cm,10000000,GI_FullDuplex);

	MulticastInterfaceData_t *pMulticastIfdata = (MulticastInterfaceData_t*)cm->interfaceData;
	pMulticastIfdata->GIData.thrReinitData.pInterfaceData = &pMulticastIfdata->sockMulticast;
	pMulticastIfdata->GIData.thrReinitData.cm = cm;
	pMulticastIfdata->GIData.thrReinitData.maxCnt = pData->reinitCount;
	pMulticastIfdata->GIData.thrReinitData.var.cnt = pMulticastIfdata->GIData.thrReinitData.maxCnt;
	pMulticastIfdata->GIData.thrReinitData.timeIntervalMs = pData->reinitRetryIntervalMs;
	pMulticastIfdata->GIData.reinitOnErrorEn = pData->reinitOnErrorEn;
	pMulticastIfdata->GIData.thrReinitData.userObject = pData->userObject;

	pMulticastIfdata->GIData.thrReinitData.onReceiveErrorCallback = (InterfaceOnReceiveError_t) pData->onInterfaceReceiveError;
	pMulticastIfdata->GIData.thrReinitData.pOnReceiveErrorData = (void*)&pMulticastIfdata->sockMulticast.IPParams;

	pMulticastIfdata->GIData.thrReinitData.onInitErrorCallback = (GeneralInterfaceOnInitError_t) pData->onInterfaceInitError;

	pMulticastIfdata->GIData.thrReinitData.onCloseCallback = (MulticastInterfaceOnClose_t) pData->onInterfaceClose;

	pMulticastIfdata->GIData.thrReinitData.onReopenCallback = (GeneralInterfaceOnReopen_t) pData->onInterfaceReopen;
	pMulticastIfdata->GIData.thrReinitData.pOnReopenData = (void*)&pMulticastIfdata->sockMulticast.IPParams;
	pMulticastIfdata->GIData.thrReinitData.IsReopenedCorrectly = MulticastIsReopenedCorrectly;
	pMulticastIfdata->GIData.WantReinit = MulticastWantReinit;
	pMulticastIfdata->GIData.thrReinitData.reopenFailed = (InterfaceReopenFailed_t) MulticastReopenFailed;
	pMulticastIfdata->GIData.thrReinitData.closeCallback = (GeneralInterfaceClose_t) SockMulticastClose;
	pMulticastIfdata->GIData.thrReinitData.Reopen = (GeneralInterfaceReopen_t) SockMulticastReopen;

	pMulticastIfdata->GIData.thrReinitData.Open = (GeneralInterfaceOpen_t) SockMulticastInterfaceOpen;
	pMulticastIfdata->GIData.thrReinitData.pErrorMsg = pMulticastIfdata->sockMulticast.errorMsg;
	pMulticastIfdata->GIData.thrReinitData.maxErrorMsgLen = SOCK_MULTICAST_ERROR_MSG_MAX_LEN;
	pMulticastIfdata->GIData.thrReinitData.onOpenCallback = (GeneralInterfaceOnOpen_t) pData->onInterfaceOpen;
	pMulticastIfdata->GIData.thrReinitData.pOnOpenData = (void*)&pMulticastIfdata->sockMulticast.IPParams;

	SockMulticastInit(&pMulticastIfdata->sockMulticast);

	return GeneraInterfaceOpen((void*)cm->interfaceData,(void*)pData);
}

