/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SERIAL__H__
#define __SERIAL__H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __USE_MISC
#define __USE_MISC
#endif
#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <termios.h>
#include "../common/threads.h"

typedef struct {
	uint8_t len;
	uint8_t *data;
	uint8_t *sended;
} sendData_t;

#define ROTBUFTYPE sendData_t
#define ROTBUFSIZE	4
#include "../../../common/rotBuf.h"


typedef int (*parserF) (void * object, uint8_t data, uint8_t timeout);
typedef void (*DataSentHandler) (void * object, uint8_t* data);
typedef void (*OnReceiveErrorHandler)(void* object, uint16_t error);

typedef struct {
	int byteTimeout; //interbyte timeout
	parserF parserFunc;
	void* parserFuncObject;
	pthread_mutex_t initialized;
	OnReceiveErrorHandler onReceiveErrorHandler;
} receiverThreadArgs;

typedef struct {
	int packetDelay;
	ROTBUF_t rotbuf;
	pthread_mutex_t mtxSending;
	pthread_mutex_t mtxWriteNew;
	DataSentHandler dataSentFunction;
	void* dataSentFuncObject;
} sendThreadArgs;


typedef struct {
	uint8_t rotbufLockEn:1;
	uint8_t receiverThrFIFOSched:1;
	uint8_t writeSyncEn:1;
	int8_t receiverThrPriority;
} serialOptConfig_t;

typedef struct {
	int fd; 	// serial port file descriptor
	receiverThreadArgs receiverThrArgs;
	sendThreadArgs sendThrArgs;
	serialOptConfig_t optConfig;
} commThreadParams;

// baud speed  is defined in termios.h ... like B115200
#define SPEED(value) B##value
#define PARITY_NONE 0
#define PARITY_ODD (PARENB | PARODD)
#define PARITY_EVEN (PARENB)
// parity MARK ans SPACE use undocumented CMSPAR bit. Use it with care. Work only with specific drivers!
#define PARITY_SPACE_8BIT (PARENB | CMSPAR)
#define PARITY_MARK_8BIT (PARENB | CMSPAR | PARODD)

#define ONE_STOPBIT		0
#define TWO_STOPBITS	1

#define SERIAL_WAIT_FOR_NEW_PACKET 1
#define SERIAL_WAIT_FOR_PACKET_BYTE 0

//int setSerialAttribs (int fd, int speed, int parity, uint8_t stopBits);
//void set_blocking (int fd, int should_block);

//receiveByteTimeout and  sendPacketDelay is in us
//if receiveByteTimeout == 0 -> wait infinit time
//portName "/dev/ttyUSB0"
//for baudSpeed use SPEED macro or use  Bxxxx (B9600)
// return serial port
commThreadParams* InitSerialPort(char* portName, uint32_t baudSpeed, uint32_t parity,  uint8_t stopBits, uint32_t receiveByteTimeoutUs, uint32_t sendPacketDelay,  parserF receiverParser, void *parserFuncObject, DataSentHandler dataSentFunction, void *dataSentFuncObject, OnReceiveErrorHandler onReceiveErrorHandler, serialOptConfig_t *optConfig);
uint8_t ChangeSerialPortParity(commThreadParams* commThrParams, uint8_t parity);
uint8_t SerialSendData(commThreadParams *commThrParams, uint8_t *data, uint8_t len, uint8_t *sended);
uint8_t CloseSerialPort(commThreadParams *commThrParams);

// simple serial port
int InitSimpleSerialPort(char* portName, uint32_t baudSpeed, uint32_t parity,  uint8_t stopBits, char timeOutInTensOfSec, char minNumOfBytesReceived);
uint32_t WriteSimpleSerialPort(int fd, char *TxData, uint32_t len);
int32_t ReadSimpleSerialPort(int fd ,char *RxData, uint32_t len, uint32_t maxReadCnt);
uint8_t CloseSimpleSerialPort(int fd);

#ifdef __cplusplus
}
#endif

#endif
