/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
*/

#ifndef __SSL_SERVER__H__
#define __SSL_SERVER__H__
//#include "../common.h"
#include "sysConfig.h"
#include "sysConfig.h"
#include <netinet/in.h>
#include <openssl/ssl.h>

// define default SSL Method for connecting
#ifndef SSLSERVER_PROTOCOL_METHOD
#define SSLSERVER_PROTOCOL_METHOD TLSv1_2_method
#endif


typedef struct SSLServerClient_t SSLServerClient_t;
typedef void (*SSLServerOnDataCallback_t)(uint8_t *data, uint32_t len, SSLServerClient_t* pSSLServerClient_t, void *parser);
typedef void (*SSLServerOnSentCallback_t) (void * object, uint8_t* data, uint8_t err);
typedef void (*SSLOnClientConnected_t)(struct SSLServerClient_t *pSSLServerClient);
typedef void (*SSLOnClientDisconected_t)(struct SSLServerClient_t *pSSLServerClient);

typedef struct {
	int verifyClient; /* To verify a client certificate, set ON */
	int listenSock;
	struct sockaddr_in serverAddr;
	SSL_CTX *ctx;
	SSL_METHOD *meth;
	X509 *serverCert;
	//EVP_PKEY *pkey;

	uint16_t sPort;
	char sIPAddr[16]; //192.168.001.001
	char serverCertFile[SSL_MAX_FILE_NAME];

	char serverKeyFile[SSL_MAX_FILE_NAME];
	char serverKeyPassPhrase[SSL_MAX_KEY_PASSPHRASE];

	char CACert[SSL_MAX_FILE_NAME];
	SSLServerOnDataCallback_t onServerDataCallback;
	void *userObject;
	char errorMsg[SSL_ERROR_MSG_MAX_LEN];
	uint8_t finish;
	SSLServerOnSentCallback_t onSentCallback;
	SSLOnClientConnected_t onClientConnectedCallback;
	SSLOnClientDisconected_t onClientDisconectedCallback;
} SSLServerSocket_t;

 struct SSLServerClient_t {
	SSLServerSocket_t *pSSLSock;
	struct sockaddr_in clientSockAddr;
	int sock;
	SSL *ssl;
	char* pRecBuf;
	uint32_t pRecBufLen;
	uint8_t  *pSendBuf;
	uint16_t pSendBufLen;
	X509 *clientCert;
	char errorMsg[SSL_ERROR_MSG_MAX_LEN];
};

uint8_t SSLSServerInit(SSLServerSocket_t *sslSock, uint8_t ip[16], uint16_t port, uint8_t verifyClient, char* serverCertFile, char* serverKeyFile, char* serverKeyPassPhrase, char* CAClientCertFile, SSLServerOnDataCallback_t onDataCallback, SSLServerOnSentCallback_t onSentCallback, SSLOnClientConnected_t onClientConected, SSLOnClientDisconected_t onClientDisconected, void *userObject );
int8_t SSLServerSendData(SSLServerClient_t *pSSLServerClient, uint8_t* data, uint16_t len);
uint8_t* SSLServerGetError(SSLServerSocket_t* sslSock);
uint8_t SSLServerClose(SSLServerSocket_t* sslSock);

#endif
