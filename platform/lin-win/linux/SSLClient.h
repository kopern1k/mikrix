/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
*/

#ifndef __SSL_CLIENT_H__
#define __SSL_CLIENT_H__
#include "../common/common.h"
#include "sysConfig.h"
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <pthread.h>

#define SSL_CLIENT_NO_ERR                   0
#define SSL_CLIENT_TIMEOUTED                1
#define SSL_CLIENT_TLS_CONNECTION_CLOSED    2
#define SSL_CLIENT_SYSTEM_CONNECTION_ERR    3
#define SSL_CLIENT_READ_ERR                 4
#define SSL_CLIENT_READ_ZERRO_DATA_LEN      5
#define SSL_CLIENT_MEM_CLEARED              6
#define SSL_CLIENT_UNSPECIFIED_ERR          7

// SSLClientSendData error codes
#define SSLCLIENT_OPERATION_OK 0
#define SSLCLIENT_ERR_NULL_DATA -1
#define SSLCLIENT_ERR_DATA_LEN -2
#define SSLCLIENT_ERR_ALREADY_CLEANING -3
#define SSLCLIENT_ERR_ALREADY_INITIALIZING -3
#define SSLCLIENT_ERR_RECEIVE_THR_NOT_RUNNING -4
#define SSLCLIENT_ERR_SOCKET_IS_CLOSED -4
#define SSLCLIENT_ERR_PROBABLY_CONNECTION_WAS_CLOSED -5
#define SSLCLIENT_ERR_DATA_NOT_TRANSFERRED -6
#define SSLCLIENT_ERR_DATA_PARTLY_TRANSFERRED -7

// define default socket connect timeout if not specified
#ifndef SSLCLIENT_SOCKET_CONNECT_TIMEOUT
#define SSLCLIENT_SOCKET_CONNECT_TIMEOUT 3000
#endif

// define default SSL Method for connecting
#ifndef SSLCLIENT_PROTOCOL_METHOD
#define SSLCLIENT_PROTOCOL_METHOD TLSv1_2_method
#endif

typedef void (*SSLClientOnDataCallback_t)(uint8_t *data, uint32_t len, void *parser);
typedef void (*SSLOnSentCallback_t) (void * object, uint8_t* data, uint16_t transferredData, int8_t err);
typedef void (*SSLOnReceiveErrorHandler)(void* object, uint16_t error);

typedef struct {
	int verifyClient; /* To verify a client certificate, set ON */
	int sock;
	struct sockaddr_in serverAddr;
	uint8_t* pRecBuf;
	uint32_t pRecBufLen;
	uint8_t  *pSendBuf;
	uint16_t pSendBufLen;
	uint32_t maxReadTimeoutMs;
	SSL_CTX *ctx;
	SSL *ssl;
	SSL_METHOD *meth;
	X509 *serverCert;
	EVP_PKEY *pkey;

	uint16_t sPort;
	char sIPAddr[16]; //192.168.001.001
	char clientCertFile[SSL_MAX_FILE_NAME];
	char clientKeyFile[SSL_MAX_FILE_NAME];
	char clientKeyPassPhrase[SSL_MAX_KEY_PASSPHRASE];
	char CACert[SSL_MAX_FILE_NAME];
	SSLClientOnDataCallback_t onDataCallback;
	void *userObject;
	char errorMsg[SSL_ERROR_MSG_MAX_LEN];
	volatile uint8_t finish;
	SSLOnSentCallback_t onSentCallback;
	SSLOnReceiveErrorHandler onReceiveErrorHandler;
	volatile uint8_t running;
	pthread_mutex_t cleaning;
	pthread_mutex_t initializing;
} SSLClientSocket_t;

/* maxReadTimeoutMs 0 == waits forewer */
uint8_t SSLClientSockOpen(SSLClientSocket_t *sslSock, uint8_t ip[16], uint16_t port, uint8_t verifyClient, char* clientCertFile, char* clientKeyFile, char* clientKeyPassPhrase, char* CAServerCertFile, SSLClientOnDataCallback_t onDataCallback, SSLOnSentCallback_t onSentCallback, void *userObject, SSLOnReceiveErrorHandler onReceiveErrorHandler, uint32_t maxReadTimeoutMs);
uint8_t* SSLClientGetError(SSLClientSocket_t* sslSock);
uint8_t SSLClientSockClose(SSLClientSocket_t* sslSock);
uint8_t SSLClientSockForceClose(SSLClientSocket_t* sslSock);
int8_t SSLClientSendData(SSLClientSocket_t *sslSock, uint8_t* data, uint16_t len);
int8_t SSLClientInitDataEx(SSLClientSocket_t *sslSock, uint8_t ignoreSigPipe);
INLINE uint8_t SSLClientInitData(SSLClientSocket_t *sslSock) {
	return SSLClientInitDataEx(sslSock,1);
}

uint8_t SSLISRunning(SSLClientSocket_t *sslSock);
uint8_t SSLClientIsInitializing(SSLClientSocket_t *sslSock);
uint8_t SSLClientIsClosing(SSLClientSocket_t *sslSock);

#endif
