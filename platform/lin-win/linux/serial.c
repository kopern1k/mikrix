/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include <stdio.h>
#include <errno.h>

#ifndef __USE_BSD
#define __USE_BSD
#endif
#ifndef __USE_MISC
#define __USE_MISC
#endif
#ifndef __USE_XOPEN_EXTENDED
#define __USE_XOPEN_EXTENDED
#endif
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/select.h>
#include "serial.h"
#include <pthread.h>
#include "../../sys/timeManager.h"

#define error_message printf


#define SERIAL_DEBUG_MSGS_EN
//#define SERIAL_PERF_DEBUG_MSGS_EN

#ifdef SERIAL_DEBUG_MSGS_EN
#define DEBUG_MSG(...) printf(__VA_ARGS__);fflush(stdout)
#else
#define DEBUG_MSG(...)
#endif

#ifdef SERIAL_PERF_DEBUG_MSGS_EN
#define PERF_DEBUG_MSG(...) printf(__VA_ARGS__);fflush(stdout)
#else
#define PERF_DEBUG_MSG(...)
#endif



/* receiver thread.
 * It takes parserF parserFunc as arg. parserFunc returns PACKET_PARSED_OK only if gets all data bytes correctly.
 * If not, timeouted parameter is set after timeout is elapsed and no another characret is received.
 */
THREAD_RETURN_TYPE receiveHandler(void *arg)
{

	((defaultThreadArgs_t *)arg)->started = 1;

	int fd = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->fd;
	parserF parserFunc = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->receiverThrArgs.parserFunc;
	int byteTimeout = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->receiverThrArgs.byteTimeout;
	void * parserFuncObject =((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->receiverThrArgs.parserFuncObject;
	OnReceiveErrorHandler onReceiveErrorHandler = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->receiverThrArgs.onReceiveErrorHandler;
	pthread_mutex_t* initilaized =&((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->receiverThrArgs.initialized;
	serialOptConfig_t *optConfig =&((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->optConfig;
	fd_set set;
	int rv;
	uint8_t buf[100];
	uint8_t timeouted = 0;
	uint8_t waitForNewPacket = SERIAL_WAIT_FOR_NEW_PACKET;
	uint32_t pfRet;
	int i,n;
	struct timeval timeout;

	//printf("thread started fd: %d\n",fd);
	//set scheduling parameters
	int policy;
	struct sched_param param;
	pthread_getschedparam(pthread_self(), &policy, &param);
	if(optConfig->receiverThrFIFOSched && optConfig->receiverThrPriority != param.__sched_priority) {
		param.__sched_priority = optConfig->receiverThrPriority;
	}
	if(optConfig->receiverThrFIFOSched) {
		policy = SCHED_FIFO;
	}
	if(pthread_setschedparam(pthread_self(), policy ,&param) < 0) {
		error_message("Could not set thread scheduling parameters!! errno==%d (%s)\n",errno, strerror(errno));
	}

	if (parserFunc == NULL) {
		error_message("Read thread exitting! No receive parser function set!!\n");
		return 0;
	}
	if (fd <= 0) {
		error_message("Read thread exitting! Bad fd: %d!!\n",fd);
		return 0;
	}
	pthread_mutex_lock(initilaized);
	do {
		FD_ZERO(&set); /* clear the set */
		FD_SET(fd, &set); /* add our file descriptor to the set */
		timeout.tv_sec = 0;
		timeout.tv_usec = byteTimeout;
		rv = select(fd + 1, &set, NULL, NULL, &timeout);
		PERF_DEBUG_MSG("[%llu] Serial receiveHandler select rv=%d\n", (unsigned long long)GetSysTickCount(), rv);

		if(rv == -1) {
			perror("select\n"); /* an error accured */
			if(onReceiveErrorHandler) {
				onReceiveErrorHandler(parserFuncObject, errno);
			}
			break;
		} else if(rv == 0) {
			PERF_DEBUG_MSG("[%llu] Serial receiveHandler timeout. waitForNewPacket == %hhu, timeouted=%hhu\n", (unsigned long long) GetSysTickCount(),waitForNewPacket, timeouted); /* a timeout occured */
			if(!waitForNewPacket)
				if(timeouted == 0){
					timeouted = 1;
					pfRet = parserFunc(parserFuncObject, 0, timeouted);
					waitForNewPacket = (pfRet == SERIAL_WAIT_FOR_NEW_PACKET) ?  1 : 0;
				}
		} else {
			n = read( fd, buf, sizeof(buf) ); /* there was data to read */
			PERF_DEBUG_MSG("[%llu] Serial receiveHandler read %d data\n",(unsigned long long) GetSysTickCount(), n);
			if(n > 0) {
				//printf("data:%d\n",buf[0]);
				for(i = 0; i<n;i++) {
					pfRet = parserFunc(parserFuncObject, buf[i], 0);
				}
				timeouted = 0;
				waitForNewPacket = (pfRet == SERIAL_WAIT_FOR_NEW_PACKET) ?  1 : 0;
			} else {
				if(onReceiveErrorHandler) {
					onReceiveErrorHandler(parserFuncObject, errno);
				}
				if(errno == ENOENT || errno == 0) {
					error_message("Read thread finished! errno: %d\n",errno);
					return 0;
				}
			}
		}
	} while (1);
  return 0;
}

int setSerialAttribs (int fd, int speed, int parity, uint8_t stopBits) {
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
			error_message ("error %d from tcgetattr\n", errno);
			return -1;
	}

	cfsetospeed (&tty, speed);
	cfsetispeed (&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	//tty.c_iflag &= ~IGNBRK;         // ignore break signal
	tty.c_iflag = 0;
	tty.c_lflag = 0;                // no signaling chars, no echo,
									// no canonical processing
	tty.c_oflag = 0;                // no remapping, no delays
	tty.c_cc[VMIN]  = 0;            // read doesn't block, VMIN - minimum num. of characters. 0 - non-blocking
	tty.c_cc[VTIME] = 1;            // in n x 0.1 seconds read timeout. VMIN==0 && VTIME == 0 - non blocking. VMIN > 0 && VTIME == 0 - wait for VMIN characters

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
									// enable reading
	tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	if(stopBits == 2) {
		tty.c_cflag |= CSTOPB;
	}

	tty.c_cflag &= ~CRTSCTS;

	tcflush(fd, TCIOFLUSH); //flush pending input and output

	if (tcsetattr (fd, TCSANOW, &tty) != 0)
	{
			error_message ("error %d from tcsetattr\n", errno);
			return -1;
	}
	return 0;
}

int setSerialParity(int fd, uint8_t parity) {
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
			error_message ("error %d from tcgetattr\n", errno);
			return -1;
	}

	tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	tty.c_cflag |= parity;

	tcflush(fd, TCIOFLUSH); //flush pending input and output

	if (tcsetattr (fd, TCSANOW, &tty) != 0)
	{
			error_message ("error %d from tcsetattr\n", errno);
			return -1;
	}
	return 0;
}


void setupSerialTimeouts(int fd, char minNumOfBytesReceived, char maxElapseTimebetweenBytesinTensOfSec ) {
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (fd, &tty) != 0)
	{
			error_message ("error %d from tggetattr\n", errno);
			return;
	}

	tty.c_cc[VMIN]  = minNumOfBytesReceived;							// min. num of bytes received
	tty.c_cc[VTIME] = maxElapseTimebetweenBytesinTensOfSec;            // 1 = 0.1 seconds read timeout

	if (tcsetattr (fd, TCSANOW, &tty) != 0) {
			error_message ("error %d setting term attributes\n", errno);
	}
}



void setBlockingForNBytes (int fd, char n)
{
	setupSerialTimeouts(fd,n,0);
}



// add data to circle buffer
// warning! sended as volatile variable!!

uint8_t SerialSendData(commThreadParams *commThrParams, uint8_t *data, uint8_t len, uint8_t *sended) {
	int ret = 0;
	if(commThrParams->optConfig.writeSyncEn) {
		if(write(commThrParams->fd, data, len) < 0) {
			ret = 100;
		}

//serial_send_tcdrain_lb:
		tcflush(commThrParams->fd, TCOFLUSH);
		DEBUG_MSG("tcflush called\n");
		/*
		DEBUG_MSG("tcdrain calling\n");
		if(tcdrain(commThrParams->fd) <0) {
			if(errno == ENOTTY || errno == 0 || errno == EBADF){
				return 1;
			}
			if(errno == EINTR) {
				DEBUG_MSG("tcdrain interrupted\n");
				goto serial_send_tcdrain_lb;
			}
		} else {
			DEBUG_MSG("tcdrain finished\n");
		}
		*/
		*sended = 1;
		//call callback
		commThrParams->sendThrArgs.dataSentFunction(commThrParams->sendThrArgs.dataSentFuncObject,data);
		return ret;
	}
	sendData_t tmpData;
	int numOfBytes;
	uint8_t writeFirstPacket = 0;
	if(commThrParams == NULL)
		return 1;
	tmpData.data = data;
	tmpData.len = len;
	tmpData.sended = sended;
	if(tmpData.sended) {
		*tmpData.sended = 0;
	}
	if(commThrParams->optConfig.rotbufLockEn) {
		pthread_mutex_lock(&commThrParams->sendThrArgs.mtxWriteNew);
	}
	if(RotBufEmpty(&commThrParams->sendThrArgs.rotbuf)) {
		writeFirstPacket = 1;
	}
	ret = RotBufAdd(&commThrParams->sendThrArgs.rotbuf, tmpData);
	DEBUG_MSG("Serial: Data written to write buffer!\n");
	if(writeFirstPacket) {
		DEBUG_MSG("wtiting first packet\n");
		if((numOfBytes = write(commThrParams->fd, data, len)) < 0) {
			ret = 100;
		}
	} else {
		DEBUG_MSG("wtiting other packet\n");
	}

	if(commThrParams->optConfig.rotbufLockEn) {
		pthread_mutex_unlock(&commThrParams->sendThrArgs.mtxWriteNew);
	}
	if(pthread_mutex_unlock(&commThrParams->sendThrArgs.mtxSending)) {
		error_message("Could not unlock mtxSending mutex!\n");
	}
	return ret;
}

THREAD_RETURN_TYPE sendHandler(void *arg)
{

	((defaultThreadArgs_t *)arg)->started = 1;
	sendData_t data;
	int fd = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->fd;
	int* pfd = &((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->fd;
	int packetDelay = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.packetDelay;
	DataSentHandler dataSentFunction = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.dataSentFunction;
	void* dataSentFuncObject = ((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.dataSentFuncObject;
	serialOptConfig_t * optConfig = &(((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->optConfig);
	int ret;
	ROTBUF_t *rotbuf = &((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.rotbuf;
	pthread_mutex_t* mtxSending = &((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.mtxSending;
	pthread_mutex_t* mtxWriteNew =&((commThreadParams *)((defaultThreadArgs_t *)arg)->args)->sendThrArgs.mtxWriteNew;

	//printf("thread started fd: %d\n",fd);

	if (fd <= 0) {
		return 0;
	}
	int writeEn = 0;
	int numOfBytes;

	//pthread_mutex_lock(mtxSending);

	do {
		if(optConfig->rotbufLockEn) {
			pthread_mutex_lock(mtxWriteNew);
		}
		//fetch data from buffer
		DEBUG_MSG("going to fetch data\n");
		ret = RotBufFetch(rotbuf,&data);

		if(optConfig->rotbufLockEn) {
			pthread_mutex_unlock(mtxWriteNew);
		}

		if(ret == 0 ) {
			if(writeEn) {
				DEBUG_MSG("serial: Going to write data to rotBuf!\n");
				if((numOfBytes  = write(fd, data.data, data.len)) < 0) {
					DEBUG_MSG("error while writting\n");
					ret = 100;
				}
			}
serial_tcdrain_lb:
			if(tcdrain(fd) <0) {
				//int data = errno;
				if(errno == ENOTTY || errno == 0 || errno == EBADF){
					return 0;
				}
				if(errno == EINTR) {
					DEBUG_MSG("tcdrain interrupted\n");
					goto serial_tcdrain_lb;
				}
			}
			DEBUG_MSG("serial port data are wtitten\n");
			if(data.sended) {
				*data.sended = 1;
			}
			if(dataSentFunction) {
				dataSentFunction(dataSentFuncObject, data.data);
			}
			if(packetDelay)
				usleep(packetDelay);
			writeEn = 1;
		} else {
			writeEn = 0;
			pthread_mutex_lock(mtxSending);

			DEBUG_MSG("Serial: Read muteunlocked!\n");

			if(*pfd <=0) {
				return 0;
			}
		}
	} while (1);
  return 0;
}

// timeouts are in us
commThreadParams* InitSerialPort(char* portName, uint32_t baudSpeed, uint32_t parity,  uint8_t stopBits, uint32_t receiveByteTimeoutUs, uint32_t sendPacketDelay,  parserF receiverParser, void *parserFuncObject, DataSentHandler dataSentFunction, void *dataSentFuncObject, OnReceiveErrorHandler onReceiveErrorHandler, serialOptConfig_t *optConfig) {
	int fd;
	//portName[8] = 0;
	if(portName == NULL) {
		return 0;
	}
	if((fd = open (portName, O_RDWR | O_NOCTTY | O_NONBLOCK /*O_SYNC*/)) <0) {
		error_message ("error %d opening %s: %s\n", errno, portName, strerror (errno));
		return 0;
	}
	setSerialAttribs (fd, baudSpeed, parity, stopBits);		// set speed to 115,200 bps, 8n1 (no parity)
	setBlockingForNBytes(fd, 0);									// set blocking for 1 byte
	commThreadParams* commThrParams = malloc(sizeof(commThreadParams));
	commThrParams->fd = fd;
	commThrParams->receiverThrArgs.parserFunc = receiverParser;
	commThrParams->receiverThrArgs.byteTimeout = receiveByteTimeoutUs;
	commThrParams->receiverThrArgs.parserFuncObject = parserFuncObject;
	commThrParams->receiverThrArgs.onReceiveErrorHandler = onReceiveErrorHandler;
	if(pthread_mutex_init(&commThrParams->receiverThrArgs.initialized,NULL)!=0){
		printf("initialized mutex not initialized\n");
	}

	commThrParams->sendThrArgs.packetDelay = sendPacketDelay; //1ms inter packet delay
	commThrParams->sendThrArgs.dataSentFunction = dataSentFunction;
	commThrParams->sendThrArgs.dataSentFuncObject = dataSentFuncObject;
	//commThrParams->sendThrArgs.mtxSending = malloc(sizeof(pthread_mutex_t));
	//commThrParams->sendThrArgs.mtxWriteNew = malloc(sizeof(pthread_mutex_t));
	if(pthread_mutex_init(&commThrParams->sendThrArgs.mtxSending,NULL)!=0){
		printf("mtxSending mutex not initialized\n");
	}
	if(pthread_mutex_init(&commThrParams->sendThrArgs.mtxWriteNew,NULL)!=0){
		printf("mtxWriteNew mutex not initialized\n");
	}
	pthread_mutex_lock(&commThrParams->sendThrArgs.mtxSending);
	pthread_mutex_lock(&commThrParams->receiverThrArgs.initialized);
	//pthread_mutex_unlock(&commThrParams->sendThrArgs.mtxSending);
	//commThrParams->sendThrArgs.rotbuf = malloc(sizeof(ROTBUF));

	// set optConfig if not set
	if(optConfig == NULL) {
		commThrParams->optConfig.receiverThrFIFOSched = 0;
		commThrParams->optConfig.receiverThrPriority = 0;
		commThrParams->optConfig.rotbufLockEn = 1;
		commThrParams->optConfig.writeSyncEn = 0;
	} else {
		commThrParams->optConfig = *optConfig;
	}
	// do not create send thread when writeSync is enabled
	if(!commThrParams->optConfig.writeSyncEn) {
		RotBufInit(&commThrParams->sendThrArgs.rotbuf);

		if(!createThread(sendHandler,(void* )commThrParams)) {
			CloseSerialPort(commThrParams);
			return 0;
		}
	}

	if (!createThread(receiveHandler,(void* )commThrParams)) {
		CloseSerialPort(commThrParams);
		return 0;
	}
	pthread_mutex_unlock(&commThrParams->receiverThrArgs.initialized);
	return commThrParams;
}

uint8_t ChangeSerialPortParity(commThreadParams* commThrParams, uint8_t parity) {
	if(commThrParams == NULL) {
		return 1;
	}

	if(setSerialParity(commThrParams->fd,parity)) {
		return 2;
	}
	return 0;
}


uint8_t CloseSerialPort(commThreadParams *commThrParams) {
	if(commThrParams != 0) {
		if(commThrParams->fd > 0) {
			if(close(commThrParams->fd)) {
				return 2;
			}
			commThrParams->fd = -1;
			pthread_mutex_unlock(&commThrParams->sendThrArgs.mtxSending);
		}
		free(commThrParams);
		return 0;
	}
	return 1;
}

/* simple serial line */
int InitSimpleSerialPort(char* portName, uint32_t baudSpeed, uint32_t parity,  uint8_t stopBits, char timeOutInTensOfSec, char minNumOfBytesReceived){
    int fd;

    if(portName == NULL) {
        return -1;
    }
    if((fd = open (portName, O_RDWR | O_NOCTTY | O_SYNC)) <0) {
        error_message ("error %d opening %s: %s\n", errno, portName, strerror (errno));
        return fd;
    }

    setSerialAttribs (fd, baudSpeed, parity, stopBits);
    setupSerialTimeouts(fd, minNumOfBytesReceived, timeOutInTensOfSec );

    return fd;
}

uint8_t CloseSimpleSerialPort(int fd) {
    if(fd == 0) {
        return 1;
    }
    if(close(fd)) {
        return 2;
    }
    return 0;
}

int32_t ReadSimpleSerialPort(int fd ,char *RxData, uint32_t len, uint32_t maxReadCnt){
    uint32_t retry=0;
    uint32_t n=0;
    int32_t ret;
    if (len == 0){
        return 0;
    }
    while ((n < len) && (retry <= maxReadCnt)){
        if((ret = read(fd, &RxData[n], len - n)) < 0) {
            error_message ("read error %d: %s\n", errno, strerror (errno));
            return -errno;
        }
        n += ret;
        retry++;
    }
    return n;
}

uint32_t WriteSimpleSerialPort(int fd, char *TxData, uint32_t len){
    return write (fd, TxData, len);
}

/* for testing purpose */

int receiverParserTest(void * object, uint8_t data, uint8_t timeout) {
	printf("received char: %c\n", data);
	return SERIAL_WAIT_FOR_NEW_PACKET;
}

void usage() {

	uint8_t ret=0;
	char *portname = "/dev/ttyUSB0";
	int baud = B115200; //B9600;//B38400; //B115200
	commThreadParams *serParams;
	serParams = InitSerialPort(portname,baud,PARITY_NONE,TWO_STOPBITS,500,1000,receiverParserTest,NULL,NULL,NULL,NULL,NULL);
	if(serParams == NULL){
		printf("Could not init serial interface. returned : %d",ret);
	}
	uint8_t buf[]={"AHOJ"};
	SerialSendData(serParams, buf,sizeof(buf),NULL);
	SerialSendData(serParams, buf,sizeof(buf),NULL);
	SerialSendData(serParams, buf,sizeof(buf),NULL);
	ret =SerialSendData(serParams, buf,sizeof(buf),NULL);
	while(ret) {
		usleep(1000);
		printf("waiting\n");
		ret =SerialSendData(serParams, buf,sizeof(buf),NULL);
	}
	int c =0;
	while(c++ < 10000) {
		usleep(100000);
		fflush( stdout );
	}

	CloseSerialPort(serParams);
}
