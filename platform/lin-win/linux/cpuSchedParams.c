/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#define __USE_GNU             /* See feature_test_macros(7) */
#include <sched.h>
#include "cpuSchedParams.h"

uint8_t SetAppSchedParams(int scheduler, uint8_t priority, uint8_t* cpuAffinity, uint8_t cpuAffinityLen, char* errMsg, uint16_t errMsgLen) {
	cpu_set_t  mask;

	if(cpuAffinity && cpuAffinityLen) {
		CPU_ZERO(&mask);
		for(int i = 0; i<cpuAffinityLen; i++) {
			CPU_SET(cpuAffinity[i], &mask);
		}
		if(sched_setaffinity(0, sizeof(mask), &mask) < 0) {
			snprintf(errMsg,errMsgLen,"sched_setaffinity returned:  Errno==%d, (%s)\n",errno, strerror(errno));
			return 1;
		}
	}

	// get scheduler
	__pid_t currentPid = getpid();
	int tmpScheduler = sched_getscheduler(currentPid);
	if(tmpScheduler < 0) {
		snprintf(errMsg,errMsgLen, "sched_getscheduler returned error. Errno==%d, (%s)\n",errno, strerror(errno));
		return 2;
	}

	//cap_t ct = cap_init();
	//getrlimit

	struct sched_param schedParam, tmpSchedParam;
	if(sched_getparam(currentPid,&schedParam)) {
		snprintf(errMsg,errMsgLen,"sched_getparam returned error. Errno==%d, (%s)\n",errno, strerror(errno));
		return 3;
	}
	tmpSchedParam = schedParam;
	//set priority
	schedParam.__sched_priority += priority;

	// set scheduler
	if(sched_setscheduler(currentPid,scheduler,&schedParam) < 0) {
		snprintf(errMsg,errMsgLen,"Could not set scheduler errno==%d (%s). Current prio=%d, sched=%d\n",errno, strerror(errno), schedParam.__sched_priority, tmpScheduler);
		// returning scheduler
		if(sched_setscheduler(currentPid,tmpScheduler,&tmpSchedParam)<0) {
			snprintf(errMsg,errMsgLen,"Could not return back scheduler errno==%d (%s). Current prio=%d, sched=%d\n",errno, strerror(errno), tmpSchedParam.__sched_priority, tmpScheduler);
			return 5;
		}
		return 4;
	}
	return 0;
}

#define MAX_ERR_MSG_LEN 100

#define PROCESS_PRIORITY_DIFF 60

uint8_t TestCpuSchedParamas() {
	uint8_t ret = 0;
    uint8_t cpuAffiny[] = {1,2};
    char errMsg[MAX_ERR_MSG_LEN + 1];
	if((ret = SetAppSchedParams(SCHED_RR,PROCESS_PRIORITY_DIFF,cpuAffiny,sizeof(cpuAffiny),errMsg,sizeof(errMsg)))) {
            errMsg[MAX_ERR_MSG_LEN] = 0;
            printf("%s\n",errMsg);
    }
	return ret;
}
