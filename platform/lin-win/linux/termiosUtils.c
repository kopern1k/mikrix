#include "termiosUtils.h"
#include "kbhit.h"
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include "errno.h"
#ifdef LINUX
#include <termios.h>
#define ENABLE_ECHO  0

// echo off, echo newline off, canonical mode off,
// extended input processing off, signal chars off
//#define ECHO_BITS (ECHO | ECHONL | ICANON | IEXTEN | ISIG)
#define ECHO_BITS (ICANON | ECHO)

#if ENABLE_ECHO != 0
  #define ECHO_DATA 0
#else
  #define ECHO_DATA ECHO_BITS
#endif

void DeleteStdoutChar() {
	putchar(8);
	putchar(' ');
	putchar(8);
	fflush(stdout);
}

uint8_t SetStdinEcho(uint8_t enable) {
	struct termios t;
	if(tcgetattr( STDIN_FILENO, &t) < 0) {
		return errno;
	}
	if(enable) {
		t.c_lflag |= (ECHO_BITS);
	} else {
		t.c_lflag &= ~(ECHO_BITS);
	}
	if(tcsetattr( STDIN_FILENO, TCSANOW, &t) < 0) {
		return errno;
	}
	return 0;
}

void ClearLine() {
	printf("%c[2K\r", 27);
}

char _getch(void) {
	struct termios oldt,newt;
    char ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
	// No line processing
	newt.c_lflag &= ~(ECHO_BITS);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return ch;
}
#endif

static struct termios termionsUtilsOldStdinAttrib;

void InitReadline() {
	tcgetattr(STDIN_FILENO, &termionsUtilsOldStdinAttrib);
}

void ResumeStdinReadline() {
	tcsetattr(STDIN_FILENO, TCSANOW, &termionsUtilsOldStdinAttrib);
}


ReadlineActions_t ReadLine(char* data, uint16_t offset, uint16_t maxDataSize, uint16_t* readChars){
	static uint8_t c =0,y = 0,z = 0;
	uint16_t charCnt = 0;
	*readChars = 0;
	ReadlineActions_t retVal = READLINE_NO_ACTION;
	data[offset] = 0;
    do {
		if(_kbhit()){
			if(offset + charCnt >= maxDataSize){
				return -1;
			}
			c=_getch();
#if ENABLE_ECHO != 0
			putchar(c);
#endif
			switch (c) {
			case 10:
				retVal = READLINE_NEW_LINE;
			case 13:
				retVal = READLINE_CARRIAGE_RETURN;
				break;
			case 27:
				y = _getch();
				z = _getch();
				if(y == 91) {
					switch (z) {
					case 51:
						//printf("del key pressed");
						retVal = READLINE_DELETE_KEY;
						break;
					case 65:
						//printf("up arrow key pressed\n");
						retVal = READLINE_ARROW_UP;
						break;
					case 66:
						//printf("down arrow key pressed\n");
						retVal = READLINE_ARROW_DOWN;
						break;
					case 67:
						//printf("right arrow key pressed\n");
						retVal = READLINE_ARROW_RIGHT;
						break;
					case 68:
						//printf("left arrow key pressed\n");
						retVal = READLINE_ARROW_LEFT;
						break;
					}
				}
				break;
			case 127: //back key
				if(charCnt >0) {
					charCnt --;
#if ENABLE_ECHO != 0
					DeleteStdoutChar();
#endif
				}
				retVal = READLINE_BACK_KEY;
				break;
			default:
				data[offset + charCnt++]=c;
				data[offset + charCnt]=0;
				break;
			}
		} else{
			break;
		}
	} while (retVal == 0);

	*readChars = charCnt;

	return retVal;
}
