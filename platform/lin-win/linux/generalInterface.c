//#define TEST
#include "generalInterface.h"
#include "../../../sys/threadManager.h"
#include "../common/printDate.h"


#include "sysConfig.h"
#ifdef GENERAL_INTERFACE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

uint8_t GeneralInterfaceReinitHandlerThr(void *data, pt* Thr){
	PT_BEGIN(Thr);
	WaitMs(Thr,((GIThrReinitData_t*)data)->var.Timer,((GIThrReinitData_t*)data)->timeIntervalMs);
	DEBUGPRINTF("GeneralInterfaceReinitHandlerThr expired!\n");

	int8_t ret;
	GIThrReinitData_t* pReinitData = ((GIThrReinitData_t*)data);
	//interface was closed by interface function. Should stay closed
	if(pReinitData->cm->state == CM_STATE_CLOSED) {
		DeleteThread(Thr);
		PT_EXIT(Thr);
	}
	if(pReinitData->onReopenCallback) {
		(pReinitData->onReopenCallback)(pReinitData->userObject, pReinitData->pOnReopenData);
	}
	ret  = pReinitData->Reopen(pReinitData->pInterfaceData);
	if(pReinitData->IsReopenedCorrectly(ret)) {
		DEBUGPRINTF("Interface opened correctly!\n");
		InitParser(pReinitData->cm);
		ChangeInterfaceState(pReinitData->cm, CM_STATE_ENABLED);
		if(pReinitData->onOpenCallback){
			pReinitData->onOpenCallback(pReinitData->userObject, pReinitData->pOnOpenData);
		}
		PrintThreadsInfo();
		DeleteThread(Thr);
		PT_EXIT(Thr);
	}
	//	DEBUGPRINTF("%s\n",((TCPClient_t*)(pReinitData->cm->interfaceData))->errorMsg);
	if(pReinitData->reopenFailed) {
		pReinitData->reopenFailed(ret,(pReinitData->cm->interfaceData));
	}
	if(pReinitData->maxCnt != 0) {
		if(--(pReinitData->var.cnt) == 0) {
			ChangeInterfaceState(pReinitData->cm,CM_STATE_CLOSED);
			DeleteThread(Thr);
			DEBUGPRINTF("Calling InterfaceClose!\n");
			if(pReinitData->cm->InterfaceClose) {
				pReinitData->cm->InterfaceClose(pReinitData->cm);
			}
			free(data);
		}
	}
	PT_END(Thr);
 }




#ifndef MAX_REINIT_THREAD_RETRY_CNT
#define MAX_REINIT_THREAD_RETRY_CNT 60
#endif

void GeneralInterfaceOnReceiveError(void* object, uint16_t error) {
#if MAX_REINIT_THREAD_RETRY_CNT != 0
	uint16_t thrRetryCnt = 0;
#endif
	COM_MNG_DATA* pCM = (COM_MNG_DATA*) object;
	GIData_t* pGIData = ((GIData_t*)pCM->interfaceData);
	// call callback on error, when callback is set. It is possible to change IP connection parameters for reinitialization
	if(pGIData->thrReinitData.onReceiveErrorCallback) {
		pGIData->thrReinitData.onReceiveErrorCallback(error, pGIData->thrReinitData.userObject, pGIData->thrReinitData.pOnReopenData);
	}
	if(pGIData->WantReinit(error)) {
		if(pGIData->reinitOnErrorEn) {
			//Start reinitialization pthread
			ChangeInterfaceState(pCM,CM_STATE_REINITIALIZING);

			while(AddThread((void*)&pGIData->thrReinitData, GeneralInterfaceReinitHandlerThr) == 0) {
				//thread was not created!
				DEBUGPRINTF("GeneralInterfaceOnReceiveError. AddThread == 0. Thread was not created!\n");
#if MAX_REINIT_THREAD_RETRY_CNT != 0
				if(thrRetryCnt++ > MAX_REINIT_THREAD_RETRY_CNT) {
					DEBUGPRINTF("GeneralInterfaceOnReceiveError. AddThread == 0. Exiting \n");
					break;
				}
#endif
				SLEEP(1000);
			}

		} else {
			if(pGIData->thrReinitData.cm && pGIData->thrReinitData.cm->InterfaceClose){
				pGIData->thrReinitData.cm->InterfaceClose(pCM);
			}
		}
	}

}

void _GeneralInterafceOnData(uint8_t  *data, uint16_t len, COM_MNG_DATA *interfaceObject){
	uint16_t i;
	char buf[31];

	*buf = 0;
	if(len == 0) {
		return;
	}
	DEBUG_DATA(data, len,"[%llu] - %s  OnSockData:len=%d, pObject=%p\n", (unsigned long long)GetSysTickCount(), PrintDate(buf,sizeof(buf)), len, interfaceObject);
	PrintParserInfo(interfaceObject);
	for(i=0;i<len;i++){
		OnDataReceive(interfaceObject, data[i]);
	}
}

void GeneralInterafceOnData(uint8_t  *data, uint16_t len, COM_MNG_DATA *interfaceObject){
	int maxCnt = 10000;
	while(interfaceObject->state != CM_STATE_ENABLED && --maxCnt) { //when interface is not marked as initialized, but data already arrived, wait for interface state
		SLEEP(1);
		DEBUGPRINTF("GeneralInterafceOnData %d\n",maxCnt);
	}
	_GeneralInterafceOnData(data,len,interfaceObject);
}


void GeneralInterfaceWrite(void *interfaceData, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, GeneralInterfaceSendData_t SendData, volatile uint8_t *Sending) {
	uint8_t buf[SEND_BUFF_SIZE];
	uint16_t len=0;
	int ret;
	if(startInd == endInd){
		*Sending = PORT_WRITE_ERROR;
		return;
	}
	endInd=(endInd + 1)& mask;
	do{
		buf[len++]=pData[startInd];
		startInd=( startInd+1 )& mask;
	}while(startInd!=endInd);
	//buf[len++]=pData[startInd];
	ret = SendData(interfaceData,buf, len);
	if(ret < 0) {
		  *Sending = PORT_WRITE_ERROR;
		  return;
	}
	*Sending = 0;
}

void GeneralInterfacePreinit(COM_MNG_DATA *cm, uint32_t speed, GI_Duplex_t duplex) {
	ChangeInterfaceState(cm, CM_STATE_INITIALIZING);
	cm->fullDuplex = (uint8_t)duplex;
	cm->communicationSpeed = speed;
	cm->oneByteDelayUs = WAIT_FOR_ONE_BYTE(cm->communicationSpeed);
	InitRandomTime(cm->communicationSpeed);
}

uint8_t GeneraInterfaceOpen(void *pInterfaceData, void* config) {
	GIData_t* pGIData = pInterfaceData;
	if(pGIData == NULL) {
		return GI_ERROR_NULL_DATA;
	}
	GIThrReinitData_t* pReinitData = &pGIData->thrReinitData;
	COM_MNG_DATA* cm = pGIData->thrReinitData.cm;

	uint16_t ret = pGIData->thrReinitData.Open(pInterfaceData, config);
	if(ret) {
		if(pReinitData->onInitErrorCallback) {
			pReinitData->onInitErrorCallback(pReinitData->userObject, ret, pReinitData->pErrorMsg);
		}
		ChangeInterfaceState(cm, CM_STATE_CLOSED);
		free(cm->interfaceData);
		cm->interfaceData = NULL;
		return ret;
	}
	ChangeInterfaceState(cm, CM_STATE_ENABLED);
	if(pReinitData->onOpenCallback){
		pReinitData->onOpenCallback(pReinitData->userObject, pReinitData->pOnOpenData);
	}
	return 0;


}

uint8_t GeneralInterfaceClose(GIData_t *data, uint8_t freeData){
	if(data->thrReinitData.onCloseCallback) {
		data->thrReinitData.onCloseCallback(data->thrReinitData.userObject);
	}
	data->thrReinitData.closeCallback(data->thrReinitData.pInterfaceData);
	if(freeData && data->thrReinitData.pInterfaceData) {
		free(data->thrReinitData.pInterfaceData);
		data->thrReinitData.pInterfaceData = NULL;
	}
	ChangeInterfaceState(data->thrReinitData.cm,CM_STATE_CLOSED);
	return 0;
}
