/*
 * Copyright (c) 2017, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "dirList.h"

#include <inttypes.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

void PrintDirList() {
	DIR *dp;
	struct dirent *ep;

	dp = opendir ("./");
	if (dp != NULL) {
		while ((ep = readdir (dp))) {
			printf("%d - %s\n",ep->d_type, ep->d_name);
		}
		closedir(dp);
	} else {
		perror ("Couldn't open the directory");
	}
}

#define MAX_APP_DIR_NAME_LEN 200

int16_t GetDirFiles(char* dir, uint8_t maxFiles, uint8_t maxFileNameLen, char* files, uint8_t includeLinkFiles ) {
	if(dir == NULL) {
		return -1;
	}
	if(files == NULL) {
		return -2;
	}
	struct dirent *ep;
	DIR *dp = opendir(dir);
	if(dp == NULL) {
		return -3;
	}
	int i = 0;
	while ((ep = readdir(dp))) {
		if(i > maxFiles) {
			closedir(dp);
			return -4;
		}
		if(!(ep->d_type == (uint8_t)dt_file || (includeLinkFiles && (ep->d_type == dt_symLink)))) {
			continue;
		}
		files[(i+1)*maxFileNameLen - 1] = 0;
		strncpy(files + (i++*maxFileNameLen),ep->d_name,maxFileNameLen - 1);
	}
	closedir(dp);
	return i;
}

void ClearDirList(int maxItems, int maxNameLen, char* dirList) {
	for(int i = 0; i < maxItems; i++) {
		dirList[maxNameLen*i] = 0;
	}
}

