/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "sysTimer.h"
#include "../../../common/common.h"
#include "../../../sys/timeManagerDep.h"
#include "../../../sys/timeCorr.h"
#include "../common/crossPlatformTimer.h"
#include "../common/threads.h"
#include "../common/sleep.h"
#include "sysConfig.h"

uint16_t sysTimerMaxVal=0xFFFF;
//uint16_t timer1;
uint16_t sysTimerCompareMax = DEFAULT_SYS_TIM_COMPAREMAX;
uint16_t timOffset=0;
volatile uint8_t timerRunning = 0;

#define __GET_SYS_TIME()	(((uint64_t)GetTimer(1) + timOffset))
#ifdef SIMPLE_TIMER
#define GET_SYS_TIME() ((uint64_t)__GET_SYS_TIME())
#else
#define GET_SYS_TIME() (uint16_t)__GET_SYS_TIME()
#endif
#ifdef SIMPLE_TIMER
uint64_t GET_SYS_TIMER_VAL_SCALED(){
	return (uint64_t)GET_SYS_TIME();
#else
uint16_t GET_SYS_TIMER_VAL_SCALED(){
	return GET_SYS_TIME() % sysTimerCompareMax;
#endif
}

void SET_SYS_TIMER_VAL_SCALED(uint16_t value){
	timOffset = value;
	StartTimer();
}



THREAD_RETURN_TYPE TimerThr(void *arg){
	((defaultThreadArgs_t *)arg)->started = 1;
	StartTimer();
	uint16_t tim, lastTim=0;
	timerRunning = 1;
	while(timerRunning){
		tim=(uint16_t)GET_SYS_TIME();

		while((tim>=sysTimerMaxVal )&& (lastTim<sysTimerMaxVal)){
			//OnCompareVal();
			lastTim = tim;
			tim=(uint16_t)GET_SYS_TIME();
		}
		lastTim=tim;
		if( tim>=sysTimerCompareMax){
			timOffset =0;//GET_TIME() % compareMax;
#ifdef TIMER_COMPENSATE
#ifdef SIMPLE_TIMER
#error SIMPLE_TIMER could not be used with TIMER_COMPENSATE ! Please remove SIMPLE_TIMER definition in your config file when you want to use TIMER_COMPENSATE function
#endif
			OnTimerCorr();
#else
			OnTimMax();
#endif
			StartTimer();
			lastTim=0;
		}

		SLEEP(1);
	} 
	return 0;
}

int8_t InitSysTimer(){
	if(timerRunning) {
		return 1;
	}
	InitCrossPlatformTimer();
	//create working thread
#ifndef SIMPLE_TIMER
	thread_handle t;
	t = createThread((thread_start_fn)TimerThr, NULL);
	if (t == 0){
		PRINTF("Error creating new thread, error\n");
		return -1;
	}
#endif
  //and detach the thread so that it is released when it terminates
  return 0;

}


double ComputeTimeDiff( LARGE_INTEGER tick1, LARGE_INTEGER tick2){
	LARGE_INTEGER ticksPerSecond;
	QueryPerformanceFrequency(&ticksPerSecond);
	return (double)(tick2.QuadPart-tick1.QuadPart)/(double)(ticksPerSecond.QuadPart);
}
