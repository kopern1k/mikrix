/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../../include/TCPServerInterface.h"

#include "../../../sys/threadManager.h"

#include "sysConfig.h"
#ifdef TCP_CLIENT_INTERFACE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

void OnTCPServerSockData(void *object, uint8_t *data, uint32_t len, TCPConnectionInfo_t* info) {
	((TCPServerInterfaceData_t*)((COM_MNG_DATA*)object)->interfaceData)->GIData.thrReinitData.pOnReceiveErrorData = info;
	GeneralInterafceOnData(data,len,(COM_MNG_DATA*) object);
}


uint8_t TCPServerWantReinit(uint8_t error) {
	return error != TCP_SERVER_ERR_CLIENT_DISCONNECTED;
}

uint8_t TCPServerIsReopenedCorrectly(uint8_t reopenRetVal) {
	return reopenRetVal == 0;
}

void TCPServerOnReceiveError(void* object, uint16_t error, TCPConnectionInfo_t* info) {
	((TCPServerInterfaceData_t*)((COM_MNG_DATA*)object)->interfaceData)->GIData.thrReinitData.pOnReceiveErrorData = info;
	GeneralInterfaceOnReceiveError(object,error);
}

void TCPServerReopenFailed(uint8_t error, TCPServerInterfaceData_t* data) {
	DEBUGPRINTF("%s, error= %hhu\n",data->tcpServer.errorMsg, error);
}

void TCPServerSocketInterfaceWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending) {
	GeneralInterfaceWrite((void*)&(((TCPServerInterfaceData_t*)cm->interfaceData)->tcpServer),
						  pData, startInd, endInd, mask,(GeneralInterfaceSendData_t)TCPServerSendDataAll,Sending);
}

uint8_t TCPServerSocketInterfaceClose(COM_MNG_DATA *cm){
	return GeneralInterfaceClose( &(((TCPServerInterfaceData_t*)cm->interfaceData)->GIData), 1);
}

uint8_t TCPServerSocketInterfaceOpen(TCPServerInterfaceData_t *pInterfaceData, TCPServerConfig_t* config) {
	return TCPServerOpen(&(pInterfaceData->tcpServer),
						 config->port,
						 OnTCPServerSockData,
						 config->userObject,
						 pInterfaceData->GIData.thrReinitData.cm,
						 TCPServerOnReceiveError);
}

uint8_t TCPServerSocketInterfaceInit( uint8_t*data, COM_MNG_DATA *cm) {
	TCPServerConfig_t* pData = (TCPServerConfig_t*)data;

	if(pData == NULL) {
		return GI_ERROR_CONFIG_DATA;
	}
	if((cm->interfaceData = malloc(sizeof(TCPServerInterfaceData_t))) == NULL) {
		return GI_ERROR_MEMORY_ALLOC;
	}
	GeneralInterfacePreinit(cm,10000000,GI_FullDuplex);

	TCPServerInterfaceData_t *pTCPIfdata = (TCPServerInterfaceData_t*)cm->interfaceData;
	pTCPIfdata->GIData.thrReinitData.pInterfaceData = &pTCPIfdata->tcpServer;
	pTCPIfdata->GIData.thrReinitData.cm = cm;
	pTCPIfdata->GIData.thrReinitData.maxCnt = pData->reinitCount;
	pTCPIfdata->GIData.thrReinitData.var.cnt = pTCPIfdata->GIData.thrReinitData.maxCnt;
	pTCPIfdata->GIData.thrReinitData.timeIntervalMs = pData->reinitRetryIntervalMs;
	pTCPIfdata->GIData.reinitOnErrorEn = pData->reinitOnErrorEn;
	pTCPIfdata->GIData.thrReinitData.userObject = pData->userObject;

	pTCPIfdata->GIData.thrReinitData.onReceiveErrorCallback = (InterfaceOnReceiveError_t) pData->onReceiveError;
	pTCPIfdata->GIData.thrReinitData.pOnReceiveErrorData = NULL;

	pTCPIfdata->GIData.thrReinitData.onInitErrorCallback = (GeneralInterfaceOnInitError_t) pData->onInterfaceInitError;

	pTCPIfdata->GIData.thrReinitData.onCloseCallback = (TCPServerInterfaceOnClose_t) pData->onInterfaceClose;

	pTCPIfdata->GIData.thrReinitData.onReopenCallback = (GeneralInterfaceOnReopen_t) pData->onInterfaceReopen;
	pTCPIfdata->GIData.thrReinitData.pOnReopenData = &pTCPIfdata->tcpServer;
	pTCPIfdata->GIData.thrReinitData.IsReopenedCorrectly = TCPServerIsReopenedCorrectly;
	pTCPIfdata->GIData.WantReinit = TCPServerWantReinit;
	pTCPIfdata->GIData.thrReinitData.reopenFailed = (InterfaceReopenFailed_t) TCPServerReopenFailed;
	pTCPIfdata->GIData.thrReinitData.closeCallback = (GeneralInterfaceClose_t) TCPServerClose;
	pTCPIfdata->GIData.thrReinitData.Reopen = (GeneralInterfaceReopen_t) TCPServerReinit;

	pTCPIfdata->GIData.thrReinitData.Open = (GeneralInterfaceOpen_t) TCPServerSocketInterfaceOpen;
	pTCPIfdata->GIData.thrReinitData.pErrorMsg = pTCPIfdata->tcpServer.errorMsg;
	pTCPIfdata->GIData.thrReinitData.maxErrorMsgLen = TCP_ERROR_MSG_MAX_LEN;
	pTCPIfdata->GIData.thrReinitData.onOpenCallback = (GeneralInterfaceOnOpen_t) pData->onInterfaceOpen;
	pTCPIfdata->GIData.thrReinitData.pOnOpenData = (void*)&pTCPIfdata->tcpServer;

	return GeneraInterfaceOpen((void*)cm->interfaceData,(void*)pData);
}
