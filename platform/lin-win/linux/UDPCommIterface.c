/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

//#define TEST
#include "sysConfig.h"
#include "../../include/UDPCommInterface.h"
#include <string.h>

#ifdef UDP_COMM_INTERFACE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

void OnUDPConnSockData(void *object, uint8_t *data, uint32_t len) {
	GeneralInterafceOnData(data,len,(COM_MNG_DATA*) object);
}

#ifdef TEST
void UDPConnOnSentCallback(void * userObject, uint8_t* data, uint16_t transferredDataLen, int8_t err) {
	if(transferredDataLen == 0 && err) {
		DEBUGPRINTF("UDPConnOnSentCallback: %hhd,%s\n",err,strerror(err));
	}
}
#else
#define UDPConnOnSentCallback NULL
#endif


uint8_t UDPConnWantReinit(uint8_t error) {
	return error > 0 ;
}

uint8_t UDPConnIsReopenedCorrectly(uint8_t reopenRetVal) {
	return reopenRetVal == 0;
}

void UDPConnOnReceiveError(void* object, uint16_t error) {
	GeneralInterfaceOnReceiveError(object,error);

}

void UDPConnReopenFailed(uint8_t error, UDPConnInterfaceData_t* data) {
	DEBUGPRINTF("%s, error= %hhu\n",data->udpConn.errorMsg, error);
}

void UDPConnSocketInterfaceWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending) {
	GeneralInterfaceWrite((void*)&(((UDPConnInterfaceData_t*)cm->interfaceData)->udpConn),
						  pData, startInd, endInd, mask,(GeneralInterfaceSendData_t)UDPConnSendData,Sending);
}

uint8_t UDPConnSocketInterfaceClose(COM_MNG_DATA *cm){

	return GeneralInterfaceClose( &(((UDPConnInterfaceData_t*)cm->interfaceData)->GIData), 1);
}

uint8_t UDPConnSocketInterfaceOpen(UDPConnInterfaceData_t *pInterfaceData, UDPConnConfig_t* config) {
	return  UDPConnOpen(&pInterfaceData->udpConn,
						(char*)config->hostName,
						config->port,
						OnUDPConnSockData,
						UDPConnOnSentCallback,
						pInterfaceData->GIData.thrReinitData.cm,
						UDPConnOnReceiveError,
						config->broadcasting);
}

uint8_t UDPConnSocketInterfaceInit( uint8_t*data, COM_MNG_DATA *cm) {
	UDPConnConfig_t* pData = (UDPConnConfig_t*)data;

	if(pData == NULL) {
		return GI_ERROR_CONFIG_DATA;
	}
	if((cm->interfaceData = malloc(sizeof(UDPConnInterfaceData_t))) == NULL) {
		return GI_ERROR_MEMORY_ALLOC;
	}

	GeneralInterfacePreinit(cm,10000000,GI_FullDuplex);

	UDPConnInterfaceData_t *pUDPIfdata = (UDPConnInterfaceData_t*)cm->interfaceData;
	pUDPIfdata->GIData.thrReinitData.pInterfaceData = &pUDPIfdata->udpConn;
	pUDPIfdata->GIData.thrReinitData.cm = cm;
	pUDPIfdata->GIData.thrReinitData.maxCnt = pData->reinitCount;
	pUDPIfdata->GIData.thrReinitData.var.cnt = pUDPIfdata->GIData.thrReinitData.maxCnt;
	pUDPIfdata->GIData.thrReinitData.timeIntervalMs = pData->reinitRetryIntervalMs;
	pUDPIfdata->GIData.reinitOnErrorEn = pData->reinitOnErrorEn;
	pUDPIfdata->GIData.thrReinitData.userObject = pData->userObject;

	pUDPIfdata->GIData.thrReinitData.onReceiveErrorCallback = (InterfaceOnReceiveError_t) pData->onInterfaceReceiveError;
	pUDPIfdata->GIData.thrReinitData.pOnReceiveErrorData = (void*)&pUDPIfdata->udpConn.IPParams;

	pUDPIfdata->GIData.thrReinitData.onInitErrorCallback = (GeneralInterfaceOnInitError_t) pData->onInterfaceInitError;

	pUDPIfdata->GIData.thrReinitData.onCloseCallback = (UDPConnInterfaceOnClose_t) pData->onInterfaceClose;

	pUDPIfdata->GIData.thrReinitData.onReopenCallback = (GeneralInterfaceOnReopen_t) pData->onInterfaceReopen;
	pUDPIfdata->GIData.thrReinitData.pOnReopenData = (void*)&pUDPIfdata->udpConn.IPParams;
	pUDPIfdata->GIData.thrReinitData.IsReopenedCorrectly = UDPConnIsReopenedCorrectly;
	pUDPIfdata->GIData.WantReinit = UDPConnWantReinit;
	pUDPIfdata->GIData.thrReinitData.reopenFailed = (InterfaceReopenFailed_t) UDPConnReopenFailed;
	pUDPIfdata->GIData.thrReinitData.closeCallback = (GeneralInterfaceClose_t) UDPConnClose;
	pUDPIfdata->GIData.thrReinitData.Reopen = (GeneralInterfaceReopen_t) UDPConnReopen;

	pUDPIfdata->GIData.thrReinitData.Open = (GeneralInterfaceOpen_t) UDPConnSocketInterfaceOpen;
	pUDPIfdata->GIData.thrReinitData.pErrorMsg = pUDPIfdata->udpConn.errorMsg;
	pUDPIfdata->GIData.thrReinitData.maxErrorMsgLen = UDP_CONN_ERROR_MSG_MAX_LEN;
	pUDPIfdata->GIData.thrReinitData.onOpenCallback = (GeneralInterfaceOnOpen_t) pData->onInterfaceOpen;
	pUDPIfdata->GIData.thrReinitData.pOnOpenData = (void*)&pUDPIfdata->udpConn.IPParams;

	UDPConnInit(&(((UDPConnInterfaceData_t*)cm->interfaceData)->udpConn));
	return GeneraInterfaceOpen((void*)cm->interfaceData,(void*)pData);
}
