/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../../include/serialInterface.h"
#include "serial.h"
#include "../common/sleep.h"

commThreadParams* com;

int OnDataWrapperEx(void* objectData, uint8_t data, uint8_t timeouted) {
	OnDataWrapper(objectData,data);
	return 0;
}

uint8_t SerialInitWr(uint8_t *config, COM_MNG_DATA *cm){
	return SerialInit((SERIAL_CONFIG *)config, cm);
}
uint8_t SerialInit(SERIAL_CONFIG *config, COM_MNG_DATA *cm){
	ChangeInterfaceState(cm, CM_STATE_INITIALIZING);
	com = InitSerialPort((char*)config->portName, config->speed,PARITY_NONE, TWO_STOPBITS, 0, 0, OnDataWrapperEx, (void*) cm, NULL, NULL, NULL,NULL);
	if(com == NULL){
		ChangeInterfaceState(cm,CM_STATE_CLOSED);
		return 1;
	}
	ChangeInterfaceState(cm,CM_STATE_ENABLED);
	cm->fullDuplex = 1;
	cm->communicationSpeed = config->speed;
	cm->oneByteDelayUs = WAIT_FOR_ONE_BYTE(cm->communicationSpeed);
	InitRandomTime(cm->communicationSpeed);
	return 0;
}


uint8_t SerialClose(COM_MNG_DATA *cm){
	return CloseSerialPort(com);
}

void SerialWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending){
	char buf[SEND_BUFF_SIZE];
	static	uint8_t sended;
	uint16_t len=0;
	endInd=(endInd+1)& mask;
	do{
		buf[len++]=pData[startInd];
		startInd=( startInd+1 )& mask;
	}while(startInd!=endInd);

	if(SerialSendData(com,(uint8_t*)buf,len, &sended) == 0) {
		while(sended == 0) {
			SLEEP(1);
		}
		*Sending =0;
	}
	*Sending = PORT_WRITE_ERROR;
}


