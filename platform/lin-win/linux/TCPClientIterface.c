/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "sysConfig.h"
#include "../../include/TCPClientInterface.h"

#ifdef TCP_CLIENT_INTERFACE_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../common/debugPrintf.h"


void OnTCPClientSockData(void *object, uint8_t *data, uint32_t len) {
	GeneralInterafceOnData(data,len,(COM_MNG_DATA*) object);
}


uint8_t TCPClientWantReinit(uint8_t error) {
	return error > 0;
}

uint8_t TCPClientIsReopenedCorrectly(uint8_t reopenRetVal) {
	return reopenRetVal == 0;
}

void TCPClientOnReceiveError(void* object, uint16_t error) {
	GeneralInterfaceOnReceiveError(object,error);

}

void TCPClientReopenFailed(uint8_t error, TCPClientInterfaceData_t* data) {
	DEBUGPRINTF("%s, error= %hhu\n",data->tcpClient.errorMsg, error);
}

void TCPClientSocketInterfaceWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending) {
	GeneralInterfaceWrite((void*)&(((TCPClientInterfaceData_t*)cm->interfaceData)->tcpClient),
						  pData, startInd, endInd, mask,(GeneralInterfaceSendData_t)TCPClientSendData,Sending);
}

uint8_t TCPClientSocketInterfaceClose(COM_MNG_DATA *cm){

	return GeneralInterfaceClose( &(((TCPClientInterfaceData_t*)cm->interfaceData)->GIData), 1);
}

uint8_t TCPClientSocketInterfaceOpen(TCPClientInterfaceData_t *pInterfaceData, TCPClientConfig_t* config) {
	return TCPClientOpen(&pInterfaceData->tcpClient,
						 (char*)config->hostName,
						 config->port,
						 OnTCPClientSockData,
						 NULL,
						 pInterfaceData->GIData.thrReinitData.cm,
						 TCPClientOnReceiveError,
						 config->keepAliveExpireTime);
}

uint8_t TCPClientSocketInterfaceInit(uint8_t *data, COM_MNG_DATA *cm) {
	TCPClientConfig_t* pData = (TCPClientConfig_t*)data;

	if(pData == NULL) {
		return GI_ERROR_CONFIG_DATA;
	}
	if((cm->interfaceData = malloc(sizeof(TCPClientInterfaceData_t))) == NULL) {
		return GI_ERROR_MEMORY_ALLOC;
	}

	GeneralInterfacePreinit(cm, 100000000, GI_FullDuplex);

	TCPClientInterfaceData_t *pTCPIfdata = (TCPClientInterfaceData_t*)cm->interfaceData;
	pTCPIfdata->GIData.thrReinitData.pInterfaceData = &pTCPIfdata->tcpClient;
	pTCPIfdata->GIData.thrReinitData.cm = cm;
	pTCPIfdata->GIData.thrReinitData.maxCnt = pData->reinitCount;
	pTCPIfdata->GIData.thrReinitData.var.cnt = pTCPIfdata->GIData.thrReinitData.maxCnt;
	pTCPIfdata->GIData.thrReinitData.timeIntervalMs = pData->reinitRetryIntervalMs;
	pTCPIfdata->GIData.reinitOnErrorEn = pData->reinitOnErrorEn;
	pTCPIfdata->GIData.thrReinitData.userObject = pData->userObject;

	pTCPIfdata->GIData.thrReinitData.onReceiveErrorCallback = (InterfaceOnReceiveError_t) pData->onReceiveErrorCallback;
	pTCPIfdata->GIData.thrReinitData.pOnReceiveErrorData = (void*)&pTCPIfdata->tcpClient.IPParams;

	pTCPIfdata->GIData.thrReinitData.onInitErrorCallback = (GeneralInterfaceOnInitError_t) pData->onInterfaceInitError;

	pTCPIfdata->GIData.thrReinitData.onCloseCallback = (TCPClientInterfaceOnClose_t) pData->onCloseCallback;

	pTCPIfdata->GIData.thrReinitData.onReopenCallback = (GeneralInterfaceOnReopen_t) pData->onReopenCallback;
	pTCPIfdata->GIData.thrReinitData.pOnReopenData = (void*)&pTCPIfdata->tcpClient.IPParams;
	pTCPIfdata->GIData.thrReinitData.IsReopenedCorrectly = TCPClientIsReopenedCorrectly;
	pTCPIfdata->GIData.WantReinit = TCPClientWantReinit;
	pTCPIfdata->GIData.thrReinitData.reopenFailed = (InterfaceReopenFailed_t) TCPClientReopenFailed;
	pTCPIfdata->GIData.thrReinitData.closeCallback = (GeneralInterfaceClose_t) TCPClientClose;
	pTCPIfdata->GIData.thrReinitData.Reopen = (GeneralInterfaceReopen_t) TCPClientReopen;
	pTCPIfdata->GIData.thrReinitData.Open = (GeneralInterfaceOpen_t) TCPClientSocketInterfaceOpen;
	pTCPIfdata->GIData.thrReinitData.pErrorMsg = pTCPIfdata->tcpClient.errorMsg;
	pTCPIfdata->GIData.thrReinitData.maxErrorMsgLen = TCP_CLIENT_ERROR_MSG_MAX_LEN;
	pTCPIfdata->GIData.thrReinitData.onOpenCallback = (GeneralInterfaceOnOpen_t) pData->onInterfaceOpen;
	pTCPIfdata->GIData.thrReinitData.pOnOpenData = (void*)&pTCPIfdata->tcpClient.IPParams;

	TCPClientInit(&(((TCPClientInterfaceData_t*)cm->interfaceData)->tcpClient));
	return GeneraInterfaceOpen((void*)cm->interfaceData,(void*)pData);
}
