#include "availableInterfaces.h"
#include "../../include/serialInterface.h"
#include "../../include/multicastInterface.h"
#include "../../include/debugInterface.h"
#include "../../include/TCPClientInterface.h"
#include "../../include/TCPServerInterface.h"
#include "../../include/UDPCommInterface.h"

INTERFACE availableInterfaces[__LAST_INTERFACE__TAG__]={
LINUX_INTERFACE_LIST
};

uint8_t IsInterfaceType(COM_MNG_DATA* cm,INTERFACE_TYPE_ID interfaceID) {
	return cm->selectedInterfaceTypeID == interfaceID;
}
