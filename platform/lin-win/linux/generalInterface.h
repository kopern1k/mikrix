#ifndef __GENERAL_INTERFACE__H__
#define __GENERAL_INTERFACE__H__
#include "../../common/common.h"
#include "../../sys/communicationManager.h"
#include "../../sys/communicationInterface.h"


// handlers

typedef void (*GeneralInterfaceOnOpen_t)(void* userObject, void* interfaceData);
typedef void (*GeneralInterfaceOnReopen_t)(void* userObject, void* interfaceData);
typedef uint8_t (*GeneralInterfaceClose_t)(void *interfaceData);
typedef void (*GeneralInterfaceOnClose_t)(void* userObject);
typedef void (*InterfaceOnReceiveError_t)(uint8_t error,void* userData, void* interfaceData);
typedef void (*GeneralInterfaceOnInitError_t)(void* userObject, int16_t err, char* errMsg);
typedef void (*InterfaceReopenFailed_t)(uint8_t error, void* data);
typedef uint8_t (*GeneralInterfaceOpen_t)(void* interfaceData, void* config);
typedef uint8_t (*GeneralInterfaceReopen_t)(void* interfaceData);
typedef uint8_t (*WantReinit_t)(uint8_t error);
typedef uint8_t (*IsReopenedCorrectly_t)(uint8_t reopenRetVal);

typedef int (*GeneralInterfaceSendData_t)(void *interfaceData,uint8_t* data, int len);

typedef enum { GI_HalfDuplex = 0, GI_FullDuplex} GI_Duplex_t;

#define GI_ERROR_OFFSET 250

#define GI_ERROR_CONFIG_DATA GI_ERROR_OFFSET + 1
#define GI_ERROR_MEMORY_ALLOC GI_ERROR_OFFSET + 2
#define GI_ERROR_NULL_DATA GI_ERROR_OFFSET + 3


typedef struct {
	uint8_t cnt;
	mTime_t Timer;
} GIThrReinitVar_t;
typedef struct {
	GIThrReinitVar_t var;
	COM_MNG_DATA *cm;
	uint16_t timeIntervalMs;
	uint8_t maxCnt; // fill at begin 0 - > never ending loop
	void* userObject;
	void* pInterfaceData;
	char* pErrorMsg;
	uint16_t maxErrorMsgLen;
	GeneralInterfaceOnOpen_t onOpenCallback;
	GeneralInterfaceOnReopen_t onReopenCallback;
	InterfaceOnReceiveError_t onReceiveErrorCallback;
	GeneralInterfaceOnInitError_t onInitErrorCallback;
	GeneralInterfaceClose_t closeCallback;
	GeneralInterfaceOnClose_t onCloseCallback;
	InterfaceReopenFailed_t reopenFailed;
	IsReopenedCorrectly_t IsReopenedCorrectly;
	GeneralInterfaceOpen_t Open;
	GeneralInterfaceReopen_t Reopen;
	void* pOnOpenData;
	void* pOnReopenData;
	void* pOnReceiveErrorData;
} GIThrReinitData_t;

/*
 *Note. If you use GIReinit helper functions, your implementation must include
 * GIReinitData_t as first variable interface user data
 */
typedef struct {
	GIThrReinitData_t thrReinitData;
	WantReinit_t WantReinit;
	uint8_t reinitOnErrorEn;
} GIData_t;



void GeneralInterfacePreinit(COM_MNG_DATA *cm, uint32_t speed, GI_Duplex_t duplex);
uint8_t GeneraInterfaceOpen(void *pInterfaceData, void* config);
uint8_t GeneralInterfaceReinitHandlerThr(void *data, pt* Thr);
void GeneralInterfaceOnReceiveError(void* object, uint16_t error);
void GeneralInterafceOnData(uint8_t  *data, uint16_t len, COM_MNG_DATA *interfaceObject);
void GeneralInterfaceWrite(void *interfaceData, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, GeneralInterfaceSendData_t SendData, volatile uint8_t *Sending);
uint8_t GeneralInterfaceClose(GIData_t *data, uint8_t freeData);
#endif
