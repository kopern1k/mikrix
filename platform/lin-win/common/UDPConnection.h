/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __UDP_CONNECTION_H__
#define __UDP_CONNECTION_H__

#include <sys/types.h>
#include <sys/select.h>
#include <pthread.h>
#include "socketCommon.h"
#include "inttypes.h"
#include "sysConfig.h"

typedef void (*UDPConnOnDataCallback_t)(void *userObject, uint8_t *data, uint32_t len);
typedef void (*UDPConnOnSentCallback_t) (void * userObject, uint8_t* data, uint16_t transferredDataLen, int8_t err);
typedef void (*UDPConnOnReceiveErrorHandler_t)(void* userObject, uint16_t error);

#define UDP_CONN_MAX_HOSTNAME_LEN 100

#define UDP_CONN_DEFAULT_KEEPALIVE_PACKET_CNT 3
#define UDP_CONN_MIN_KEEPALIVE_TIME 2

#define UDP_CONN_ERROR_MSG_MAX_LEN 150
#define UDP_CONN_MAX_BUF_SIZE 255
#define UDP_CONN_MAX_OUT_BUF_SIZE 1024

//#define UDP_CONN_ERR_DISCONNECTED	1
#define UDP_CONN_ERR_UNDEFINED 200
#define UDP_CONN_ERR_DISCONNECTED 201
#define UDP_CONN_ERR_BAD_HEADER		202

typedef struct {
	uint16_t port;
	char hostName[UDP_CONN_MAX_HOSTNAME_LEN];
} UDPConnIPParams_t;

typedef struct {
	SOCKET sock;
	UDPConnIPParams_t IPParams;
	fd_set active_fd_set;
	fd_set read_fd_set;
	char errorMsg[UDP_CONN_ERROR_MSG_MAX_LEN];
	uint8_t buf[UDP_CONN_MAX_BUF_SIZE];
	uint8_t outBuf[UDP_CONN_MAX_OUT_BUF_SIZE];
	UDPConnOnDataCallback_t onDataCallback;
	UDPConnOnSentCallback_t onSentCallback;
	UDPConnOnReceiveErrorHandler_t onReceiveErrorHandler;
	void *userObject;
	uint8_t isOpened;
	pthread_mutex_t initializing;
	uint8_t broadcast;
	uint8_t skipOwnMessages;
	uint64_t ownRandom;
	struct sockaddr_in dest;
} UDPConn_t;

typedef struct ATTRIB_ALIGN_8() {
	uint64_t identifier;
	uint16_t size;
} UDPIntentityHdr_t;


int UDPConnSendData(UDPConn_t *udpConn,uint8_t* data, int len);
uint8_t UDPConnInit(UDPConn_t *udpConn);
uint8_t UDPConnOpen(UDPConn_t *udpConn, char* hostName, uint16_t port, UDPConnOnDataCallback_t onDataCallback, UDPConnOnSentCallback_t onSentCallback, void *userObject, UDPConnOnReceiveErrorHandler_t onReceiveErrorHandler, uint8_t broarcast);
void UDPConnClose(UDPConn_t *udpConn);
uint8_t UDPConnReopen(UDPConn_t *udpConn);
uint8_t UDPConnGetConnectionInfo(UDPConn_t *udpConn, UDPConnIPParams_t* udpConnectionInfo);
uint8_t UDPConnUpdateConnectionInfo(UDPConn_t *udpConn, UDPConnIPParams_t* udpConnectionInfo);
#endif // CLIENT_H
