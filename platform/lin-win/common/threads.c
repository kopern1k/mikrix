/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "threads.h"
#include "sleep.h"
#include "../../../common/common.h"

#define THREAD_START_CONFIRM_TIMEOUT 500


thread_handle createThread(thread_start_fn f, void *arg)
{
  defaultThreadArgs_t a;
  a.started = 0;
  a.args = arg;

#if defined WIN32 || defined WIN64
  HANDLE t;
  DWORD tid;
  t = CreateThread(0, 0, f, &a, 0, &tid);
  if (t == 0) {
    PRINTF("Error creating new thread, error=%d\n", GetLastError());
    return 0;
  }
  //and detach the thread so that it is released when it terminates
  CloseHandle(t);
#else
  pthread_t t;
  int errno = pthread_create(&t, 0, f, &a);
  if (errno) {
    PRINTF("Error creating new thread, errno=%d\n", errno);
    return 0;
  }
  //and detach the thread so that it is released when it terminates
  errno = pthread_detach(t);
  if (errno) {
    PRINTF("Error detaching thread, errno=%d\n", errno);
    return 0;
  }
#endif

  int count = 0;
  while ((!a.started) && (count < THREAD_START_CONFIRM_TIMEOUT)) { SLEEP(10); count++; }
  if(count>=THREAD_START_CONFIRM_TIMEOUT) {
	PRINTF("Thread not responded, started parameter!");
	//
  }
  return t;
}
