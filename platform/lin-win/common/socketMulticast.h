/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SOCKETMULTICAST__H__
#define __SOCKETMULTICAST__H__

#include "../../common/common.h"
#include "socketCommon.h"
#include <netinet/in.h>
#include <pthread.h>

#define SOCK_MULTICAST_MAX_HOSTNAME_LEN 100
#define SOCK_MULTICAST_ERROR_MSG_MAX_LEN 200
#define MAX_SOCKET_MULTICAST_REC_BUFSIZE 1024
#define DEFAULT_MULTICAST_LISTEN_PORT	4242
#define DEFAULT_MULTICAST_GROUP "224.0.0.11"


#define SOCK_MULTICAST_ERR_UNDEFINED 200
#define SOCK_MULTICAST_ERR_DISCONNECTED 201

typedef void (*SockMulticastOnDataCallback_t)(void *userObject, uint8_t *data, uint32_t len);
typedef void (*SockMulticastOnSentCallback_t) (void * userObject, uint8_t* data, uint16_t transferredDataLen, int8_t err);
typedef void (*SockMulticastOnReceiveErrorHandler_t)(void* userObject, uint16_t error);

typedef struct {
	uint16_t port;
	char hostName[SOCK_MULTICAST_MAX_HOSTNAME_LEN];
} sockMulticastIPParams_t;

enum MulticastTTL_t {restrictedToSameHost = 0,restrictedToSameSubnet=1, restrictedToSameSite = 32};

typedef struct {
	SOCKET sock;
	sockMulticastIPParams_t IPParams;
	char errorMsg[SOCK_MULTICAST_ERROR_MSG_MAX_LEN];
	uint8_t buf[MAX_SOCKET_MULTICAST_REC_BUFSIZE];
	SockMulticastOnDataCallback_t onDataCallback;
	SockMulticastOnSentCallback_t onSentCallback;
	SockMulticastOnReceiveErrorHandler_t onReceiveErrorHandler;
	void *userObject;
	uint8_t isConnected;
	pthread_mutex_t initializing;
	//MulticastTTL_t TTLOptions;
	struct sockaddr_in senderAddr;
	struct sockaddr_in clAddr; // received address
} sockMulticast_t;


uint8_t SockMulticastInit(sockMulticast_t *sockMulticast);
uint8_t SockMulticastOpen(sockMulticast_t *sockMulticast, char* hostName, uint16_t port, SockMulticastOnDataCallback_t onDataCallback, SockMulticastOnSentCallback_t onSentCallback, void *userObject, SockMulticastOnReceiveErrorHandler_t onReceiveErrorHandler);
void SockMulticastClose(sockMulticast_t *sockMulticast);
uint8_t SockMulticastReopen(sockMulticast_t *sockMulticast);
int SockMulticastSendData(sockMulticast_t *socketMulticast, uint8_t * data, uint16_t len);
#endif
