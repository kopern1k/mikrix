/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#ifndef __USE_BSD
	#define __USE_BSD
#endif
#ifndef __USE_XOPEN_EXTENDED
	#define __USE_XOPEN_EXTENDED
#endif
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "threads.h"
#include "socketCommon.h"
#include "TCPServer.h"



#define TCP_SERVER_TEST_PORT    5555


LIST_ITEM(connectionInfoItem_t,
		  TCPConnectionInfo_t info;
		  volatile uint8_t markCriticalOp;

);

void MarkCriticalOp(connectionInfoItem_t* connectionIfoItem) {
	connectionIfoItem->markCriticalOp = 1;
}

void ClearCriticalOp(connectionInfoItem_t* connectionIfoItem) {
	connectionIfoItem->markCriticalOp = 0;
}


// while any error return sock<0
int MakeServerSocket(uint16_t port, char* errorBuf) {
	int sock;
	struct sockaddr_in name;

	/* Create the socket. */
	sock = socket (PF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		sprintf(errorBuf,"socket error (%d)",sock);
		return -1;
	}

	/* Give the socket a name. */
	name.sin_family = AF_INET;
	name.sin_port = htons (port);
	name.sin_addr.s_addr = htonl (INADDR_ANY);
	int err;
	if ((err = bind (sock, (struct sockaddr *) &name, sizeof (name))) < 0) {
		sprintf(errorBuf,"bind error (%d)",err);
		return -2;
	}

	return sock;
}

int ReadFromClient (TCPServer_t* tcpServer, TCPConnectionInfo_t *info) {
	char buffer[TCP_SERVER_MAX_REC_MSG_LEN];
	int nbytes;

	nbytes = read(info->sock, buffer, TCP_SERVER_MAX_REC_MSG_LEN);
	if (nbytes < 0) {
		/* Read error. */
		sprintf(tcpServer->errorMsg,"Error read (%d), errno:%d",nbytes,errno);
		return -1;
	 } else if (nbytes == 0) {
		/* End-of-file. */
		sprintf(tcpServer->errorMsg,"eol read!");
		return -2;
	 }
	else {
		/* Data read. */
		if(tcpServer->onDataCallback) {
			tcpServer->onDataCallback(tcpServer->userObject,(uint8_t*)buffer,nbytes,info);
		}

		return nbytes;
	}
}


THREAD_RETURN_TYPE TCPServerReadThread(void *arg)
{
	TCPServer_t *tcpServer;
	tcpServer = (TCPServer_t *)((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;
	uint8_t ret = 0;
	int err;
	connectionInfoItem_t* connectionInfoItem;
	fd_set exceptfds;
	struct timeval tv;

	//possible to bind from other process to this socket
	//int one = 1;
	//setsockopt(tcpServer->listeningSock,SOL_SOCKET,SO_REUSEADDR,&one,sizeof(int));

	/* Initialize the set of active sockets. */
	FD_ZERO (&tcpServer->active_fd_set);
	FD_SET (tcpServer->listeningSock, &tcpServer->active_fd_set);

	tcpServer->isListening = 1;
	while (1) {
		/* Wait up to five seconds. */
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		/* Block until input arrives on one or more active sockets. */
		tcpServer->read_fd_set = tcpServer->active_fd_set;

		if ((err = (select (FD_SETSIZE, &tcpServer->read_fd_set, NULL, NULL, &tv))) < 0) {
			sprintf(tcpServer->errorMsg,"Error select (%d)",err );
			ret = TCP_SERVER_ERR_SELECT_PROBLEM;
			break;
		}
		if(err == 0) {
			if(SockIsclosed(tcpServer->listeningSock)) {
				break;
			} else {
				continue;
			}

		}
		if(FD_ISSET(tcpServer->listeningSock, &exceptfds)) {
			ret = 2;
			break;
		}
		if (FD_ISSET (tcpServer->listeningSock, &tcpServer->read_fd_set)) {
		  /* Connection request on original socket. */
			int newConnection;
			size_t size;
			struct sockaddr_in clientname;
			size = sizeof (clientname);
			newConnection = accept (tcpServer->listeningSock, (struct sockaddr *) &clientname, (socklen_t*)&size);
			if (newConnection < 0) {
				sprintf(tcpServer->errorMsg,"error Accept (%d)",newConnection);
				ret = TCP_SERVER_ERR_ACCEPT_CLIENT_PROBLEM;
				break;
			}
			// Add new connection to list
			NEW_LIST_ITEM(connectionInfoItem_t, connectionInfoItem);
			connectionInfoItem->info.sock = newConnection;
			connectionInfoItem->info.clientname = clientname;
			connectionInfoItem->info.size = size;
			connectionInfoItem->markCriticalOp = 0;
			ListAdd(&tcpServer->allConnections,connectionInfoItem);

			//fprintf (stderr, "Server: connect from host %s, port %hu.\n", inet_ntoa(clientname.sin_addr), ntohs(clientname.sin_port));
			FD_SET (newConnection, &tcpServer->active_fd_set);
			continue;
		}

		/* Service all the sockets with input pending. */
		connectionInfoItem_t* current = (connectionInfoItem_t*) tcpServer->allConnections;
		while(current) {
			if (FD_ISSET (current->info.sock, &tcpServer->read_fd_set)) {
			  /* Connection request on original socket. */
				if (ReadFromClient(tcpServer, &current->info) < 0) {

					close (current->info.sock);
					FD_CLR (current->info.sock, &tcpServer->active_fd_set);
					ret = TCP_SERVER_ERR_CLIENT_DISCONNECTED;
					if(tcpServer->onReceiveErrorHandler) {
						tcpServer->onReceiveErrorHandler(tcpServer->userObject,ret,&current->info);
					}
					ListRemove(&tcpServer->allConnections,(listItem_t*)current);

					if(tcpServer->allConnections == 0) {
						while (current->markCriticalOp) {usleep(1000);}
						FREE_LIST_ITEM(current);
						break;
					}
					connectionInfoItem_t* tmp = current;
					current = current->next;
					while (tmp->markCriticalOp) {usleep(1000);}
					FREE_LIST_ITEM(tmp);
					continue;
				}
			}
			current = current->next;
		}

	} //while(1)
	//close all connections
	connectionInfoItem_t* current = (connectionInfoItem_t*) tcpServer->allConnections;
	while(current) {
		close(current->info.sock);
		ListRemove(&tcpServer->allConnections,(listItem_t*)current);
		if(tcpServer->allConnections == 0) {
			while (current->markCriticalOp) {usleep(1000);}
			FREE_LIST_ITEM(current);
			break;
		}
		connectionInfoItem_t* tmp = current;
		current = current->next;
		while (tmp->markCriticalOp) {usleep(1000);}
		FREE_LIST_ITEM(tmp);
		continue;
	}
	fprintf(stderr,"exitting server thread!\n");
	close(tcpServer->listeningSock);
	tcpServer->isListening = 0;
	if(ret) {
		//call error handler
		if(tcpServer->onReceiveErrorHandler) {
			tcpServer->onReceiveErrorHandler(tcpServer->userObject,ret,NULL);
		}
	}

	return 0;
}

char* TCPGetLastErrorStr(TCPServer_t *tcpServer) {
	return tcpServer->errorMsg;
}


int TCPServerSendData(TCPServer_t *tcpServer, SOCKET sock,uint8_t * data, uint16_t len) {
	int sendlen;
	sendlen = send(sock,(const char*)data,len,MSG_DONTWAIT);

	if(tcpServer->onSentCallback) {
		tcpServer->onSentCallback(tcpServer->userObject,data,sendlen<0?0:sendlen,sendlen<0?-1:0);
	}
	return sendlen;
}

int TCPServerSendDataAll(TCPServer_t *tcpServer,uint8_t * data, uint16_t len) {
	int ret = 0,err = 0;

	connectionInfoItem_t* tmpItem;
	connectionInfoItem_t* pItem =(connectionInfoItem_t*) tcpServer->allConnections;
	tmpItem = pItem;
	while (pItem) {
		MarkCriticalOp(pItem);
		ret = write(pItem->info.sock,(const char*)data,len);
		tmpItem = pItem;
		pItem = pItem->next;
		ClearCriticalOp(tmpItem);
		if(ret != len) {
			err = -1;
		}
		if(tcpServer->onSentCallback) {
			if(err) {
				ret = 0;
			}
			tcpServer->onSentCallback(tcpServer->userObject,data,ret,err);
		}
	}
	return err;
}



uint8_t TCPServerOpen(TCPServer_t *tcpServer, uint16_t port, TCPServerOnDataCallback_t onDataCallback, TCPServerOnSentCallback_t onSentCallback, void *userObject, TCPServerOnReceiveErrorHandler_t onReceiveErrorHandler) {


	int err;

	if(tcpServer == NULL) {
		return 1;
	}

	if (!InitSockets()) {
		return 2;
	}

	tcpServer->onDataCallback = onDataCallback;
	tcpServer->onSentCallback = onSentCallback;
	tcpServer->onReceiveErrorHandler = onReceiveErrorHandler;
	tcpServer->userObject = userObject;
	tcpServer->errorMsg[0] = 0;
	tcpServer->isListening = 0;
	InitList(tcpServer->allConnections);


	if((tcpServer->listeningSock = MakeServerSocket(port,tcpServer->errorMsg))<0) {
		return 3;
	}

	if ((err = listen(tcpServer->listeningSock, 1)) < 0) {
		sprintf(tcpServer->errorMsg,"Error listen (%d)",err);
		return 4;
	}

	// Create thread
	if((err = createThread(TCPServerReadThread, tcpServer)) == 0){
		sprintf(tcpServer->errorMsg,"Error thread not created (%d)",err);
		return 5;
	}
	return 0;
}

uint8_t TCPServerReinit(TCPServer_t *tcpServer) {
	if(tcpServer == NULL) {
		return 100;
	}
	return TCPServerOpen(tcpServer,tcpServer->port,tcpServer->onDataCallback,tcpServer->onSentCallback,tcpServer->userObject,tcpServer->onReceiveErrorHandler);
}

uint8_t TCPServerClose(TCPServer_t *tcpServer) {
	//int err;

	if(tcpServer->listeningSock>0) {
		shutdown(tcpServer->listeningSock,SHUT_RDWR);
		//err = shutdown(tcpServer->listeningSock,SHUT_RDWR);
		//PRINTF("ret %d\n",err);
		close(tcpServer->listeningSock);
	}
	tcpServer->listeningSock = 0;
	return 0;
}


void TCPTestOnDataCallback(void *object, uint8_t *data, uint32_t len, TCPConnectionInfo_t* info ) {
	PRINTF("Data from: %s, %hu, %s\n", inet_ntoa(info->clientname.sin_addr), ntohs(info->clientname.sin_port),data);
}

void TCPTestTCPServerOnReceiveErrorHandler(void* object, uint16_t error, TCPConnectionInfo_t* info ) {
	PRINTF("Error: %d\n",error);
}

void TCPTestOnSentCallback (void * object, uint8_t* data, uint16_t transferredDataLen, int8_t err) {
	PRINTF("Data sended!\n");
}


void TestServer() {
	TCPServer_t tcpServer;
	int err;
	err = TCPServerOpen(&tcpServer,TCP_SERVER_TEST_PORT,TCPTestOnDataCallback,TCPTestOnSentCallback,NULL,TCPTestTCPServerOnReceiveErrorHandler);
	if(err) {
		PRINTF("TCP server not initialized: %s",tcpServer.errorMsg);
		return;
	}
	/* Create the socket and set it up to accept connections. */
	int i = 0;
	while(i++<60000) {
		err = TCPServerSendDataAll(&tcpServer,(uint8_t*)"BLA ",5);
		if(err<0) {
			PRINTF("problems!\n");
		}
		usleep(1000);
	}
	PRINTF("closing server!\n");
	TCPServerClose(&tcpServer);
/*
	while(1) {
		sleep(1);
	}
*/
}
