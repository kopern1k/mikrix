/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "storage.h"
#include "../../sys/system.h" //definition systemName
#include <stdio.h>
#include <string.h>
#ifdef WIN32
#include <windows.h>
#endif

uint8_t SaveConfig(DATA *data){
	//open config file for writing (how to separate file for more platform instances? 
	FILE *f;
	char filename[20];
	if(data==NULL){
		return 1;
	}
	if(data->data == NULL){
		return 2;
	}
	strncpy(filename,(char*)systemName,sizeof(systemName));
	strcat(filename,".cfg");
	f=fopen(filename,"w+");
	fwrite(data->data,1,data->len,f);
	fclose(f);
	return 0;
}

//input:	buffer and its size
//output:	buffer 
#define LOAD_CONFIG_FILENAME_SIZE sizeof(systemName) + 5
uint8_t LoadConfig(DATA *data){
	FILE *f;
	char filename[LOAD_CONFIG_FILENAME_SIZE];
	uint16_t lSize=0;
	filename[LOAD_CONFIG_FILENAME_SIZE-1] = 0;
	if(data==NULL){
		return 1;
	}

	strncpy(filename,(char*)systemName,sizeof(systemName));
	filename[sizeof(systemName)] = 0;
	strcat(filename,".cfg");
	f=fopen(filename,"r");
	if(f==NULL){
		return 1;
	}
	// obtain file size:
	fseek (f , 0 , SEEK_END);
	lSize =(uint16_t) ftell (f);
	rewind (f);
	data->len=fread(data->data,1,lSize,f);
	fclose(f);
	return 0;
}
