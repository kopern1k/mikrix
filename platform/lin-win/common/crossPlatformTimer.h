/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef CROSS_PLATFORM_TIMER_H
#define CROSS_PLATFORM_TIMER_H

#include "../../../common/inttypes.h"
#ifndef WIN32

bool QueryPerformanceFrequency(LARGE_INTEGER *frequency);
bool QueryPerformanceCounter(LARGE_INTEGER *performance_count);
#endif 

void InitCrossPlatformTimer();

//! Returns current time in milisecs or microsecs.
//! \param micro Returns micro seconds if set to true.
//double timer_get(bool inMicrosec = false);

//! Helper functions, wraps QueryPerformanceCounter fns.
//! \note This function can not be nested. You must call \a timer_start() and \a timer_stop() before calling another \a timer_start().
void StartTimer();
//! Helper functions, wraps QueryPerformanceCounter fns.
//! \note This function can not be nested. You must call \a timer_start() and \a timer_stop() before calling another \a timer_start().
//! \return time in miliseconds.
uint64_t StopTimer(bool inMicrosec);

uint64_t GetTimer(bool inMicrosec);

#endif // CROSS_PLATFORM_TIMER_H
