/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */


#include "socketMulticast.h"
#include "../../../common/common.h"

#include "threads.h"
//#include <conio.h>
#ifdef WIN32
#include <process.h>
#endif

int SockMulticastSendData(sockMulticast_t *socketMulticast,uint8_t * data, uint16_t len){
	int sendLen;
	sendLen = sendto(socketMulticast->sock,(const char*)data,len,0,(struct sockaddr*) &socketMulticast->senderAddr,sizeof(socketMulticast->senderAddr));
	if(socketMulticast->onSentCallback) {
		socketMulticast->onSentCallback(socketMulticast->userObject, data, sendLen < 0 ? 0 : sendLen, sendLen < 0 ? errno:0);
	}
	return sendLen;
}



THREAD_RETURN_TYPE MulticastReceiveDataHandler(void *arg)
{
	sockMulticast_t *socketMulticast;
	socketMulticast = (sockMulticast_t *)((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;

	SOCKET *s;
	s = &socketMulticast->sock;
	((defaultThreadArgs_t *)arg)->started = 1;

	struct sockaddr_in clAddr;
	socketMulticast->isConnected = 1;
	int size = sizeof(clAddr);
	int len;
	int tmpErrno = SOCK_MULTICAST_ERR_UNDEFINED;
	memset(&clAddr, 0, sizeof(clAddr));
	do{
	 len = recvfrom(*s,socketMulticast->buf,sizeof(socketMulticast->buf),0 /*MSG_WAITALL*/,(struct sockaddr*)&clAddr,(socklen_t *)&size);
	 tmpErrno = errno;
	 if(len>0){
		 socketMulticast->clAddr = clAddr;
		  if(socketMulticast->onDataCallback != NULL){
				socketMulticast->onDataCallback(socketMulticast->userObject, (uint8_t*)socketMulticast->buf, len);
		  }
	  } else {
		  PRINTF("closing receive thread errno =%d - %s", errno, strerror(errno));
		  if(len == 0 && tmpErrno == 0) {
			  tmpErrno = SOCK_MULTICAST_ERR_DISCONNECTED;
		  }
		  break;
	  }
	}while(1);

	socketMulticast->isConnected = 0;

	if(SockIsclosed(socketMulticast->sock)) {
		SocketClose(socketMulticast->sock);
	}
	if(socketMulticast->onReceiveErrorHandler != NULL) {
		socketMulticast->onReceiveErrorHandler(socketMulticast->userObject,tmpErrno);
	}

	return 0;
}


uint8_t SockMulticastOpen(sockMulticast_t *sockMulticast, char* hostName, uint16_t port, SockMulticastOnDataCallback_t onDataCallback, SockMulticastOnSentCallback_t onSentCallback, void *userObject, SockMulticastOnReceiveErrorHandler_t onReceiveErrorHandler){

	uint8_t ret = 0;

	if(sockMulticast == NULL) {
	 return 1;
	}
	pthread_mutex_lock(&sockMulticast->initializing);

	if(sockMulticast->isConnected) {
	 ret =  100;
	 goto SockMulticastOpenExit;
	}

	if (!InitSockets()) {
	 ret = 2;
	 goto SockMulticastOpenExit;
	}
	if(hostName == NULL) {
	 strncpy(sockMulticast->IPParams.hostName, DEFAULT_MULTICAST_GROUP, SOCK_MULTICAST_MAX_HOSTNAME_LEN);
	} else if(sockMulticast->IPParams.hostName != hostName) {
	 strncpy(sockMulticast->IPParams.hostName, hostName, SOCK_MULTICAST_MAX_HOSTNAME_LEN);
	}

	sockMulticast->IPParams.port = port;
	sockMulticast->onDataCallback = onDataCallback;
	sockMulticast->onSentCallback = onSentCallback;
	sockMulticast->onReceiveErrorHandler = onReceiveErrorHandler;
	sockMulticast->userObject = userObject;
	sockMulticast->errorMsg[0] = 0;
	sockMulticast->isConnected = 0;


	int err;

	sockMulticast->sock = socket (AF_INET, SOCK_DGRAM, 0);
	if (sockMulticast->sock < 0) {
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg),"Could not create socket!");
		ret = 3;
		goto SockMulticastOpenExit;
	}

	int yes = true;
	if(setsockopt(sockMulticast->sock, SOL_SOCKET, SO_REUSEADDR, (const char*) &yes, sizeof(int)) == -1){
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg),"setsockopt problem!\n");
		ret = 4;
	}
	/* set up receiver destination address */
	struct sockaddr_in receiverAddr;
	memset(&receiverAddr,0,sizeof(receiverAddr));
	receiverAddr.sin_family = AF_INET;
	receiverAddr.sin_addr.s_addr = INADDR_ANY;
	receiverAddr.sin_port = htons((uint16_t)sockMulticast->IPParams.port);

	// Going to bind the socket
	if (bind(sockMulticast->sock, (struct sockaddr*) &receiverAddr, sizeof(receiverAddr)) == INVALID_SOCKET){
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg),"bind() failed: %d., Only listening\n", SocketGetLastError());
		ret = 5;
		goto SockMulticastOpenCleanSock;
	}

	struct hostent *hostinfo;
	hostinfo = gethostbyname (sockMulticast->IPParams.hostName);
	if (hostinfo == NULL) {
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg), "Unknown host %s.\n", sockMulticast->IPParams.hostName);
		ret = 6;
		goto SockMulticastOpenCleanSock;
	}

	struct ip_mreq mreq;
	mreq.imr_multiaddr.s_addr =  *( in_addr_t *) hostinfo->h_addr;
	mreq.imr_interface.s_addr = INADDR_ANY; //inet_addr(localIp);
	if (setsockopt(sockMulticast->sock, IPPROTO_IP,IP_ADD_MEMBERSHIP, (const char*)&mreq,sizeof(mreq)) < 0) {
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg),"InitSockReceive: Error at socket(): %d. \n Maybe multicast loopback is not set: (route add -net 224.0.0.0 netmask 240.0.0.0 eth0)", SocketGetLastError());
		ret = 7;
		goto SockMulticastOpenCleanSock;
	}

	/* set up sender destination address */
	memset(&sockMulticast->senderAddr,0,sizeof(sockMulticast->senderAddr));
	sockMulticast->senderAddr.sin_family = AF_INET;
	sockMulticast->senderAddr.sin_addr.s_addr =  *( in_addr_t *) hostinfo->h_addr;
	sockMulticast->senderAddr.sin_port = htons((uint16_t)sockMulticast->IPParams.port);


	//Create read thread
	if((err = createThread(MulticastReceiveDataHandler, sockMulticast)) == 0){
		snprintf(sockMulticast->errorMsg,sizeof(sockMulticast->errorMsg),"Error thread not created (%d)",err);
		SocketClose(sockMulticast->sock);
		ret = 8;
		goto SockMulticastOpenCleanSock;
	}
	goto SockMulticastOpenExit;
SockMulticastOpenCleanSock:
	close(sockMulticast->sock);
SockMulticastOpenExit:
	pthread_mutex_unlock(&sockMulticast->initializing);
	return ret;
}

uint8_t SockMulticastInit(sockMulticast_t *sockMulticast) {
	if(sockMulticast == NULL) {
		return 1;
	}
	sockMulticast->isConnected = 0;
	pthread_mutex_init(&sockMulticast->initializing, NULL);
	return 0;
}


uint8_t SockMulticastReopen(sockMulticast_t *sockMulticast) {
	return SockMulticastOpen(sockMulticast,sockMulticast->IPParams.hostName,sockMulticast->IPParams.port,sockMulticast->onDataCallback,sockMulticast->onSentCallback,sockMulticast->userObject,sockMulticast->onReceiveErrorHandler);
}


void SockMulticastClose(sockMulticast_t *sockMulticast) {
	SocketClose(sockMulticast->sock);
	sockMulticast->isConnected = 0;
}


void TestOnDataCallback(void *object, uint8_t *data, uint32_t len) {
	if(len) {
		PRINTF("%s",data);
	}
}


void SockMulticastTest() {
	sockMulticast_t sockMulticast;
	if(SockMulticastOpen(&sockMulticast,NULL,DEFAULT_MULTICAST_LISTEN_PORT,TestOnDataCallback,NULL,NULL,NULL)) {
		printf("Could not init communication: %s\n",sockMulticast.errorMsg);
		return;
	}
	int err;
	int i = 0;
	while(i++<10000) {
		err = SockMulticastSendData(&sockMulticast,(uint8_t*)"BLA",4);
		if(err<0) {
			PRINTF("problem with sending data! Ending!\n");
			break;
		}
		usleep(1000);
	}
	PRINTF("closing!\n");
	SockMulticastClose(&sockMulticast);
}
