#ifndef __INTERRUPTABLE_SLEEP__H__
#define __INTERRUPTABLE_SLEEP__H__

#include <inttypes.h>

uint8_t InitSleepFd(int fds[2]);
void CloseSleepFd(int fds[2]);
uint8_t InterruptableSleepEx(int sleepPipeFd[2], int timeInUs, uint8_t closeFds, uint8_t *isSleeping);
void BreakSleepEx(int sleepPipeFd[2], uint8_t closeFds, uint8_t *isSleeping);
uint8_t SleepBySelect(int fd,int timeInUs, uint8_t *isSleeping);

#endif
