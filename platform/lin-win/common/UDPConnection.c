/*
 * Copyright (c) 2016, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */




#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#ifndef __USE_BSD
	#define __USE_BSD
#endif
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "threads.h"
#include "UDPConnection.h"
#define __USE_GNU
#include <netdb.h>


#include "../../common/common.h"
#include "../../sys/timeManager.h"

#include "sysConfig.h"
#ifdef UDP_CONNECTION_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

#undef TEST

#define UDP_CONNECTION_PORT            5556
#define UDP_CONNECTION_TEST_MESSAGE    "Yow!!! Are we having fun yet?!?"
#define UDP_CONNECTION_SERVERHOST      "127.0.0.1"


#define ERROR_MSG(...) snprintf(udpConn->errorMsg,UDP_CONN_ERROR_MSG_MAX_LEN,__VA_ARGS__)
     
int PrepareServerAddr(struct sockaddr_in *name, uint16_t port) {
	name->sin_family = AF_INET;
	name->sin_port = htons (port);
	name->sin_addr.s_addr = htons(INADDR_ANY);
	return 0;
}

int PrepareDestAddr(struct sockaddr_in *name,const char *hostname, uint16_t port) {
	struct hostent *hostinfo;

	name->sin_family = AF_INET;
	name->sin_port = htons (port);
	hostinfo = gethostbyname (hostname);
	if (hostinfo == NULL) {
		fprintf (stderr, "Unknown host %s.\n", hostname);
		return 1;
	}
	name->sin_addr = *(struct in_addr *) hostinfo->h_addr;
	return 0;
}




int UDPConnSendData(UDPConn_t *UDPConn,uint8_t* data, int len) {
	int sendLen;
	void* pData = data;
	if(len < UDP_CONN_MAX_OUT_BUF_SIZE - sizeof(UDPIntentityHdr_t)) {
		if(UDPConn->broadcast && UDPConn->skipOwnMessages) {
			len += sizeof(UDPIntentityHdr_t);
			memcpy(UDPConn->outBuf + sizeof(UDPIntentityHdr_t),data,len);
			((UDPIntentityHdr_t*)UDPConn->outBuf)->identifier = UDPConn->ownRandom;
			((UDPIntentityHdr_t*)UDPConn->outBuf)->size = len;
			pData = UDPConn->outBuf;
		}
		sendLen = sendto(UDPConn->sock, pData, len, MSG_DONTWAIT,(const struct sockaddr *)&UDPConn->dest,sizeof(UDPConn->dest));
	} else {
		sendLen = 0;
	}

	if(UDPConn->onSentCallback) {
		UDPConn->onSentCallback(UDPConn->userObject,data,sendLen<0?0:sendLen,sendLen<0?errno:0);
	}
	return sendLen;
}



THREAD_RETURN_TYPE UDPConnReadThread(void *arg)
{
	UDPConn_t *udpConn;
	udpConn = (UDPConn_t *)((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;
	int len;

	struct sockaddr addr;
#ifdef TEST
	struct sockaddr_in *pAddr;
	pAddr =  (struct sockaddr_in*)&addr;
	char ipstr[INET_ADDRSTRLEN + 1];
#endif
	socklen_t fromlen;

	udpConn->isOpened = 1;
	int tmpErrno = UDP_CONN_ERR_UNDEFINED;
	do {
	   len = recvfrom(udpConn->sock,udpConn->buf,sizeof(udpConn->buf),0,&addr, &fromlen);
	   tmpErrno = errno;
	   // accept only IPV4 packets
	   if(!(addr.sa_family == AF_INET || addr.sa_family == AF_UNSPEC)) {
		   continue;
	   }
	   //filter own messages
	   DEBUGPRINTF("[%llu] UDPConnReadThread received %d data, from addr=%s\n", (long long unsigned int) GetSysTickCount(), len, inet_ntop(AF_INET,pAddr,ipstr, sizeof(ipstr)));
		if(len>0){
			if(udpConn->skipOwnMessages) {
				if(len < sizeof(UDPIntentityHdr_t)) {
					if(udpConn->onReceiveErrorHandler != NULL) {
						udpConn->onReceiveErrorHandler(udpConn->userObject, UDP_CONN_ERR_BAD_HEADER);
					}
					continue;
				}
				// skip packets with same random identifier -> do not receive own messages
				if(((UDPIntentityHdr_t*)udpConn->buf)->identifier == udpConn->ownRandom) {
					continue;
				}
			}
			if(udpConn->onDataCallback != NULL) {
				  udpConn->onDataCallback(udpConn->userObject,(uint8_t*)udpConn->buf + sizeof(UDPIntentityHdr_t),len - sizeof(UDPIntentityHdr_t));
			}
		}else {
			PRINTF("closing receive thread. Received %d, errno =%d - %s\n",len,errno,strerror(errno));
			if(len == 0 && tmpErrno == 0) {
				tmpErrno = UDP_CONN_ERR_DISCONNECTED;
			}
			break;
		}
	} while(1);
	udpConn->isOpened = 0;
	if(SockIsclosed(udpConn->sock)) {
		SocketClose(udpConn->sock);
	}
	if(udpConn->onReceiveErrorHandler != NULL) {
		udpConn->onReceiveErrorHandler(udpConn->userObject, tmpErrno);
	}
	return NULL;
}

uint8_t UDPConnInit(UDPConn_t *udpConn) {
	if(udpConn == NULL) {
		return 1;
	}
	udpConn->isOpened = 0;
	pthread_mutex_init(&udpConn->initializing, NULL);

	return 0;
}

uint8_t UDPConnOpen(UDPConn_t *udpConn, char* hostName, uint16_t port, UDPConnOnDataCallback_t onDataCallback, UDPConnOnSentCallback_t onSentCallback, void *userObject, UDPConnOnReceiveErrorHandler_t onReceiveErrorHandler, uint8_t broadcast){
	struct sockaddr_in hostname;
	uint8_t ret = 0;

	if(udpConn == NULL) {
		return 1;
	}

	pthread_mutex_lock(&udpConn->initializing);

	if(udpConn->isOpened) {
		ret =  100;
		goto UDPConnOpenExit;
	}

	if (!InitSockets()) {
		ret = 2;
		goto UDPConnOpenExit;
	}
	if(hostName == NULL) {
		ret = 3;
		goto UDPConnOpenExit;
	}

	srand(time(NULL));

	if(udpConn->IPParams.hostName != hostName) {
		if(broadcast) {
			strncpy(udpConn->IPParams.hostName,"255.255.255.255", UDP_CONN_MAX_HOSTNAME_LEN);
		} else {
			strncpy(udpConn->IPParams.hostName, hostName,UDP_CONN_MAX_HOSTNAME_LEN);
		}
	}
	udpConn->IPParams.port = port;
	udpConn->onDataCallback = onDataCallback;
	udpConn->onSentCallback = onSentCallback;
	udpConn->onReceiveErrorHandler = onReceiveErrorHandler;
	udpConn->userObject = userObject;
	udpConn->errorMsg[0] = 0;
	udpConn->isOpened = 0;
	udpConn->broadcast = broadcast;
	udpConn->skipOwnMessages = 1;

	udpConn->ownRandom = rand();

	//udpConn->sock = 0;
	int err;

	udpConn->sock = socket (AF_INET, SOCK_DGRAM, 0);
	if (udpConn->sock < 0) {
		ERROR_MSG("Could not create socket!");
		ret = 4;
		goto UDPConnOpenExit;
	}

	err = PrepareServerAddr (&hostname, udpConn->IPParams.port);
	if(err) {
		ERROR_MSG("Could not resolve hostname %s",udpConn->IPParams.hostName);
		ret = 5;
		goto UDPConnOpenCleanSock;
	}

	err = PrepareDestAddr(&udpConn->dest, udpConn->IPParams.hostName,udpConn->IPParams.port);
	if(err) {
		ERROR_MSG("Could not resolve hostname %s",udpConn->IPParams.hostName);
		ret = 6;
		goto UDPConnOpenCleanSock;
	}

	int yes = true;
	if(setsockopt(udpConn->sock, SOL_SOCKET, SO_REUSEADDR, (const char*) &yes, sizeof(int)) == -1){
		ERROR_MSG("setsockopt problem!\n");
		ret = 7;
	}


	if ((err = bind (udpConn->sock, (struct sockaddr *) &hostname, sizeof (hostname))) < 0) {
		ERROR_MSG("bind error (%d), errno=%d:%s",err,errno,strerror(errno));
		ret = 8;
		goto UDPConnOpenCleanSock;
	}

	err = SetSockBroadcasting(udpConn->sock, udpConn->broadcast);
	if(err) {
		 ERROR_MSG("Could not set broadcast. err: %d, errno=%d\n",err,errno);
		 ret = 9;
		 goto UDPConnOpenCleanSock;
	 }
/*
	if(SockConnect(udpConn->sock,(struct sockaddr_in*) &hostname,sizeof(hostname), 2000)) {
		sprintf(udpConn->errorMsg,"Could not connect to host %s, port %d",udpConn->IPParams.hostName, udpConn->IPParams.port);
		ret = 9;
		goto UDPConnOpenCleanSock;
	}
	*/

	//Create read thread
	if((err = createThread(UDPConnReadThread, udpConn)) == 0){
		ERROR_MSG("Error thread not created (%d)",err);
		SocketClose(udpConn->sock);
		ret = 10;
		goto UDPConnOpenExit;
	}
	goto UDPConnOpenExit;
UDPConnOpenCleanSock:
	close(udpConn->sock);
UDPConnOpenExit:
	pthread_mutex_unlock(&udpConn->initializing);
	return ret;
}

uint8_t UDPConnReopen(UDPConn_t *udpConn) {
	return UDPConnOpen(udpConn,udpConn->IPParams.hostName,udpConn->IPParams.port,udpConn->onDataCallback,udpConn->onSentCallback,udpConn->userObject,udpConn->onReceiveErrorHandler,udpConn->broadcast);
}

void UDPConnClose(UDPConn_t *udpConn) {
	SocketClose(udpConn->sock);
	udpConn->isOpened = 0;
}

void TestUDPOnDataCallback(void *object, uint8_t *data, uint32_t len) {
	if(len) {
		PRINTF("%s",data);
	}
}


void UDPConnTest() {
	UDPConn_t udpConn;
	if(UDPConnOpen(&udpConn,UDP_CONNECTION_SERVERHOST,UDP_CONNECTION_PORT,TestUDPOnDataCallback,NULL,NULL,NULL,0)) {
		printf("Could not init communication\n");
		return;
	}
	int err;
	int i = 0;
	while(i++<10000) {
		err = UDPConnSendData(&udpConn,(uint8_t*)"BLA",4);
		if(err<0) {
			PRINTF("problem with sending data! Ending!\n");
			break;
		}
		usleep(1000);
	}
	PRINTF("closing Client!\n");
	UDPConnClose(&udpConn);
}
