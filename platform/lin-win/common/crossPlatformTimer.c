/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "crossPlatformTimer.h"
#ifdef WIN32
#include "windows.h"
#include "winbase.h"
#else
#include "sys/time.h"
#endif

static long long start, end, freq;

#ifndef WIN32
/* Helpful conversion constants. */
static const uint32_t usec_per_sec = 1000000;
static const uint32_t usec_per_msec = 1000;

/* These functions are written to match the win32
   signatures and behavior as closely as possible.
*/
bool QueryPerformanceFrequency(LARGE_INTEGER *frequency)
{
	/* Sanity check. */
	if(frequency == NULL) {
		return false;
	}
	/* gettimeofday reports to microsecond accuracy. */
	(*frequency).QuadPart = usec_per_sec;

	return true;
}

bool QueryPerformanceCounter(LARGE_INTEGER *performance_count)
{
	struct timeval time;

	/* Sanity check. */
	if(performance_count == NULL) {
		return false;
	}
	/* clock_gettime can be also used */
	/* Grab the current time. */
	gettimeofday(&time, NULL);
	(*performance_count).QuadPart = time.tv_usec + /* Microseconds. */
						 ((uint64_t)time.tv_sec) * usec_per_sec; /* Seconds. */

	return true;
}

#endif

#define MICRO_CONST 1000000
#define MILI_CONST	1000

#define TYPECONV LARGE_INTEGER*

void InitCrossPlatformTimer(){
	QueryPerformanceFrequency((TYPECONV)&freq);
}

uint64_t GetGlobalTimer(bool micro)
{
	uint64_t t;
	
	QueryPerformanceCounter((TYPECONV)&t);
	return (uint64_t)((double)t / (double)freq) * (micro ? MICRO_CONST : MILI_CONST);
}

uint64_t GetTimer(bool inMicrosec)
{
	uint64_t t;
	
	QueryPerformanceCounter((TYPECONV)&t);
	return (uint64_t)((double)(t-start) / (double)freq * (inMicrosec ? MICRO_CONST : MILI_CONST));
}



void StartTimer(){
	QueryPerformanceCounter((TYPECONV)&start);
}

uint64_t StopTimer(bool inMicrosec){
	QueryPerformanceCounter((TYPECONV)&end);
	double t = ((double)(end-start))/ (double)freq * (inMicrosec ? MICRO_CONST : MILI_CONST);
	return (uint64_t)t;
}
