/*
 * Copyright (c) 2015, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include <stdio.h>
#include <sys/select.h>
#include <fcntl.h> 
#include <stdlib.h>
#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <unistd.h>

#include "interruptableSleep.h"
#include "sysConfig.h"
#include "../../common/common.h"

#ifdef INTERRUPTABLE_SLEEP_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

uint8_t SleepBySelect(int fd,int timeInUs, uint8_t *isSleeping) {
	int ret;
	struct timeval tv;
	fd_set active_fd_set;
	fd_set* curr_fd_set = NULL;
	int nfds=0;
	if(fd > 1) {
		FD_ZERO (&active_fd_set);
		FD_SET (fd, &active_fd_set);
		curr_fd_set = &active_fd_set;
		nfds = fd + 1;
	}
	
	tv.tv_sec = timeInUs / 1000000;
	tv.tv_usec = timeInUs % 1000000;
	*isSleeping = 1;
	if((ret = select(nfds, curr_fd_set, NULL, NULL,timeInUs ? &tv : NULL))>0) {
		*isSleeping = 0;
		return 0;
	}
	*isSleeping = 0;
	// err
	return 1;
}

uint8_t InitSleepFd(int fds[2]) {
	fds[0] = 0; fds[1] = 0;
	if (pipe2(fds, O_NONBLOCK) == -1) {
		perror("pipe");
		return 1;
	}
	return 0;
}

void CloseSleepFd(int fds[2]) {
	if(fds[0]) {
		close(fds[0]);
		fds[0] = 0;
	}
	if(fds[1]) {
		close(fds[1]);
		fds[1] = 0;
	}
}

uint8_t InterruptableSleepEx(int sleepPipeFd[2], int timeInUs, uint8_t closeFds, uint8_t *isSleeping) {
	uint8_t c;
	if(sleepPipeFd[0] == 0 && sleepPipeFd[1] == 0) {
		InitSleepFd(sleepPipeFd);
	}

	if(SleepBySelect(sleepPipeFd[0],timeInUs, isSleeping) == 0) {
		if(sleepPipeFd[0]!= 0) {
			if(read(sleepPipeFd[0],&c,1) != 1) {
				return 1;
			}
			while (read(sleepPipeFd[0], &c, 1) > 0);
		}
		DEBUGPRINTF("InterruptableSleepEx woked up (%d)!\n",timeInUs);
	}
	
	if(closeFds != 0) {
		CloseSleepFd(sleepPipeFd);
	}
	return 0;
}

void BreakSleepEx(int sleepPipeFd[2], uint8_t closeFds, uint8_t *isSleeping) {
	char c = 0;
	if(sleepPipeFd[0] == 0) {
		return;
	}
	if(*isSleeping == 0) {
		return;
	}
	DEBUGPRINTF("BreakSleepEx\n");
	write(sleepPipeFd[1],&c,1);
	if(closeFds) {
		CloseSleepFd(sleepPipeFd);
	}
}
