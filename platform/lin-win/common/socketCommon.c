/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#ifndef __USE_UNIX98
#define __USE_UNIX98
#endif
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#endif
#include "socketCommon.h"
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <ifaddrs.h>

#include "sysConfig.h"
#ifdef SOCKET_COMMON_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

void PrintMyIp()
{
    char hn[81];
    struct hostent *th;
	struct in_addr **addr_list;
	int i;
	gethostname(hn, sizeof(hn) - 1);
    th = gethostbyname(hn);

    //printf("Official name is: %s\n", th->h_name);
    printf("    IP addresses: ");
    addr_list = (struct in_addr **)th->h_addr_list;
    for(i = 0; addr_list[i] != NULL; i++) {
        printf("%s ", inet_ntoa(*addr_list[i]));
    }
    printf("\n");
}



void PrintMyIpEx()
{
	struct ifreq ifr;
	struct ifconf ifc;
	char buf[1024];

	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sock == -1) { /* handle error*/ };

	ifc.ifc_len = sizeof(buf);
	ifc.ifc_buf = buf;
	if (ioctl(sock, SIOCGIFCONF, &ifc) == -1) { /* handle error */ }

	struct ifreq* it = ifc.ifc_req;
	const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

	for (; it != end; ++it) {
		strcpy(ifr.ifr_name, it->ifr_name);
		if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) {
			if (! (ifr.ifr_flags & IFF_LOOPBACK)) { // don't count loopback

				if(ioctl(sock, SIOCGIFADDR, &ifr) == 0) {
					printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
				}
				if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {
					printf("\tInterface : <%s>\n",it->ifr_name );
					uint8_t* pData =(uint8_t*) ifr.ifr_hwaddr.sa_data;

					printf("%.2x.%.2x.%.2x.%.2x.%.2x.%.2x\n",pData[0],pData[1],pData[2],pData[3],pData[4],pData[5]);
				}
			}
		}
		else { /* handle error */ }
	}

	close(sock);
}

int8_t GetNetAdaptersInfo(NetAdaptersInfo_t *pAdapterInfo, uint8_t maxAdapterCnt) {
	struct ifreq ifr;
	struct ifconf ifc;
	char buf[1024];
	int8_t ret  = 0;

	if(pAdapterInfo == NULL) {
		return -1;
	}

	pAdapterInfo->adapterCnt = 0;

	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sock == -1) {
		return -2;
	};

	ifc.ifc_len = sizeof(buf);
	ifc.ifc_buf = buf;
	if (ioctl(sock, SIOCGIFCONF, &ifc) == -1) {
		return -3;
	}

	struct ifreq* it = ifc.ifc_req;
	const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

	for (; it != end; ++it) {
		strcpy(ifr.ifr_name, it->ifr_name);
		if(pAdapterInfo->adapterCnt + 1 > maxAdapterCnt) {
			ret = 1;
			break;
		}
		if (ioctl(sock, SIOCGIFFLAGS, &ifr) != 0) {
			ret = 2;
			continue;
		}
		if(ifr.ifr_flags & IFF_LOOPBACK) { // don't count loopback
			continue;
		}
		if (ioctl(sock, SIOCGIFHWADDR, &ifr) != 0) {
			ret = 3;
			continue;
		}


		strncpy(pAdapterInfo->netAdapterInfo[pAdapterInfo->adapterCnt].name,it->ifr_name,sizeof(pAdapterInfo->netAdapterInfo[pAdapterInfo->adapterCnt].name));
		printf("\tInterface : <%s>\n",it->ifr_name );

		uint8_t* pData =(uint8_t*) ifr.ifr_hwaddr.sa_data;
		memcpy(pAdapterInfo->netAdapterInfo[pAdapterInfo->adapterCnt].mac,pData, sizeof(MACAddress_t));
		printf("%.2x.%.2x.%.2x.%.2x.%.2x.%.2x\n",pData[0],pData[1],pData[2],pData[3],pData[4],pData[5]);

		if (ioctl(sock, SIOCGIFADDR, &ifr) != 0) {
			ret = 4;
			continue;
		}
		pAdapterInfo->netAdapterInfo[pAdapterInfo->adapterCnt].ip = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;
		printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

		pAdapterInfo->adapterCnt++;
		//if(ioctl(sock, SIOCGIFADDR, &ifr) == 0) {
		//	printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
		//}
	}

	close(sock);
	return ret;
}

void GetMyIpStr(char* ip, char len){
	uint8_t i;
	char hn[81];
    struct hostent *th;
	struct in_addr **addr_list;

	if( len<16) return;
	ip[0]=0;
    gethostname(hn, 80);
    th = gethostbyname(hn);
	addr_list = (struct in_addr **)th->h_addr_list;
    for(i = 0; addr_list[i] != NULL; i++) {
        strcat(ip, inet_ntoa(*addr_list[i]));
    }
}


uint8_t isValidIp4Address(char *ipAddress) {
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
	return result != 0;
}


// good value maxKeepAlivePacketCnt = 2, waitForSendKeepAlivePacketInSec = 2, timeIntervalBetweenPacketsInSec = 2
uint8_t SetSockKeepAliveEx(int sock, int maxKeepAlivePacketCnt, int waitForSendKeepAlivePacketInSec, int timeIntervalBetweenPacketsInSec) {
	int optval;
	socklen_t optlen;

	// enable keepAlive
	optval = 1;
	optlen = sizeof(optval);
	if(setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
		return 1;
	}


	//local ? 3 : 5;
	if(setsockopt(sock, SOL_TCP, TCP_KEEPCNT, &maxKeepAlivePacketCnt, sizeof(maxKeepAlivePacketCnt)) < 0) {
		return 2;
	}

	//local ? 2 : 5;
	if(setsockopt(sock, SOL_TCP, TCP_KEEPIDLE, &waitForSendKeepAlivePacketInSec, sizeof(waitForSendKeepAlivePacketInSec)) < 0) {
		return 3;
	}

	//2;
	if(setsockopt(sock, SOL_TCP, TCP_KEEPINTVL, &timeIntervalBetweenPacketsInSec, sizeof(timeIntervalBetweenPacketsInSec)) < 0) {
		return 4;
	}

	return 0;
}

// valid values are in the range [1,7]
// 1- low priority, 7 - high priority
uint8_t SetSockPriority(int sock, uint8_t priority) {
	int optval=priority; // valid values are in the range [1,7]
	if(setsockopt(sock, SOL_SOCKET, SO_PRIORITY, &optval, sizeof(optval))<0) {
		return  1;
	};
	return  0;
}


uint8_t SetSockBroadcasting(int sock, int broadcast) {
	if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) < 0) {
		return 1;
	}
	return 0;
}


uint8_t SockConnect(int sock, struct sockaddr_in *address, socklen_t addrlen,  uint32_t maxWaitTimeoutMs) {
	fd_set fdset;
	struct timeval tv;
	int ret = 0;

	//get socket options
	int tmpSockOp = fcntl(sock, F_GETFL);
	ret = fcntl(sock, F_SETFL, O_NONBLOCK);

	ret = connect(sock, (struct sockaddr *)address, addrlen); // connect should not be blocking, we will wait in select
	if(ret < 0) {
		//PRINTF("connect error: %d - %s\n", errno,strerror(errno));
	}
	FD_ZERO(&fdset);
	FD_SET(sock, &fdset);
	tv.tv_sec = maxWaitTimeoutMs / 1000;
	tv.tv_usec = (maxWaitTimeoutMs % 1000) * 1000;

	ret = select(sock + 1, NULL, &fdset, NULL, &tv);
	DEBUGPRINTF("select returned %d\n",ret);
	if(ret == 1) {
		int so_error;
		socklen_t len = sizeof so_error;
		ret = SOCKET_CONNECTED;
		getsockopt(sock, SOL_SOCKET, SO_ERROR, &so_error, &len);

		if (so_error != 0) {
			PRINTF("getsockopt find connect error: %d - %s\n",so_error,strerror(so_error));
			ret = so_error;
		}
	} else if(ret == 0){
		ret = SOCKET_TIMEOUTED; // TIMEOUTED
	} else {
		PRINTF("select error = %d, %s\n",errno,strerror(errno));
		ret = errno;
	}
	//set socket options
	fcntl(sock,F_SETFL, tmpSockOp);
	return ret;
}

#ifdef WIN32
#else
uint8_t SockIsclosed(SOCKET sock) {
	fd_set rfd;
	if(sock<0){
		return 1;
	}
	FD_ZERO(&rfd);
	FD_SET(sock, &rfd);
	struct timeval tv = { 0 };
	select(sock+1, &rfd, 0, 0, &tv);
	if (!FD_ISSET(sock, &rfd))
		return 0;
	int n = 0;
	ioctl(sock, FIONREAD, &n);
	return n == 0;
}
#endif

int InitSockets()
{
#ifdef WIN32
    WSADATA wsaData;
    //initialize windows sockets
    int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
    if ( iResult != NO_ERROR )
    {
      printf("Error initializing windows sockets\n");
      return 0;
    }
#endif
    return 1;
}

void CleanupSockets()
{
#ifdef WIN32
    WSACleanup();
#endif
}

int MakeSocketNonblocking(SOCKET s)
{
#ifdef WIN32
  //make the socket nonblocking
  u_long ul_arg = 1;
  if (SOCKET_ERROR == ioctlsocket(s, FIONBIO, &ul_arg))
  {
    printf("Error making socket non-blocking, error=%d\n", WSAGetLastError());
	return 0;
  }
  return 1;
#else
  //make the socket non-blocking
  int flags = fcntl(s, F_GETFL);
  if (flags == -1)
  {
	printf("Error getting flags for socket, errno=%d\n", SocketGetLastError());
    return 0;
  }
  if (-1 == fcntl(s, F_SETFL, flags | O_NONBLOCK))
  {
	printf("Error setting flags for socket, errno=%d\n", SocketGetLastError());
    return 0;
  }
  return 1;
#endif
}

void SocketClose(SOCKET s)
{
#ifdef WIN32
  closesocket(s);
#else
  shutdown(s, SHUT_RDWR);
  close(s);
#endif
}

