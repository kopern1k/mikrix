/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TCP_CLIENT_H__
#define __TCP_CLIENT_H__

#include <sys/types.h>
#include <sys/select.h>
#include <pthread.h>
#include "socketCommon.h"
#include "inttypes.h"

typedef void (*TCPClientOnDataCallback_t)(void *userObject, uint8_t *data, uint32_t len);
typedef void (*TCPClientOnSentCallback) (void * userObject, uint8_t* data, uint16_t transferredDataLen, int8_t err);
typedef void (*TCPClientOnReceiveErrorHandler_t)(void* userObject, uint16_t error);

#define TCP_CLIENT_MAX_HOSTNAME_LEN 100

#define TCP_CLIENT_DEFAULT_KEEPALIVE_PACKET_CNT 3
#define TCP_CLIENT_MIN_KEEPALIVE_TIME 2

#define TCP_CLIENT_ERROR_MSG_MAX_LEN 100
#define TCP_CLIENT_MAX_BUF_SIZE 255

#define TCP_CLIENT_ERR_UNDEFINED 200
#define TCP_CLIENT_ERR_DISCONNECTED 201

typedef struct {
	uint16_t port;
	char hostName[TCP_CLIENT_MAX_HOSTNAME_LEN];
} TCPClientIPParams_t;

typedef struct {
	SOCKET sock;
	TCPClientIPParams_t IPParams;
	char errorMsg[TCP_CLIENT_ERROR_MSG_MAX_LEN];
	uint8_t buf[TCP_CLIENT_MAX_BUF_SIZE];
	TCPClientOnDataCallback_t onDataCallback;
	TCPClientOnSentCallback onSentCallback;
	TCPClientOnReceiveErrorHandler_t onReceiveErrorHandler;
	void *userObject;
	uint8_t isConnected;
	pthread_mutex_t initializing;
	uint32_t keepAliveTime; //sec

} TCPClient_t;


int TCPClientSendData(TCPClient_t *tcpClient,uint8_t* data, int len);
uint8_t TCPClientInit(TCPClient_t *tcpClient);
uint8_t TCPClientOpen(TCPClient_t *tcpClient, char* hostName, uint16_t port, TCPClientOnDataCallback_t onDataCallback, TCPClientOnSentCallback onSentCallback, void *userObject, TCPClientOnReceiveErrorHandler_t onReceiveErrorHandler,uint32_t keepAliveExpireTimeSec);
void TCPClientClose(TCPClient_t *tcpClient);
uint8_t TCPClientReopen(TCPClient_t *tcpClient);
//uint8_t TCPClientGetConnectionInfo(TCPClient_t *tcpClient, TCPClientIPParams_t* tcpConnectionInfo);
//uint8_t TCPClientUpdateConnectionInfo(TCPClient_t *tcpClient, TCPClientIPParams_t* tcpConnectionInfo);
#endif // CLIENT_H
