/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef SOCKET_COMMON_H_
#define SOCKET_COMMON_H_
#include <stdio.h>
#ifdef WIN32
#include <windows.h>
#else
#include <stdlib.h>
#ifndef __USE_MISC
#define __USE_MISC
#endif
#ifndef __USE_XOPEN_EXTENDED
#define __USE_XOPEN_EXTENDED
#endif
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include "../common/common.h"
#endif


#ifdef WIN32
#define socklen_t int
#else
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define SOCKET int
#ifdef WIN32
  #define SocketGetLastError()  WSAGetLastError()
#else
  #define SocketGetLastError()  errno
#endif
#define WSAEINTR EBADF
#define FAR
#endif

typedef struct {
	char name[6];
	uint32_t ip;
	MACAddress_t mac;
} NetAdapterInfo_t;

typedef struct {
	uint8_t adapterCnt;
	NetAdapterInfo_t netAdapterInfo[];
} NetAdaptersInfo_t;


uint8_t SockIsclosed(SOCKET sock);
void PrintMyIp();
void PrintMyIpEx();
void GetMyIpStr(char* ip, char len);
uint8_t isValidIp4Address(char *ipAddress);
int8_t GetNetAdaptersInfo(NetAdaptersInfo_t *pAdapterInfo, uint8_t maxAdapterCnt);

int InitSockets();
#define ERRNO_BOUNDARY 200
#define SOCKET_CONNECTED 0
#define SOCKET_TIMEOUTED ERRNO_BOUNDARY + 2

uint8_t SockConnect(int sock, struct sockaddr_in *address, socklen_t addrlen, uint32_t maxWaitTimeoutMs);
void CleanupSockets();
int MakeSocketNonblocking(SOCKET s);
uint8_t SetSockKeepAliveEx(int sock, int maxKeepAlivePacketCnt, int waitForSendKeepAlivePacketInSec, int timeIntervalBetweenPacketsInSec);

// valid values are in the range [1,7]
// 1- low priority, 7 - high priority
uint8_t SetSockPriority(int sock, uint8_t priority);

INLINE uint8_t SetSockKeepAlive(int sock) {
	return SetSockKeepAliveEx(sock,2,2,2);
}

uint8_t SetSockBroadcasting(int sock, int broadcast);

void SocketClose(SOCKET s);

#endif
