/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#define  _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "../../../sys/flatStorage.h"
#include "flatFileStorage.h"
#include "../../../sys/crc16.h"
#include "sysConfig.h"
#include "threads.h"
#include "../../../common/common.h"


//#define FFS_ASYNC_WRITE 	//configre in sysConfig.h
//#define FFS_SHOW_DEBUG_INFO
#ifdef FFS_SHOW_DEBUG_INFO
#define FFS_ERR_PRINTF(...) fprintf(stderr,__VA_ARGS__);fflush(stderr)
#else
#define FFS_ERR_PRINTF(...)
#endif



uint8_t FFStorageInit(FS_t *fs, uint16_t size, void *config) {
	fs->storageData = config;
	return 0;
}

uint8_t WriteSyncData(FSF_t *f,void *data, uint16_t size) {
	int r;
	f->op = FSF_WRITTING;
	if(f->userData == NULL) {
		return 1;
	}
	if(((flatFileUserData_t*)f->userData)->f == NULL) {
		return 2;
	}
	if(data == NULL) {
		return 3;
	}
	//freopen (NULL,"wb+",((flatFileUserData_t*)f->userData)->f);
	fseek(((flatFileUserData_t*)f->userData)->f,0,SEEK_SET);
	r = fwrite(data,sizeof(char),size,((flatFileUserData_t*)f->userData)->f);
	if(r != size) {
		fprintf(stderr,"Error: write less data r: %d !=  %hus\n", r,size);
		if (errno) {
			fprintf(stderr,"%s\n", strerror(errno));
		}
		f->op = FSF_WAIT_FOR_OPERATION;
		return 4;
	}
	//compute crc
	uint16_t crc = 0xFFFF;
	uint16_t i;
	for(i=0;i<size;i++) {
		crc = crc_ccitt_update(crc,((uint8_t*)data)[i]);
	}

	//write data crc
	r = fwrite(&crc,sizeof(char),sizeof(uint16_t),((flatFileUserData_t*)f->userData)->f);
	if(r != sizeof(uint16_t)) {
		fprintf(stderr,"Error - CRC wite: write less data r: %d !=  %d\n", r,(int)sizeof(uint16_t));
		if (errno) {
			fprintf(stderr,"%s\n", strerror(errno));
		}
		f->op = FSF_WAIT_FOR_OPERATION;
		return 5;
	}

	return 0;
}

THREAD_RETURN_TYPE WriteThr(void *arg) {
	uint8_t data[MAX_FSF_WRITE_THREAD_BUF_SIZE];
	uint16_t size;
	int r;

	FSF_t *f = ((FSF_t *)((defaultThreadArgs_t *)arg)->args);
	((defaultThreadArgs_t *)arg)->started = 1;

	((flatFileUserData_t*)f->userData)->isWriteThrRunning = true;

	while (true) {
		//block read from pipe
		r = read(((flatFileUserData_t*)f->userData)->pipeFd[0],&size,2);
		if(r<0) {
			FFS_ERR_PRINTF("FFSWrite: Pipe error! %d\n",r);
			break;
		}
		if(r!=2) {
			FFS_ERR_PRINTF("FFSWrite: Pipe error! Read not same data then sended! %d != %d\n",size,r);
			break;
		}

		r = read(((flatFileUserData_t*)f->userData)->pipeFd[0], data, size);

		if(r<0) {
			FFS_ERR_PRINTF("FFSWrite: Pipe error1! %d\n",r);
			break;
		}
		if(r!=size) {
			FFS_ERR_PRINTF("FFSWrite: Pipe error! Read not same data then sended! %d != %d\n",size,r);
		}
		//write data to pipe
		FFS_ERR_PRINTF("FFSWrite: goig to write data [%d]\n",size);
		WriteSyncData(f,data,size);

		//sync data to disc
		f->op = FSF_SYNCING;
		FFS_ERR_PRINTF("FFSWrite: syncing\t %d,\tf:%p\top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f,f->op);

		fdatasync(fileno(((flatFileUserData_t*)f->userData)->f));

		//freopen (NULL,"rb+",((flatFileUserData_t*)f->userData)->f);

		FFS_ERR_PRINTF("FFSWrite: exit\t %d,\tf:%p\top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f,f->op);

		f->op = FSF_WAIT_FOR_OPERATION;
	}

	close(((flatFileUserData_t*)f->userData)->pipeFd[0]);
	((flatFileUserData_t*)f->userData)->isWriteThrRunning = false;


	return 0;
}



uint8_t FFSOpen(FS_t *fs, FSF_t *f, char *fileName) {
	char tmpName[MAX_FLAT_FILE_STORAGE_FILE_PATH];
	flatFileUserData_t* FFUserData = NULL;
	if(f->userData != 0) {
		fprintf(stderr, "FFSOpen: File (! %d,f:%p op:%d) is already open! exitting\n",fileno(f->userData),f->userData,f->op);
		return 1;
	}

	FFUserData = malloc(sizeof(flatFileUserData_t));
	if(FFUserData == NULL) {
		return 2;
	}
	FFUserData->isWriteThrRunning = false;

	f->userData = (void*)FFUserData;

	tmpName[0] = 0;
	if(fs->storageData) {
		strcpy(tmpName,((flatFileStorageConfig_t*)fs->storageData)->path);
		strcat(tmpName,((flatFileStorageConfig_t*)fs->storageData)->name);
	}
	strcat(tmpName,fileName);
	if((((flatFileUserData_t*)f->userData)->f= (void*)fopen(tmpName,"rb+")) == NULL) {
		// if file not exist, create it
		if((((flatFileUserData_t*)f->userData)->f= (void*)fopen(tmpName,"wb+"))) {
			((flatFileUserData_t*)f->userData)->f = freopen (NULL,"rb+",((flatFileUserData_t*)f->userData)->f);
		}
	}
	f->io = fs->io;
	if(((flatFileUserData_t*)f->userData)->f == NULL) {
		free((flatFileUserData_t*)f->userData);
		f->userData = NULL;
		return 3;
	}
	FFS_ERR_PRINTF("FFSOpen exitting\t %d,\tf:%p\top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),f->userData,f->op);

#ifdef FFS_ASYNC_WRITE
	//create pipe
	if(pipe(((flatFileUserData_t*)f->userData)->pipeFd) < 0) {
		free((flatFileUserData_t*)f->userData);
		f->userData = NULL;
		return 3;
	}
#if defined (FFS_ASYNC_WRITE) && defined(FFS_ASYNC_PIPE_SIZE)
	int ret = 0;
	int fd1;
	fd1 = ((flatFileUserData_t*)f->userData)->pipeFd[1];
	ret = fcntl(fd1,F_SETPIPE_SZ, (FFS_ASYNC_PIPE_SIZE));
	if(ret < 0) {
		FFS_ERR_PRINTF("FFSOpen. Could not change pipe fd[1]=%d size. err=%s.\n",fd1, strerror(errno));
	}
	FFS_ERR_PRINTF("FFSOpen.pipe size fd[1] = %d\n",fcntl(fd1, F_GETPIPE_SZ));
#endif

	//create async writting thread if FFS_ASYNC_WRITE is defined
	createThread(WriteThr,f);
#endif


	f->op = FSF_WAIT_FOR_OPERATION;
	return 0;
}

uint8_t FFSRead(FSF_t *f, void *data, uint16_t size) {
	int r;
	if(f->userData == NULL) {
		return 1;
	}
	if(((flatFileUserData_t*)f->userData)->f == 0) {
		return 2;
	}
	if(data == NULL) {
		return 3;
	}
	FFS_ERR_PRINTF("FFSRead syncing\t %d,\tf:%p\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f);

	// wait while write or read operation is in progress
	while(f->op != FSF_WAIT_FOR_OPERATION);
	f->op = FSF_READING;
	fseek(((flatFileUserData_t*)f->userData)->f,0,SEEK_SET);
	r = fread(data,sizeof(char),size,((flatFileUserData_t*)f->userData)->f);
	if(r != size) {
		fprintf(stderr,"Error: read less data r: %d !=  %hu\n", r,size);
		if (errno) {
			fprintf(stderr,"%s\n", strerror(errno));
		}
		f->op = FSF_WAIT_FOR_OPERATION;
		return 4;
	}

	//compute crc
	uint16_t crc = 0xFFFF;
	uint16_t i;
	for(i=0;i<size;i++) {
		crc = crc_ccitt_update(crc,((uint8_t*)data)[i]);
	}

	//read crc
	uint16_t readCRC;
	r = fread(&readCRC,sizeof(char),sizeof(uint16_t),((flatFileUserData_t*)f->userData)->f);
	if(r != sizeof(uint16_t)) {
		fprintf(stderr,"Error - CRC read: read less data r: %d !=  %d\n", r,(int)sizeof(uint16_t));
		if (errno) {
			fprintf(stderr,"%s\n", strerror(errno));
		}
		f->op = FSF_WAIT_FOR_OPERATION;
		return 5;
	}

	// compare crc
	if(readCRC != crc) {
		f->op = FSF_WAIT_FOR_OPERATION;
		return 6;
	}

	f->op = FSF_WAIT_FOR_OPERATION;
	return 0;

}



THREAD_RETURN_TYPE SyncData(void *arg) {

	FSF_t *f = ((FSF_t *)((defaultThreadArgs_t *)arg)->args);
	((defaultThreadArgs_t *)arg)->started = 1;

	f->op = FSF_SYNCING;

	fdatasync(fileno(((flatFileUserData_t*)f->userData)->f));

	FFS_ERR_PRINTF("FFSWrite: syncing\t %d,\tf:%p\top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f,f->op);

	//freopen (NULL,"rb+",((flatFileUserData_t*)f->userData)->f);

	FFS_ERR_PRINTF("FFSWrite: exit\t %d,\tf:%p\top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f,f->op);

	f->op = FSF_WAIT_FOR_OPERATION;
	return 0;
}


uint8_t FFSWrite(FSF_t *f, void *data, uint16_t size) {
	if(f->userData == NULL) {
		return 1;
	}
	if(((flatFileUserData_t*)f->userData)->f == 0) {
		return 2;
	}
	if(data == NULL) {
		return 3;
	}
	if(size == 0) {
		return 4;
	}

	FFS_ERR_PRINTF("FFSWrite: enterred\t %d,\tf:%p, \top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f), ((flatFileUserData_t*)f->userData)->f, f->op);

	// wait while write or read operation is in progress
	while(f->op == FSF_READING || f->op == FSF_WRITTING);

	//FFS_ERR_PRINTF("FFSWrite: going to reopen\t %d,\tf:%p, \top:%d\n",fileno(f->userData),f->userData,f->op);

#ifdef FFS_ASYNC_WRITE
	//write data to pipe & save them in writting thread
	write(((flatFileUserData_t*)f->userData)->pipeFd[1], &size, sizeof(size));
	write(((flatFileUserData_t*)f->userData)->pipeFd[1], data, size);
#else
	//write data to file
	WriteSyncData(f,data,size);

	FFS_ERR_PRINTF("FFSWrite: going to create sync thread!\t %d,\tf:%p, \top:%d\n",fileno(((flatFileUserData_t*)f->userData)->f),((flatFileUserData_t*)f->userData)->f,f->op);

	createThread(SyncData,f);
#endif
	return 0;
}


uint8_t FFSClose(FSF_t *f) {
	if(f->userData == NULL) {
		return 1;
	}
	if(((flatFileUserData_t*)f->userData)->f == 0) {
		return 1;
	}
	// wait while write or read operation is in progress
	while(f->op != FSF_WAIT_FOR_OPERATION);

#ifdef FFS_ASYNC_WRITE
	//close pipe write file descriptor
	close(((flatFileUserData_t*)f->userData)->pipeFd[1]);

	while(((flatFileUserData_t*)f->userData)->isWriteThrRunning);
#endif
	if(fclose(((flatFileUserData_t*)f->userData)->f)) {
		return 1;
	}

    if(f->userData) {
        free((flatFileUserData_t*)f->userData);
    }
	f->userData = NULL;
	return 0;
}

uint8_t FFSFileExist(FS_t *fs, char *fileName) {
	char tmpName[MAX_FLAT_FILE_STORAGE_FILE_PATH];
	tmpName[0] = 0;
	if(fs->storageData) {
		strcpy(tmpName,((flatFileStorageConfig_t*)fs->storageData)->path);
		strcat(tmpName,((flatFileStorageConfig_t*)fs->storageData)->name);
	}
	strcat(tmpName,fileName);
	if(access((char*)tmpName, F_OK) != -1) {
		return 1;
	}
	return 0;
}

FSIO_t flatFileFSIO = {
	.Init = FFStorageInit,
	.Open = FFSOpen,
	.Read = FFSRead,
	.Write = FFSWrite,
	.Close = FFSClose,
	.FileExist = FFSFileExist
};


/*
typedef struct {
	uint32_t data;
	char test[10];
} testFlatFileStorageData_t;

int testFlatFileStorage(void) {
	uint8_t ret;

	FS_t myFS;
	flatFileStorageConfig_t config = {
		.name ="test",
		.path = ""
	};
	ret = InitFS(&myFS,&flatFileFSIO,1024,&config);

	FSF_t f;

	ret = OpenFS(&myFS,&f,"test.txt");

	testFlatFileStorageData_t testData;
	testData.data = 1000;
	strcpy(testData.test, "xyz");

	ret = WriteFSF(&f,(void*)&testData,sizeof(testFlatFileStorageData_t));

	testFlatFileStorageData_t readTestData;

	ret = ReadFSF(&f,(void*)&readTestData,sizeof(testFlatFileStorageData_t));
	printf("Test data: %d, %s\n",readTestData.data,readTestData.test);
	CloseFSF(&f);

	return 0;
}
*/
