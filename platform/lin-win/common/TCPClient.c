/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#ifndef __USE_BSD
	#define __USE_BSD
#endif
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "threads.h"
#include "TCPClient.h"
#ifndef __USE_GNU
	#define __USE_GNU
#endif
#include <netdb.h>

#include "sysConfig.h"
#include "../../common/common.h"
#include "../../sys/timeManager.h"

#ifdef TCP_CLIENT_DBG_MSG_EN
	#define DEBUGPRINTF_EN
#endif
#include "../../common/debugPrintf.h"

#define TCP_CLIENT_PORT            5555
#define TCP_CLIENT_TEST_MESSAGE    "Yow!!! Are we having fun yet?!?"
#define TCP_CLIENT_SERVERHOST      "127.0.0.1"


     
int InitClientSock(struct sockaddr_in *name,const char *hostname, uint16_t port) {
	struct hostent *hostinfo;
  
	name->sin_family = AF_INET;
	name->sin_port = htons (port);
	hostinfo = gethostbyname (hostname);
	if (hostinfo == NULL) {
		fprintf (stderr, "Unknown host %s.\n", hostname);
		return 1;
	}
	name->sin_addr = *(struct in_addr *) hostinfo->h_addr;
	return 0;
}


int TCPClientSendData(TCPClient_t *tcpClient,uint8_t* data, int len) {
	int sendLen;
	sendLen = send(tcpClient->sock, data, len, MSG_DONTWAIT);
	if(tcpClient->onSentCallback) {
		tcpClient->onSentCallback(tcpClient->userObject,data,sendLen<0?0:sendLen,sendLen<0?errno:0);
	}
	return sendLen;
}



THREAD_RETURN_TYPE TCPClientReadThread(void *arg)
{
	TCPClient_t *tcpClient;
	tcpClient = (TCPClient_t *)((defaultThreadArgs_t *)arg)->args;
	((defaultThreadArgs_t *)arg)->started = 1;
	int len;
	int tmpErrno = TCP_CLIENT_ERR_UNDEFINED;

	tcpClient->isConnected = 1;
	do {
		len = recv(tcpClient->sock,tcpClient->buf,sizeof(tcpClient->buf),0);
		tmpErrno = errno;
		DEBUGPRINTF("[%llu] TCPClientReadThread received %d data\n", GetSysTickCount(), len);

		if(len>0){
			if(tcpClient->onDataCallback != NULL) {
				  tcpClient->onDataCallback(tcpClient->userObject,(uint8_t*)tcpClient->buf,len);
			}
		}else {
			PRINTF("closing receive thread. Received %d, errno %d = %s\n",len,errno,strerror(errno));
			if(len == 0 && tmpErrno == 0) {
				tmpErrno = TCP_CLIENT_ERR_DISCONNECTED;
			}
			break;
		}
	} while(1);
	tcpClient->isConnected = 0;
	if(SockIsclosed(tcpClient->sock)) {
		SocketClose(tcpClient->sock);
	}
	if(tcpClient->onReceiveErrorHandler != NULL) {
		tcpClient->onReceiveErrorHandler(tcpClient->userObject, tmpErrno);
	}
	return NULL;
}

uint8_t TCPClientInit(TCPClient_t *tcpClient) {
	if(tcpClient == NULL) {
		return 1;
	}
	tcpClient->isConnected = 0;
	pthread_mutex_init(&tcpClient->initializing, NULL);

	return 0;
}

uint8_t TCPClientOpen(TCPClient_t *tcpClient, char* hostName, uint16_t port, TCPClientOnDataCallback_t onDataCallback, TCPClientOnSentCallback onSentCallback, void *userObject, TCPClientOnReceiveErrorHandler_t onReceiveErrorHandler, uint32_t keepAliveExpireTimeSec){
	struct sockaddr_in servername;
	uint8_t ret = 0;

	if(tcpClient == NULL) {
		return 1;
	}
	pthread_mutex_lock(&tcpClient->initializing);

	if(tcpClient->isConnected) {
		ret =  100;
		goto TCPClientOpenExit;
	}

	if (!InitSockets()) {
		ret = 2;
		goto TCPClientOpenExit;
	}
	if(hostName == NULL) {
		ret = 3;
		goto TCPClientOpenExit;
	}
	if(tcpClient->IPParams.hostName != hostName) {
		strncpy(tcpClient->IPParams.hostName, hostName,TCP_CLIENT_MAX_HOSTNAME_LEN);
	}
	tcpClient->IPParams.port = port;
	tcpClient->onDataCallback = onDataCallback;
	tcpClient->onSentCallback = onSentCallback;
	tcpClient->onReceiveErrorHandler = onReceiveErrorHandler;
	tcpClient->userObject = userObject;
	tcpClient->errorMsg[0] = 0;
	tcpClient->isConnected = 0;
	tcpClient->keepAliveTime = 0;
	if(keepAliveExpireTimeSec) {
		tcpClient->keepAliveTime = keepAliveExpireTimeSec / (TCP_CLIENT_DEFAULT_KEEPALIVE_PACKET_CNT + 1);
		if(tcpClient->keepAliveTime == 0) {
			tcpClient->keepAliveTime = TCP_CLIENT_MIN_KEEPALIVE_TIME;
		}
	}
	//tcpClient->sock = 0;
	int err;

	tcpClient->sock = socket (PF_INET, SOCK_STREAM, 0);
	if (tcpClient->sock < 0) {
		sprintf(tcpClient->errorMsg,"Could not create socket!");
		ret = 4;
		goto TCPClientOpenExit;
	}

	err = InitClientSock (&servername, tcpClient->IPParams.hostName, tcpClient->IPParams.port);
	if(err) {
		sprintf(tcpClient->errorMsg,"Could not resolve hostname %s",tcpClient->IPParams.hostName);
		ret = 5;
		goto TCPClientOpenCleanSock;
	}

	if((err = SockConnect(tcpClient->sock,(struct sockaddr_in*) &servername,sizeof(servername), 2000))) {
		sprintf(tcpClient->errorMsg,"Could not connect to host %s, port %d, err = %d - %s",tcpClient->IPParams.hostName, tcpClient->IPParams.port,err, strerror(err));
		ret = 6;
		goto TCPClientOpenCleanSock;
	}

	if(keepAliveExpireTimeSec) {
		err = SetSockKeepAliveEx(tcpClient->sock,TCP_CLIENT_DEFAULT_KEEPALIVE_PACKET_CNT, tcpClient->keepAliveTime, tcpClient->keepAliveTime);
		if(err) {
			sprintf(tcpClient->errorMsg,"Could not set keepalive. err: %d\n",err);
		}
	}
	//Create read thread
	if((err = createThread(TCPClientReadThread, tcpClient)) == 0){
		sprintf(tcpClient->errorMsg,"Error thread not created (%d)",err);
		SocketClose(tcpClient->sock);
		ret = 7;
		goto TCPClientOpenExit;
	}
	goto TCPClientOpenExit;
TCPClientOpenCleanSock:
	close(tcpClient->sock);
TCPClientOpenExit:
	pthread_mutex_unlock(&tcpClient->initializing);
	return ret;
}

uint8_t TCPClientReopen(TCPClient_t *tcpClient) {
	return TCPClientOpen(tcpClient,tcpClient->IPParams.hostName,tcpClient->IPParams.port,tcpClient->onDataCallback,tcpClient->onSentCallback,tcpClient->userObject,tcpClient->onReceiveErrorHandler,tcpClient->keepAliveTime);
}

void TCPClientClose(TCPClient_t *tcpClient) {
	SocketClose(tcpClient->sock);
	tcpClient->isConnected = 0;
}

void TestClientOnDataCallback(void *object, uint8_t *data, uint32_t len) {
	if(len) {
		PRINTF("%s",data);
	}
}


void TCPClientTest() {
	TCPClient_t tcpClient;
	if(TCPClientOpen(&tcpClient,TCP_CLIENT_SERVERHOST,TCP_CLIENT_PORT,TestClientOnDataCallback,NULL,NULL,NULL,0)) {
		printf("Could not init communication\n");
		return;
	}
	int err;
	int i = 0;
	while(i++<10000) {
		err = TCPClientSendData(&tcpClient,(uint8_t*)"BLA",4);
		if(err<0) {
			PRINTF("problem with sending data! Ending!\n");
			break;
		}
		usleep(1000);
	}
	PRINTF("closing Client!\n");
	TCPClientClose(&tcpClient);
}
