/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "processInstanceHelper.h"
#ifdef WIN32
#include "windows.h"
#include "Psapi.h"

int isAnotherInstanceRunning(char* pName, char *pParam) {
	unsigned long aProcesses[1024], cbNeeded, cProcesses;
	unsigned int cnt=0;
	if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return -1;

	cProcesses = cbNeeded / sizeof(unsigned long);
	for(unsigned int i = 0; i < cProcesses; i++)
	{
		if(aProcesses[i] == 0)
			continue;

		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, aProcesses[i]);
		char buffer[50];
		GetModuleBaseName(hProcess, 0, buffer, 50);
		CloseHandle(hProcess);
		if(strcmp(pName,buffer) == 0){
			cnt++; //return 1;
		}
			
	}
	if(cnt>1){
		return 1;
	}
	return 0;
}

#else
#include <stdio.h>
#include <glob.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int isAnotherInstanceRunning(char *pName, char *pParam) {
	pid_t pid = -1;
	glob_t pglobComm,pglobCmdline;
	char *readbuf;
	if(pParam == NULL)
		return -1;
	if(pName == NULL)
		return -2;
	int buflen = 100 + strlen(pName) + strlen(pParam) + 4;
	unsigned i;
	unsigned int cnt=0;

	/* Get a list of all comm files. man 5 proc */
	if (glob("/proc/*/comm", 0, NULL, &pglobComm) != 0)
	    return -3;
	if (glob("/proc/*/cmdline", 0, NULL, &pglobCmdline) != 0)
	    return -4;

	/* The comm files include trailing newlines, so... */

	/* readbuff will hold the contents of the comm files. */
	readbuf = malloc(buflen);
	for (i = 0; i < pglobComm.gl_pathc; ++i) {
	    FILE *comm;
	    char *ret;

		/* skip self instance */
		if(strcmp("/proc/self/comm",pglobComm.gl_pathv[i]) == 0) {
			continue;
		}
	    /* Read the contents of the file. */
	    if ((comm = fopen(pglobComm.gl_pathv[i], "r")) == NULL)
		continue;
	    ret = fgets(readbuf, buflen, comm);
	    fclose(comm);
	    if (ret == NULL)
		continue;

	    /*
	    If comm matches our process name, extract the process ID from the
	    path, convert it to a pid_t, and return it.
	    */
	    if (strncmp(readbuf, pName,strlen(pName) ) == 0) {
			pid = (pid_t)atoi(pglobComm.gl_pathv[i] + strlen("/proc/"));
			(void)pid; // only for not getting compiler warning
			if ((comm = fopen(pglobCmdline.gl_pathv[i], "r")) == NULL)
				continue;
			ret = fgets(readbuf, buflen, comm);
			fclose(comm);
			if (ret == NULL)
				continue;
			if (strcmp(readbuf + strlen(readbuf) + 1 ,pParam) == 0)
				cnt++;
	    }
	}

	/* Clean up. */
	free(readbuf);
	globfree(&pglobComm);
	globfree(&pglobCmdline);
	if(cnt>1){
		return 1;
	}
	return 0;
}

#endif
