/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TCP_SERVER_H__
#define __TCP_SERVER_H__

#include "../../common/list.h"
#include <sys/types.h>
#include <sys/select.h>
#include "socketCommon.h"


#define TCP_ERROR_MSG_MAX_LEN 255
#define TCP_SERVER_MAX_REC_MSG_LEN  512


#define TCP_SERVER_ERR_SELECT_PROBLEM 1
#define TCP_SERVER_ERR_ACCEPT_CLIENT_PROBLEM 2
#define TCP_SERVER_ERR_CLIENT_DISCONNECTED 3


typedef struct {
	SOCKET sock;
	struct sockaddr_in clientname;
	size_t size;
} TCPConnectionInfo_t;

typedef void (*TCPServerOnDataCallback_t)(void *object, uint8_t *data, uint32_t len, TCPConnectionInfo_t* info );
typedef void (*TCPServerOnSentCallback_t) (void * object, uint8_t* data, uint16_t transferredDataLen, int8_t err);
typedef void (*TCPServerOnReceiveErrorHandler_t)(void* object, uint16_t error, TCPConnectionInfo_t* info );


typedef struct {
	SOCKET listeningSock;
	list_t allConnections;
	uint16_t port;
	fd_set active_fd_set;
	fd_set read_fd_set;
	char errorMsg[TCP_ERROR_MSG_MAX_LEN];
	TCPServerOnDataCallback_t onDataCallback;
	void *userObject;
	TCPServerOnSentCallback_t onSentCallback;
	TCPServerOnReceiveErrorHandler_t onReceiveErrorHandler;
	uint8_t isListening;
} TCPServer_t;

char* TCPGetLastErrorStr(TCPServer_t *tcpServer);
int TCPServerSendData(TCPServer_t *tcpServer, SOCKET sock,uint8_t * data, uint16_t len);
int TCPServerSendDataAll(TCPServer_t *tcpServer,uint8_t * data, uint16_t len);
uint8_t TCPServerOpen(TCPServer_t *tcpServer, uint16_t port, TCPServerOnDataCallback_t onDataCallback, TCPServerOnSentCallback_t onSentCallback, void *userObject, TCPServerOnReceiveErrorHandler_t onReceiveErrorHandler);
uint8_t TCPServerReinit(TCPServer_t *tcpServer);
uint8_t TCPServerClose(TCPServer_t *tcpServer);
int MakeServerSocket(uint16_t port, char* errorBuf) ;

#endif // SERVER_H
