/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "initPlatform.h"

#include <string.h>
#include <stdlib.h>
#include "../../common/common.h"
#include "../../sys/system.h"
#include "../../sys/configurationManager.h"
#include "../../sys/moduleManager.h"
#include "../../sys/threadManager.h"
#include "defaultModules.h"

DECLARE_ID_ENUM(SYSTEM_MODULE_LIST);

int8_t InitPlatform(uint8_t *name, NODE_ADDR address, bool isMaster, INTERFACE_TYPE_ID defaultInterfacetId, uint8_t* interfaceConfig) {
	if(name == NULL) {
        return ERR_PLATFORM_NAME_NULL;
	}
	if(strlen((const char*)name) > MAX_SYSTEM_NAME_LEN ) {
        return ERR_PLATFORM_NAME_TOO_BIG;
	}
	strncpy((char*)systemName,(const char*)name,strlen((const char*)name));
	if(isMaster) {
		SetMaster();
		//Add address manager module
	}
	SetSystemNodeAddr(address);
	InitSystem();
	//Add modules
	AddModule(SystemModule_ID,NULL);

	if(isMaster) {
		AddModule(AddressManager_ID,NULL);
	}

	//configure default communication interface
	int8_t freeInterface = GetFreeInterface();
	if(freeInterface < 0) {
		return ERR_PLATFORM_NOT_FREE_INTERFACE;
	}
	if(InitInterface(freeInterface,defaultInterfacetId,(uint8_t*) interfaceConfig) != 0) {
        return ERR_PLATFORM_DEFAULT_INTERFACE_NOT_INITITIALIZED;
	}
	return freeInterface;
}
