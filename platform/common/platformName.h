#ifndef __PLATFORM_NAME_H__
#define __PLATFORM_NAME_H__

#ifdef PLATFORM_ID_NOT_SPECIFIED
    #define PLATFORM_NAME "NA"
#elif defined(PLATFORM_ID_LINUX)
    #define PLATFORM_NAME "LIN"
#elif defined(PLATFORM_ID_WINDOWS)
    #define PLATFORM_NAME "WIN"
#elif defined(PLATFORM_IO_ATM8SL)
    #define PLATFORM_NAME "BM" //Bare Metal
#else
    #define PLATFORM_NAME "NA" //Not defined
#endif
#endif