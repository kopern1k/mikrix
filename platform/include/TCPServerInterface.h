/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TCP_SERVER_INTERFACE__H__
#define __TCP_SERVER_INTERFACE__H__
#include "../../common/common.h"
#include "../../sys/communicationManager.h"
#include "../common/TCPServer.h"
#include "../../sys/communicationInterface.h"
#include "generalInterface.h"

#define TCP_SERVER_INTERFACE {TCPServerSocketInterfaceWrite,TCPServerSocketInterfaceInit,TCPServerSocketInterfaceClose,  {"TCP Server par:[port]",PointToPoint}}

typedef void (*TCPServerInterfaceOnError_t)(uint8_t error,void* userObject, TCPConnectionInfo_t* connectionInfo);
typedef void (*TCPServerInterfaceOnClose_t)(void* userObject);
typedef void (*TCPServerInterfaceOnOpen_t)(void* userObject, TCPServer_t* tcpServer);
typedef void (*TCPServerInterfaceOnReopen_t)(void* userObject, TCPServer_t* tcpServer);
typedef void (*TCPServerInterfaceOnInitError_t)(void* userObject, int16_t err, char* errMsg);

typedef struct {
	uint16_t port;
	uint8_t  reinitOnErrorEn;		// reinitialize interface on troubles
        uint32_t reinitRetryIntervalMs;	// when reinitialization needed, how often should reinitialize interface
	uint8_t reinitCount;
        void* userObject;
        TCPServerInterfaceOnError_t onReceiveError;
        TCPServerInterfaceOnOpen_t onInterfaceOpen;
        TCPServerInterfaceOnClose_t onInterfaceClose;
        TCPServerInterfaceOnReopen_t onInterfaceReopen;
        TCPServerInterfaceOnInitError_t onInterfaceInitError;
} TCPServerConfig_t;


typedef struct {
    GIData_t GIData;
    TCPServer_t tcpServer;
} TCPServerInterfaceData_t;

//void OnTCPServerSockData( uint8_t  *data, uint16_t len, void *object);
uint8_t TCPServerSocketInterfaceInit( uint8_t *data, COM_MNG_DATA *cm);
uint8_t TCPServerSocketInterfaceClose(COM_MNG_DATA *cm);
void TCPServerSocketInterfaceWrite(COM_MNG_DATA *cm, uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending);

#endif
