/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TCP_CLIENT_INTERFACE__H__
#define __TCP_CLIENT_INTERFACE__H__
#include "../../common/common.h"
#include "../../sys/communicationManager.h"
#include "../common/TCPClient.h"
#include "../../sys/communicationInterface.h"
#include "generalInterface.h"


#define TCP_CLIENT_INTERFACE {TCPClientSocketInterfaceWrite,TCPClientSocketInterfaceInit,TCPClientSocketInterfaceClose,  {"TCP Client:[host/IP],[port]",PointToPoint}}

typedef void (*TCPClientInterfaceOnError_t)(uint8_t error,void* userObject, TCPClientIPParams_t* currentIPParams);
typedef void (*TCPClientInterfaceOnClose_t)(void* userObject);
typedef void (*TCPClientInterfaceOnOpen_t)(void* userObject, TCPClientIPParams_t* currentIPParams);
typedef void (*TCPClientInterfaceOnReopen_t)(void* userObject, TCPClientIPParams_t* currentIPParams);
typedef void (*TCPClientInterfaceOnInitError_t)(void* userObject, int16_t err, char* errMsg);

typedef struct {
	uint8_t* hostName;
	uint16_t port;
	uint8_t  reinitOnErrorEn;		// reinitialize interface on troubles
        uint32_t reinitRetryIntervalMs;	// when reinitialization needed, how often should reinitialize interface
	uint8_t reinitCount;
	uint32_t keepAliveExpireTime;			// keep-Alive expire time in sec. If 0, no keep alive is used
	void *userObject;
        TCPClientInterfaceOnError_t onReceiveErrorCallback;
	TCPClientInterfaceOnClose_t onCloseCallback;
        TCPClientInterfaceOnReopen_t onReopenCallback;
        TCPClientInterfaceOnInitError_t onInterfaceInitError;
        TCPClientInterfaceOnOpen_t onInterfaceOpen;
} TCPClientConfig_t;
/*
typedef struct {
	TCPClient_t tcpClient;
	uint8_t reinitOnErrorEn;
	uint16_t reinitRetryInterval;
	uint8_t maxReinitCount; // 0 -  loop until success reinitialzed
	void *userObject;
	TCPClientInterfaceOnError_t onErrorCallback;
	TCPClientInterfaceOnClose_t onCloseCallback;
        TCPClientInterfaceOnReopen_t onReopenInterface;
} TCPClientInterfaceData_t;
*/
typedef struct {
    GIData_t GIData;
    TCPClient_t tcpClient;
} TCPClientInterfaceData_t;
void OnTCPClientSockData(void *object, uint8_t *data, uint32_t len);
uint8_t TCPClientSocketInterfaceInit( uint8_t *data, COM_MNG_DATA *cm);
uint8_t TCPClientSocketInterfaceClose(COM_MNG_DATA *cm);
void TCPClientSocketInterfaceWrite(COM_MNG_DATA *cm,uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending);

#endif
