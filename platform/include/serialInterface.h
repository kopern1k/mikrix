/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __SERIAL_INTERFACE__H__
#define __SERIAL_INTERFACE__H__

#include "../../common/common.h"
#include "../../sys/communicationManager.h"
#include "../../sys/communicationInterface.h"
#include "sysConfig.h"

typedef struct {
	 char portName[MAX_SERIAL_INTERFACE_NAME_SIZE];
	 uint32_t speed;
} SERIAL_CONFIG;

#define SERIAL_INTERFACE {SerialWrite,SerialInitWr,SerialClose, {"RS232 port par:[port, speed]",PointToPoint}}

//#define MAKE_SERIAL_CONFIG(PORT_NAME, SPEED) ((SERIAL_CONFIG){.portName=PORT_NAME,.speed=SPEED})
#define MAKE_SERIAL_CONFIG(PORT_NAME, SPEED) ((SERIAL_CONFIG){PORT_NAME,SPEED})

uint8_t SerialInitWr(uint8_t *config, COM_MNG_DATA *cm);
uint8_t SerialInit(SERIAL_CONFIG *config, COM_MNG_DATA *cm);
uint8_t SerialClose();
void SerialWrite(COM_MNG_DATA *cm,uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t mask, volatile uint8_t *Sending);

#endif
