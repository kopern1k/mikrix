/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../include/serialInterface.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//specify serial line parameters
//#define F_OSC 8000000UL		           /* oscilator-frequency in Hz */
//#define FREQ_DIV8 F_OSC/8
//#define MHz 8
#define UART_BAUD_RATE 38400

#define UART_BAUD_CALC(UART_BAUD_RATE,F_OSC) ((F_OSC)/((UART_BAUD_RATE)*16ULL)-1)

#ifdef __AVR_ATmega64__
	#define EN_UDRIE() set(UCSR0B,BIT(UDRIE))
	#define DI_UDRIE() clear(UCSR0B,BIT(UDRIE))
#else
	#define EN_UDRIE() set(UCSRB,BIT(UDRIE))
	#define DI_UDRIE() clear(UCSRB,BIT(UDRIE))
#endif

uint8_t start;
uint8_t end;
uint8_t mask;
uint8_t *pBuf;
volatile uint8_t *sending;
COM_MNG_DATA *cmTmp;
#ifdef  __AVR_ATmega64__
SIGNAL (SIG_UART0_DATA){
#else
SIGNAL (SIG_UART_DATA){
#endif
	if(start!=end){
		start=(start+1)&mask;
#ifdef __AVR_ATmega64__
		UDR0 = pBuf[start];
#else		
		UDR = pBuf[start];
#endif		
	}else{ //tx empty
		DI_UDRIE();
		*sending = 0;
	}
}

void SerialWrite(uint8_t *pData, uint8_t startInd, uint8_t endInd, uint8_t Mask, volatile uint8_t *Sending){
	start= startInd;
	end = endInd;
	mask = Mask;
	pBuf = pData;
	sending = Sending;
	EN_UDRIE();
}

#ifdef __AVR_ATmega64__
SIGNAL (SIG_UART0_RECV) { // USART RX interrupt
OnDataReceive(cmTmp,UDR0);
}
#else
SIGNAL (SIG_UART_RECV) { // USART RX interrupt
OnDataReceive(cmTmp,UDR);
}
#endif


uint8_t SerialInitWr(uint8_t *config, COM_MNG_DATA *cm){
	return SerialInit((SERIAL_CONFIG *)config,cm);
}


INLINE uint8_t SerialInit(SERIAL_CONFIG *config, COM_MNG_DATA *cm){
	//TxTop=0;
	//actTx=0;
	// set baud rate
	cmTmp = cm;
	cm->fullDuplex=1;
	cm->enabled =1;
#ifdef __AVR_ATmega64__
	#ifdef CONFIGURABLE_SERIAL_PORT
		UBRR0H = (uint8_t)(UART_BAUD_CALC(config->speed,F_OSC)>>8);
		UBRR0L = (uint8_t)UART_BAUD_CALC(config->speed,F_OSC);
		cm->communicationSpeed=config->speed;
	#else
		UBRR0H = (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE,F_OSC)>>8);
		UBRR0L = (uint8_t)UART_BAUD_CALC(UART_BAUD_RATE,F_OSC);
		cm->communicationSpeed=UART_BAUD_RATE;
	#endif
		// Enable receiver and transmitter; enable RX interrupt
		UCSR0B = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
		//asynchronous 8N1
		UCSR0C =  (3 << UCSZ0);
#else	
	#ifdef CONFIGURABLE_SERIAL_PORT
		UBRRH = (uint8_t)(UART_BAUD_CALC(config->speed,F_OSC)>>8);
		UBRRL = (uint8_t)UART_BAUD_CALC(config->speed,F_OSC);
		cm->communicationSpeed=config->speed;
	#else
		UBRRH = (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE,F_OSC)>>8);
		UBRRL = (uint8_t)UART_BAUD_CALC(UART_BAUD_RATE,F_OSC);
		cm->communicationSpeed=UART_BAUD_RATE;
	#endif
		// Enable receiver and transmitter; enable RX interrupt
		UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
		//asynchronous 8N1
		UCSRC = (1 << URSEL) | (3 << UCSZ0);
#endif	
	cm->fullDuplex = 0;
	cm->oneByteDelayUs = WAIT_FOR_ONE_BYTE(cm->communicationSpeed);
	InitRandomTime(cm->communicationSpeed);	
	return 0;
}

