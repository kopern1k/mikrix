/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __FLASH_H__
#define __FLASH_H__

#include "flash.h"
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>


#define FLASH_STORE_PAGE	(FLASHEND - FLASH_STORE_SPACE - BOOTFLASHSIZE)+1 ///SPM_PAGESIZE
#define FLASH_STORE_ADDR	(FLASHEND - FLASH_STORE_SPACE - BOOTFLASHSIZE)+1

uint8_t  SaveFlash(uint8_t *data, uint8_t size){
	if(size > FLASH_STORE_SPACE){
		return 1;
	}
	char offset=0;
	for(char c = 0 ;c< (FLASH_STORE_SPACE / SPM_PAGESIZE ); c++){
		BootProgramPage(FLASH_STORE_PAGE + offset, data + offset);
		offset+=SPM_PAGESIZE;
	}

//	BootProgramPage(FLASH_STORE_PAGE , data );
	return 0;
}

uint8_t ReadFlash(uint8_t *data, uint8_t size){

	if(size<FLASH_STORE_SPACE){
		return 1;
	}
	/*
	for(char i = 0; i<FLASH_STORE_SPACE;i++){
		data[i]=pgm_read_byte(FLASH_STORE_ADDR+i);

	}
	*/
	memcpy_P(data,(uint_farptr_t)FLASH_STORE_ADDR,FLASH_STORE_SPACE);
	return 0;
}


void BootProgramPage(uint32_t page, uint8_t *buf)
{
    uint16_t i;
    uint8_t sreg;

    // Disable interrupts.

    sreg = SREG;
    cli();

    eeprom_busy_wait ();

    boot_page_erase (page);
    boot_spm_busy_wait ();      // Wait until the memory is erased.

    for (i=0; i<SPM_PAGESIZE; i+=2)
    {
        // Set up little-endian word.

        uint16_t w = *buf++;
        w += (*buf++) << 8;

        boot_page_fill (page + i, w);
    }

    boot_page_write (page);     // Store buffer in flash page.
    boot_spm_busy_wait();       // Wait until the memory is written.

    // Reenable RWW-section again. We need this if we want to jump back
    // to the application after bootloading.

    boot_rww_enable ();

    // Re-enable interrupts (if they were ever enabled).

    SREG = sreg;
}
#endif
