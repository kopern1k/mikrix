/*
 * Copyright (c) 2006, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __hardware__H_
#define __hardware__H_
#include  "..\\..\\common\\common.h"

//#define MHz						16

enum MultiIO {MIO1=0,MIO2,MIO3,MIO4,MIO5,MIO6,MIO7,MIO8};

#define MIO1Pin	BIT0	//PC
#define MIO2Pin	BIT1	//PC
#define MIO3Pin	BIT2	//PC
#define MIO4Pin	BIT3	//PC
#define MIO5Pin	BIT4	//PC
#define MIO6Pin	BIT5	//PC
#define MIO7Pin	BIT5 	//PD
#define MIO8Pin	BIT0	//PB

#define LED1	BIT3 	//PD
#define LED2	BIT4	//PD

#define MIO9_DAC0Pin	BIT6	//PD
#define MIO10_DAC1Pin	BIT7	//PD

#define SPI_CSMAPin 	BIT1	//PB
#define SSPin			BIT2	//PB
#define MOSIPin			BIT3	//PB
#define MISOPin			BIT4	//PB
#define SCKPin			BIT5	//PB

#define LED1Port	PORTD
#define LED2Port	PORTD
#define LED1Dir		DDRD
#define LED2Dir		DDRD


#define MIO1_6Port 	PORTC
#define MIO7Port	PORTD
#define MIO8Port	PORTB

//out

#define SetOutputMIO1_6(num)  	set(DDRC,(num))

#define SetOutputMIO1()  	set(DDRC,MIO1Pin)
#define SetOutputMIO2()  	set(DDRC,MIO2Pin)
#define SetOutputMIO3()  	set(DDRC,MIO3Pin)
#define SetOutputMIO4()  	set(DDRC,MIO4Pin)
#define SetOutputMIO5()  	set(DDRC,MIO5Pin)
#define SetOutputMIO6()  	set(DDRC,MIO6Pin)
#define SetOutputMIO7()  	set(DDRD,MIO7Pin)
#define SetOutputMIO8()  	set(DDRB,MIO8Pin)

#define SetMIO1_6(num)  	set(PORTC,(num))

#define SetMIO1()	set(PORTC,MIO1Pin)
#define SetMIO2()  	set(PORTC,MIO2Pin)
#define SetMIO3()  	set(PORTC,MIO3Pin)
#define SetMIO4()  	set(PORTC,MIO4Pin)
#define SetMIO5()  	set(PORTC,MIO5Pin)
#define SetMIO6()  	set(PORTC,MIO6Pin)
#define SetMIO7()  	set(PORTD,MIO7Pin)
#define SetMIO8()  	set(PORTB,MIO8Pin)

#define SetDataMIO1_6(data,mask) (setData(MIO1_6Port,data,mask))//(PORTC=(PORTC &~ MASK) | (data & MASK))
#define ClearMIO1_6(num)	clear(PORTC,(num))

#define ClearMIO1()		clear(PORTC,MIO1Pin)
#define ClearMIO2()		clear(PORTC,MIO2Pin)
#define ClearMIO3()		clear(PORTC,MIO3Pin)
#define ClearMIO4()		clear(PORTC,MIO4Pin)
#define ClearMIO5()		clear(PORTC,MIO5Pin)
#define ClearMIO6()		clear(PORTC,MIO6Pin)
#define ClearMIO7()		clear(PORTD,MIO7Pin)
#define ClearMIO8()		clear(PORTB,MIO8Pin)

#define GetMIO1_6(num)	get(PINC,(num))

#define GetMIO1()		get(PINC,MIO1Pin)
#define GetMIO2()		get(PINC,MIO2Pin)
#define GetMIO3()		get(PINC,MIO3Pin)
#define GetMIO4()		get(PINC,MIO4Pin)
#define GetMIO5()		get(PINC,MIO5Pin)
#define GetMIO6()		get(PINC,MIO6Pin)
#define GetMIO7()		get(PIND,MIO7Pin)
#define GetMIO8()		get(PINB,MIO8Pin)


//in
#define SetInputMIO1_6(num)	clear(DDRC,(num))

#define SetInputMIO1()  	clear(DDRC,MIO1Pin)
#define SetInputMIO2()  	clear(DDRC,MIO2Pin)
#define SetInputMIO3()  	clear(DDRC,MIO3Pin)
#define SetInputMIO4()  	clear(DDRC,MIO4Pin)
#define SetInputMIO5()  	clear(DDRC,MIO5Pin)
#define SetInputMIO6()  	clear(DDRC,MIO6Pin)
#define SetInputMIO7()  	clear(DDRD,MIO7Pin)
#define SetInputMIO8()  	clear(DDRB,MIO8Pin)


//#define EnablePullUp()
#define SetPullUpMIO1_6(num)		set(PORTC,(num))

#define SetPullUpMIO1()		set(PORTC,MIO1Pin)
#define SetPullUpMIO2()		set(PORTC,MIO2Pin)
#define SetPullUpMIO3()		set(PORTC,MIO3Pin)
#define SetPullUpMIO4()		set(PORTC,MIO4Pin)
#define SetPullUpMIO5()		set(PORTC,MIO5Pin)
#define SetPullUpMIO6()		set(PORTC,MIO6Pin)
#define SetPullUpMIO7()		set(PORTD,MIO7Pin)
#define SetPullUpMIO8()		set(PORTB,MIO8Pin)

#define ClearPullUpMIO1_6(num)		clear(PORTC,(num))

#define ClearPullUpMIO1()		clear(PORTC,MIO1Pin)
#define ClearPullUpMIO2()		clear(PORTC,MIO2Pin)
#define ClearPullUpMIO3()		clear(PORTC,MIO3Pin)
#define ClearPullUpMIO4()		clear(PORTC,MIO4Pin)
#define ClearPullUpMIO5()		clear(PORTC,MIO5Pin)
#define ClearPullUpMIO6()		clear(PORTC,MIO6Pin)
#define ClearPullUpMIO7()		clear(PORTD,MIO7Pin)
#define ClearPullUpMIO8()		clear(PORTB,MIO8Pin)


#define LED1Init()  	set(LED1Dir,LED1)
#define LED2Init()  	set(LED2Dir,LED2)
#define LED1On()		set(LED1Port,LED1)
#define LED2On()		set(LED2Port,LED2)
#define LED1Off()		clear(LED1Port,LED1)
#define LED2Off()		clear(LED2Port,LED2)
#define XorLED1()		xor(LED1Port,LED1)
#define XorLED2()		xor(LED2Port,LED2)


#endif



