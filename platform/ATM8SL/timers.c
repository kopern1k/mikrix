/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "..\\..\\common\\common.h"
#include "timers.h"
#include "avr/io.h"
//#include "sysConfig.h"
#include "sysTimer.h"


inline void InitTimer0(void){
    //init OC0 pin to output
    //set(DDRB,BIT(PINB3));
    
	// Enable timer0 compare interrupt
	//TIMSK = BIT(OCIE0);
	// Enable timer0 compare interrupt
	//set(TIMSK,BIT(TOIE0));
    // Sets the compare value
	//OCR0 = 199;

	// Set Clear on Timer Compare (CTC) mode
	TCCR0 = BIT(COM00)|BIT(WGM01)|BIT(COM00);
	PAUSE_TIMER0();
}

//#define SET_COMPARE_VALUE(value) OCR1A=value 




inline void InitTimer1(void){
	//16 bit mode 
    //init OC1A  pin to output
    //set(DDRD,BIT(PIND5)|BIT(PIND4));
    //set(PORTD,BIT(PIND5));//=0
    //init 
    
	// Sets the compare value
	OCR1A = DEFAULT_SYS_TIM_COMPAREMAX; 
	//set mode
	//CTC MODE TOGGLE OC1A ON MATCH FCLK/8
	//TCCR1A = BIT(COM1A0);
	//TCCR1B = BIT(WGM12)|BIT(CS11);
	
	//Output mode 
	
	//set(TCCR1A, BIT(COM1A0) ); 	// set  pin OC1A when CTC
	//set(TCCR1A, BIT(COM1B0) ); 	// set toggle pin OC1B when CTC
	set(TCCR1B, BIT(WGM12));	//CTC mode
	clear(TCCR1B, BIT(WGM13));
	//NORMAL MODE (WITHOUT OUTPUT PIN TOGGLING)
	//TCCR1A=0;
	//INPUT CAPTURE NOISE CANCELER (ICNC1), INPUT CAPTURE EDGE SELECT (ICES1)  0-FALLING 1-RISING
	//WHEN ICES1 EVENT OCCUR THEN COUNTER VALUE IS COPIED INTO ICR1
	//set(TCCR1B,BIT(ICNC1)|BIT(ICES1));
	
	// Enable timer1 Output Compare A Match Interrupt Enable
	set(TIMSK , BIT(OCIE1A) );
	//enable Timer 1 Input capture interrupt
	//set(TIMSK , BIT(TICIE1) );
	//ENABLE TIMER1 OVERFLOW AND 
	//set(TIMSK,BIT(TOIE1)|BIT(TICIE1));
	//set(TIMSK,BIT(TOIE1));
}
