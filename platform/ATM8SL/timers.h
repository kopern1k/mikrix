/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

//crystal runs on 8000724

#ifndef __TIMERS__H_
#define __TIMERS__H_

#define PAUSE_TIMER0() 				clear(TCCR0, BIT(CS02) | BIT(CS01) | BIT(CS00))
#define ENABLE_TIMER0_DIV1() 		set(TCCR0 , BIT(CS00))
#define ENABLE_TIMER0_DIV8() 		set(TCCR0 , BIT(CS01))
#define ENABLE_TIMER0_DIV256() 		set(TCCR0 , BIT(CS02))
#define ENABLE_TIMER0_DIV1024() 	set(TCCR0 , BIT(CS02)|BIT(CS00))

#define PAUSE_TIMER1() 				clear(TCCR1B , BIT(CS12)|BIT(CS11)|BIT(CS10))
#define ENABLE_TIMER1_DIV1()   		set(TCCR1B , BIT(CS10))
#define ENABLE_TIMER1_DIV8()   		set(TCCR1B , BIT(CS11))
#define ENABLE_TIMER1_DIV256()   	set(TCCR1B , BIT(CS12))
#define ENABLE_TIMER1_DIV64()  		set(TCCR1B , BIT(CS11)| BIT(CS10) )

#define WRITE_TIMER1_VALUE(value) 	TCNT1=value
#define CLEAR_TIMER1_VALUE() 		TCNT1=0
#define SETOCR1A(value)				OCR1A=value
#define SETOCR1B(value)				OCR1B=value

#define DI_TICIE1() 				clear(TIMSK,BIT(TICIE1))
#define EN_TICIE1() 				set(TIMSK,BIT(TICIE1))

//timer1 input capture edge setting
#define IC1_RISING_EDGE()  			set(TCCR1B,BIT(ICES1))
#define IC1_FALLING_EDGE() 			clear(TCCR1B,BIT(ICES1))
#define CLEAR_ICF1() 				set(TIFR,BIT(ICF1))
#define CLEAR_TOV1()				set(TIFR,BIT(TOV1))

void InitTimer0(void);
void InitTimer1(void);
#endif
