/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef __TIMRRHW__H__
#define __TIMERHW__H__

#include "..\\..\\common\\inttypes.h"
#include "timers.h"

uint8_t InitSysTimer();

#define DEFAULT_SYS_TIM_COMPAREMAX	20000ULL
#define SET_SYS_TIM_CMP_VAL(val)	SETOCR1A(val)
#define GET_SYS_TIM_CMP_VAL()		GET_OCR1()
#define STANDARD_TIMER_MAX 10000ULL
#define CRYSTAL_SCALE(time) 	(time)<<((DEFAULT_SYS_TIM_COMPAREMAX/STANDARD_TIMER_MAX) - 1)
#define TIMER_TO_STANDARD(time)	(time)>>((DEFAULT_SYS_TIM_COMPAREMAX/STANDARD_TIMER_MAX) - 1)

uint16_t GET_SYS_TIMER_VAL_SCALED();
void SET_SYS_TIMER_VAL_SCALED(uint16_t value);
//#define PAUSE_CMP_VAL()	SETOCR1A(0xFFFF)	
//#define GET_CMP_VAL()		GET_OCR1()
//void SetTimMax(uint16_t max);
//double ComputeTimeDiff( LARGE_INTEGER tick1, LARGE_INTEGER tick2);
#endif
