/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "../../common/common.h"

//#define MHz						16

#define SPI_SS_Pin				BIT4
#define SPI_SS_Port				PORTB
#define SPI_SS_Dir				DDRB
#define SPI_SSHiZ()				clear(SPI_SS_Dir,SPI_SS_Pin)
#define SPI_SS_Out_Dir()		set(SPI_SS_Dir,SPI_SS_Pin)
#define SPI_SS_Hi()				set(SPI_SS_Port,SPI_SS_Pin)
#define SPI_SS_Lo()				clear(SPI_SS_Port,SPI_SS_Pin)
#define SPI_MOSI_Port			PORTB
#define SPI_MOSI_Pin			BIT5
#define SPI_MOSI_Dir			DDRB
#define SPI_MOSI_HiZ()			clear(SPI_MOSI_Dir,SPI_MOSI_Pin)
#define SPI_MOSI_Out_Dir()		set(SPI_MOSI_Dir,SPI_MOSI_Pin)
#define SPI_MOSI_Clear()		clear(SPI_MOSI_Port,SPI_MOSI_Pin)
#define SPI_MISO_Port			PORTB
#define SPI_MISO_Pin			BIT6
#define SPI_MISO_Dir			DDRB
#define SPI_MISO_HiZ()			clear(SPI_MISO_Dir,SPI_MISO_Pin);clear(SPI_MISO_Port,SPI_MISO_Pin)
#define SPI_MISO_Out_Dir()		set(SPI_MISO_Dir,SPI_MISO_Pin)
//#define SPI_MISO_In_Dir()		clear(SPI_MISO_Dir,SPI_MISO_Pin)
#define SPI_SCK_Pin				BIT7
#define SPI_SCK_Dir				DDRB
#define SPI_SCK_HiZ()			clear(SPI_SCK_Dir,SPI_SCK_Pin)
#define SPI_SCK_Out_Dir()		set(SPI_SCK_Dir,SPI_SCK_Pin)
#define SPI_Init()				SPI_MOSI_Out_Dir();SPI_MOSI_Clear();SPI_MISO_HiZ();SPI_SCK_Out_Dir();SPI_SS_Out_Dir()


/*
#define DIG0_PORT				PINA
#define DIG0_DIR				DDRA
#define DIG0_PIN				BIT0|BIT1|BIT2|BIT3
#define INIT_DIG0()				clear(DIG0_DIR,DIG0_PIN)
#define GetDig0()				get(DIG0_PORT,DIG0_PIN)




inline void InitPorts(void){

}
*/
