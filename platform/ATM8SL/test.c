/*
 * Copyright (c) 2013, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

/*
 * This application is based on Timer 1 (16bit, CTC mode updated on OCR1A, which 
 * stores also frequency)
 * OCR1B je fill register
 * PWM2 is output from TIM1
 * PWM1 - TIM1/2

*/
#include <avr/io.h>
#include <avr/interrupt.h>

#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

//#define TESTING
//#ifdef TESTING

#include "..\\..\\common\\common.h"
#include ".\\hardware.h"
#include "uart.h"
#include "timers.h"
#include "flash.h"

//global variables

volatile unsigned char ADCVal;


enum Commands {OK=0,readXStep1,readXStep2,readYStep1,readYStep2,readQuality, readAllMotion};



typedef union {
		char all;
		struct  {
		volatile char cmd:4;
		};
} STATE;
STATE states;

char debug=0;

#define KMShift 1
uint8_t outMotorData[8]={ 1<<KMShift,9<<KMShift,8<<KMShift,12<<KMShift,4<<KMShift,6<<KMShift,2<<KMShift,3<<KMShift};

#define SetOutputMIO2()  	set(DDRC,MIO2Pin)

enum PinFunc{ Output, Input, PullUpInput, SpecialFunction };

//basic config (8bit)
#define ADCSTART() 		set(ADCSRA,BIT(ADEN))		//enable ADC (ADC pin enabled ???)
#define ADCSTOP()		clear(ADCSRA,BIT(ADEN))		//disable ADC
#define ADCCONVERT()	set(ADCSRA,BIT(ADSC))		//start conversion
#define ADC_CONVERT_FINISHED()	get(ADCSRA,	BIT(ADIF))
#define ADCDATA			ADCH
//set ADC Pin
#define SetADCPin(ADCNUM) set(ADMUX,ADCNUM)			// 0- MIO1, ... 5-MIO6 !!! Don't forget set ADC iopin as input!




enum rotation {Left=0, Right};
int8_t motState=0;

void MoveLeft(){
	if(++motState>7){
		motState=0;
	}
	SetDataMIO1_6(outMotorData[motState],(MIO2Pin|MIO3Pin|MIO4Pin|MIO5Pin));
	//PORTC=outMotorData[motState];
}


void MoveRight(){
	if(--motState<0){
		motState=7;
	}
	SetDataMIO1_6(outMotorData[motState],(MIO2Pin|MIO3Pin|MIO4Pin|MIO5Pin));
//	PORTC=outMotorData[motState];
}

SIGNAL (SIG_UART_RECV) { // USART RX interrupt
	unsigned char c;
	char str[6]="0x00,";
	char tmp[FLASH_STORE_SPACE];
	c = UDR;
	switch(c){
		case 'v'://get software  version
			UartPuts("KROKMOT v 1.0\n");
			UartPutn();
			break;
		case 'r':
			//UartPuts("Reading memory 128B\n");
			UartPutc( ReadFlash(tmp,sizeof(tmp) ));
			for(char i=0;i<FLASH_STORE_SPACE;i++){
				UartPutc(tmp[i]);
			}
			break;
		case 'w':
			for(char i=0;i<FLASH_STORE_SPACE;i++){
				tmp[i] = i;
			}
			UartPutc(SaveFlash(tmp, sizeof(tmp)));
			break;
		case 'd':
			xor(debug,1);
			break;
	}
}

//max = 31250 ==1Hz , min 31 == 1000Hz
#define speed() ( (uint16_t)(31 + (uint16_t)ADCVal*19))
//#define plnenie()	((OCR1A>>2))


volatile uint8_t direction;
//interrrupt od citaca1  periodu, stabilny
SIGNAL (SIG_OUTPUT_COMPARE1A){
//	char str[5];
	if(direction == Left){
		MoveLeft();
	}else{
		MoveRight();
	}
	SETOCR1A(speed());
//	UartPuts("; ");
//	itoa(outMotorData[motState],str,10);
//	UartPuts(str);
}



int main(void) {

	char str[12];
	uint8_t started=1;
	//char count=0;
	direction=Left;
	InitUart();
	InitTimer1();

	SetOutputMIO1_6(MIO2Pin|MIO3Pin|MIO4Pin|MIO5Pin); 	//Stepper motor output
	SetInputMIO7();
	SetInputMIO8();
	SetPullUpMIO7();
	SetPullUpMIO8();
	LED1Init();									//LED output
	ClearMIO1_6(MIO2Pin|MIO3Pin|MIO4Pin|MIO5Pin);		//start with power off motor

	SetInputMIO1(); //for ADC

	//		VCC 5V, res. in ADCH (hi 8bit) from  ADC0
	ADMUX = BIT(REFS0) | BIT(ADLAR) | MIO1;
	//SetADCPin(MIO1);

	//XTAL/16
	ADCSRA = BIT(ADPS2) ;
	ADCSTART();


	ENABLE_TIMER1_DIV256();

	sei();
	//InitPorts();
	states.all=0;
	itoa(ADCDATA,str,10);


	do{
		wait_us(8,10000);
		ADCCONVERT();
		while(!ADC_CONVERT_FINISHED());
		ADCVal=ADCDATA;
		//nastav smer otacania
		if(GetMIO7()==0){
			direction=Left;
		}
		if(GetMIO8()==0){
			direction=Right;
		}

		if(ADCVal>=254){
		//pojdu iba piny, na nastavenie smeru.
		//zastavim TIMER
			PAUSE_TIMER1();
			started=0;

			if(GetMIO7()==0){
				MoveLeft();
			}
			if(GetMIO8()==0){
				MoveRight();
			}
			UartPuts(": ");
			itoa(outMotorData[motState],str,10);
			UartPuts(str);
		}else{
			//nakopni timer ak nie je uz spusteny
			if(!started){
				ENABLE_TIMER1_DIV256();
				started=1;
			}
		}

		itoa(ADCDATA,str,10);
		if(debug){
			UartPuts(" ADC: ");
			UartPuts(str);
	//		UartPuts(" In: ");
	//		itoa(GetMIO7(),str,10);
	//		UartPuts(str);
			UartPutn();
		}
		XorLED1();

	}while(1);
}

//#endif

