/*
 * Copyright (c) 2017, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef GENERALPATTERNMATCH_H
#define GENERALPATTERNMATCH_H
#include "inttypes.h"

typedef enum {RANGE_UINT8=0, RANGE_UINT16, RANGE_UINT32, RANGE_UINT64, MAX_RANGE_TYPE} rangeType_t;
typedef enum {DT_uint8_t=0, DT_uint16_t, DT_uint32_t, DT_string} DataType_t;
typedef enum {MatchDTString,MatchDTFormattedString, MatchDTRangeNum} MatchDataType_t;

typedef struct {
	void* pData;
	DataType_t type;
	char* formatStr;
}cmpData_t;

typedef struct {
	cmpData_t cmpData;
	MatchDataType_t matchType;
	char* separator;
} MatchPattern_t;

#define ERR_GENERAL_MATCH_NULL_DATA -1
#define ERR_GENERAL_MATCH_DATA_FORMAT_WRONG -2
#define ERR_GENERAL_MATCH_STR_LEN_SHORT -3
#define ERR_GENERAL_MATCH_DATA_NOT_PARSED -4
#define ERR_GENERAL_MATCH_BAD_MATCH_DATA_TYPE -5

#define MAX_SCANF_FORMAT_BUF 15

//int GetMinMaxNumRangeStr(char* rangeStr,uint8_t strLen, void* min, void* max, rangeType_t rangeType);
uint8_t CompareDataType(void* data, void *min, void *max, DataType_t type);
int8_t GeneralAuthenticityMatch(char* srcStr,uint16_t len, MatchPattern_t patterns[], uint8_t itemsNum);

#endif // GENERALPATTERNMATCH_H
