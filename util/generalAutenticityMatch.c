/*
 * Copyright (c) 2017, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include "generalAutenticityMatch.h"
#include <stdio.h>
#include <string.h>

char* rangeTypeCodes[]={"%hhu","%hu","%u","%llu"};

int GetNumRangeElementSize(char* data, char len, char separator){
	if(data == NULL) {
		return 0;
	}
	if(separator == 0) {
		return len;
	}
	char* separatorPtr = data;
	separatorPtr = strchr(separatorPtr,separator);
	if(separator =='-' && data[0] == '[' && len > 1) {
		separatorPtr = strchr(separatorPtr+1,separator);
	}
	if(separatorPtr != NULL) {

		return separatorPtr - data;
	}
	return len;
}


int GetMinMaxNumRangeStr(char* rangeStr,uint8_t strLen, void* min, void* max, rangeType_t rangeType) {
	if(rangeStr == NULL) {
		return 1;
	}

	int len = strLen;
	if(len == 0){
		return 2;
	}

	if(rangeType >= MAX_RANGE_TYPE) {
		return 3;
	}
	char* pStr = rangeStr;
	char* pMinStr = NULL;
	char* pMaxStr = NULL;
	int tmpMin = 0;
	int tmpMax = 0;

	if(rangeStr[0] >= '0' &&  rangeStr[0] <= '9') {
		// should be normal number. Min and Max are same
		pMinStr=rangeStr;
		pMaxStr=rangeStr;
		goto GetMinMaxNumRangeStr_lb;
	}
	if(rangeStr[0] !='[') {
		return 4;
	}
	len--;
	pStr++;
	//find dash
	char* dashPtr = strchr(pStr,'-');
	if(dashPtr == NULL) {
		return 5;
	}
	//*dashPtr = 0;
	pMinStr= pStr;
	if(dashPtr==pStr) {
		return 6;
	}
	len-=dashPtr - pStr + 1;
	pStr=dashPtr+1;
	if(len<=1){
		return 7;
	}
	if(pStr[len-1] !=']') {
		return 8;
	}
	//pStr[len-1] = 0;
	pMaxStr = pStr;

GetMinMaxNumRangeStr_lb:
	if(sscanf(pMinStr,rangeTypeCodes[rangeType],&tmpMin) != 1) {
		return 9;
	}
	if(sscanf(pMaxStr,rangeTypeCodes[rangeType],&tmpMax) != 1) {
		return 10;
	}
	switch(rangeType){
	case RANGE_UINT8:
		(*(uint8_t*)min) = tmpMin;
		(*(uint8_t*)max) = tmpMax;
		break;
	case RANGE_UINT16:
		(*(uint16_t*)min) = tmpMin;
		(*(uint16_t*)max) = tmpMax;
		break;
	case RANGE_UINT32:
		(*(uint32_t*)min) = tmpMin;
		(*(uint32_t*)max) = tmpMax;
		break;
	case RANGE_UINT64:
		(*(uint64_t*)min) = tmpMin;
		(*(uint64_t*)max) = tmpMax;
		break;
	default:
		return 11;

	}
	return 0;
}

uint8_t CompareDataType(void* data, void *min, void *max, DataType_t type) {
	switch (type) {
	case DT_uint8_t:
		if((*(uint8_t*)data) >= (*(uint8_t*)min) && (*(uint8_t*)data) <= (*(uint8_t*)max)) {
			return 1;
		}
		break;
	case DT_uint16_t:
		if((*(uint16_t*)data) >= (*(uint16_t*)min) && (*(uint16_t*)data) <= (*(uint16_t*)max)) {
			return 1;
		}
		break;
	case DT_uint32_t:
		if((*(uint32_t*)data) >= (*(uint32_t*)min) && (*(uint32_t*)data) <= (*(uint32_t*)max)) {
			return 1;
		}
		break;
	default:
		break;
	}
	return 0;
}

int8_t GeneralAuthenticityMatch(char* srcStr,uint16_t len, MatchPattern_t patterns[], uint8_t itemsNum){
	if(patterns == NULL) {
		return ERR_GENERAL_MATCH_NULL_DATA;
	}

	int authenticityLevel = 0;
	char scanfFormatBuf[MAX_SCANF_FORMAT_BUF];
	uint64_t tmpLen = 0;
	uint64_t tmpData;
	uint64_t tmpMin;
	uint64_t tmpMax;
	for(uint8_t i = 0; i< itemsNum;i++){
		switch (patterns[i].matchType) {
		case MatchDTString:
			if(patterns[i].cmpData.type != DT_string) {
				return ERR_GENERAL_MATCH_DATA_FORMAT_WRONG;
			}
			tmpLen = strlen(patterns[i].cmpData.pData);
			if(strlen(srcStr) < tmpLen) {
				return ERR_GENERAL_MATCH_STR_LEN_SHORT;
			}
			if(strncmp(patterns[i].cmpData.pData,srcStr,tmpLen)) {
				return 0;
			}
			break;
		case MatchDTFormattedString:
			if(patterns[i].cmpData.type > DT_uint32_t) {
				return ERR_GENERAL_MATCH_DATA_FORMAT_WRONG;
			}
			scanfFormatBuf[0]=0;
			tmpData = 0;
			strncpy(scanfFormatBuf,patterns[i].cmpData.formatStr,sizeof(scanfFormatBuf));
			strcat(scanfFormatBuf,"%n");
			if(sscanf(srcStr,scanfFormatBuf,&tmpData, &tmpLen) != 1) {
				return ERR_GENERAL_MATCH_DATA_NOT_PARSED;
			}
			if(CompareDataType(patterns[i].cmpData.pData,&tmpData,&tmpData,patterns[i].cmpData.type) == 0) {
				return 0;
			}
			break;
		case MatchDTRangeNum:
			if(patterns[i].cmpData.type > DT_uint32_t) {
				return ERR_GENERAL_MATCH_DATA_FORMAT_WRONG;
			}

			tmpMin = 0;
			tmpMax = 0;
			tmpLen = GetNumRangeElementSize(srcStr,len,patterns[i].separator[0]);
			if( GetMinMaxNumRangeStr(srcStr,tmpLen, &tmpMin,&tmpMax, patterns[i].cmpData.type) != 0) {
				return 0;
			}
			if(CompareDataType(patterns[i].cmpData.pData,&tmpMin,&tmpMax,patterns[i].cmpData.type) == 0) {
				return 0;
			}
			if(tmpMax == tmpMin) {
				authenticityLevel++;
			}
			break;
		default:
			return ERR_GENERAL_MATCH_BAD_MATCH_DATA_TYPE;
			break;
		}
		authenticityLevel += 2;
		len -= tmpLen;
		srcStr +=tmpLen;

		if(len == 0) {
			break;
		}

		tmpLen = strlen(patterns[i].separator);

		if(strncmp(srcStr,patterns[i].separator,tmpLen) != 0) {
			return 0;
		}

		len -= tmpLen;
		srcStr +=tmpLen;
		if(len<=0) {
			return 0;
		}
	}

	return authenticityLevel;
}


void Test() {
	char permitCode[] = "TS";
	MatchPattern_t data[] = {{{"BAR",DT_string,""},MatchDTString,"-"},
							 {{permitCode,DT_uint8_t,""},MatchDTString,"-"}};

	char testStr[] = "BAR-TS-[500-510]-255.255.ini";
	GeneralAuthenticityMatch(testStr,strlen(testStr),data,sizeof(data)/sizeof(MatchPattern_t));
}

