/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#ifndef BACKUPDATA_H
#define BACKUPDATA_H

#include "../sys/flatStorage.h"
#include "sysConfig.h"

#ifdef __cplusplus
extern "C" {
#endif


#define MAX_BACKUP_DATA_NAME_LEN 30
#define BD_ERR_BUSY -1
#define BD_ERR_NOT_SUCCESS -2

typedef enum {BD_NO_OPERATION, BD_SAVING, BD_READING, BD_CLOSING} BD_OP_t;

typedef struct {
	char name[MAX_BACKUP_DATA_NAME_LEN];
	uint8_t copies;
	FS_t *fs;
	FSF_t f[MAX_BACKUP_DATA_COPIES + 1];
	volatile BD_OP_t op;

} BackupData_t;

uint8_t BackupDataInit(BackupData_t * bd, FS_t *fs, char* name, uint8_t copies);

int8_t GetBackupedData(BackupData_t *bd, void *data, uint16_t size);
int8_t SaveBackupData(BackupData_t *bd, void *data, uint16_t size);
int8_t CloseBackupData(BackupData_t *bd);

#ifdef __cplusplus
}
#endif

#endif // BACKUPDATA_H
