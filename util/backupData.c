/*
 * Copyright (c) 2014, Michal Bachraty (bachraty@mibasystems.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted for non-commercial use, provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 * For commercial use in source and birary forms, with or without
 * modification, you must contact Author with your requirements.  Upon your
 * requirements, special licence agreement will be provided.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of Mikrix system.
 *
 */

#include <stdio.h>
#include <string.h>
#include "backupData.h"
#include "sysConfig.h"


// backup files are identified with bk<num>


#ifdef BACUP_DATA_DEBUG_EN
#define DEBUG_MSG(...) fprintf(stderr,__VA_ARGS__);fflush(stderr)
#else
#define DEBUG_MSG(...)
#endif

uint8_t BackupDataInit(BackupData_t * bd, FS_t *fs, char* name, uint8_t copies) {
	uint8_t i,r;
	char tmpFileName[MAX_BACKUP_DATA_NAME_LEN + 4];
	if(fs == NULL) return 1;
	if(name == NULL) return 2;
	if(copies > MAX_BACKUP_DATA_COPIES) {
		return 3;
	}
	bd->fs = fs;
	strncpy(bd->name,name,sizeof(bd->name));
	bd->copies = copies;
	bd->op = BD_NO_OPERATION;

	strncpy(tmpFileName, bd->name, sizeof(tmpFileName));

	for(i = 0; i <= copies;i++) {
		InitFSFile(&bd->f[i]);
		//generate file name
		if(i) {
			sprintf(&tmpFileName[strlen(tmpFileName)],".bk%hhu",i);
		}
		//try open file and create bk file
		if((r = OpenFS(bd->fs,&bd->f[i],tmpFileName))) {
			fprintf(stderr,"Could not open file %s. ret [%hhu]\n",tmpFileName,r);
			InitFSFile(&bd->f[i]);
			continue;
		}
		fprintf(stderr,"Opened backup data file %s: %p\n",tmpFileName,bd->f[i].userData);
	}
	if(r)
		return 1;
	return 0;
}

int8_t GetBackupedData(BackupData_t *bd, void *data, uint16_t size) {
	// get backup name
	uint8_t r;
	uint8_t i;

	//wait for data are stored / readen
	if(bd->op != BD_NO_OPERATION) {
		return BD_ERR_BUSY;
	}

	bd->op = BD_READING;

	for(i = 0; i <= bd->copies; i++) {
		if((r = ReadFSF(&bd->f[i],data,size))) {
			fprintf(stderr,"Could not read file %s, copy %hhu [%hhu]\n",bd->name, i, r);
			//try to reopen file??
			//CloseFSF(&bd->f[i]);
			continue;
		}
		break;
	}
	bd->op = BD_NO_OPERATION;
	if(r) {
		return BD_ERR_NOT_SUCCESS;
	}
	return i;
}


int8_t SaveBackupData(BackupData_t *bd, void *data, uint16_t size) {
	// get backup name
	uint8_t r;
	uint8_t i;
	uint8_t errCnt = 0;

	if(bd->op != BD_NO_OPERATION) {
		return BD_ERR_BUSY;
	}
	bd->op = BD_SAVING;

	DEBUG_MSG("Writing backup to file %s \n",bd->name);

	for(i = 0; i <= bd->copies; i++) {
		if((r = WriteFSF(&bd->f[i],data,size))) {
			fprintf(stderr,"Could not write file %s copy %d. ret [%hhu]\n",bd->name, i, r);
			errCnt++;
		}
	}
	bd->op = BD_NO_OPERATION;
	if(errCnt >= bd->copies) {
		return BD_ERR_NOT_SUCCESS;
	}
	return errCnt;
}


int8_t CloseBackupData(BackupData_t *bd) {
	uint8_t i = 0;
	if(bd->op != BD_NO_OPERATION) {
		return BD_ERR_BUSY;
	}

	bd->op = BD_CLOSING;

	for(i = 0; i <= bd->copies; i++) {
		if(IsFSFileOpen(&bd->f[i]))
			CloseFSF(&bd->f[i]);
	}

	bd->op = BD_NO_OPERATION;

	return 0;
}




